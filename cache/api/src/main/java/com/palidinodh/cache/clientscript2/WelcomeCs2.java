package com.palidinodh.cache.clientscript2;

import com.palidinodh.cache.id.ScriptId;
import java.util.List;
import lombok.Getter;

@Getter
public class WelcomeCs2 extends ClientScript2 {

  private String allText;

  public WelcomeCs2() {
    super(ScriptId.WELCOME_8205);
  }

  public static WelcomeCs2 builder() {
    return new WelcomeCs2();
  }

  public WelcomeCs2 text(List<String> lines) {
    StringBuilder linesAsString = new StringBuilder();
    for (int i = 0; i < lines.size(); i++) {
      linesAsString.append(lines.get(i)).append(i + 1 < lines.size() ? "|" : "");
    }
    allText = linesAsString.toString();
    return this;
  }

  @Override
  public byte[] build() {
    if (allText.endsWith("|")) {
      allText = allText.substring(0, allText.length() - 1);
    }
    addString(allText);
    return super.build();
  }
}
