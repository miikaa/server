package com.palidinodh.cache.clientscript2;

import com.palidinodh.cache.id.ScriptId;
import lombok.Getter;

@Getter
public class ScreenSelectionCs2 extends ClientScript2 {

  private String title = "Choose an Option";
  private String allText;
  private ControlsType controls = ControlsType.NONE;
  private int font = 495;
  private int textAlignment = 1;
  private String menuOption = "Select";
  private int entryPadding = 8;
  private int entryLines = 1;

  public ScreenSelectionCs2() {
    super(ScriptId.SCREEN_SELECTION_8197);
  }

  public static ScreenSelectionCs2 builder() {
    return new ScreenSelectionCs2();
  }

  public ScreenSelectionCs2 title(String s) {
    title = s;
    return this;
  }

  public ScreenSelectionCs2 allText(String s) {
    allText = s;
    return this;
  }

  public ScreenSelectionCs2 controls(ControlsType c) {
    controls = c;
    return this;
  }

  public ScreenSelectionCs2 textAlignment(int i) {
    textAlignment = i;
    return this;
  }

  public ScreenSelectionCs2 menuOption(String s) {
    menuOption = s;
    return this;
  }

  public ScreenSelectionCs2 entryPadding(int i) {
    entryPadding = i;
    return this;
  }

  public ScreenSelectionCs2 entryLines(int i) {
    entryLines = i;
    return this;
  }

  @Override
  public byte[] build() {
    if (allText.endsWith("|")) {
      allText = allText.substring(0, allText.length() - 1);
    }
    addString(title);
    addString(allText);
    addInt(controls.showRefresh() ? 1 : 0);
    addInt(controls.showAdd() ? 1 : 0);
    addInt(controls.showBack() ? 1 : 0);
    addInt(font);
    addInt(textAlignment);
    addString(menuOption == null ? "" : menuOption);
    addInt(entryPadding);
    addInt(entryLines);
    return super.build();
  }

  public enum ControlsType {
    NONE,
    REFRESH,
    REFRESH_ADD,
    REFRESH_BACK;

    public boolean showRefresh() {
      return name().contains("REFRESH");
    }

    public boolean showAdd() {
      return name().contains("ADD");
    }

    public boolean showBack() {
      return name().contains("BACK");
    }
  }
}
