package com.palidinodh.cache.clientscript2;

import com.palidinodh.cache.id.ScriptId;

public class ScrollbarSizeCs2 extends ClientScript2 {

  private int root;
  private int container;
  private int scrollbar;
  private int width;
  private int height;
  private boolean keepScrollPosition;

  public ScrollbarSizeCs2() {
    super(ScriptId.SCROLL_MAX_8193);
  }

  public static ScrollbarSizeCs2 builder() {
    return new ScrollbarSizeCs2();
  }

  public ScrollbarSizeCs2 root(int i) {
    root = i;
    return this;
  }

  public ScrollbarSizeCs2 container(int i) {
    container = i;
    return this;
  }

  public ScrollbarSizeCs2 scrollbar(int i) {
    scrollbar = i;
    return this;
  }

  public ScrollbarSizeCs2 width(int i) {
    width = i;
    return this;
  }

  public ScrollbarSizeCs2 height(int i) {
    height = i;
    return this;
  }

  public ScrollbarSizeCs2 keepScrollPosition(boolean b) {
    keepScrollPosition = b;
    return this;
  }

  @Override
  public byte[] build() {
    addInt(root << 16 | container);
    if (scrollbar == -1) {
      addInt(-1);
    } else {
      addInt(root << 16 | scrollbar);
    }
    addInt(width);
    addInt(height);
    addInt(keepScrollPosition ? 1 : 0);
    return super.build();
  }
}
