package com.palidinodh.cache.store.util;

import java.util.HashMap;
import java.util.Map;

public final class Djb2 {

  private static final Map<Integer, String> HASH_NAMES = new HashMap<>();

  private Djb2() {}

  public static int hash(String str) {
    var hash = 0;
    for (var i = 0; i < str.length(); i++) {
      hash = str.charAt(i) + ((hash << 5) - hash);
    }
    return hash;
  }

  public static long fullHash(String str) {
    var hash = 5381L;
    for (var i = 0; i < str.length(); i++) {
      hash = str.charAt(i) + ((hash << 5) + hash);
    }
    return hash;
  }

  public static String getUnhashedName(int hash) {
    return HASH_NAMES.get(hash);
  }

  public static void addHashNames(Map<Integer, String> names) {
    HASH_NAMES.putAll(names);
  }
}
