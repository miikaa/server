package com.palidinodh.rs.adaptive;

import com.palidinodh.util.PString;

public enum RsGameMode {
  UNSET,
  REGULAR,
  IRONMAN,
  ULTIMATE_IRONMAN,
  HARDCORE_IRONMAN,
  DEADMAN;

  public String getFormattedName() {
    return PString.formatName(name().toLowerCase().replace("_", " "));
  }

  public boolean isSet() {
    return this != UNSET;
  }

  public boolean isUnset() {
    return this == UNSET;
  }

  public boolean isRegular() {
    return this == REGULAR;
  }

  public boolean isIronman() {
    return this == IRONMAN;
  }

  public boolean isUltimateIronman() {
    return this == ULTIMATE_IRONMAN;
  }

  public boolean isHardcoreIronman() {
    return this == HARDCORE_IRONMAN;
  }

  public boolean isDeadman() {
    return this == DEADMAN;
  }

  public boolean isIronType() {
    if (this == ULTIMATE_IRONMAN) {
      return true;
    }
    if (this == HARDCORE_IRONMAN) {
      return true;
    }
    return this == IRONMAN;
  }
}
