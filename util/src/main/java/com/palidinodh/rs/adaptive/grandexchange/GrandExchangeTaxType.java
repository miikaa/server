package com.palidinodh.rs.adaptive.grandexchange;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum GrandExchangeTaxType {
  LOW(10_000_000, 0.0025),
  MEDIUM(100_000_000, 0.005),
  HIGH(-1, 0.01);

  private final int value;
  private final double tax;

  public static double getTax(int value) {
    for (var bracket : values()) {
      if (value < bracket.value || bracket.value == -1) {
        return bracket.tax;
      }
    }
    return 0;
  }
}
