package com.palidinodh.rs.communication.event;

import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import com.palidinodh.rs.setting.Settings;
import lombok.Getter;

@Getter
public class WorldPlayercountEvent extends CommunicationEvent {

  private int worldId;
  private int playerCount;

  public WorldPlayercountEvent(int worldId, int playerCount) {
    this.worldId = worldId;
    this.playerCount = playerCount;
  }

  @Override
  public WorldPlayercountEvent getRepliedEvent() {
    return (WorldPlayercountEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    if (Settings.getInstance().isLocal()) {
      return;
    }
    if (Settings.getInstance().isBeta()) {
      return;
    }
    if (Settings.getInstance().isStaffOnly()) {
      return;
    }
    server.addSqlEvent(
        new SqlEvent(
            "UPDATE phrase SET text = '"
                + playerCount
                + "' WHERE varname = 's2_world_"
                + worldId
                + "_playercount'"));
  }
}
