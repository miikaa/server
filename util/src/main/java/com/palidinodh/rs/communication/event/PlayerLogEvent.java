package com.palidinodh.rs.communication.event;

import com.palidinodh.io.Writers;
import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import com.palidinodh.rs.setting.Settings;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
public class PlayerLogEvent extends CommunicationEvent {

  private LogType type;
  private int selectedUserId;
  private String selectedFilename;
  private String line;

  public PlayerLogEvent(LogType type, String line) {
    this.type = type;
    selectedUserId = -1;
    this.line = line;
    if (this.line.length() > 2_048) {
      this.line = this.line.substring(0, 2_048);
    }
    if (Settings.getInstance().isBeta() || Settings.getInstance().isLocal()) {
      this.line = null;
    }
  }

  public PlayerLogEvent(LogType type, int selectedUserId, String line) {
    this.type = type;
    this.selectedUserId = selectedUserId;
    this.line = line;
    if (Settings.getInstance().isBeta() || Settings.getInstance().isLocal()) {
      this.line = null;
    }
  }

  public PlayerLogEvent(LogType type, String selectedFilename, String line) {
    this.type = type;
    selectedUserId = -1;
    this.selectedFilename = selectedFilename;
    this.line = line;
    if (Settings.getInstance().isBeta() || Settings.getInstance().isLocal()) {
      this.line = null;
    }
  }

  @Override
  public PlayerLogEvent getRepliedEvent() {
    return (PlayerLogEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    if (type == null) {
      return;
    }
    if (line == null) {
      return;
    }
    var userId = getUserId();
    if (selectedUserId != -1) {
      userId = selectedUserId;
    }
    if (userId != -1) {
      Writers.writeLog(
          Settings.getInstance().getPlayerLogsDirectory(), type.getFilename(userId), line);
      if (type == LogType.CLAN_CHAT && getUserId() != userId) {
        Writers.writeLog(
            Settings.getInstance().getPlayerLogsDirectory(),
            LogType.CLAN_CHAT.getFilename(getUserId()),
            line);
      }
    }
    if (selectedFilename != null && !selectedFilename.isEmpty()) {
      Writers.writeLog(
          Settings.getInstance().getPlayerLogsDirectory(),
          type.getFilename(selectedFilename),
          line);
    }
    switch (type) {
      case PUBLIC_CHAT:
      case AUTO_CHAT:
      case PRIVATE_CHAT:
      case CLAN_CHAT:
      case YELL:
        Writers.writeLog(
            Settings.getInstance().getPlayerLogsDirectory(),
            LogType.ALL_CHAT.getFilename(userId),
            line);
        break;
    }
  }

  @AllArgsConstructor
  @Getter
  public enum LogType {
    DEATH("death"),
    PVP_RISKED_ITEM_DEATH("pk"),
    PVP_TOURNAMENT("pvptournament"),
    BOSS_LOOT("bossloot"),
    BANK("bank"),
    SHOP("shop"),
    BOND("bond"),
    TRADE("trade"),
    DUEL("duel"),
    LOOT_BOX("lootbox"),
    MAP_ITEM("mapitem"),
    EXCHANGE("exchange"),
    WELL_OF_GOODWILL("wellofgoodwill"),
    TELEPORT("teleport"),
    COMMAND("commands"),
    STAFF("staff"),
    PUBLIC_CHAT("publicchat"),
    AUTO_CHAT("autochat"),
    PRIVATE_CHAT("privatechat"),
    CLAN_CHAT("clanchat"),
    YELL("yell"),
    ALL_CHAT("chat"),
    CONNECT("ip"),
    NEWS("news");

    private String directoryName;

    public String getFilename(int userId) {
      return getFilename(Integer.toString(userId));
    }

    public String getFilename(String name) {
      return directoryName + "/" + name + ".txt";
    }
  }
}
