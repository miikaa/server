package com.palidinodh.rs.communication.event;

import com.palidinodh.rs.adaptive.grandexchange.GrandExchangeItem;
import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import lombok.Getter;

@Getter
public class GrandExchangeCollectAbortEvent extends CommunicationEvent {

  private int slot;
  private GrandExchangeItem item;

  public GrandExchangeCollectAbortEvent(int slot, GrandExchangeItem item) {
    this.slot = slot;
    this.item = item;
  }

  @Override
  public GrandExchangeCollectAbortEvent getRepliedEvent() {
    return (GrandExchangeCollectAbortEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    var user = server.getGrandExchangeUser(getUserId());
    if (user == null) {
      return;
    }
    user.insertItem(slot, item);
    server.addGrandExchangeUpdate(user);
    MainCommunicationServer.saveGrandExchangeUser(user);
  }
}
