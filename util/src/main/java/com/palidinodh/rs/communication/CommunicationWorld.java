package com.palidinodh.rs.communication;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import lombok.Getter;
import lombok.Setter;

public class CommunicationWorld {
  @Getter @Setter private MainCommunicationSession session;
  private Queue<Integer> keys = new ConcurrentLinkedQueue<>();

  public boolean hasKey(int key) {
    return keys.contains(key);
  }

  public void addKey(int key) {
    while (keys.size() >= 32_768) {
      keys.remove();
    }
    keys.add(key);
  }

  public int keyCount() {
    return keys.size();
  }
}
