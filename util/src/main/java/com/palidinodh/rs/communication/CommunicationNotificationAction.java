package com.palidinodh.rs.communication.event;

import com.palidinodh.rs.communication.CommunicationNotification;

public abstract class CommunicationNotificationAction<T extends CommunicationNotification> {

  public abstract Class<T> getNotificationClass();

  public abstract void accept(T notification);
}
