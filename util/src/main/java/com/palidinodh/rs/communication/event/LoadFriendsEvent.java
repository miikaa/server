package com.palidinodh.rs.communication.event;

import com.palidinodh.rs.adaptive.RsFriend;
import com.palidinodh.rs.adaptive.RsPlayer;
import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import java.util.List;
import lombok.Getter;

@Getter
public class LoadFriendsEvent extends CommunicationEvent {

  private int privateChatState;
  private List<RsFriend> friends;
  private List<String> ignores;
  private int icon1;
  private int icon2;

  @Override
  public void loadRsPlayer(RsPlayer player) {
    privateChatState = player.getPrivateChatStatus();
    friends = player.getFriends();
    ignores = player.getIgnores();
    icon1 = player.getIcon();
    icon2 = player.getIcon2();
  }

  @Override
  public LoadFriendsEvent getRepliedEvent() {
    return (LoadFriendsEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    var player = server.getPlayerById(getUserId());
    if (player == null) {
      return;
    }
    player.setIcon(icon1);
    player.setIcon2(icon2);
    player.loadFriends(friends, ignores, privateChatState);
    server.getClan(player);
    for (var rp : server.getPlayersById().values()) {
      var indexOf = friends.indexOf(new RsFriend(rp.getUsername()));
      if (indexOf == -1) {
        continue;
      }
      if (player.getRights() == RsPlayer.RIGHTS_NONE && !RsFriend.canRegister(player, rp)) {
        continue;
      }
      friends.get(indexOf).setWorldId(rp.getWorldId());
    }
  }
}
