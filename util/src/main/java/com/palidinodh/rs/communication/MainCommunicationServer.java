package com.palidinodh.rs.communication;

import com.palidinodh.io.DiscordBot;
import com.palidinodh.io.FileManager;
import com.palidinodh.io.JsonIO;
import com.palidinodh.io.Writers;
import com.palidinodh.rs.adaptive.Clan;
import com.palidinodh.rs.adaptive.RsGameMode;
import com.palidinodh.rs.adaptive.RsPlayer;
import com.palidinodh.rs.adaptive.grandexchange.GrandExchangeItem;
import com.palidinodh.rs.adaptive.grandexchange.GrandExchangeUser;
import com.palidinodh.rs.communication.event.AuthEvent;
import com.palidinodh.rs.communication.event.LoginEvent;
import com.palidinodh.rs.communication.event.LogoutEvent;
import com.palidinodh.rs.communication.event.SqlEvent;
import com.palidinodh.rs.communication.notification.ClanChatUpdateNotification;
import com.palidinodh.rs.communication.notification.GrandExchangeUpdateNotification;
import com.palidinodh.rs.setting.SecureSettings;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PArrayList;
import com.palidinodh.util.PLogger;
import com.palidinodh.util.PNumber;
import com.palidinodh.util.PTime;
import com.palidinodh.util.PUtil;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.Queue;
import lombok.Getter;

public final class MainCommunicationServer implements Runnable {

  @Getter private static MainCommunicationServer instance;

  private int head;
  private int tail;
  private boolean running;
  private EventLoopGroup bossGroup;
  private EventLoopGroup workerGroup;
  private Map<Integer, CommunicationWorld> worlds = new HashMap<>();
  private Map<String, RsPlayer> playersByUsername = new HashMap<>();
  @Getter private Map<Integer, RsPlayer> playersById = new HashMap<>();
  private Map<String, Clan> clans = new HashMap<>();
  private List<Clan> clanUpdates = new ArrayList<>();
  @Getter private Map<Integer, GrandExchangeUser> grandExchange = new HashMap<>();
  private Queue<CommunicationEvent> events =
      new PriorityQueue<>(
          16,
          (a, b) -> {
            if (a instanceof AuthEvent) {
              return -1;
            }
            if (b instanceof AuthEvent) {
              return 1;
            }
            if (!isLoginEvent(a) || !isLoginEvent(b)) {
              return 0;
            }
            if (a.getUsername() == null || b.getUsername() == null) {
              return 0;
            }
            if (!a.getUsername().equalsIgnoreCase(b.getUsername())) {
              return 0;
            }
            if (a instanceof LogoutEvent) {
              return -1;
            }
            if (b instanceof LogoutEvent) {
              return 1;
            }
            if (a instanceof LoginEvent) {
              return -1;
            }
            if (b instanceof LoginEvent) {
              return 1;
            }
            return 0;
          });
  private List<CommunicationEvent> loginEvents = new ArrayList<>();
  private List<SqlEvent> sqlEvents = new ArrayList<>();
  private List<CommunicationNotification> notifications = new ArrayList<>();
  private long lastPrintStatsTime = PTime.currentTimeMillis();

  private MainCommunicationServer(String ip, int port) {
    try {
      loadGrandExchange();

      bossGroup = new NioEventLoopGroup();
      workerGroup = new NioEventLoopGroup();
      var bootstrap = new ServerBootstrap();
      var self = this;
      bootstrap
          .group(bossGroup, workerGroup)
          .channel(NioServerSocketChannel.class)
          .childHandler(
              new ChannelInitializer<SocketChannel>() {
                @Override
                public void initChannel(SocketChannel c) {
                  c.pipeline().addLast(new MainCommunicationSession(self));
                }
              })
          .option(ChannelOption.SO_BACKLOG, 128)
          .childOption(ChannelOption.SO_KEEPALIVE, true)
          .childOption(ChannelOption.TCP_NODELAY, true);
      try {
        bootstrap.bind(ip, port).sync();
      } catch (InterruptedException ignored) {
      }
      running = true;
    } catch (Exception e) {
      PLogger.error(e);
    }
  }

  public static void init(SecureSettings settings) {
    instance = new MainCommunicationServer("0.0.0.0", settings.getCommunicationPort());
    new Thread(instance).start();
  }

  public static void main(String[] args) {
    try {
      var properties = new HashMap<String, String>();
      for (var arg : args) {
        var argSet = arg.split("=", 2);
        properties.put(argSet[0], argSet[1]);
      }
      Settings.setInstance(JsonIO.read(new File(properties.get("settings")), Settings.class));
      Settings.setSecure(
          JsonIO.read(
              new File(properties.get("settings").replace(".json", "_secure.json")),
              SecureSettings.class));
      FileManager.loadSql();
      DiscordBot.init(Settings.getSecure().getDiscordToken(), -1);
      init(Settings.getSecure());
    } catch (Exception e) {
      PLogger.error(e);
    }
  }

  public static boolean isLoginEvent(CommunicationEvent event) {
    if (event instanceof LogoutEvent) {
      return true;
    }
    return event instanceof LoginEvent;
  }

  public static Clan loadClan(String username) {
    var file =
        new File(
            Settings.getInstance().getPlayerClanDirectory(),
            username.replace(" ", "_").toLowerCase() + ".json");
    if (!file.exists()) {
      return null;
    }
    var clan = JsonIO.read(file, Clan.class);
    if (clan != null) {
      clan.load();
    }
    return clan;
  }

  public static void saveGrandExchangeUser(GrandExchangeUser user) {
    var hasItems = !user.getItems().isEmpty();
    if (!hasItems) {
      new File(Settings.getInstance().getPlayerExchangeDirectory(), user.getUserId() + ".json")
          .delete();
      return;
    }
    JsonIO.write(
        new File(Settings.getInstance().getPlayerExchangeDirectory(), user.getUserId() + ".json"),
        user);
  }

  public static void saveClan(Clan clan) {
    if (Clan.getGlobal(clan.getOwner()) != null) {
      return;
    }
    JsonIO.write(
        new File(
            Settings.getInstance().getPlayerClanDirectory(),
            clan.getOwner().replace(" ", "_").toLowerCase() + ".json"),
        clan);
  }

  @Override
  public void run() {
    var tempWorlds = new PArrayList<CommunicationWorld>();
    var tempEvents = new PArrayList<CommunicationEvent>();
    var tempNotifications = new PArrayList<CommunicationNotification>();
    while (running) {
      if (tempWorlds.isEmpty()) {
        PLogger.println("No active worlds");
      }
      PUtil.gc();
      PTime.update();
      tempEvents.forEach(
          e -> {
            if (!(e instanceof AuthEvent) && !e.getMainSession().isAuthenticated()) {
              PLogger.println("World " + e.getMainSession().getWorldId() + " not authenticated");
              e.getMainSession().close();
              return;
            }
            e.mainComplete(this);
          });
      tempNotifications.forEach(
          n ->
              tempWorlds.forEach(
                  w -> {
                    if (w.getSession() == null) {
                      return;
                    }
                    n.write(w.getSession());
                  }));
      clanUpdates.removeIf(
          c -> {
            if (!c.canUpdate()) {
              return false;
            }
            addNotification(new ClanChatUpdateNotification(c));
            return true;
          });
      clans.values().removeIf(c -> c.getActiveUsers() == null || c.getActiveUsers().isEmpty());
      sqlEvents.removeIf(SqlEvent::isComplete);
      loginEvents.clear();
      tempWorlds.clear();
      tempEvents.clear();
      tempNotifications.clear();
      synchronized (this) {
        try {
          wait(hasPendingActions() ? 20 : 10_000);
        } catch (InterruptedException ie) {
          break;
        }
        tempEvents.addAll(events);
        events.clear();
        head = tail;
        tempEvents.forEach(
            e -> {
              if (!isLoginEvent(e)) {
                return;
              }
              loginEvents.add(e);
            });
        tempNotifications.addAll(notifications);
        notifications.clear();
        tempWorlds.addAll(worlds.values());
      }
      if (PTime.currentTimeMillis() - lastPrintStatsTime > PTime.hourToMilli(24)) {
        printStats();
      }
    }
    workerGroup.shutdownGracefully();
    bossGroup.shutdownGracefully();
    PLogger.println("MainCommunicationServer shutdown");
  }

  public CommunicationWorld getWorld(int worldId) {
    return worlds.get(worldId);
  }

  public void addWorld(MainCommunicationSession session, List<RsPlayer> players) {
    var world = worlds.get(session.getWorldId());
    if (world == null) {
      worlds.put(session.getWorldId(), world = new CommunicationWorld());
      players.forEach(
          p -> {
            addPlayer(p);
            if (p.getClanChatUsername() != null) {
              joinClan(p, p.getClanChatUsername());
            }
          });
    }
    world.setSession(session);
    session.setWorld(world);
  }

  public void removeWorld(MainCommunicationSession session) {
    worlds.remove(session.getWorldId());
  }

  public void addNotification(CommunicationNotification notification) {
    synchronized (this) {
      notifications.add(notification);
      tail++;
      notifyAll();
    }
  }

  public void addEvents(List<CommunicationEvent> readEvents) {
    synchronized (this) {
      events.addAll(readEvents);
      tail++;
      notifyAll();
    }
  }

  public SqlEvent addSqlEvent(SqlEvent event) {
    sqlEvents.add(event);
    tail++;
    return event;
  }

  public RsPlayer getPlayerById(int userId) {
    return playersById.get(userId);
  }

  public RsPlayer getPlayerByUsername(String username) {
    return playersByUsername.get(username);
  }

  public void addPlayer(RsPlayer player) {
    playersById.put(player.getId(), player);
    playersByUsername.put(player.getUsername().toLowerCase(), player);
  }

  public void removePlayer(RsPlayer player) {
    if (player == null) {
      return;
    }
    playersById.remove(player.getId());
    playersByUsername.remove(player.getUsername().toLowerCase());
  }

  public boolean hasLoginEvent(int userId, CommunicationEvent fromEvent) {
    for (var event : loginEvents) {
      if (fromEvent == event) {
        continue;
      }
      if (userId != event.getUserId()) {
        continue;
      }
      if (!isLoginEvent(event)) {
        continue;
      }
      return true;
    }
    return false;
  }

  public Clan getClan(RsPlayer player) {
    return getClan(player, null);
  }

  public Clan getClan(RsPlayer player, String name) {
    var configuiringName = name != null && !name.isEmpty();
    var username = player.getUsername().toLowerCase();
    var globalClan = Clan.getGlobal(username);
    if (globalClan != null) {
      return Clan.getGlobal(username);
    }
    var clan = clans.get(username);
    if (clan == null) {
      clan = loadClan(username);
      if (clan != null) {
        if (player.getId() != -1) {
          clan.setOwner(username);
          clan.setOwnerId(player.getId());
        }
        clans.put(username, clan);
      }
    }
    if (clan == null) {
      if (!configuiringName || player.getId() == -1) {
        return null;
      }
      clan = new Clan(name, username, player.getId(), player.getFriends(), player.getIgnores());
      clans.put(username, clan);
    }
    if (configuiringName) {
      clan.setName(name);
    }
    if (player.getId() != -1) {
      clan.updateFriends(player.getFriends(), player.getIgnores());
    }
    saveClan(clan);
    return clan;
  }

  public void addClanUpdate(RsPlayer player) {
    var clan = getClan(player);
    if (clan == null) {
      return;
    }
    if (clanUpdates.contains(clan)) {
      return;
    }
    clanUpdates.add(clan);
  }

  public boolean joinClan(RsPlayer player, String clanChatUsername) {
    if (clanChatUsername == null) {
      return false;
    }
    var clanPlayer = new RsPlayer(-1, clanChatUsername);
    Clan clan = getClan(clanPlayer);
    if (clan == null && player.getUsername().equalsIgnoreCase(clanChatUsername)) {
      clan = getClan(player, player.getUsername());
    }
    if (clan == null || !clan.canJoin(player.getUsername(), player.getRights())) {
      return false;
    }
    clan.join(player.getUsername(), player.getWorldId(), player.getRights());
    addClanUpdate(clanPlayer);
    return true;
  }

  public GrandExchangeUser getGrandExchangeUser(int userId) {
    return grandExchange.get(userId);
  }

  public GrandExchangeUser getOrCreateGrandExchangeUser(int userId) {
    var user = grandExchange.get(userId);
    if (user == null) {
      grandExchange.put(userId, user = new GrandExchangeUser(userId));
    }
    return user;
  }

  public void addGrandExchangeUpdate(GrandExchangeUser user) {
    var player = playersById.get(user.getUserId());
    if (player == null) {
      return;
    }
    addNotification(new GrandExchangeUpdateNotification(player.getWorldId(), user.getUserId()));
  }

  public void checkGrandExchangeUser(GrandExchangeUser checkUser) {
    if (checkUser.getItems() == null) {
      return;
    }
    checkUser.getItems().removeIf(GrandExchangeItem::canRemove);
    var users = new ArrayList<>(grandExchange.values());
    Collections.shuffle(users);
    for (var compareUser : users) {
      if (checkUser.getUserId() == compareUser.getUserId()) {
        continue;
      }
      for (var checkIndex = 0; checkIndex < checkUser.getItems().size(); checkIndex++) {
        var checkItem = checkUser.getItems().get(checkIndex);
        if (checkItem == null) {
          continue;
        }
        if (checkItem.isAborted()) {
          continue;
        }
        if (!checkItem.isStateBuying() && !checkItem.isStateSelling()) {
          continue;
        }
        if (checkItem.getRemainingAmount() <= 0) {
          continue;
        }
        for (var compareIndex = 0; compareIndex < compareUser.getItems().size(); compareIndex++) {
          var compareItem = compareUser.getItems().get(compareIndex);
          if (compareItem == null) {
            continue;
          }
          if (compareItem.isAborted()) {
            continue;
          }
          if (!compareItem.isStateBuying() && !compareItem.isStateSelling()) {
            continue;
          }
          if (compareItem.getRemainingAmount() <= 0) {
            continue;
          }
          if (checkItem.getId() != compareItem.getId()) {
            continue;
          }
          if (checkItem.getState() == compareItem.getState()) {
            continue;
          }
          if (checkItem.isStateBuying() && checkItem.getPrice() < compareItem.getPrice()) {
            continue;
          }
          if (checkItem.isStateSelling() && checkItem.getPrice() > compareItem.getPrice()) {
            continue;
          }
          if (checkItem.getIp() != null
              && checkItem.getIp().equals(compareItem.getIp())
              && checkUser.getUserId() != 1
              && compareUser.getUserId() != 1) {
            continue;
          }
          var amount = Math.min(checkItem.getRemainingAmount(), compareItem.getRemainingAmount());
          var price = Math.min(checkItem.getPrice(), compareItem.getPrice());
          if (amount < 1 || price < 1) {
            continue;
          }
          var log1String = "";
          var log2String = "";
          var user1String =
              "["
                  + checkUser.getUserId()
                  + "; "
                  + checkItem.getIp()
                  + "] "
                  + checkUser.getUsername();
          var user2String =
              "["
                  + compareUser.getUserId()
                  + "; "
                  + compareItem.getIp()
                  + "] "
                  + compareUser.getUsername();
          var itemString =
              "["
                  + checkItem.getId()
                  + "] "
                  + checkItem.getName()
                  + " x"
                  + PNumber.formatNumber(amount)
                  + " for "
                  + PNumber.formatNumber(price);
          if (checkItem.isStateBuying()) {
            log1String =
                user1String
                    + " bought "
                    + itemString
                    + " from "
                    + user2String
                    + " on the Grand Exchange.";
            log2String =
                user2String
                    + " sold "
                    + itemString
                    + " to "
                    + user1String
                    + " on the Grand Exchange.";
          } else if (checkItem.isStateSelling()) {
            log1String =
                user1String
                    + " sold "
                    + itemString
                    + " to "
                    + user2String
                    + " on the Grand Exchange.";
            log2String =
                user2String
                    + " bought "
                    + itemString
                    + " from "
                    + user1String
                    + " on the Grand Exchange.";
          }
          Writers.writePlayerLog("exchange/" + checkUser.getUserId() + ".txt", log1String);
          Writers.writePlayerLog("exchange/" + compareUser.getUserId() + ".txt", log2String);
          checkItem.increaseExchanged(amount, price);
          compareItem.increaseExchanged(amount, price);
          addGrandExchangeUpdate(checkUser);
          addGrandExchangeUpdate(compareUser);
          saveGrandExchangeUser(checkUser);
          saveGrandExchangeUser(compareUser);
        }
      }
    }
  }

  public void shutdown() {
    running = false;
    workerGroup.shutdownGracefully();
    bossGroup.shutdownGracefully();
    synchronized (this) {
      notifyAll();
    }
    instance = null;
  }

  private GrandExchangeUser getGrandExchangeUser(String username) {
    for (var user : grandExchange.values()) {
      if (!username.equalsIgnoreCase(user.getUsername())) {
        continue;
      }
      return user;
    }
    return null;
  }

  private void loadGrandExchange() {
    var exchangeFiles = Settings.getInstance().getPlayerExchangeDirectory().listFiles();
    if (exchangeFiles == null) {
      return;
    }
    for (var file : exchangeFiles) {
      if (file.isHidden()) {
        continue;
      }
      if (!file.getName().endsWith(".json")) {
        continue;
      }
      var user = JsonIO.read(file, GrandExchangeUser.class);
      if (user.getItems() == null) {
        user.setItems(new ArrayList<>());
      }
      user.getItems().removeIf(Objects::isNull);
      while (user.getItems().size() > 16) {
        user.getItems().remove(user.getItems().size() - 1);
      }
      user.expireItems();
      if (user.getGameMode() == 0) {
        user.setGameMode(RsGameMode.REGULAR.ordinal());
      }
      grandExchange.put(user.getUserId(), user);
    }
  }

  private boolean hasPendingActions() {
    if (!events.isEmpty()) {
      return true;
    }
    if (!notifications.isEmpty()) {
      return true;
    }
    if (!sqlEvents.isEmpty()) {
      return true;
    }
    return head != tail;
  }

  private void printStats() {
    Runtime rt = Runtime.getRuntime();
    long usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
    PLogger.println(
        "Used: "
            + usedMB
            + "MB; Free: "
            + rt.freeMemory() / 1024 / 1024
            + "MB; Total: "
            + rt.totalMemory() / 1024 / 1024
            + "MB");
    PLogger.println("worlds: " + worlds.size());
    PLogger.println("playersByUsername: " + playersByUsername.size());
    PLogger.println("playersById: " + playersById.size());
    PLogger.println("clans: " + clans.size());
    PLogger.println("clanUpdates: " + clanUpdates.size());
    PLogger.println("grandExchange: " + grandExchange.size());
    PLogger.println("events: " + events.size());
    PLogger.println("loginEvents: " + loginEvents.size());
    PLogger.println("sqlEvents: " + sqlEvents.size());
    PLogger.println("notifications: " + notifications.size());
    for (var entry : worlds.entrySet()) {
      if (entry.getValue().getSession() == null) {
        PLogger.println("world " + entry.getKey() + "; disconnected");
        continue;
      }
      PLogger.println(
          "world "
              + entry.getKey()
              + "; in: "
              + entry.getValue().getSession().getIn().readerIndex()
              + ", "
              + entry.getValue().getSession().getIn().capacity());
      PLogger.println("world " + entry.getKey() + "; keys: " + entry.getValue().keyCount());
    }
    lastPrintStatsTime = PTime.currentTimeMillis();
  }
}
