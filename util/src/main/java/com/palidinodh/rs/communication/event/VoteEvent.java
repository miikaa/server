package com.palidinodh.rs.communication.event;

import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.rs.setting.SqlUserField;
import lombok.Getter;

@Getter
public class VoteEvent extends CommunicationEvent {

  private Type type;

  public VoteEvent(Type type) {
    this.type = type;
  }

  @Override
  public VoteEvent getRepliedEvent() {
    return (VoteEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    if (Settings.getInstance().isLocal()) {
      return;
    }
    String sql = null;
    if (!Settings.getInstance().isLocal()) {
      switch (type) {
        case GET:
          sql =
              Settings.getSecure()
                  .getForum()
                  .getUserInfo(SqlUserField.PENDING_VOTE_POINTS, getUserId());
          break;
        case REMOVE:
          sql =
              Settings.getSecure()
                  .getForum()
                  .buildUserUpdate(SqlUserField.PENDING_VOTE_POINTS, "0", getUserId());
          break;
      }
    }
    if (sql == null || sql.isEmpty()) {
      return;
    }
    setSqlEvent(server.addSqlEvent(new SqlEvent(sql)));
  }

  public enum Type {
    GET,
    REMOVE
  }
}
