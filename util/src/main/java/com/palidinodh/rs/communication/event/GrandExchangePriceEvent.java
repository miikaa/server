package com.palidinodh.rs.communication.event;

import com.palidinodh.rs.adaptive.RsGameMode;
import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import lombok.Getter;

@Getter
public class GrandExchangePriceEvent extends CommunicationEvent {

  private RsGameMode gameMode;
  private int itemId;
  private int totalBuy;
  private int averageBuy;
  private int totalSell;
  private int averageSell;
  private int cheapestSell;

  public GrandExchangePriceEvent(RsGameMode gameMode, int itemId) {
    this.gameMode = gameMode;
    this.itemId = itemId;
  }

  @Override
  public GrandExchangePriceEvent getRepliedEvent() {
    return (GrandExchangePriceEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    var totalBuyPrice = 0;
    var totalSellPrice = 0;
    cheapestSell = -1;
    for (var user : server.getGrandExchange().values()) {
      for (var item : user.getItems()) {
        if (item == null) {
          continue;
        }
        if (itemId != item.getId()) {
          continue;
        }
        if (item.isAborted()) {
          continue;
        }
        if (!item.isStateBuying() && !item.isStateSelling()) {
          continue;
        }
        if (item.isStateBuying()) {
          totalBuy++;
          totalBuyPrice += item.getPrice();
        } else if (item.isStateSelling()) {
          totalSell++;
          totalSellPrice += item.getPrice();
          if (cheapestSell == -1 || item.getPrice() < cheapestSell) {
            cheapestSell = item.getPrice();
          }
        }
      }
    }
    if (totalBuy > 0) {
      averageBuy = totalBuyPrice / totalBuy;
    }
    if (totalSell > 0) {
      averageSell = totalSellPrice / totalSell;
    }
  }
}
