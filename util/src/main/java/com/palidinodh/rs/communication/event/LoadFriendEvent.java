package com.palidinodh.rs.communication.event;

import com.palidinodh.rs.adaptive.RsFriend;
import com.palidinodh.rs.adaptive.RsPlayer;
import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import com.palidinodh.rs.communication.notification.PrivateChatStateNotification;
import lombok.Getter;

@Getter
public class LoadFriendEvent extends CommunicationEvent {

  private RsFriend friend;

  public LoadFriendEvent(RsFriend friend) {
    this.friend = friend;
  }

  @Override
  public LoadFriendEvent getRepliedEvent() {
    return (LoadFriendEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    var player = server.getPlayerById(getUserId());
    if (player == null) {
      return;
    }
    player.loadFriend(friend);
    var friendPlayer = server.getPlayerByUsername(friend.getUsername().toLowerCase());
    if (friendPlayer != null
        && (player.getRights() != RsPlayer.RIGHTS_NONE
            || RsFriend.canRegister(player, friendPlayer))) {
      friend.setWorldId(friendPlayer.getWorldId());
    } else {
      friend.setWorldId(0);
    }
    server.addClanUpdate(player);
    server.addNotification(new PrivateChatStateNotification(player));
  }
}
