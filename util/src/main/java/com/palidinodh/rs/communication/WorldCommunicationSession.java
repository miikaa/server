package com.palidinodh.rs.communication;

import com.palidinodh.io.JsonIO;
import com.palidinodh.io.Readers;
import com.palidinodh.util.PLogger;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.Getter;

@Getter
public class WorldCommunicationSession extends ChannelInboundHandlerAdapter {

  private WorldCommunicationServer server;
  private ByteBuf in = Unpooled.buffer(1_048_576);

  public WorldCommunicationSession(WorldCommunicationServer server) {
    this.server = server;
  }

  @Override
  public void channelRead(ChannelHandlerContext ctx, Object msg) {
    ByteBuf byteBuf = (ByteBuf) msg;
    in.writeBytes(byteBuf);
    byteBuf.release();
    try {
      while (in.readableBytes() >= 10) {
        in.markReaderIndex();
        var length = in.readInt();
        if (length > in.readableBytes()) {
          in.resetReaderIndex();
          break;
        }
        var className = readString();
        var jsonSize = in.readInt();
        var jsonBytes = new byte[jsonSize];
        in.readBytes(jsonBytes);
        var decompressedJson = Readers.gzDecompress(jsonBytes);
        var json = new String(decompressedJson);
        var jsonObject = JsonIO.read(json, Class.forName(className));
        if (jsonObject instanceof CommunicationEvent) {
          var event = (CommunicationEvent) jsonObject;
          CommunicationEvent existingEvent;
          synchronized (server) {
            existingEvent = server.getEvents().get(event.getKey());
            if (existingEvent != null) {
              existingEvent.setMainEvent(event);
              existingEvent.setSqlEvent(event.getSqlEvent());
            }
          }
        } else if (jsonObject instanceof CommunicationNotification) {
          var notification = (CommunicationNotification) jsonObject;
          server.addNotification(notification);
        } else {
          ctx.close();
          break;
        }
      }
    } catch (Exception e) {
      ctx.close();
      PLogger.error(e);
      return;
    }
    if (in.readableBytes() == 0) {
      in.clear();
    }
    if (in.readerIndex() > 1_048_576) {
      in.discardReadBytes();
    }
  }

  private String readString() {
    int aChar;
    var stringBuilder = new StringBuilder();
    while ((aChar = in.readByte()) != 0 && stringBuilder.length() < 1_048_576) {
      stringBuilder.append((char) aChar);
    }
    return stringBuilder.toString();
  }
}
