package com.palidinodh.rs.communication;

public abstract class CommunicationWorldAction<T extends CommunicationEvent> {

  public abstract Class<T> getEventClass();

  public abstract void accept(T event);
}
