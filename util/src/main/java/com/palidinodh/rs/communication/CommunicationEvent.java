package com.palidinodh.rs.communication;

import com.palidinodh.cache.store.util.Stream;
import com.palidinodh.io.JsonIO;
import com.palidinodh.io.Writers;
import com.palidinodh.rs.adaptive.RsPlayer;
import com.palidinodh.rs.communication.event.SqlEvent;
import com.palidinodh.util.PTime;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("ALL")
@Getter(AccessLevel.PROTECTED)
@Setter(AccessLevel.PROTECTED)
public abstract class CommunicationEvent {

  private static int uniqueKey;

  private transient MainCommunicationSession mainSession;
  private transient CommunicationEvent mainEvent;
  private transient Object worldObject;
  private transient Object attachment;
  @Setter private transient long worldWriteTime;
  private int key = getUniqueKey();
  private CompleteState state = CompleteState.CREATED;
  @Getter private int userId;
  @Getter private String username;
  @Getter private SqlEvent sqlEvent;

  private static synchronized int getUniqueKey() {
    return uniqueKey++;
  }

  public void loadRsPlayer(RsPlayer player) {}

  public void worldCreated(WorldCommunicationServer server) {}

  public final CommunicationEvent addRsPlayer(RsPlayer player) {
    userId = player.getId();
    username = player.getUsername();
    loadRsPlayer(player);
    return this;
  }

  public final boolean isComplete() {
    if (sqlEvent != null && !sqlEvent.isComplete()) {
      return false;
    }
    return state == CompleteState.RECEIVED || state == CompleteState.RECEIVED_DUPLICATE;
  }

  public final boolean isDuplicate() {
    return state == CompleteState.RECEIVED_DUPLICATE;
  }

  protected abstract CommunicationEvent getRepliedEvent();

  protected abstract void mainAction(MainCommunicationServer server);

  protected final void loadMain(MainCommunicationSession _session) {
    mainSession = _session;
  }

  protected final void mainComplete(MainCommunicationServer server) {
    var world = mainSession.getWorld();
    if (world != null && world.hasKey(key)) {
      state = CompleteState.RECEIVED_DUPLICATE;
    } else {
      if (world != null) {
        world.addKey(key);
      }
      mainAction(server);
      state = CompleteState.RECEIVED;
    }
    writeMain();
  }

  protected final <T extends CommunicationEvent> boolean worldComplete(
      WorldCommunicationServer server) {
    if (mainEvent == null || !mainEvent.isComplete()) {
      return false;
    }
    state = mainEvent.getState();
    sqlEvent = mainEvent.getSqlEvent();
    var action = server.getWorldAction(getClass());
    if (state == CompleteState.RECEIVED && action != null) {
      Writers.inject(action, attachment);
      action.accept(mainEvent);
    }
    return true;
  }

  protected boolean canWriteWorld() {
    if (worldWriteTime == 0) {
      return true;
    }
    return PTime.betweenMilliToSec(worldWriteTime) > 30;
  }

  protected void writeWorld(WorldCommunicationServer server) {
    if (isComplete()) {
      return;
    }
    if (!server.isOpen()) {
      return;
    }
    if (!canWriteWorld()) {
      return;
    }
    worldWriteTime = PTime.currentTimeMillis();
    var clazz = getClass();
    var className = clazz.getName();
    var json = Writers.gzCompress(JsonIO.write(this, clazz).getBytes());
    var out = new Stream(json.length + 128);
    out.writeInt((className.length() + 1) + (json.length + 4));
    out.writeString(className);
    out.writeInt(json.length);
    out.writeBytes(json);
    server.write(out);
  }

  private void writeMain() {
    var clazz = getClass();
    var className = clazz.getName();
    var json = Writers.gzCompress(JsonIO.write(this, clazz).getBytes());
    var out = new Stream(json.length + 128);
    out.writeInt((className.length() + 1) + (json.length + 4));
    out.writeString(className);
    out.writeInt(json.length);
    out.writeBytes(json);
    mainSession.write(out);
  }

  public enum CompleteState {
    CREATED,
    SENDING,
    RECEIVED,
    RECEIVED_DUPLICATE
  }
}
