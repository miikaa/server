package com.palidinodh.rs.communication;

import com.palidinodh.cache.store.util.Stream;
import com.palidinodh.io.JsonIO;
import com.palidinodh.io.Writers;
import java.util.List;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter(AccessLevel.PROTECTED)
public class CommunicationNotification {

  private transient List<Integer> worlds;

  public CommunicationNotification(List<Integer> worlds) {
    this.worlds = worlds;
  }

  protected final void write(MainCommunicationSession session) {
    if (worlds != null && !worlds.isEmpty() && !worlds.contains(session.getWorldId())) {
      return;
    }
    var clazz = getClass();
    var className = clazz.getName();
    var json = Writers.gzCompress(JsonIO.write(this, clazz).getBytes());
    var out = new Stream(512);
    out.writeInt((className.length() + 1) + (json.length + 4));
    out.writeString(className);
    out.writeInt(json.length);
    out.writeBytes(json);
    session.write(out);
  }
}
