package com.palidinodh.rs.communication.event;

import com.palidinodh.rs.adaptive.RsClanActiveUser;
import com.palidinodh.rs.adaptive.RsPlayer;
import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import lombok.Getter;

@Getter
public class ClanChatLeaveEvent extends CommunicationEvent {

  private String clanChatUsername;
  private String joiningClanChatUsername;

  public ClanChatLeaveEvent(String clanChatUsername, String joiningClanChatUsername) {
    this.clanChatUsername = clanChatUsername;
    this.joiningClanChatUsername = joiningClanChatUsername;
  }

  @Override
  public ClanChatLeaveEvent getRepliedEvent() {
    return (ClanChatLeaveEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    var fromUsername = joiningClanChatUsername;
    if (clanChatUsername != null) {
      fromUsername = clanChatUsername;
    }
    if (fromUsername == null) {
      return;
    }
    var clanPlayer = new RsPlayer(-1, fromUsername);
    var clan = server.getClan(clanPlayer);
    if (clan == null) {
      return;
    }
    RsClanActiveUser myUser = null;
    for (var user : clan.getActiveUsers()) {
      if (!getUsername().equalsIgnoreCase(user.getUsername())) {
        continue;
      }
      myUser = user;
    }
    if (myUser == null) {
      return;
    }
    clan.remove(myUser);
    server.addClanUpdate(clanPlayer);
  }
}
