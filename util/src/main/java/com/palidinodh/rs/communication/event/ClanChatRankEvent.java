package com.palidinodh.rs.communication.event;

import com.palidinodh.rs.adaptive.RsFriend;
import com.palidinodh.rs.communication.CommunicationEvent;
import com.palidinodh.rs.communication.MainCommunicationServer;
import lombok.Getter;

@Getter
public class ClanChatRankEvent extends CommunicationEvent {

  private RsFriend friend;

  public ClanChatRankEvent(RsFriend friend) {
    this.friend = friend;
  }

  @Override
  public ClanChatRankEvent getRepliedEvent() {
    return (ClanChatRankEvent) getMainEvent();
  }

  @Override
  public void mainAction(MainCommunicationServer server) {
    var player = server.getPlayerById(getUserId());
    if (player == null) {
      return;
    }
    player.loadFriendClanRank(friend);
    var clan = server.getClan(player);
    if (clan == null) {
      server.getClan(player, player.getUsername());
    }
    server.addClanUpdate(player);
  }
}
