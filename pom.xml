<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright (c) 2019, Dalton <palidinodh@gmail.com> All rights reserved. 
	Redistribution and use in source and binary forms, with or without modification, 
	are permitted provided that the following conditions are met: 1. Redistributions 
	of source code must retain the above copyright notice, this list of conditions 
	and the following disclaimer. 2. Redistributions in binary form must reproduce 
	the above copyright notice, this list of conditions and the following disclaimer 
	in the documentation and/or other materials provided with the distribution. 
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
	ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
	LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE 
	USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. -->
<project xmlns="http://maven.apache.org/POM/4.0.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>com.palidinodh</groupId>
  <artifactId>BattleScape</artifactId>
  <version>1.3.214</version>
  <packaging>pom</packaging>
  <name>BattleScape</name>
  <description>BattleScape RSPS</description>
  <url>https://www.battle-scape.com</url>
  <properties>
    <!--suppress UnresolvedMavenProperty -->
    <project.root>${maven.multiModuleProjectDirectory}</project.root>
    <deploy.dir>${project.root}/target/deploy</deploy.dir>
    <java.version>11</java.version>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.source>${java.version}</maven.compiler.source>
    <maven.compiler.target>${java.version}</maven.compiler.target>
    <maven.javadoc.skip>true</maven.javadoc.skip>
    <checkstyle.skip>true</checkstyle.skip>
  </properties>
  <licenses>
    <license>
      <name>2-Clause BSD License</name>
      <url>https://opensource.org/licenses/BSD-2-Clause</url>
    </license>
  </licenses>
  <inceptionYear>2019</inceptionYear>
  <developers>
    <developer>
      <id>Palidino</id>
    </developer>
  </developers>
  <scm>
    <connection>scm:svn:http://none</connection>
    <developerConnection>scm:svn:https://none</developerConnection>
    <url>scm:svn:https://none</url>
  </scm>
  <repositories>
    <repository>
      <id>local-maven-repo</id>
      <name>Local Maven Repo</name>
      <url>file:${project.root}/util/data/repository</url>
      <releases>
        <enabled>true</enabled>
        <updatePolicy>always</updatePolicy>
      </releases>
    </repository>
  </repositories>
  <modules>
    <module>util</module>
    <module>cache</module>
    <module>server-script</module>
  </modules>
  <profiles>
    <profile>
      <id>Core Access</id>
      <activation>
        <file>
          <exists>${basedir}/server-core/pom.xml</exists>
        </file>
      </activation>
      <modules>
        <module>cache-tools</module>
        <module>client</module>
        <module>server-core</module>
      </modules>
    </profile>
  </profiles>
  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>io.netty</groupId>
        <artifactId>netty-all</artifactId>
        <version>4.1.79.Final</version>
      </dependency>
      <dependency>
        <groupId>com.google.code.gson</groupId>
        <artifactId>gson</artifactId>
        <version>2.9.1</version>
      </dependency>
      <dependency>
        <groupId>com.google.inject</groupId>
        <artifactId>guice</artifactId>
        <version>5.1.0</version>
      </dependency>
      <dependency>
        <groupId>org.apache.commons</groupId>
        <artifactId>commons-compress</artifactId>
        <version>1.21</version>
      </dependency>
      <dependency>
        <groupId>org.javacord</groupId>
        <artifactId>javacord</artifactId>
        <version>3.8.0</version>
        <type>pom</type>
      </dependency>
      <dependency>
        <groupId>com.maxmind.geoip2</groupId>
        <artifactId>geoip2</artifactId>
        <version>3.0.1</version>
      </dependency>
      <dependency>
        <groupId>org.apache.logging.log4j</groupId>
        <artifactId>log4j-api</artifactId>
        <version>2.18.0</version>
      </dependency>
      <dependency>
        <groupId>org.apache.logging.log4j</groupId>
        <artifactId>log4j-core</artifactId>
        <version>2.18.0</version>
      </dependency>
      <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>8.0.30</version>
      </dependency>
      <dependency>
        <groupId>pull-parser</groupId>
        <artifactId>pull-parser</artifactId>
        <version>2.1.10</version>
      </dependency>
      <dependency>
        <groupId>com.thoughtworks.xstream</groupId>
        <artifactId>xstream</artifactId>
        <version>1.4.19</version>
      </dependency>
      <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
        <version>1.18.24</version>
        <scope>provided</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>
  <build>
    <finalName>${project.artifactId}</finalName>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.10.0</version>
        <configuration>
          <source>${java.version}</source>
          <target>${java.version}</target>
          <fork>true</fork>
          <maxmem>2048m</maxmem>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-shade-plugin</artifactId>
        <version>3.2.4</version>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-antrun-plugin</artifactId>
        <version>3.0.0</version>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-deploy-plugin</artifactId>
        <version>3.0.0-M1</version>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-dependency-plugin</artifactId>
        <version>3.1.2</version>
        <executions>
          <execution>
            <id>purge-local-dependencies</id>
            <phase>install</phase>
            <goals>
              <goal>purge-local-repository</goal>
            </goals>
            <configuration>
              <manualInclude>com.palidinodh:${project.artifactId}</manualInclude>
            </configuration>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>build-helper-maven-plugin</artifactId>
        <version>3.2.0</version>
      </plugin>
      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>versions-maven-plugin</artifactId>
        <version>2.8.1</version>
      </plugin>
      <plugin>
        <groupId>com.coveo</groupId>
        <artifactId>fmt-maven-plugin</artifactId>
        <version>2.10</version>
        <configuration>
          <displayFiles>false</displayFiles>
        </configuration>
        <executions>
          <execution>
            <goals>
              <goal>format</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
</project>
