package com.palidinodh.maparea.kharidiandesert.uzer.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class UzerHunterNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(3415, 3076), NpcId.ORANGE_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(3417, 3079), NpcId.ORANGE_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(3418, 3074), NpcId.ORANGE_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(3411, 3072), NpcId.ORANGE_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(3409, 3077), NpcId.ORANGE_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(3407, 3081), NpcId.ORANGE_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(3413, 3083), NpcId.ORANGE_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(3416, 3083), NpcId.ORANGE_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(3414, 3070), NpcId.ORANGE_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(3410, 3088), NpcId.ORANGE_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(3403, 3087), NpcId.ORANGE_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(3401, 3093), NpcId.ORANGE_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(3407, 3090), NpcId.ORANGE_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(3409, 3095), NpcId.ORANGE_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(3407, 3096), NpcId.ORANGE_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(3400, 3099), NpcId.ORANGE_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(3406, 3099), NpcId.ORANGE_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(3405, 3132), NpcId.ORANGE_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(3405, 3136), NpcId.ORANGE_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(3401, 3134), NpcId.ORANGE_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(3409, 3134), NpcId.ORANGE_SALAMANDER));
    spawns.add(new NpcSpawn(8, new Tile(3413, 3073), NpcId.GOLDEN_WARBLER));
    spawns.add(new NpcSpawn(8, new Tile(3413, 3077), NpcId.GOLDEN_WARBLER));
    spawns.add(new NpcSpawn(8, new Tile(3414, 3081), NpcId.GOLDEN_WARBLER));
    spawns.add(new NpcSpawn(8, new Tile(3409, 3079), NpcId.GOLDEN_WARBLER));
    spawns.add(new NpcSpawn(8, new Tile(3412, 3085), NpcId.GOLDEN_WARBLER));
    spawns.add(new NpcSpawn(8, new Tile(3406, 3086), NpcId.GOLDEN_WARBLER));
    spawns.add(new NpcSpawn(8, new Tile(3406, 3092), NpcId.GOLDEN_WARBLER));
    spawns.add(new NpcSpawn(8, new Tile(3402, 3094), NpcId.GOLDEN_WARBLER));
    spawns.add(new NpcSpawn(8, new Tile(3405, 3101), NpcId.GOLDEN_WARBLER));
    spawns.add(new NpcSpawn(8, new Tile(3406, 3129), NpcId.GOLDEN_WARBLER));
    spawns.add(new NpcSpawn(8, new Tile(3409, 3119), NpcId.GOLDEN_WARBLER));
    spawns.add(new NpcSpawn(8, new Tile(3404, 3114), NpcId.GOLDEN_WARBLER));
    spawns.add(new NpcSpawn(8, new Tile(3402, 3107), NpcId.GOLDEN_WARBLER));
    spawns.add(new NpcSpawn(8, new Tile(3405, 3105), NpcId.GOLDEN_WARBLER));
    spawns.add(new NpcSpawn(8, new Tile(3401, 3104), NpcId.GOLDEN_WARBLER));
    spawns.add(new NpcSpawn(8, new Tile(3399, 3116), NpcId.GOLDEN_WARBLER));
    spawns.add(new NpcSpawn(8, new Tile(3400, 3120), NpcId.GOLDEN_WARBLER));
    spawns.add(new NpcSpawn(8, new Tile(3401, 3126), NpcId.GOLDEN_WARBLER));
    spawns.add(new NpcSpawn(8, new Tile(3409, 3124), NpcId.GOLDEN_WARBLER));
    spawns.add(new NpcSpawn(8, new Tile(3409, 3110), NpcId.GOLDEN_WARBLER));
    spawns.add(new NpcSpawn(8, new Tile(3410, 3090), NpcId.GOLDEN_WARBLER));
    spawns.add(new NpcSpawn(8, new Tile(3417, 3076), NpcId.GOLDEN_WARBLER));

    return spawns;
  }
}
