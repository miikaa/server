package com.palidinodh.maparea.runecrafting;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(9291)
public class WrathAltarArea extends Area {}
