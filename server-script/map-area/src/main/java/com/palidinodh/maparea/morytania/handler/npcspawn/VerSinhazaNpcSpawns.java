package com.palidinodh.maparea.morytania.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class VerSinhazaNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3649, 3206), NpcId.BANKER_8322));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3651, 3206), NpcId.BANKER_8321));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3652, 3206), NpcId.BANKER_8322));
    spawns.add(new NpcSpawn(4, new Tile(3650, 3214), NpcId.MEIYERDITCH_CITIZEN_8331));
    spawns.add(new NpcSpawn(4, new Tile(3653, 3211), NpcId.VYRELORD_8334));
    spawns.add(new NpcSpawn(4, new Tile(3666, 3221), NpcId.MEIYERDITCH_CITIZEN_8330));
    spawns.add(new NpcSpawn(4, new Tile(3655, 3218), NpcId.VYRELADY_8333));
    spawns.add(new NpcSpawn(4, new Tile(3669, 3217), NpcId.MEIYERDITCH_CITIZEN_8328));
    spawns.add(
        new NpcSpawn(Tile.Direction.WEST, new Tile(3673, 3223), NpcId.MYSTERIOUS_STRANGER_8325));
    spawns.add(new NpcSpawn(4, new Tile(3671, 3219), NpcId.VYRELORD_8332));

    return spawns;
  }
}
