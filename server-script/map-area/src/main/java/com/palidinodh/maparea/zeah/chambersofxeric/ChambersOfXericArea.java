package com.palidinodh.maparea.zeah.chambersofxeric;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  12889, 13136, 13137, 13138, 13139, 13140, 13141, 13145, 13393, 13394, 13395, 13396, 13397, 13401
})
public class ChambersOfXericArea extends Area {}
