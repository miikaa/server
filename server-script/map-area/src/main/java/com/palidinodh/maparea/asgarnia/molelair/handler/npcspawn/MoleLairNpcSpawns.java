package com.palidinodh.maparea.asgarnia.molelair.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class MoleLairNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(64, new Tile(1759, 5184), NpcId.GIANT_MOLE_230));
    spawns.add(new NpcSpawn(8, new Tile(1749, 5222), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1754, 5224), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1759, 5218), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1766, 5217), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1770, 5222), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1772, 5230), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1773, 5239), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1781, 5234), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1782, 5224), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1780, 5215), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1783, 5204), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1774, 5205), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1773, 5195), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1784, 5194), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1778, 5187), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1780, 5180), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1775, 5172), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1780, 5164), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1779, 5155), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1781, 5150), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1774, 5149), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1767, 5151), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1757, 5152), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1751, 5149), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1743, 5151), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1737, 5154), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1738, 5161), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1744, 5165), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1750, 5173), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1741, 5174), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1741, 5181), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1741, 5189), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1739, 5196), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1735, 5203), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1736, 5212), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1742, 5207), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1751, 5207), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1755, 5200), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1749, 5195), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1758, 5210), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1742, 5221), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1735, 5228), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1764, 5198), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1766, 5176), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1756, 5162), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1761, 5166), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1763, 5162), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1770, 5164), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1756, 5178), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1755, 5184), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1761, 5190), NpcId.BABY_MOLE));
    spawns.add(new NpcSpawn(8, new Tile(1766, 5184), NpcId.BABY_MOLE));

    return spawns;
  }
}
