package com.palidinodh.maparea.wilderness.bossescapecaves.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class BossEscapeCavesNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(8, new Tile(3357, 10326), NpcId.CALLISTO_470_6609));
    spawns.add(new NpcSpawn(8, new Tile(3294, 10201, 1), NpcId.VETION_454));
    spawns.add(new NpcSpawn(8, new Tile(3421, 10201, 2), NpcId.VENENATIS_464_6610));
    spawns.add(new NpcSpawn(8, new Tile(3351, 10248), NpcId.GRIZZLY_BEAR_83));
    spawns.add(new NpcSpawn(8, new Tile(3369, 10249), NpcId.GRIZZLY_BEAR_83));
    spawns.add(new NpcSpawn(8, new Tile(3375, 10259), NpcId.GRIZZLY_BEAR_83));
    spawns.add(new NpcSpawn(8, new Tile(3383, 10274), NpcId.GRIZZLY_BEAR_83));
    spawns.add(new NpcSpawn(8, new Tile(3376, 10289), NpcId.GRIZZLY_BEAR_83));
    spawns.add(new NpcSpawn(8, new Tile(3354, 10294), NpcId.GRIZZLY_BEAR_83));
    spawns.add(new NpcSpawn(8, new Tile(3337, 10282), NpcId.GRIZZLY_BEAR_83));
    spawns.add(new NpcSpawn(8, new Tile(3334, 10265), NpcId.GRIZZLY_BEAR_83));
    spawns.add(new NpcSpawn(8, new Tile(3343, 10258), NpcId.GRIZZLY_BEAR_83));
    spawns.add(new NpcSpawn(8, new Tile(3353, 10268), NpcId.GRIZZLY_BEAR_83));
    spawns.add(new NpcSpawn(8, new Tile(3359, 10277), NpcId.GRIZZLY_BEAR_83));
    spawns.add(new NpcSpawn(8, new Tile(3363, 10269), NpcId.GRIZZLY_BEAR_83));
    spawns.add(new NpcSpawn(8, new Tile(3358, 10249), NpcId.POISON_SPIDER_48));
    spawns.add(new NpcSpawn(8, new Tile(3373, 10254), NpcId.POISON_SPIDER_48));
    spawns.add(new NpcSpawn(8, new Tile(3384, 10265), NpcId.POISON_SPIDER_48));
    spawns.add(new NpcSpawn(8, new Tile(3380, 10282), NpcId.POISON_SPIDER_48));
    spawns.add(new NpcSpawn(8, new Tile(3367, 10294), NpcId.POISON_SPIDER_48));
    spawns.add(new NpcSpawn(8, new Tile(3343, 10289), NpcId.POISON_SPIDER_48));
    spawns.add(new NpcSpawn(8, new Tile(3334, 10273), NpcId.POISON_SPIDER_48));
    spawns.add(new NpcSpawn(8, new Tile(3345, 10253), NpcId.POISON_SPIDER_48));
    spawns.add(new NpcSpawn(8, new Tile(3349, 10265), NpcId.POISON_SPIDER_48));
    spawns.add(new NpcSpawn(8, new Tile(3371, 10265), NpcId.POISON_SPIDER_48));
    spawns.add(new NpcSpawn(8, new Tile(3360, 10285), NpcId.POISON_SPIDER_48));
    spawns.add(new NpcSpawn(8, new Tile(3357, 10272), NpcId.POISON_SPIDER_48));
    spawns.add(new NpcSpawn(8, new Tile(3360, 10270), NpcId.POISON_SPIDER_48));

    return spawns;
  }
}
