package com.palidinodh.maparea.morytania.mosleharmless;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({14637, 14638, 14639, 14893, 14894, 14895, 15149, 15150, 15151, 15405, 15406, 15407})
public class MosLeHarmlessArea extends Area {}
