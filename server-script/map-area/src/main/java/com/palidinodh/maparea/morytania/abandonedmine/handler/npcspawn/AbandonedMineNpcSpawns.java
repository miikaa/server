package com.palidinodh.maparea.morytania.abandonedmine.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class AbandonedMineNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(3434, 9637), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(3428, 9638), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(3427, 9634), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(3426, 9631), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(3423, 9637), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(3427, 9645), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(3429, 9648), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(3425, 9652), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(3414, 9636), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(3410, 9641), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(3418, 9629), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(3412, 9621), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(3421, 9622), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(new Tile(2727, 4491), NpcId.MINE_CART));
    spawns.add(new NpcSpawn(8, new Tile(2798, 4571), NpcId.HAZE));
    spawns.add(new NpcSpawn(8, new Tile(2793, 4576), NpcId.HAZE));
    spawns.add(new NpcSpawn(8, new Tile(2807, 4570), NpcId.HAZE));
    spawns.add(new NpcSpawn(8, new Tile(2784, 4488), NpcId.HAZE));
    spawns.add(new NpcSpawn(4, new Tile(2791, 4498), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(2788, 4501), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(2791, 4504), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(8, new Tile(2785, 4569), NpcId.HAZE));
    spawns.add(new NpcSpawn(8, new Tile(2785, 4580), NpcId.HAZE));
    spawns.add(new NpcSpawn(8, new Tile(2793, 4584), NpcId.HAZE));
    spawns.add(new NpcSpawn(8, new Tile(2800, 4593), NpcId.HAZE));
    spawns.add(new NpcSpawn(8, new Tile(2808, 4599), NpcId.HAZE));
    spawns.add(new NpcSpawn(8, new Tile(2801, 4578), NpcId.HAZE));
    spawns.add(new NpcSpawn(new Tile(2739, 4531), NpcId.MINE_CART));
    spawns.add(new NpcSpawn(new Tile(2795, 4462), NpcId.MINE_CART));
    spawns.add(new NpcSpawn(new Tile(2791, 4452), NpcId.MINE_CART));
    spawns.add(new NpcSpawn(new Tile(2785, 4447), NpcId.MINE_CART));
    spawns.add(new NpcSpawn(new Tile(2787, 4451), NpcId.LOADING_CRANE));
    spawns.add(new NpcSpawn(new Tile(2787, 4446), NpcId.LOADING_CRANE));
    spawns.add(new NpcSpawn(new Tile(2788, 4455), NpcId.INNOCENT_LOOKING_KEY));
    spawns.add(new NpcSpawn(new Tile(2782, 4458), NpcId.LOADING_CRANE));
    spawns.add(new NpcSpawn(new Tile(2793, 4458), NpcId.LOADING_CRANE));

    return spawns;
  }
}
