package com.palidinodh.maparea.kandarin.fishingguild.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class FishingGuildNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(new Tile(2615, 3398, 1), NpcId.BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(2584, 3422), NpcId.BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(2584, 3421), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(2584, 3418), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(2584, 3419), NpcId.BANKER));
    spawns.add(new NpcSpawn(4, new Tile(2597, 3400), NpcId.ROACHEY));
    spawns.add(new NpcSpawn(new Tile(2612, 3412), NpcId.FISHING_SPOT_1514));
    spawns.add(new NpcSpawn(new Tile(2612, 3411), NpcId.FISHING_SPOT_1514));
    spawns.add(new NpcSpawn(new Tile(2608, 3410), NpcId.FISHING_SPOT_1514));
    spawns.add(new NpcSpawn(new Tile(2607, 3410), NpcId.FISHING_SPOT_1514));
    spawns.add(new NpcSpawn(new Tile(2606, 3410), NpcId.FISHING_SPOT_1514));
    spawns.add(new NpcSpawn(new Tile(2605, 3410), NpcId.FISHING_SPOT_1514));
    spawns.add(new NpcSpawn(new Tile(2602, 3411), NpcId.ROD_FISHING_SPOT));
    spawns.add(new NpcSpawn(new Tile(2602, 3412), NpcId.ROD_FISHING_SPOT));
    spawns.add(new NpcSpawn(new Tile(2602, 3413), NpcId.ROD_FISHING_SPOT));
    spawns.add(new NpcSpawn(new Tile(2602, 3414), NpcId.ROD_FISHING_SPOT));
    spawns.add(new NpcSpawn(new Tile(2602, 3415), NpcId.ROD_FISHING_SPOT));
    spawns.add(new NpcSpawn(new Tile(2602, 3416), NpcId.ROD_FISHING_SPOT));
    spawns.add(new NpcSpawn(new Tile(2605, 3416), NpcId.FISHING_SPOT_1510));
    spawns.add(new NpcSpawn(new Tile(2606, 3416), NpcId.FISHING_SPOT_1510));
    spawns.add(new NpcSpawn(new Tile(2607, 3416), NpcId.FISHING_SPOT_1510));
    spawns.add(new NpcSpawn(new Tile(2608, 3416), NpcId.FISHING_SPOT_1510));
    spawns.add(new NpcSpawn(new Tile(2609, 3416), NpcId.FISHING_SPOT_1510));
    spawns.add(new NpcSpawn(new Tile(2610, 3416), NpcId.FISHING_SPOT_1510));
    spawns.add(new NpcSpawn(new Tile(2611, 3416), NpcId.FISHING_SPOT_1510));
    spawns.add(new NpcSpawn(new Tile(2612, 3415), NpcId.FISHING_SPOT_1510));
    spawns.add(new NpcSpawn(new Tile(2612, 3414), NpcId.FISHING_SPOT_1510));
    spawns.add(new NpcSpawn(new Tile(2603, 3417), NpcId.ROD_FISHING_SPOT));
    spawns.add(new NpcSpawn(new Tile(2604, 3417), NpcId.ROD_FISHING_SPOT));
    spawns.add(new NpcSpawn(new Tile(2605, 3413), NpcId.ROD_FISHING_SPOT));
    spawns.add(new NpcSpawn(new Tile(2611, 3413), NpcId.FISHING_SPOT_1510));
    spawns.add(new NpcSpawn(new Tile(2610, 3413), NpcId.FISHING_SPOT_1514));
    spawns.add(new NpcSpawn(new Tile(2609, 3413), NpcId.FISHING_SPOT_1514));
    spawns.add(new NpcSpawn(new Tile(2608, 3413), NpcId.FISHING_SPOT_1510));
    spawns.add(new NpcSpawn(new Tile(2607, 3413), NpcId.FISHING_SPOT_1510));
    spawns.add(new NpcSpawn(new Tile(2606, 3413), NpcId.FISHING_SPOT_1514));
    spawns.add(new NpcSpawn(new Tile(2604, 3419), NpcId.FISHING_SPOT_4316));
    spawns.add(new NpcSpawn(new Tile(2603, 3419), NpcId.FISHING_SPOT_4316));
    spawns.add(new NpcSpawn(new Tile(2602, 3419), NpcId.FISHING_SPOT_4316));
    spawns.add(new NpcSpawn(new Tile(2605, 3420), NpcId.FISHING_SPOT_4316));
    spawns.add(new NpcSpawn(new Tile(2605, 3421), NpcId.FISHING_SPOT_4316));
    spawns.add(new NpcSpawn(new Tile(2604, 3422), NpcId.FISHING_SPOT_4316));
    spawns.add(new NpcSpawn(new Tile(2603, 3422), NpcId.FISHING_SPOT_4316));
    spawns.add(new NpcSpawn(new Tile(2602, 3422), NpcId.FISHING_SPOT_4316));
    spawns.add(new NpcSpawn(new Tile(2601, 3422), NpcId.FISHING_SPOT_4316));
    spawns.add(new NpcSpawn(new Tile(2600, 3419), NpcId.FISHING_SPOT_4712));
    spawns.add(new NpcSpawn(new Tile(2599, 3419), NpcId.FISHING_SPOT_4712));
    spawns.add(new NpcSpawn(new Tile(2598, 3419), NpcId.FISHING_SPOT_4712));
    spawns.add(new NpcSpawn(new Tile(2601, 3423), NpcId.FISHING_SPOT_1511));
    spawns.add(new NpcSpawn(new Tile(2602, 3423), NpcId.FISHING_SPOT_1511));
    spawns.add(new NpcSpawn(new Tile(2603, 3423), NpcId.FISHING_SPOT_1511));
    spawns.add(new NpcSpawn(new Tile(2604, 3423), NpcId.FISHING_SPOT_1511));
    spawns.add(new NpcSpawn(new Tile(2605, 3424), NpcId.FISHING_SPOT_1511));
    spawns.add(new NpcSpawn(new Tile(2605, 3425), NpcId.FISHING_SPOT_1511));
    spawns.add(new NpcSpawn(new Tile(2604, 3426), NpcId.FISHING_SPOT_1511));
    spawns.add(new NpcSpawn(new Tile(2603, 3426), NpcId.FISHING_SPOT_1511));
    spawns.add(new NpcSpawn(new Tile(2602, 3426), NpcId.FISHING_SPOT_1511));
    spawns.add(new NpcSpawn(new Tile(2598, 3423), NpcId.FISHING_SPOT_1510));
    spawns.add(new NpcSpawn(new Tile(2598, 3422), NpcId.FISHING_SPOT_1510));
    spawns.add(new NpcSpawn(2, new Tile(2614, 3445), NpcId.KYLIE_MINNOW_7728));
    spawns.add(new NpcSpawn(new Tile(2610, 3443), NpcId.FISHING_SPOT_7730));
    spawns.add(new NpcSpawn(new Tile(2611, 3444), NpcId.FISHING_SPOT_7731));
    spawns.add(new NpcSpawn(new Tile(2618, 3443), NpcId.FISHING_SPOT_7730));
    spawns.add(new NpcSpawn(new Tile(2619, 3444), NpcId.FISHING_SPOT_7731));

    return spawns;
  }
}
