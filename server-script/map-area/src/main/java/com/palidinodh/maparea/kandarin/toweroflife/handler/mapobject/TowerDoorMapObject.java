package com.palidinodh.maparea.kandarin.toweroflife.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.map.Region;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.TOWER_DOOR)
class TowerDoorMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (mapObject.isBusy() || player.getX() != mapObject.getX()) {
      return;
    }
    player.getMovement().clear();
    if (player.getY() == mapObject.getY()) {
      player.getMovement().addMovement(mapObject.getX(), mapObject.getY() - 1);
    } else {
      player.getMovement().addMovement(mapObject.getX(), mapObject.getY());
    }
    Region.openDoor(player, mapObject, 1, false, false);
  }
}
