package com.palidinodh.maparea.asgarnia.donator.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.CAVE_ENTRANCE_2123, ObjectId.CAVE_ENTRANCE_5007})
public class CaveEntranceMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    switch (mapObject.getId()) {
      case ObjectId.CAVE_ENTRANCE_2123:
        player.getMovement().ladderDownTeleport(new Tile(2532, 9294, 12));
        break;
      case ObjectId.CAVE_ENTRANCE_5007:
        player.getMovement().ladderDownTeleport(new Tile(2532, 9294, 16));
        break;
    }
  }
}
