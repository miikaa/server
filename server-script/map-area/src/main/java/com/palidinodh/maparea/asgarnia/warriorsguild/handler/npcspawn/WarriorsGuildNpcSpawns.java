package com.palidinodh.maparea.asgarnia.warriorsguild.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class WarriorsGuildNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(8, new Tile(2869, 3544), NpcId.HARRALLAK_MENAROUS));
    spawns.add(new NpcSpawn(2, new Tile(2879, 3544), NpcId.GHOMMAL));
    spawns.add(new NpcSpawn(2, new Tile(2857, 3543), NpcId.SHANOMI));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(2841, 3543), NpcId.JADE));
    spawns.add(new NpcSpawn(2, new Tile(2846, 3551), NpcId.LILLY));
    spawns.add(new NpcSpawn(2, new Tile(2840, 3551), NpcId.LIDIO));
    spawns.add(new NpcSpawn(4, new Tile(2856, 3536, 1), NpcId.ANTON));
    spawns.add(new NpcSpawn(4, new Tile(2840, 3544, 2), NpcId.CYCLOPS_56_2463));
    spawns.add(new NpcSpawn(4, new Tile(2839, 3548, 2), NpcId.CYCLOPS_56_2463));
    spawns.add(new NpcSpawn(4, new Tile(2842, 3552, 2), NpcId.CYCLOPS_56_2463));
    spawns.add(new NpcSpawn(4, new Tile(2845, 3546, 2), NpcId.CYCLOPS_56_2463));
    spawns.add(new NpcSpawn(4, new Tile(2848, 3551, 2), NpcId.CYCLOPS_56_2463));
    spawns.add(new NpcSpawn(4, new Tile(2850, 3542, 2), NpcId.CYCLOPS_56_2463));
    spawns.add(new NpcSpawn(4, new Tile(2850, 3536, 2), NpcId.CYCLOPS_56_2463));
    spawns.add(new NpcSpawn(4, new Tile(2856, 3537, 2), NpcId.CYCLOPS_56_2463));
    spawns.add(new NpcSpawn(4, new Tile(2861, 3539, 2), NpcId.CYCLOPS_56_2463));
    spawns.add(new NpcSpawn(4, new Tile(2856, 3543, 2), NpcId.CYCLOPS_56_2463));
    spawns.add(new NpcSpawn(4, new Tile(2853, 3548, 2), NpcId.CYCLOPS_56_2463));
    spawns.add(new NpcSpawn(4, new Tile(2858, 3552, 2), NpcId.CYCLOPS_56_2463));
    spawns.add(new NpcSpawn(4, new Tile(2862, 3546, 2), NpcId.CYCLOPS_56_2463));
    spawns.add(new NpcSpawn(4, new Tile(2866, 3541, 2), NpcId.CYCLOPS_56_2463));
    spawns.add(new NpcSpawn(4, new Tile(2871, 3539, 2), NpcId.CYCLOPS_56_2463));
    spawns.add(new NpcSpawn(4, new Tile(2872, 3545, 2), NpcId.CYCLOPS_56_2463));
    spawns.add(new NpcSpawn(3, new Tile(2868, 3548, 2), NpcId.CYCLOPS_56_2463));
    spawns.add(new NpcSpawn(4, new Tile(2872, 3552, 2), NpcId.CYCLOPS_56_2463));
    spawns.add(new NpcSpawn(4, new Tile(2863, 3551, 2), NpcId.CYCLOPS_56_2463));
    spawns.add(new NpcSpawn(4, new Tile(2906, 9962), NpcId.CYCLOPS_106));
    spawns.add(new NpcSpawn(4, new Tile(2908, 9959), NpcId.CYCLOPS_106));
    spawns.add(new NpcSpawn(4, new Tile(2913, 9963), NpcId.CYCLOPS_106));
    spawns.add(new NpcSpawn(4, new Tile(2915, 9958), NpcId.CYCLOPS_106));
    spawns.add(new NpcSpawn(4, new Tile(2919, 9962), NpcId.CYCLOPS_106));
    spawns.add(new NpcSpawn(4, new Tile(2923, 9958), NpcId.CYCLOPS_106));
    spawns.add(new NpcSpawn(4, new Tile(2916, 9966), NpcId.CYCLOPS_106));
    spawns.add(new NpcSpawn(4, new Tile(2914, 9971), NpcId.CYCLOPS_106));
    spawns.add(new NpcSpawn(4, new Tile(2920, 9970), NpcId.CYCLOPS_106));
    spawns.add(new NpcSpawn(4, new Tile(2922, 9966), NpcId.CYCLOPS_106));
    spawns.add(new NpcSpawn(4, new Tile(2927, 9962), NpcId.CYCLOPS_106));
    spawns.add(new NpcSpawn(4, new Tile(2929, 9958), NpcId.CYCLOPS_106));
    spawns.add(new NpcSpawn(4, new Tile(2927, 9968), NpcId.CYCLOPS_106));
    spawns.add(new NpcSpawn(4, new Tile(2932, 9970), NpcId.CYCLOPS_106));
    spawns.add(new NpcSpawn(4, new Tile(2932, 9965), NpcId.CYCLOPS_106));
    spawns.add(new NpcSpawn(4, new Tile(2933, 9960), NpcId.CYCLOPS_106));
    spawns.add(new NpcSpawn(4, new Tile(2937, 9958), NpcId.CYCLOPS_106));
    spawns.add(new NpcSpawn(4, new Tile(2937, 9964), NpcId.CYCLOPS_106));
    spawns.add(new NpcSpawn(4, new Tile(2936, 9969), NpcId.CYCLOPS_106));

    return spawns;
  }
}
