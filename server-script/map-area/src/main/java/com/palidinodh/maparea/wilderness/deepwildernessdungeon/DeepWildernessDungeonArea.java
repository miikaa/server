package com.palidinodh.maparea.wilderness.deepwildernessdungeon;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(12193)
public class DeepWildernessDungeonArea extends Area {

  @Override
  public boolean inWilderness() {
    return true;
  }
}
