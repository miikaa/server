package com.palidinodh.maparea.asgarnia.donator.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class DonatorNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3049, 3500), NpcId.MARTIN_THWAIT));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3065, 3497), NpcId.NIEVE));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3065, 3496), NpcId.KRYSTILIA));
    spawns.add(new NpcSpawn(Tile.Direction.SOUTH, new Tile(3058, 3515), NpcId.LLIANN));
    spawns.add(new NpcSpawn(Tile.Direction.SOUTH, new Tile(3053, 3505), NpcId.WIZARD_16048));
    spawns.add(new NpcSpawn(Tile.Direction.SOUTH, new Tile(3042, 3501), NpcId.BOB_BARTER_HERBS));
    spawns.add(new NpcSpawn(new Tile(3043, 3501), NpcId.SKILLING_SELLER));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3056, 3501), NpcId.ILFEEN));
    spawns.add(new NpcSpawn(new Tile(3041, 3495), NpcId.ROD_FISHING_SPOT_6825));
    spawns.add(new NpcSpawn(new Tile(3042, 3495), NpcId.ROD_FISHING_SPOT_6825));
    spawns.add(new NpcSpawn(new Tile(3043, 3495), NpcId.ROD_FISHING_SPOT_6825));
    spawns.add(new NpcSpawn(new Tile(3044, 3495), NpcId.FISHING_SPOT_1511));
    spawns.add(new NpcSpawn(new Tile(3045, 3495), NpcId.FISHING_SPOT_1511));
    spawns.add(new NpcSpawn(new Tile(3046, 3495), NpcId.FISHING_SPOT_1511));
    spawns.add(new NpcSpawn(new Tile(3041, 3494), NpcId.FISHING_SPOT_1536));
    spawns.add(new NpcSpawn(new Tile(3042, 3494), NpcId.FISHING_SPOT_1536));
    spawns.add(new NpcSpawn(new Tile(3043, 3494), NpcId.FISHING_SPOT_1536));
    spawns.add(new NpcSpawn(new Tile(3044, 3494), NpcId.FISHING_SPOT_4712));
    spawns.add(new NpcSpawn(new Tile(3045, 3494), NpcId.FISHING_SPOT_4712));
    spawns.add(new NpcSpawn(new Tile(3046, 3494), NpcId.FISHING_SPOT_4712));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3020, 3481), NpcId.AFK_GUARDIAN_16080));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3041, 3507), NpcId.BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3041, 3508), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3041, 3510), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3041, 3511), NpcId.BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3024, 3503), NpcId.BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3024, 3504), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3024, 3502), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3065, 3498), NpcId.STEVE_6799));
    spawns.add(new NpcSpawn(new Tile(3061, 3514), NpcId.ZAFF));

    return spawns;
  }
}
