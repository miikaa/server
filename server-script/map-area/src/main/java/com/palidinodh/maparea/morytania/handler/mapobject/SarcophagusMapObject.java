package com.palidinodh.maparea.morytania.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.barrows.BarrowsPlugin;
import com.palidinodh.playerplugin.barrows.BrotherType;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PCollection;
import java.util.Map;

@ReferenceId({
  ObjectId.SARCOPHAGUS_20720,
  ObjectId.SARCOPHAGUS_20721,
  ObjectId.SARCOPHAGUS_20722,
  ObjectId.SARCOPHAGUS_20770,
  ObjectId.SARCOPHAGUS_20771,
  ObjectId.SARCOPHAGUS_20772
})
public class SarcophagusMapObject implements MapObjectHandler {

  private static final Map<Integer, BrotherType> BROTHERS =
      PCollection.toMap(
          ObjectId.SARCOPHAGUS_20720, BrotherType.DHAROK,
          ObjectId.SARCOPHAGUS_20721, BrotherType.TORAG,
          ObjectId.SARCOPHAGUS_20722, BrotherType.GUTHAN,
          ObjectId.SARCOPHAGUS_20770, BrotherType.AHRIM,
          ObjectId.SARCOPHAGUS_20771, BrotherType.KARIL,
          ObjectId.SARCOPHAGUS_20772, BrotherType.VERAC);

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    var brother = BROTHERS.get(mapObject.getId());
    if (brother == null) {
      return;
    }
    player.getPlugin(BarrowsPlugin.class).searchSarcophagus(brother);
  }

  @Override
  public void itemOnMapObject(Player player, Item item, MapObject mapObject) {
    if (item.getId() != ItemId.BARROWS_AMULET_60038) {
      return;
    }
    var brother = BROTHERS.get(mapObject.getId());
    if (brother == null) {
      return;
    }
    player.getPlugin(BarrowsPlugin.class).useAmulet(brother);
  }
}
