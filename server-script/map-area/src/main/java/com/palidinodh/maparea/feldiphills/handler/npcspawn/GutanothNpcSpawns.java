package com.palidinodh.maparea.feldiphills.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class GutanothNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2618, 3070), NpcId.DWARF_10));
    spawns.add(new NpcSpawn(4, new Tile(2617, 3066), NpcId.DWARF_10));
    spawns.add(new NpcSpawn(4, new Tile(2612, 3068), NpcId.DWARF_10));
    spawns.add(new NpcSpawn(4, new Tile(2590, 3067), NpcId.UNICORN_15));
    spawns.add(new NpcSpawn(4, new Tile(2576, 3063), NpcId.UNICORN_15));
    spawns.add(new NpcSpawn(4, new Tile(2569, 3049), NpcId.OGRE_53_2095));

    return spawns;
  }
}
