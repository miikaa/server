package com.palidinodh.maparea.fremennikprovince.rellekka;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({10810, 10811, 10812})
public class RellekkaHunterArea extends Area {}
