package com.palidinodh.maparea.fremennikprovince.icestrykewyrmcave;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(5516)
public class IceStrykewyrmCaveArea extends Area {}
