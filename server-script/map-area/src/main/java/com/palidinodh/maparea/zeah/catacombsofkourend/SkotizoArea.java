package com.palidinodh.maparea.zeah.catacombsofkourend;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(9048)
public class SkotizoArea extends Area {

  @Override
  public boolean inMultiCombat() {
    return true;
  }
}
