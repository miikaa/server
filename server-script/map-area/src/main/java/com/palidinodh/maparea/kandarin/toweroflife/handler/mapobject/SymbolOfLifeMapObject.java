package com.palidinodh.maparea.kandarin.toweroflife.handler.mapobject;

import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;

@ReferenceId(ObjectId.SYMBOL_OF_LIFE)
class SymbolOfLifeMapObject implements MapObjectHandler {

  private static final Map<Tile, CreatureCreation> CREATURES =
      Map.of(
          new Tile(3034, 4361),
          new CreatureCreation(
              new NpcSpawn(new Tile(3031, 4360), NpcId.SWORDCHICK_46),
              Arrays.asList(new Item(ItemId.RAW_CHICKEN), new Item(ItemId.RAW_SWORDFISH))));

  private static CreatureCreation getCreature(Tile tile) {
    for (var entry : CREATURES.entrySet()) {
      if (!tile.matchesTile(entry.getKey())) {
        continue;
      }
      return entry.getValue();
    }
    return null;
  }

  private static void spawn(Player player, CreatureCreation creature) {
    if (creature == null) {
      return;
    }
    if (!player.getInventory().hasItems(creature.getItems())) {
      player
          .getGameEncoder()
          .sendMessage("You need the following items: " + creature.getItemNames() + ".");
      return;
    }
    player.getInventory().deleteItems(creature.getItems());
    var npc = player.getController().addNpc(creature.getNpcSpawn());
    npc.getCombat().setTarget(player);
  }

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    switch (option.getText()) {
      case "activate":
        spawn(player, getCreature(mapObject));
        break;
    }
  }

  @Override
  public void itemOnMapObject(Player player, Item item, MapObject mapObject) {
    spawn(player, getCreature(mapObject));
  }

  @AllArgsConstructor
  @Getter
  private static class CreatureCreation {

    private NpcSpawn npcSpawn;
    private List<Item> items;

    public String getItemNames() {
      StringBuilder names = new StringBuilder();
      for (var i = 0; i < items.size(); i++) {
        names.append(ItemDefinition.getName(items.get(i).getId()));
        if (items.size() == 1 || i + 1 == items.size()) {
          break;
        }
        names.append(i + 2 < items.size() ? ", " : " and ");
      }
      return names.toString();
    }
  }
}
