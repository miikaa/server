package com.palidinodh.maparea.kandarin.ardougne.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class WestArdougneNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2458, 3309), NpcId.DARK_MAGE_7752));
    spawns.add(new NpcSpawn(4, new Tile(2559, 3303), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(new Tile(2559, 3266), NpcId.OMART));
    spawns.add(new NpcSpawn(new Tile(2556, 3266), NpcId.KILRON));
    spawns.add(new NpcSpawn(4, new Tile(2533, 3298), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2531, 3296), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2538, 3303), NpcId.JETHICK));
    spawns.add(new NpcSpawn(4, new Tile(2541, 3311), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2523, 3299), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2546, 3306), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2523, 3309), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2537, 3324), NpcId.WOMAN_3));
    spawns.add(new NpcSpawn(4, new Tile(2534, 3320), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2530, 3330), NpcId.TED_REHNISON));
    spawns.add(new NpcSpawn(4, new Tile(2532, 3330), NpcId.MARTHA_REHNISON));
    spawns.add(new NpcSpawn(4, new Tile(2531, 3333), NpcId.BILLY_REHNISON));
    spawns.add(new NpcSpawn(4, new Tile(2530, 3332, 1), NpcId.MILLI_REHNISON));
    spawns.add(new NpcSpawn(4, new Tile(2538, 3289), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2533, 3292), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2533, 3273), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2539, 3273), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2550, 3291), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2541, 3278), NpcId.WOMAN_3));
    spawns.add(new NpcSpawn(4, new Tile(2548, 3277), NpcId.WOMAN_3));
    spawns.add(new NpcSpawn(4, new Tile(2517, 3274), NpcId.NURSE_SARAH));
    spawns.add(new NpcSpawn(4, new Tile(2545, 3271), NpcId.CHILD));
    spawns.add(new NpcSpawn(4, new Tile(2546, 3277), NpcId.CHILD));
    spawns.add(new NpcSpawn(4, new Tile(2542, 3285), NpcId.HEAD_MOURNER));
    spawns.add(new NpcSpawn(4, new Tile(2529, 3317), NpcId.CLERK));
    spawns.add(new NpcSpawn(4, new Tile(2537, 3313), NpcId.BRAVEK));
    spawns.add(new NpcSpawn(4, new Tile(2529, 3287), NpcId.PRIEST));
    spawns.add(new NpcSpawn(4, new Tile(2477, 3313), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2470, 3304), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2470, 3290), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2481, 3290), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2486, 3302), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2492, 3327), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2480, 3323), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2470, 3331), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2554, 3317), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2553, 3320), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2543, 3329), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2546, 3331), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2554, 3331), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2548, 3328), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2552, 3326), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2549, 3323), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2553, 3322), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2544, 3326), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2549, 3327, 1), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2550, 3323, 1), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2471, 3289), NpcId.RAT));
    spawns.add(new NpcSpawn(4, new Tile(2467, 3300), NpcId.RAT));
    spawns.add(new NpcSpawn(4, new Tile(2469, 3307), NpcId.RAT));
    spawns.add(new NpcSpawn(4, new Tile(2471, 3315), NpcId.RAT));
    spawns.add(new NpcSpawn(4, new Tile(2471, 3321), NpcId.RAT));
    spawns.add(new NpcSpawn(4, new Tile(2482, 3327), NpcId.RAT));
    spawns.add(new NpcSpawn(4, new Tile(2488, 3321), NpcId.RAT));
    spawns.add(new NpcSpawn(4, new Tile(2492, 3318), NpcId.RAT));
    spawns.add(new NpcSpawn(4, new Tile(2506, 3323), NpcId.RAT));
    spawns.add(new NpcSpawn(4, new Tile(2511, 3316), NpcId.RAT));
    spawns.add(new NpcSpawn(4, new Tile(2507, 3306), NpcId.RAT));
    spawns.add(new NpcSpawn(4, new Tile(2521, 3290), NpcId.RAT));
    spawns.add(new NpcSpawn(4, new Tile(2523, 3280), NpcId.RAT));
    spawns.add(new NpcSpawn(4, new Tile(2533, 3280), NpcId.RAT));
    spawns.add(new NpcSpawn(4, new Tile(2543, 3280), NpcId.RAT));
    spawns.add(new NpcSpawn(4, new Tile(2549, 3272), NpcId.RAT));
    spawns.add(new NpcSpawn(4, new Tile(2536, 3273), NpcId.RAT));
    spawns.add(new NpcSpawn(4, new Tile(2521, 3273), NpcId.RAT));
    spawns.add(new NpcSpawn(4, new Tile(2535, 3321), NpcId.RAT));
    spawns.add(new NpcSpawn(4, new Tile(2525, 3321), NpcId.RAT));
    spawns.add(new NpcSpawn(4, new Tile(2504, 3319), NpcId.CIVILIAN));
    spawns.add(new NpcSpawn(4, new Tile(2508, 3308), NpcId.CIVILIAN));
    spawns.add(new NpcSpawn(4, new Tile(2480, 3307), NpcId.CIVILIAN));
    spawns.add(new NpcSpawn(4, new Tile(2509, 3289), NpcId.GHOST_19));
    spawns.add(new NpcSpawn(4, new Tile(2506, 3297), NpcId.ZOMBIE_13));
    spawns.add(new NpcSpawn(4, new Tile(2500, 3291), NpcId.ZOMBIE_13));
    spawns.add(new NpcSpawn(4, new Tile(2500, 3283), NpcId.GHOST_19));
    spawns.add(new NpcSpawn(4, new Tile(2498, 3287), NpcId.ZOMBIE_13));
    spawns.add(new NpcSpawn(4, new Tile(2509, 3285), NpcId.GHOST_19));
    spawns.add(new NpcSpawn(4, new Tile(2556, 3302), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2549, 3305), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2548, 3313), NpcId.MOURNER_11));
    spawns.add(new NpcSpawn(4, new Tile(2437, 3318), NpcId.KOFTIK));

    return spawns;
  }
}
