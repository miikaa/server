package com.palidinodh.maparea.dragonkinislands.fossilisland.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.STAIRS_30681, ObjectId.STAIRS_30682})
public class StairsMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    switch (mapObject.getId()) {
      case ObjectId.STAIRS_30681:
        player.getMovement().ladderUpTeleport(new Tile(3757, 3869, 1));
        break;
      case ObjectId.STAIRS_30682:
        player.getMovement().ladderDownTeleport(new Tile(3753, 3869));
        break;
    }
  }
}
