package com.palidinodh.maparea.asgarnia.donator.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Smithing;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.FURNACE_16469)
public class FurnaceMapObject implements MapObjectHandler {

  private int[] ORE = {
    ItemId.COPPER_ORE,
    ItemId.TIN_ORE,
    ItemId.IRON_ORE,
    ItemId.SILVER_ORE,
    ItemId.GOLD_ORE,
    ItemId.MITHRIL_ORE,
    ItemId.ADAMANTITE_ORE,
    ItemId.RUNITE_ORE,
    ItemId.COAL,
    ItemId.BLURITE_ORE,
  };

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (player.getInventory().hasItem(ORE)) {
      Smithing.openSmelt(player);
    }
  }

  @Override
  public void itemOnMapObject(Player player, Item item, MapObject mapObject) {
    switch (item.getId()) {
      case ItemId.COPPER_ORE:
      case ItemId.TIN_ORE:
      case ItemId.IRON_ORE:
      case ItemId.SILVER_ORE:
      case ItemId.GOLD_ORE:
      case ItemId.MITHRIL_ORE:
      case ItemId.ADAMANTITE_ORE:
      case ItemId.RUNITE_ORE:
      case ItemId.COAL:
      case ItemId.BLURITE_ORE:
        Smithing.openSmelt(player);
        break;
    }
  }
}
