package com.palidinodh.maparea.misthalin;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(12598)
public class GrandExchangeArea extends Area {}
