package com.palidinodh.maparea.karamja.tzhaar.handler.mapobject;

import com.palidinodh.cache.clientscript2.ScreenSelectionCs2;
import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.LargeOptions2Dialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.tzhaar.TzhaarPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.CAVE_EXIT_30283)
class TheInfernoExitMapObject implements MapObjectHandler {

  private static void spawnDialogue(Player player) {
    var plugin = player.getPlugin(TzhaarPlugin.class);
    player.openDialogue(
        new LargeOptions2Dialogue(
            new DialogueOption(
                "Unpause the minigame.",
                (c, s) -> {
                  plugin.getWavesMinigame().setPaused(false);
                  plugin.infernoWaveTeleport();
                  player.getGameEncoder().sendMessage("The Inferno has been unpaused.");
                }),
            new DialogueOption(
                "Get items.",
                (c, s) -> {
                  if (player.isLocked()) {
                    return;
                  }
                  player.openShop("spawn");
                }),
            new DialogueOption(
                "Set skill levels.",
                (c, s) -> {
                  skillsDialogue(player);
                }),
            new DialogueOption(
                "Load loadout.",
                (c, s) -> {
                  loadLoadout(player);
                }),
            new DialogueOption(
                "Save loadout.",
                (c, s) -> {
                  saveLoadout(player);
                }),
            new DialogueOption(
                "Exit the minigame.",
                (c, s) -> {
                  plugin.endInferno();
                })));
  }

  private static void skillsDialogue(Player player) {
    var cs2 = new ScreenSelectionCs2();
    player.openDialogue(
        new LargeOptions2Dialogue(
            ScreenSelectionCs2.builder().title("Choose a Skill"),
            new DialogueOption(
                "Attack",
                (c, s) -> {
                  setLevel(player, Skills.ATTACK);
                }),
            new DialogueOption(
                "Strength",
                (c, s) -> {
                  setLevel(player, Skills.STRENGTH);
                }),
            new DialogueOption(
                "Ranged",
                (c, s) -> {
                  setLevel(player, Skills.RANGED);
                }),
            new DialogueOption(
                "Magic",
                (c, s) -> {
                  setLevel(player, Skills.MAGIC);
                }),
            new DialogueOption(
                "Defence",
                (c, s) -> {
                  setLevel(player, Skills.DEFENCE);
                }),
            new DialogueOption(
                "Hitpoints",
                (c, s) -> {
                  setLevel(player, Skills.HITPOINTS);
                }),
            new DialogueOption(
                "Prayer",
                (c, s) -> {
                  setLevel(player, Skills.PRAYER);
                })));
  }

  private static void loadLoadout(Player player) {
    var plugin = player.getPlugin(TzhaarPlugin.class);
    var minigame = plugin.getWavesMinigame();
    var inferno = plugin.getInferno();
    if (!player.getController().is("TzhaarInfernoController")) {
      return;
    }
    if (inferno.getWave() == 0) {
      return;
    }
    if (!minigame.isPractice()) {
      return;
    }
    if (!minigame.isItemSpawn()) {
      return;
    }
    if (inferno.getStatsLoadout() != null) {
      minigame.setStats(inferno.getStatsLoadout().clone());
      for (var i = 0; i < minigame.getStats().length; i++) {
        player.getSkills().setLevel(i, minigame.getStats()[i]);
        player.getGameEncoder().sendSkillLevel(i);
      }
      player.getCombat().setMaxHitpoints(minigame.getStats()[Skills.HITPOINTS]);
      player.getCombat().setHitpoints(player.getCombat().getMaxHitpoints());
    }
    player.getSkills().setCombatLevel();
    if (inferno.getInventoryLoadout() != null) {
      player.getInventory().setItems(Item.copy(inferno.getInventoryLoadout()));
    }
    if (inferno.getEquipmentLoadout() != null) {
      player.getEquipment().setItems(Item.copy(inferno.getEquipmentLoadout()));
    }
    player.getEquipment().weaponUpdate(true);
  }

  private static void saveLoadout(Player player) {
    var plugin = player.getPlugin(TzhaarPlugin.class);
    var minigame = plugin.getWavesMinigame();
    var inferno = plugin.getInferno();
    if (minigame.getStats() != null) {
      inferno.setStatsLoadout(minigame.getStats().clone());
    }
    inferno.setInventoryLoadout(Item.copy(player.getInventory().getItems()));
    inferno.setEquipmentLoadout(Item.copy(player.getEquipment().getItems()));
  }

  private static void setLevel(Player player, int index) {
    player
        .getGameEncoder()
        .sendEnterAmount(
            "Choose a Level",
            level -> {
              skillsDialogue(player);
              if (level < 1 || level > 99) {
                return;
              }
              var minigame = player.getPlugin(TzhaarPlugin.class).getWavesMinigame();
              if (minigame.getStats() == null) {
                return;
              }
              minigame.getStats()[index] = level;
              player.getSkills().setLevel(index, level);
              player.getGameEncoder().sendSkillLevel(index);
              player.getCombat().setMaxHitpoints(minigame.getStats()[Skills.HITPOINTS]);
              player.getCombat().setHitpoints(player.getCombat().getMaxHitpoints());
              player.getSkills().setCombatLevel();
            });
  }

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    var plugin = player.getPlugin(TzhaarPlugin.class);
    if (plugin.getInferno().getWave() == 0) {
      return;
    }
    if (!player.getController().is("TzhaarInfernoController")) {
      return;
    }
    if (plugin.getWavesMinigame().isPaused()) {
      if (plugin.getWavesMinigame().isItemSpawn()) {
        spawnDialogue(player);
      } else {
        plugin.getWavesMinigame().setPaused(false);
        player.getGameEncoder().sendMessage("The Inferno has been unpaused.");
      }
      return;
    }
    plugin.endInferno();
  }
}
