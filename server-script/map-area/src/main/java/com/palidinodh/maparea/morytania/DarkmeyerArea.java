package com.palidinodh.maparea.morytania;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.reference.ReferenceIdSet;

@ReferenceId(14388)
@ReferenceIdSet(
    primary = 14387,
    secondary = {22, 23, 38, 39, 54, 55})
@ReferenceIdSet(
    primary = 14389,
    secondary = {16, 32, 48, 64, 80, 96})
@ReferenceIdSet(
    primary = 14644,
    secondary = {
      0, 1, 2, 3, 4, 5, 6, 7, 16, 17, 18, 19, 20, 21, 22, 23, 32, 33, 34, 35, 36, 37, 38
    })
public class DarkmeyerArea extends Area {}
