package com.palidinodh.maparea.kandarin.yanille.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.agility.AgilityObstacle;
import com.palidinodh.playerplugin.agility.MoveAgilityEvent;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.BALANCING_LEDGE_23548)
class BalancingLedgeMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    Tile tile = null;
    if (mapObject.getY() == 9519) {
      tile = new Tile(2580, 9512);
    } else if (mapObject.getY() == 9513) {
      tile = new Tile(2580, 9520);
    }
    if (tile == null) {
      return;
    }
    var obstacle = new AgilityObstacle(player, mapObject, 40, 0);
    obstacle.add(MoveAgilityEvent.builder().tile(tile).noclip(true).animation(756).build());
    obstacle.start(mapObject);
  }
}
