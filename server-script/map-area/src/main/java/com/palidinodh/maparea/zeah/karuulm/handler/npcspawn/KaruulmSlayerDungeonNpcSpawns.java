package com.palidinodh.maparea.zeah.karuulm.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class KaruulmSlayerDungeonNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(1264, 10197), NpcId.WYRM_99));
    spawns.add(new NpcSpawn(4, new Tile(1260, 10192), NpcId.WYRM_99));
    spawns.add(new NpcSpawn(4, new Tile(1262, 10186), NpcId.WYRM_99));
    spawns.add(new NpcSpawn(4, new Tile(1266, 10181), NpcId.WYRM_99));
    spawns.add(new NpcSpawn(4, new Tile(1272, 10178), NpcId.WYRM_99));
    spawns.add(new NpcSpawn(4, new Tile(1277, 10183), NpcId.WYRM_99));
    spawns.add(new NpcSpawn(4, new Tile(1283, 10189), NpcId.WYRM_99));
    spawns.add(new NpcSpawn(4, new Tile(1271, 10186), NpcId.WYRM_99));
    spawns.add(new NpcSpawn(4, new Tile(1269, 10193), NpcId.WYRM_99));
    spawns.add(new NpcSpawn(4, new Tile(1276, 10192), NpcId.WYRM_99));
    spawns.add(new NpcSpawn(4, new Tile(1257, 10152), NpcId.WYRM_99));
    spawns.add(new NpcSpawn(4, new Tile(1265, 10152), NpcId.WYRM_99));
    spawns.add(new NpcSpawn(4, new Tile(1253, 10158), NpcId.WYRM_99));
    spawns.add(new NpcSpawn(4, new Tile(1272, 10158), NpcId.WYRM_99));
    spawns.add(new NpcSpawn(4, new Tile(1261, 10159), NpcId.WYRM_99));
    spawns.add(new NpcSpawn(4, new Tile(1311, 10231), NpcId.HYDRA_194));
    spawns.add(new NpcSpawn(4, new Tile(1305, 10232), NpcId.HYDRA_194));
    spawns.add(new NpcSpawn(4, new Tile(1301, 10237), NpcId.HYDRA_194));
    spawns.add(new NpcSpawn(4, new Tile(1315, 10236), NpcId.HYDRA_194));
    spawns.add(new NpcSpawn(4, new Tile(1322, 10239), NpcId.HYDRA_194));
    spawns.add(new NpcSpawn(4, new Tile(1317, 10243), NpcId.HYDRA_194));
    spawns.add(new NpcSpawn(4, new Tile(1312, 10247), NpcId.HYDRA_194));
    spawns.add(new NpcSpawn(4, new Tile(1306, 10247), NpcId.HYDRA_194));
    spawns.add(new NpcSpawn(4, new Tile(1303, 10242), NpcId.HYDRA_194));
    spawns.add(new NpcSpawn(4, new Tile(1304, 10260), NpcId.HYDRA_194));
    spawns.add(new NpcSpawn(4, new Tile(1311, 10260), NpcId.HYDRA_194));
    spawns.add(new NpcSpawn(4, new Tile(1304, 10267), NpcId.HYDRA_194));
    spawns.add(new NpcSpawn(4, new Tile(1310, 10266), NpcId.HYDRA_194));
    spawns.add(new NpcSpawn(4, new Tile(1310, 10273), NpcId.HYDRA_194));
    spawns.add(new NpcSpawn(4, new Tile(1324, 10262), NpcId.HYDRA_194));
    spawns.add(new NpcSpawn(4, new Tile(1328, 10260), NpcId.HYDRA_194));
    spawns.add(new NpcSpawn(4, new Tile(1330, 10267), NpcId.HYDRA_194));
    spawns.add(new NpcSpawn(4, new Tile(1324, 10268), NpcId.HYDRA_194));
    spawns.add(new NpcSpawn(4, new Tile(1307, 10230, 1), NpcId.DRAKE_192));
    spawns.add(new NpcSpawn(4, new Tile(1317, 10233, 1), NpcId.DRAKE_192));
    spawns.add(new NpcSpawn(4, new Tile(1300, 10239, 1), NpcId.DRAKE_192));
    spawns.add(new NpcSpawn(4, new Tile(1309, 10239, 1), NpcId.DRAKE_192));
    spawns.add(new NpcSpawn(4, new Tile(1320, 10244, 1), NpcId.DRAKE_192));
    spawns.add(new NpcSpawn(4, new Tile(1308, 10250, 1), NpcId.DRAKE_192));
    spawns.add(new NpcSpawn(4, new Tile(1342, 10230, 1), NpcId.DRAKE_192));
    spawns.add(new NpcSpawn(4, new Tile(1356, 10238, 1), NpcId.DRAKE_192));
    spawns.add(new NpcSpawn(4, new Tile(1344, 10246, 1), NpcId.DRAKE_192));
    spawns.add(new NpcSpawn(4, new Tile(1345, 10238, 1), NpcId.DRAKE_192));

    return spawns;
  }
}
