package com.palidinodh.maparea.kandarin.treegnomestronghold.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class StrongholdSlayerCaveNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2408, 9801), NpcId.HELLHOUND_122));
    spawns.add(new NpcSpawn(4, new Tile(2410, 9795), NpcId.HELLHOUND_122));
    spawns.add(new NpcSpawn(4, new Tile(2417, 9796), NpcId.HELLHOUND_122));
    spawns.add(new NpcSpawn(4, new Tile(2417, 9803), NpcId.HELLHOUND_122));
    spawns.add(new NpcSpawn(4, new Tile(2413, 9802), NpcId.HELLHOUND_122));
    spawns.add(new NpcSpawn(4, new Tile(2414, 9797), NpcId.HELLHOUND_122));
    spawns.add(new NpcSpawn(4, new Tile(2409, 9786), NpcId.HELLHOUND_122));
    spawns.add(new NpcSpawn(4, new Tile(2413, 9784), NpcId.HELLHOUND_122));
    spawns.add(new NpcSpawn(4, new Tile(2415, 9788), NpcId.HELLHOUND_122));
    spawns.add(new NpcSpawn(4, new Tile(2425, 9782), NpcId.HELLHOUND_122));
    spawns.add(new NpcSpawn(4, new Tile(2429, 9786), NpcId.HELLHOUND_122));
    spawns.add(new NpcSpawn(4, new Tile(2433, 9784), NpcId.HELLHOUND_122));
    spawns.add(new NpcSpawn(4, new Tile(2430, 9776), NpcId.HELLHOUND_122));
    spawns.add(new NpcSpawn(4, new Tile(2426, 9773), NpcId.HELLHOUND_122));
    spawns.add(new NpcSpawn(4, new Tile(2429, 9770), NpcId.HELLHOUND_122));
    spawns.add(new NpcSpawn(4, new Tile(2433, 9772), NpcId.HELLHOUND_122));
    spawns.add(new NpcSpawn(4, new Tile(2402, 9784), NpcId.FIRE_GIANT_86));
    spawns.add(new NpcSpawn(4, new Tile(2401, 9780), NpcId.FIRE_GIANT_86));
    spawns.add(new NpcSpawn(4, new Tile(2399, 9777), NpcId.FIRE_GIANT_86));
    spawns.add(new NpcSpawn(4, new Tile(2394, 9786), NpcId.FIRE_GIANT_86));
    spawns.add(new NpcSpawn(4, new Tile(2391, 9783), NpcId.FIRE_GIANT_86));
    spawns.add(new NpcSpawn(4, new Tile(2393, 9779), NpcId.FIRE_GIANT_86));
    spawns.add(new NpcSpawn(4, new Tile(2395, 9771), NpcId.FIRE_GIANT_86));
    spawns.add(new NpcSpawn(4, new Tile(2400, 9770), NpcId.FIRE_GIANT_86));
    spawns.add(new NpcSpawn(4, new Tile(2405, 9770), NpcId.FIRE_GIANT_86));
    spawns.add(new NpcSpawn(4, new Tile(2410, 9777), NpcId.FIRE_GIANT_86));
    spawns.add(new NpcSpawn(4, new Tile(2412, 9774), NpcId.FIRE_GIANT_86));
    spawns.add(new NpcSpawn(4, new Tile(2415, 9771), NpcId.FIRE_GIANT_86));
    spawns.add(new NpcSpawn(4, new Tile(2444, 9785), NpcId.ABERRANT_SPECTRE_96));
    spawns.add(new NpcSpawn(4, new Tile(2443, 9781), NpcId.ABERRANT_SPECTRE_96));
    spawns.add(new NpcSpawn(4, new Tile(2445, 9777), NpcId.ABERRANT_SPECTRE_96));
    spawns.add(new NpcSpawn(4, new Tile(2441, 9776), NpcId.ABERRANT_SPECTRE_96));
    spawns.add(new NpcSpawn(4, new Tile(2457, 9775), NpcId.ABERRANT_SPECTRE_96));
    spawns.add(new NpcSpawn(4, new Tile(2461, 9777), NpcId.ABERRANT_SPECTRE_96));
    spawns.add(new NpcSpawn(4, new Tile(2457, 9780), NpcId.ABERRANT_SPECTRE_96));
    spawns.add(new NpcSpawn(4, new Tile(2452, 9794), NpcId.ABERRANT_SPECTRE_96));
    spawns.add(new NpcSpawn(4, new Tile(2460, 9792), NpcId.ABERRANT_SPECTRE_96));
    spawns.add(new NpcSpawn(4, new Tile(2458, 9789), NpcId.ABERRANT_SPECTRE_96));
    spawns.add(new NpcSpawn(4, new Tile(2454, 9790), NpcId.ABERRANT_SPECTRE_96));
    spawns.add(new NpcSpawn(4, new Tile(2471, 9776), NpcId.ABERRANT_SPECTRE_96));
    spawns.add(new NpcSpawn(4, new Tile(2469, 9779), NpcId.ABERRANT_SPECTRE_96));
    spawns.add(new NpcSpawn(4, new Tile(2470, 9783), NpcId.ABERRANT_SPECTRE_96));
    spawns.add(new NpcSpawn(4, new Tile(2472, 9796), NpcId.ANKOU_75));
    spawns.add(new NpcSpawn(4, new Tile(2475, 9798), NpcId.ANKOU_75));
    spawns.add(new NpcSpawn(4, new Tile(2479, 9798), NpcId.ANKOU_75));
    spawns.add(new NpcSpawn(4, new Tile(2482, 9800), NpcId.ANKOU_75));
    spawns.add(new NpcSpawn(4, new Tile(2477, 9804), NpcId.ANKOU_75));
    spawns.add(new NpcSpawn(4, new Tile(2475, 9807), NpcId.ANKOU_75));
    spawns.add(new NpcSpawn(4, new Tile(2472, 9805), NpcId.ANKOU_75));
    spawns.add(new NpcSpawn(4, new Tile(2470, 9802), NpcId.ANKOU_75));
    spawns.add(new NpcSpawn(4, new Tile(2470, 9798), NpcId.ANKOU_75));
    spawns.add(new NpcSpawn(4, new Tile(2477, 9801), NpcId.ANKOU_75));
    spawns.add(new NpcSpawn(4, new Tile(2489, 9817), NpcId.BLOODVELD_76));
    spawns.add(new NpcSpawn(4, new Tile(2484, 9819), NpcId.BLOODVELD_76));
    spawns.add(new NpcSpawn(4, new Tile(2485, 9825), NpcId.BLOODVELD_76));
    spawns.add(new NpcSpawn(4, new Tile(2492, 9825), NpcId.BLOODVELD_76));
    spawns.add(new NpcSpawn(4, new Tile(2489, 9829), NpcId.BLOODVELD_76));
    spawns.add(new NpcSpawn(4, new Tile(2473, 9829), NpcId.BLOODVELD_76));
    spawns.add(new NpcSpawn(4, new Tile(2470, 9834), NpcId.BLOODVELD_76));
    spawns.add(new NpcSpawn(4, new Tile(2466, 9829), NpcId.BLOODVELD_76));
    spawns.add(new NpcSpawn(4, new Tile(2463, 9825), NpcId.BLOODVELD_76));
    spawns.add(new NpcSpawn(4, new Tile(2464, 9834), NpcId.BLOODVELD_76));
    spawns.add(new NpcSpawn(4, new Tile(2452, 9814), NpcId.BLOODVELD_76));
    spawns.add(new NpcSpawn(4, new Tile(2454, 9819), NpcId.BLOODVELD_76));
    spawns.add(new NpcSpawn(4, new Tile(2446, 9819), NpcId.BLOODVELD_76));
    spawns.add(new NpcSpawn(4, new Tile(2449, 9823), NpcId.BLOODVELD_76));
    spawns.add(new NpcSpawn(4, new Tile(2439, 9818), NpcId.BLOODVELD_76));
    spawns.add(new NpcSpawn(4, new Tile(2434, 9815), NpcId.BLOODVELD_76));
    spawns.add(new NpcSpawn(4, new Tile(2435, 9822), NpcId.BLOODVELD_76));
    spawns.add(new NpcSpawn(4, new Tile(2440, 9823), NpcId.BLOODVELD_76));
    spawns.add(new NpcSpawn(4, new Tile(2439, 9831), NpcId.BLOODVELD_76));
    spawns.add(new NpcSpawn(4, new Tile(2444, 9832), NpcId.BLOODVELD_76));

    return spawns;
  }
}
