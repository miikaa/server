package com.palidinodh.maparea.wilderness.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.wilderness.WildernessPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.BARRIER_39652, ObjectId.BARRIER_39653})
class FeroxBarrierMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (player.getX() != mapObject.getX() && player.getY() != mapObject.getY()) {
      return;
    }
    if (player.getMovement().getTeleportBlock() > 0) {
      player
          .getGameEncoder()
          .sendMessage(
              "A teleport block has been cast on you. It should wear off in "
                  + player.getMovement().getTeleportBlockRemaining()
                  + ".");
      return;
    }
    if (player.getController().isMagicBound()) {
      player
          .getGameEncoder()
          .sendMessage(
              "A magical force stops you from moving for "
                  + player.getMovement().getMagicBindSeconds()
                  + " more seconds.");
      return;
    }
    if (player.getPlugin(WildernessPlugin.class).hasBloodyKey()) {
      player.getGameEncoder().sendMessage("You can't use this right now.");
      return;
    }
    var onBarrier = player.withinDistance(mapObject, 0);
    Tile tile;
    switch (mapObject.getDirection()) {
      case 0:
        tile = new Tile(mapObject.getX() + (onBarrier ? -1 : 0), mapObject.getY());
        break;
      case 1:
        tile = new Tile(mapObject.getX(), mapObject.getY() + (onBarrier ? 1 : 0));
        break;
      case 2:
        tile = new Tile(mapObject.getX() + (onBarrier ? 1 : 0), mapObject.getY());
        break;
      case 3:
        tile = new Tile(mapObject.getX(), mapObject.getY() + (onBarrier ? -1 : 0));
        break;
      default:
        return;
    }
    player.getMovement().clear();
    player.getMovement().addMovement(tile);
  }
}
