package com.palidinodh.maparea.zeah.darkaltar.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.ROCKS_34741)
public class RocksMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (player.getY() <= 3873) {
      player.getMovement().teleport(1761, 3874);
    } else {
      player.getMovement().teleport(1761, 3872);
    }
  }
}
