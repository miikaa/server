package com.palidinodh.maparea.kandarin.ardougne.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.Diary;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.DARK_MAGE_7752)
class DarkMageNpc implements NpcHandler {

  private static void upgradeIbansStaff(Player player) {
    if (!player.getInventory().hasItem(ItemId.IBANS_STAFF)) {
      player.getGameEncoder().sendMessage("You need an Iban's staff to do this.");
      return;
    }
    if (player.getInventory().getCount(ItemId.COINS) < 200_000) {
      player.getGameEncoder().sendMessage("You need 200,000 coins to do this.");
      return;
    }
    player.getInventory().deleteItem(ItemId.IBANS_STAFF);
    player.getInventory().deleteItem(ItemId.COINS, 200_000);
    player.getInventory().addItem(ItemId.IBANS_STAFF_U);
    Diary.getDiaries(player)
        .forEach(d -> d.makeItem(player, -1, new Item(ItemId.IBANS_STAFF_U), null, null));
  }

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    player.getGameEncoder().sendMessage("Miles will note items for you.");
    player.openDialogue(
        new OptionsDialogue(
            new DialogueOption(
                "Upgrade Iban's staff for 200K.",
                (c, s) -> {
                  upgradeIbansStaff(player);
                }),
            new DialogueOption("Nevermind.")));
  }
}
