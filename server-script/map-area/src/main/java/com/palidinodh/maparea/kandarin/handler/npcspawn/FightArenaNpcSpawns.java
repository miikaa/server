package com.palidinodh.maparea.kandarin.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class FightArenaNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2610, 3192), NpcId.KHAZARD_GUARD_23_1210));
    spawns.add(new NpcSpawn(4, new Tile(2600, 3194), NpcId.KHAZARD_GUARD_23_1211));
    spawns.add(new NpcSpawn(4, new Tile(2589, 3193), NpcId.KHAZARD_GUARD_23_1211));
    spawns.add(new NpcSpawn(4, new Tile(2590, 3182), NpcId.KHAZARD_GUARD_23_1211));
    spawns.add(new NpcSpawn(4, new Tile(2591, 3185), NpcId.KHAZARD_GUARD_23_1211));
    spawns.add(new NpcSpawn(4, new Tile(2595, 3186), NpcId.KHAZARD_GUARD_23_1211));
    spawns.add(new NpcSpawn(4, new Tile(2598, 3182), NpcId.KHAZARD_GUARD_23_1211));
    spawns.add(new NpcSpawn(4, new Tile(2607, 3182), NpcId.KHAZARD_GUARD_23_1211));
    spawns.add(new NpcSpawn(4, new Tile(2607, 3185), NpcId.KHAZARD_GUARD_23_1211));
    spawns.add(new NpcSpawn(4, new Tile(2611, 3182), NpcId.KHAZARD_GUARD_23_1211));
    spawns.add(new NpcSpawn(4, new Tile(2613, 3186), NpcId.KHAZARD_GUARD_23_1211));
    spawns.add(new NpcSpawn(4, new Tile(2598, 3185), NpcId.KHAZARD_GUARD_23_1211));
    spawns.add(new NpcSpawn(4, new Tile(2595, 3182), NpcId.KHAZARD_GUARD_23_1211));
    spawns.add(new NpcSpawn(4, new Tile(2591, 3177), NpcId.KHAZARD_GUARD_23_1211));
    spawns.add(new NpcSpawn(4, new Tile(2597, 3177), NpcId.KHAZARD_GUARD_23_1211));
    spawns.add(new NpcSpawn(4, new Tile(2606, 3175), NpcId.KHAZARD_GUARD_23_1211));
    spawns.add(new NpcSpawn(4, new Tile(2580, 3174), NpcId.KHAZARD_GUARD_23_1211));
    spawns.add(new NpcSpawn(4, new Tile(2582, 3171), NpcId.LOCAL));
    spawns.add(new NpcSpawn(4, new Tile(2580, 3169), NpcId.KHAZARD_GUARD_23_1211));
    spawns.add(new NpcSpawn(4, new Tile(2575, 3169), NpcId.KHAZARD_GUARD_23_1211));
    spawns.add(new NpcSpawn(4, new Tile(2580, 3151), NpcId.KHAZARD_GUARD_23_1211));
    spawns.add(new NpcSpawn(4, new Tile(2567, 3148), NpcId.KHAZARD_GUARD_23_1211));
    spawns.add(new NpcSpawn(4, new Tile(2568, 3144), NpcId.KHAZARD_GUARD_23_1211));
    spawns.add(new NpcSpawn(4, new Tile(2565, 3143), NpcId.KHAZARD_GUARD_23_1211));
    spawns.add(new NpcSpawn(4, new Tile(2567, 3140), NpcId.KHAZARD_BARMAN));
    spawns.add(new NpcSpawn(4, new Tile(2583, 3141), NpcId.KHAZARD_GUARD_23));
    spawns.add(new NpcSpawn(4, new Tile(2587, 3140), NpcId.KHAZARD_GUARD_23));
    spawns.add(new NpcSpawn(4, new Tile(2596, 3140), NpcId.KHAZARD_GUARD_23));
    spawns.add(new NpcSpawn(4, new Tile(2587, 3143), NpcId.KELVIN));
    spawns.add(new NpcSpawn(5, new Tile(2590, 3143), NpcId.JOE));
    spawns.add(new NpcSpawn(4, new Tile(2594, 3143), NpcId.FIGHTSLAVE));
    spawns.add(new NpcSpawn(4, new Tile(2598, 3143), NpcId.HENGRAD));
    spawns.add(new NpcSpawn(4, new Tile(2603, 3154), NpcId.SAMMY_SERVIL_1221));
    spawns.add(new NpcSpawn(4, new Tile(2605, 3154), NpcId.GENERAL_KHAZARD_112));
    spawns.add(new NpcSpawn(4, new Tile(2607, 3150), NpcId.KHAZARD_GUARD_23_1211));
    spawns.add(new NpcSpawn(4, new Tile(2610, 3148), NpcId.KHAZARD_GUARD_23_1211));
    spawns.add(new NpcSpawn(4, new Tile(2617, 3140), NpcId.KHAZARD_GUARD_23_1209));
    spawns.add(new NpcSpawn(4, new Tile(2616, 3150), NpcId.KHAZARD_GUARD_23));
    spawns.add(new NpcSpawn(4, new Tile(2618, 3157), NpcId.KHAZARD_GUARD_23));
    spawns.add(new NpcSpawn(4, new Tile(2618, 3166), NpcId.KHAZARD_GUARD_23));
    spawns.add(new NpcSpawn(4, new Tile(2615, 3160), NpcId.FIGHTSLAVE));
    spawns.add(new NpcSpawn(4, new Tile(2615, 3164), NpcId.FIGHTSLAVE));
    spawns.add(new NpcSpawn(4, new Tile(2615, 3168), NpcId.SAMMY_SERVIL));
    spawns.add(new NpcSpawn(4, new Tile(2616, 3174), NpcId.KHAZARD_GUARD_23));

    return spawns;
  }
}
