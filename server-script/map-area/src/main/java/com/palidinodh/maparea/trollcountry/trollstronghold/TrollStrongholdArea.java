package com.palidinodh.maparea.trollcountry.trollstronghold;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(11321)
public class TrollStrongholdArea extends Area {}
