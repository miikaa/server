package com.palidinodh.maparea.kandarin.fishingguild;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(10293)
public class FishingGuildArea extends Area {}
