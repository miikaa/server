package com.palidinodh.maparea.morytania;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({14385, 14386, 14387})
public class MeiyerditchArea extends Area {}
