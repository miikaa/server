package com.palidinodh.maparea.misthalin.edgeville.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.SHERLOCK)
public class SherlockNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    if (player.getGameMode().isIronType()) {
      player.getGameEncoder().sendMessage("Sherlock doesn't have anything for your game mode.");
      return;
    }
    player.openShop("clues");
  }
}
