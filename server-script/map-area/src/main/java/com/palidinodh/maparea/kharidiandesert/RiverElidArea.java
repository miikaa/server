package com.palidinodh.maparea.kharidiandesert;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({12844, 13100, 13101, 13357, 13359, 13360})
public class RiverElidArea extends Area {}
