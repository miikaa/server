package com.palidinodh.maparea.asgarnia.donator.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.prayer.PrayerPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.ALTAR_OF_THE_OCCULT_65002)
public class OccultAltarMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    player.getPlugin(PrayerPlugin.class).altarOfTheOccultOption(option);
  }

  @Override
  public void itemOnMapObject(Player player, Item item, MapObject mapObject) {
    player.getPlugin(PrayerPlugin.class).altarOfTheOccultItem(item, mapObject);
  }
}
