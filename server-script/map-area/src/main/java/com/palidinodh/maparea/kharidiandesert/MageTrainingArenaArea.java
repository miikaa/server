package com.palidinodh.maparea.kharidiandesert;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(13363)
public class MageTrainingArenaArea extends Area {}
