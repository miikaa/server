package com.palidinodh.maparea.feldiphills.hunterarea.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.CAVE_19037)
public class CaveMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    switch (player.getHeight()) {
      case 12:
        player.getMovement().ladderUpTeleport(new Tile(3063, 3496));
        break;
      case 16:
        player.getMovement().ladderUpTeleport(new Tile(3024, 3498));
        break;
    }
  }
}
