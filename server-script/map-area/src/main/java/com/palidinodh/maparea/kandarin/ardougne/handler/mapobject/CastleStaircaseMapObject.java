package com.palidinodh.maparea.kandarin.ardougne.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.STAIRCASE_15645, ObjectId.STAIRCASE_15648})
class CastleStaircaseMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    switch (mapObject.getId()) {
      case ObjectId.STAIRCASE_15645:
        player.getMovement().ladderUpTeleport(new Tile(2572, 3294, 1));
        break;
      case ObjectId.STAIRCASE_15648:
        player.getMovement().ladderDownTeleport(new Tile(2572, 3298));
        break;
    }
  }
}
