package com.palidinodh.maparea.morytania.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.maparea.morytania.VerSinhazaArea;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.THEATRE_OF_BLOOD_32653)
public class TheatreOfBloodMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    player.openOptionsDialogue(
        new DialogueOption(
            "Enter Public",
            (c, s) -> {
              VerSinhazaArea.joinPublic(player);
            }),
        new DialogueOption(
            "Enter Private",
            (c, s) -> {
              VerSinhazaArea.joinPrivate(player);
            }));
  }
}
