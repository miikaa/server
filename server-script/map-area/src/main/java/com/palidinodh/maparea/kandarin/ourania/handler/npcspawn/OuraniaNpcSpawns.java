package com.palidinodh.maparea.kandarin.ourania.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class OuraniaNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2448, 3230), NpcId.RED_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(2448, 3227), NpcId.RED_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(2449, 3224), NpcId.RED_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(2452, 3221), NpcId.RED_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(2456, 3220), NpcId.RED_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(2470, 3241), NpcId.RED_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(2473, 3240), NpcId.RED_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(2477, 3237), NpcId.RED_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(2475, 3238), NpcId.RED_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(2469, 3244), NpcId.RED_SALAMANDER));
    spawns.add(new NpcSpawn(4, new Tile(2479, 3200), NpcId.BLACK_BEAR_19));
    spawns.add(new NpcSpawn(4, new Tile(2462, 3223), NpcId.GHOST_24));
    spawns.add(new NpcSpawn(4, new Tile(2466, 3227), NpcId.GHOST_24));
    spawns.add(new NpcSpawn(4, new Tile(2470, 3227), NpcId.GHOST_24));
    spawns.add(new NpcSpawn(4, new Tile(2471, 3224), NpcId.GHOST_24));
    spawns.add(new NpcSpawn(4, new Tile(2471, 3221), NpcId.GHOST_24));
    spawns.add(new NpcSpawn(4, new Tile(2464, 3220), NpcId.GHOST_24));
    spawns.add(new NpcSpawn(4, new Tile(2478, 3244), NpcId.MONK_OF_ZAMORAK_17));
    spawns.add(new NpcSpawn(4, new Tile(2477, 3239), NpcId.MONK_OF_ZAMORAK_17));
    spawns.add(new NpcSpawn(4, new Tile(2479, 3236), NpcId.MONK_OF_ZAMORAK_17));
    spawns.add(new NpcSpawn(4, new Tile(2474, 3245), NpcId.MONK_OF_ZAMORAK_17));
    spawns.add(new NpcSpawn(4, new Tile(2471, 3247), NpcId.MONK_OF_ZAMORAK_17));
    spawns.add(new NpcSpawn(4, new Tile(2480, 3241), NpcId.MONK_OF_ZAMORAK_17));

    return spawns;
  }
}
