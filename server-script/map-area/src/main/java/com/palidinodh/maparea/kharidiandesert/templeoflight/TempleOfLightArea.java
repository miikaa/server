package com.palidinodh.maparea.kharidiandesert.templeoflight;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({7496, 7752, 8008})
public class TempleOfLightArea extends Area {}
