package com.palidinodh.maparea.wilderness.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.maparea.wilderness.bossescapecaves.BossEscapeCavesArea;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.wilderness.WildernessPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.CAVE_47175)
public class EscapeCaveEntranceMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    var plugin = player.getPlugin(WildernessPlugin.class);
    if (plugin.getPayedBossFee() > 0) {
      BossEscapeCavesArea.enter(player, mapObject);
    } else {
      if (plugin.isAutoPayBossFee()) {
        BossEscapeCavesArea.payFeeAndEnter(player, mapObject);
      } else {
        BossEscapeCavesArea.infoDialogue(player, mapObject);
      }
    }
  }
}
