package com.palidinodh.maparea.wilderness.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.clanwars.ClanWarsStages;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.CHALLENGE_PORTAL)
public class ChallengePortalMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    player.openDialogue(
        new OptionsDialogue(
            new DialogueOption(
                "Join your clan's battle.",
                (c, s) -> {
                  ClanWarsStages.openJoin(player);
                }),
            new DialogueOption(
                "Observe your clan's battle.",
                (c, s) -> {
                  player
                      .getGameEncoder()
                      .sendEnterString(
                          "Enter username:",
                          ie -> {
                            ClanWarsStages.openView(
                                player, player.getMessaging().getClanChatUsername());
                          });
                }),
            new DialogueOption(
                "Observe a clan's battle.",
                (c, s) -> {
                  player
                      .getGameEncoder()
                      .sendEnterString(
                          "Enter username:",
                          ie -> {
                            ClanWarsStages.openView(player, ie);
                          });
                }),
            new DialogueOption("Cancel.")));
  }
}
