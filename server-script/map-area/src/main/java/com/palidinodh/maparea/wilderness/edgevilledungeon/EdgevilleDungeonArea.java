package com.palidinodh.maparea.wilderness.edgevilledungeon;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({12443, 12444})
public class EdgevilleDungeonArea extends Area {

  @Override
  public boolean inWilderness() {
    return true;
  }
}
