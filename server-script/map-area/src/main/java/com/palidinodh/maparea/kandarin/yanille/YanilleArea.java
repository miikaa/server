package com.palidinodh.maparea.kandarin.yanille;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({10032, 10288, 10544})
public class YanilleArea extends Area {}
