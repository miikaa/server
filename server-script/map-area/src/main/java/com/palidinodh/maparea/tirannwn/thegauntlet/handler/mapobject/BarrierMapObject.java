package com.palidinodh.maparea.tirannwn.thegauntlet.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.agility.AgilityObstacle;
import com.palidinodh.playerplugin.agility.MoveAgilityEvent;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.BARRIER_37339, ObjectId.BARRIER_37337})
class BarrierMapObject implements MapObjectHandler {

  private static void pass(Player player) {
    if (player.getY() <= 9378) {
      var obstacle = new AgilityObstacle(player);
      obstacle.add(
          MoveAgilityEvent.builder().tile(new Tile(player.getX(), 9380)).noclip(true).build());
      obstacle.start();
      var npc =
          player
              .getController()
              .getNpc(
                  NpcId.CRYSTALLINE_HUNLLEF_674,
                  NpcId.CRYSTALLINE_HUNLLEF_674_9022,
                  NpcId.CRYSTALLINE_HUNLLEF_674_9023,
                  NpcId.CORRUPTED_HUNLLEF_894,
                  NpcId.CORRUPTED_HUNLLEF_894_9036,
                  NpcId.CORRUPTED_HUNLLEF_894_9037);
      if (npc != null) {
        npc.pluginScript("reset_combat_total_time");
      }
    } else {
      player.rejuvenate();
      player.getController().stop();
    }
  }

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    switch (option.getText()) {
      case "pass":
        {
          player.openOptionsDialogue(
              new DialogueOption(
                  "Pass barrier.",
                  (c, s) -> {
                    pass(player);
                  }),
              new DialogueOption("Nevermind."));
          break;
        }
      case "quick-pass":
        pass(player);
        break;
    }
  }
}
