package com.palidinodh.maparea.dragonkinislands.lithkren;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(6223)
public class LithkrenVaultArea extends Area {}
