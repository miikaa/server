package com.palidinodh.maparea.kandarin.toweroflife;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({10546, 12100})
public class TowerOfLifeArea extends Area {}
