package com.palidinodh.incomingpacket.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.INVENTORY)
class InventoryWidget implements WidgetHandler {

  @Override
  public boolean rotateWidget(
      Player player,
      int useWidgetId,
      int useChildId,
      int onWidgetId,
      int onChildId,
      int useSlot,
      int useItemId,
      int onSlot,
      int onItemId) {
    if (useWidgetId == WidgetId.INVENTORY && onWidgetId == WidgetId.INVENTORY) {
      player.getInventory().rotateItems(useSlot, onSlot);
      return true;
    }
    return false;
  }
}
