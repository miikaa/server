package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Magic;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.teleports.MainTeleportType;
import com.palidinodh.playerplugin.teleports.TeleportsPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.ROYAL_SEED_POD)
class RoyalSeedPodItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (!player.getController().canTeleport(30, true)) {
      return;
    }
    Tile tile;
    if (player.getClientHeight() == 0 && player.getArea().inWilderness()) {
      tile = new Tile(MainTeleportType.EDGEVILLE.getTile());
      tile.setHeight(player.getHeight());
    } else {
      tile = player.getPlugin(TeleportsPlugin.class).getMainTeleports().getHomeTeleport().getTile();
    }
    player
        .getMovement()
        .animatedTeleport(
            tile,
            Magic.SEEDPOD_ANIMATION_START,
            Magic.SEEDPOD_ANIMATION_MID,
            Magic.SEEDPOD_ANIMATION_END,
            Magic.SEEDPOD_START_GRAPHIC,
            null,
            Magic.SEEDPOD_END_GRAPHIC,
            0,
            2);
    player.getController().stopWithTeleport();
    player.getCombat().clearHitEvents();
  }
}
