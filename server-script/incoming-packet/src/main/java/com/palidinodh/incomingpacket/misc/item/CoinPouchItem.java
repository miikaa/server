package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.COIN_POUCH_22522)
class CoinPouchItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var quantity = 0;
    switch (option.getText()) {
      case "open-all":
        quantity = item.getAmount();
        break;
      case "open":
        quantity = 1;
        break;
    }
    item.remove(quantity);
    for (var i = 0; i < quantity; i++) {
      player.getInventory().addOrDropItem(ItemId.COINS, PRandom.randomI(25_000, 100_000));
    }
  }
}
