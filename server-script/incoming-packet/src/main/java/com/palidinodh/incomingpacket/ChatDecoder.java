package com.palidinodh.incomingpacket;

import com.palidinodh.cache.store.util.Stream;
import com.palidinodh.osrscore.io.cache.CacheManager;
import com.palidinodh.osrscore.io.incomingpacket.InStreamKey;
import com.palidinodh.osrscore.io.incomingpacket.IncomingPacketDecoder;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.adaptive.Clan;
import com.palidinodh.rs.adaptive.RsClanRank;

class ChatDecoder {

  static class PublicChatDecoder extends IncomingPacketDecoder {

    @Override
    public boolean execute(Player player, Stream stream) {
      var effects = getInt(InStreamKey.EFFECTS);
      var secondaryEffect = getInt(InStreamKey.SECONDARY_EFFECT);
      var length = getInt(InStreamKey.LENGTH);
      var message = CacheManager.getHuffman().readEncryptedMessage(stream, length);
      player.getMessaging().setMessage(message, effects, secondaryEffect);
      if (secondaryEffect != 1) {
        player.clearIdleTime();
      }
      return true;
    }
  }

  static class PublicPrivateTradeDecoder extends IncomingPacketDecoder {

    @Override
    public boolean execute(Player player, Stream stream) {
      var publicChat = getInt(InStreamKey.PUBLIC);
      var privateChat = getInt(InStreamKey.PRIVATE);
      var trade = getInt(InStreamKey.TRADE);
      player.getMessaging().setPublicChatStatus(publicChat);
      player.getMessaging().setPrivateChatStatus(privateChat);
      return true;
    }
  }

  static class AddFriendDecoder extends IncomingPacketDecoder {

    @Override
    public boolean execute(Player player, Stream stream) {
      var username = getString(InStreamKey.STRING_INPUT);
      player.getMessaging().addFriend(username);
      return true;
    }
  }

  static class RemoveFriendDecoder extends IncomingPacketDecoder {

    @Override
    public boolean execute(Player player, Stream stream) {
      var username = getString(InStreamKey.STRING_INPUT);
      player.getMessaging().removeFriend(username);
      return true;
    }
  }

  static class MessageFriendDecoder extends IncomingPacketDecoder {

    @Override
    public boolean execute(Player player, Stream stream) {
      var username = stream.readString();
      var length = stream.readUnsignedByte();
      var message = CacheManager.getHuffman().readEncryptedMessage(stream, length);
      player.getMessaging().setPrivateMessage(username, message);
      player.clearIdleTime();
      return true;
    }
  }

  static class AddIgnoreDecoder extends IncomingPacketDecoder {

    @Override
    public boolean execute(Player player, Stream stream) {
      var username = getString(InStreamKey.STRING_INPUT);
      player.getMessaging().addIgnore(username);
      return true;
    }
  }

  static class RemoveIgnoreDecoder extends IncomingPacketDecoder {

    @Override
    public boolean execute(Player player, Stream stream) {
      var username = getString(InStreamKey.STRING_INPUT);
      player.getMessaging().removeIgnore(username);
      return true;
    }
  }

  static class JoinClanDecoder extends IncomingPacketDecoder {

    @Override
    public boolean execute(Player player, Stream stream) {
      var username = getString(InStreamKey.STRING_INPUT);
      if (username.isEmpty() || username.equals(Clan.LEAVE_CLAN)) {
        if (player.getCombat().isDead()) {
          return true;
        }
        player.getMessaging().leaveClan();
      } else {
        player.getMessaging().joinClan(username);
      }
      return true;
    }
  }

  static class ClanRankDecoder extends IncomingPacketDecoder {

    @Override
    public boolean execute(Player player, Stream stream) {
      var username = getString(InStreamKey.STRING_INPUT);
      var rankId = getInt(InStreamKey.RANK);
      var rank = RsClanRank.NOT_IN_CLAN;
      switch (rankId) {
        case 0:
          rank = RsClanRank.NOT_IN_CLAN;
          break;
        case 1:
          rank = RsClanRank.RECRUIT;
          break;
        case 2:
          rank = RsClanRank.CORPORAL;
          break;
        case 3:
          rank = RsClanRank.SERGEANT;
          break;
        case 4:
          rank = RsClanRank.LIEUTENANT;
          break;
        case 5:
          rank = RsClanRank.CAPTAIN;
          break;
        case 6:
          rank = RsClanRank.GENERAL;
          break;
      }
      player.getMessaging().setFriendClanRank(username, rank);
      return true;
    }
  }

  static class KickClanUserDecoder extends IncomingPacketDecoder {

    @Override
    public boolean execute(Player player, Stream stream) {
      var username = getString(InStreamKey.STRING_INPUT);
      player.getMessaging().kickClanUser(username);
      return true;
    }
  }

  static class UsernameListDecoder extends IncomingPacketDecoder {

    @Override
    public boolean execute(Player player, Stream stream) {
      getString(InStreamKey.STRING_INPUT);
      return true;
    }
  }
}
