package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.Arrays;
import java.util.List;

@ReferenceId(ItemId.EASTER_BASKET)
class EasterBasketItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    List<Tile> tiles;
    tiles = Arrays.asList(new Tile(player).randomize(2), new Tile(player).randomize(2));
    for (var tile : tiles) {
      player
          .getController()
          .sendMapProjectile(null, new Tile(player), tile, 1282, 50, 0, 10, 50, 100, 0);
    }
    player.setAnimation(2876);
  }
}
