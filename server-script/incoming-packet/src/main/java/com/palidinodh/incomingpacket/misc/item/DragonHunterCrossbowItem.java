package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.DRAGON_HUNTER_CROSSBOW,
  ItemId.DRAGON_HUNTER_CROSSBOW_T,
  ItemId.DRAGON_HUNTER_CROSSBOW_B
})
class DragonHunterCrossbowItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (item.getId()) {
      case ItemId.DRAGON_HUNTER_CROSSBOW_T:
        {
          switch (option.getText()) {
            case "dismantle":
              item.replace(new Item(ItemId.DRAGON_HUNTER_CROSSBOW));
              player.getInventory().addOrDropItem(ItemId.VORKATHS_HEAD_21907);
              break;
          }
          break;
        }
      case ItemId.DRAGON_HUNTER_CROSSBOW_B:
        {
          switch (option.getText()) {
            case "dismantle":
              item.replace(new Item(ItemId.DRAGON_HUNTER_CROSSBOW));
              player.getInventory().addOrDropItem(ItemId.KBD_HEADS);
              break;
          }
          break;
        }
    }
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    if (ItemHandler.used(
        useItem, onItem, ItemId.VORKATHS_HEAD_21907, ItemId.DRAGON_HUNTER_CROSSBOW)) {
      onItem.replace(new Item(ItemId.DRAGON_HUNTER_CROSSBOW_T));
      useItem.remove();
      return true;
    } else if (ItemHandler.used(useItem, onItem, ItemId.KBD_HEADS, ItemId.DRAGON_HUNTER_CROSSBOW)) {
      onItem.replace(new Item(ItemId.DRAGON_HUNTER_CROSSBOW_B));
      useItem.remove();
      return true;
    }
    return false;
  }
}
