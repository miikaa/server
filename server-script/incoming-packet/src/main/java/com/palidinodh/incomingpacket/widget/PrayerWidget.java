package com.palidinodh.incomingpacket.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.PrayerChild;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.playerplugin.skill.SkillPlugin;
import com.palidinodh.playerplugin.skill.SkillRestore;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({WidgetId.PRAYER, WidgetId.QUICK_PRAYER})
class PrayerWidget implements WidgetHandler {

  private static void checkRapidHeal(Player player) {
    var hitpoints = player.getPlugin(SkillPlugin.class).getRestore(Skills.HITPOINTS);
    if (hitpoints.getDelay() >= SkillRestore.DELAY / 2) {
      return;
    }
    hitpoints.setDelay(SkillRestore.DELAY / 2);
  }

  @Override
  public boolean isLockedUsable() {
    return true;
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    if (widgetId == WidgetId.PRAYER) {
      player.getPrayer().activate(childId);
      if (PrayerChild.get(childId) == PrayerChild.RAPID_HEAL) {
        checkRapidHeal(player);
      }
    } else if (widgetId == WidgetId.QUICK_PRAYER) {
      switch (childId) {
        case 4:
          int prayerOffset = 5;
          if (slot <= 23 || slot >= 27) {
            player.getPrayer().activate(slot + prayerOffset);
          } else if (slot == 24) {
            player.getPrayer().activate(slot + prayerOffset + 2);
          } else if (slot == 25 || slot == 26) {
            player.getPrayer().activate(slot + prayerOffset - 1);
          }
          break;
        case 5:
          player.getPrayer().switchQuick();
          if (player.getPrayer().hasActive(PrayerChild.RAPID_HEAL)) {
            checkRapidHeal(player);
          }
          break;
      }
    }
  }
}
