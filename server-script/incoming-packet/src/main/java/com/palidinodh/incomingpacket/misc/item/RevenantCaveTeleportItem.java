package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.REVENANT_CAVE_TELEPORT)
class RevenantCaveTeleportItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (!player.getController().canTeleport(true)) {
      return;
    }
    item.remove(1);
    var height = 0;
    if (player.getArea().inMainland() && player.getClientHeight() == 0) {
      height = player.getHeight();
    }
    player.getMovement().animatedTeleport(new Tile(3128, 3832, height), 3864, new Graphic(1039), 2);
    player.getController().stopWithTeleport();
    player.getCombat().clearHitEvents();
  }
}
