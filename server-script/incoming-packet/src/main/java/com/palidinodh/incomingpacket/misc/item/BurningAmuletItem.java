package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.magic.SpellTeleport;
import com.palidinodh.rs.reference.ReferenceId;
import lombok.AllArgsConstructor;
import lombok.Getter;

@ReferenceId({
  ItemId.BURNING_AMULET_1,
  ItemId.BURNING_AMULET_2,
  ItemId.BURNING_AMULET_3,
  ItemId.BURNING_AMULET_4,
  ItemId.BURNING_AMULET_5
})
class BurningAmuletItem implements ItemHandler {

  private static void teleport(Player player, Location location) {
    player.openDialogue(
        new OptionsDialogue(
            "Are you sure you want to teleport into the wilderness?",
            new DialogueOption(
                "Yes, teleport me into the wilderness!",
                (c, s) -> {
                  if (!player.getController().canTeleport(true)) {
                    return;
                  }
                  SpellTeleport.normalTeleport(player, location.getTile());
                  player.getController().stopWithTeleport();
                }),
            new DialogueOption("No!")));
  }

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "rub":
        {
          player.openDialogue(
              new OptionsDialogue(
                  new DialogueOption(
                      "Chaos Temple",
                      (c, s) -> {
                        teleport(player, Location.CHAOS_TEMPLE);
                      }),
                  new DialogueOption(
                      "Bandit Camp",
                      (c, s) -> {
                        teleport(player, Location.BANDIT_CAMP);
                      }),
                  new DialogueOption(
                      "Lava Maze",
                      (c, s) -> {
                        teleport(player, Location.LAVA_MAZE);
                      })));
        }
        break;
      case "chaos temple":
        teleport(player, Location.CHAOS_TEMPLE);
        break;
      case "bandit camp":
        teleport(player, Location.BANDIT_CAMP);
        break;
      case "lava maze":
        teleport(player, Location.LAVA_MAZE);
        break;
    }
  }

  @AllArgsConstructor
  @Getter
  private enum Location {
    CHAOS_TEMPLE(new Tile(3233, 3637)),
    BANDIT_CAMP(new Tile(3039, 3651)),
    LAVA_MAZE(new Tile(3027, 3839));

    private final Tile tile;
  }
}
