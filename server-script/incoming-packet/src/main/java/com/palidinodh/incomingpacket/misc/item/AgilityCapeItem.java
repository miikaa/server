package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.PMovement;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.agility.AgilityPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.AGILITY_CAPE, ItemId.AGILITY_CAPE_T})
class AgilityCapeItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var agilityPlugin = player.getPlugin(AgilityPlugin.class);
    if (!agilityPlugin.canUseCapeStaminaBoost()) {
      player.getGameEncoder().sendMessage("You can't use this again today.");
      return;
    }
    agilityPlugin.updateCapeStaminaBoostDate();
    player.getMovement().setEnergy(PMovement.MAX_ENERGY);
    player.getSkills().setStaminaTime(100);
  }
}
