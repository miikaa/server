package com.palidinodh.incomingpacket;

import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.store.util.Stream;
import com.palidinodh.osrscore.io.incomingpacket.InStreamKey;
import com.palidinodh.osrscore.io.incomingpacket.IncomingPacketDecoder;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapItem;
import com.palidinodh.osrscore.model.map.route.ProjectileRoute;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playermisc.PickupItem;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PLogger;

class MapItemOptionDecoder extends IncomingPacketDecoder {

  @Override
  public boolean execute(Player player, Stream stream) {
    var option = getInt(InStreamKey.PACKET_OPTION);
    var itemId = getInt(InStreamKey.ITEM_ID);
    var tileX = getInt(InStreamKey.TILE_X);
    var tileY = getInt(InStreamKey.TILE_Y);
    var ctrlRun = getInt(InStreamKey.CTRL_RUN);
    player.clearIdleTime();
    var message =
        "[MapItemOption("
            + option
            + ")] itemId="
            + itemId
            + "/"
            + ItemId.valueOf(itemId)
            + "; tileX="
            + tileX
            + "; tileY="
            + tileY
            + "; ctrlRun="
            + ctrlRun;
    if (Settings.getInstance().isLocal()) {
      PLogger.println(message);
    }
    if (player.getOptions().isDebug()) {
      player.getGameEncoder().sendMessage(message);
    }
    if (player.isLocked()) {
      return false;
    }
    if (!player.getMovement().isTeleportStateNone()) {
      return false;
    }
    if (player.getMovement().isViewing()) {
      return false;
    }
    if (player.getDialogue() != null && player.getDialogue().isDisableActions()) {
      return false;
    }
    player.getMovement().fullRoute(tileX, tileY, ctrlRun);
    return true;
  }

  @Override
  public boolean complete(Player player) {
    var option = getInt(InStreamKey.PACKET_OPTION);
    var itemId = getInt(InStreamKey.ITEM_ID);
    var tileX = getInt(InStreamKey.TILE_X);
    var tileY = getInt(InStreamKey.TILE_Y);
    var reach = 0;
    var t = new Tile(tileX, tileY, player.getHeight());
    if (!player.getMovement().isRouting()
        && player.withinDistance(t, 1)
        && ProjectileRoute.INSTANCE.allow(player, t)) {
      reach = 1;
    }
    if (player.isLocked()) {
      return false;
    }
    if (!player.getMovement().isTeleportStateNone()) {
      return false;
    }
    if (!player.withinDistance(t, reach)) {
      return false;
    }
    if (reach == 1 && !player.withinDistance(t, 0)) {
      player.setAnimation(827);
    }
    player.getMovement().clear();
    var definitionOption = ItemDefinition.getDefinition(itemId).getMapOption(option);
    if (definitionOption == null) {
      return true;
    }
    switch (definitionOption.getText()) {
      case "take":
        PickupItem.pickup(player, itemId, tileX, tileY);
        break;
      default:
        {
          var worldEvent =
              player
                  .getWorld()
                  .getWorldEvents()
                  .getIf(e -> e.getItemHandler(player, itemId) != null);
          var itemHandler = worldEvent == null ? null : worldEvent.getItemHandler(player, itemId);
          if (itemHandler == null) {
            itemHandler = PlayerPlugin.getItemHandler(itemId);
          }
          if (itemHandler != null && definitionOption != null) {
            itemHandler.mapItemOption(
                player,
                definitionOption,
                new MapItem(
                    player.getController(),
                    new Item(itemId),
                    new Tile(tileX, tileY, player.getHeight())));
          }
          break;
        }
    }
    return true;
  }
}
