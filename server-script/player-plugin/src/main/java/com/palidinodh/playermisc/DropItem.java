package com.palidinodh.playermisc;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.ItemList;
import com.palidinodh.osrscore.model.map.MapItem;
import com.palidinodh.playerplugin.bountyhunter.MysteriousEmblem;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.lootkey.LootKeyPlugin;
import com.palidinodh.playerplugin.wilderness.WildernessPlugin;
import com.palidinodh.rs.communication.event.PlayerLogEvent;
import com.palidinodh.rs.setting.UserRank;
import com.palidinodh.worldevent.wildernesskey.WildernessKeyEvent;

public class DropItem {

  public static void drop(Player player, int slot) {
    var item = player.getInventory().getItem(slot);
    if (item == null) {
      return;
    }
    var itemId = item.getId();
    player.getWidgetManager().removeInteractiveWidgets();
    if (FamiliarPlugin.isPetItem(itemId)) {
      player.getPlugin(FamiliarPlugin.class).summonByItem(itemId);
      return;
    }
    if (MysteriousEmblem.isEmblem(itemId) && player.getArea().inWilderness()) {
      player.getGameEncoder().sendMessage("You can't drop this here.");
      return;
    }
    if (blockWildernessUntradeableDrop(player, item)) {
      player.getGameEncoder().sendMessage("You can't drop this right now.");
      return;
    }
    if (item.getDef().hasOption("destroy")) {
      player.openDialogue(
          new OptionsDialogue(
              "Are you sure you want to destroy this item?",
              new DialogueOption(
                  "Yes, destroy " + item.getName() + ".",
                  (c, s) -> {
                    if (item.getId() == ItemId.LOOT_KEY
                        && (item.getAttachment() instanceof ItemList)) {
                      var lootKeyItemList = (ItemList) item.getAttachment();
                      var lootKeyPlugin = player.getPlugin(LootKeyPlugin.class);
                      for (var keyItem : lootKeyItemList.getItems()) {
                        lootKeyPlugin.incrimentKeysContainedValue(keyItem);
                        lootKeyPlugin.incrimentKeysDestroyedValue(keyItem);
                      }
                      lootKeyItemList.clear();
                    }
                    item.remove();
                  }),
              new DialogueOption("Nevermind.")));
      return;
    }
    if (!handleDropAction(player, item)) {
      player.getController().addMapItem(item, player, player);
    }
    player.log(PlayerLogEvent.LogType.MAP_ITEM, "dropped " + item.getLogName());
    player.getInventory().deleteItem(itemId, item.getAmount(), slot);
    player.getController().sendMapSound(2739);
  }

  private static boolean handleDropAction(Player player, Item item) {
    var itemId = item.getId();
    if (WildernessPlugin.isActiveBloodyKey(item.getId())) {
      var keyDropTile = player;
      var appearTime = MapItem.ALWAYS_APPEAR;
      var damagedByPlayer = player.getCombat().getPlayerFromHitCount(false);
      if (player.withinVisibilityDistance(damagedByPlayer)) {
        keyDropTile = damagedByPlayer;
        appearTime = MapItem.NORMAL_TIME - 20;
      }
      player
          .getWorld()
          .getWorldEvent(WildernessKeyEvent.class)
          .addMapItem(itemId, keyDropTile, MapItem.NORMAL_TIME, appearTime);
      return true;
    }
    if (item.getId() == ItemId.DARK_ESSENCE_FRAGMENTS) {
      player.getCharges().decreaseDarkEssenceCharges(player.getCharges().getDarkEssenceFragments());
      return true;
    }
    if (player.isUsergroup(UserRank.YOUTUBER)) {
      player.getController().addMapItem(item, player, MapItem.NORMAL_TIME, MapItem.NEVER_APPEAR);
      return true;
    }
    if (player.getController().isInstanced()) {
      var appearTime =
          item.getInfoDef().getUntradable() ? MapItem.NEVER_APPEAR : MapItem.NORMAL_APPEAR;
      player.getController().addMapItem(item, player, MapItem.LONG_TIME, appearTime);
      return true;
    }
    if (player.getArea().inWilderness() && !item.getInfoDef().getUntradable()) {
      if (player.getController().isFood(itemId) || player.getController().isDrink(itemId)) {
        player.getController().addMapItem(item, player, MapItem.NORMAL_TIME, MapItem.NEVER_APPEAR);
      } else {
        var appearTime =
            item.getInfoDef().getUntradable() ? MapItem.NEVER_APPEAR : MapItem.ALWAYS_APPEAR;
        player.getController().addMapItem(item, player, MapItem.NORMAL_TIME, appearTime);
      }
      return true;
    }
    return false;
  }

  private static boolean blockWildernessUntradeableDrop(Player player, Item item) {
    if (!player.getArea().inWilderness()) {
      return false;
    }
    if (!item.getInfoDef().getUntradable()) {
      return false;
    }
    if (item.getId() == ItemId.LOOTING_BAG) {
      return true;
    }
    if (item.getId() == ItemId.LOOTING_BAG_22586) {
      return true;
    }
    if (item.getId() == ItemId.LOOT_KEY) {
      if (!(item.getAttachment() instanceof ItemList)) {
        return false;
      }
      var itemList = (ItemList) item.getAttachment();
      return itemList.getValue() > 30_000;
    }
    return WildernessPlugin.isBloodyKey(item.getId());
  }
}
