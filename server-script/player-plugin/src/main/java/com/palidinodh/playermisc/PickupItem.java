package com.palidinodh.playermisc;

import com.palidinodh.cache.id.VarbitId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.ItemDef;
import com.palidinodh.osrscore.model.map.MapItem;
import com.palidinodh.playerplugin.loot.LootPlugin;
import com.palidinodh.playerplugin.wilderness.WildernessPlugin;
import com.palidinodh.worldevent.wildernesskey.WildernessKeyEvent;
import java.util.ArrayList;
import java.util.List;

public class PickupItem {

  public static boolean pickup(Player player, int itemId, int tileX, int tileY) {
    var plugin = player.getPlugin(LootPlugin.class);
    plugin.refreshItems();
    if (canUseLootWidget(player, itemId, tileX, tileY)) {
      player.getWidgetManager().sendInteractiveOverlay(WidgetId.LOOT_1022);
      return false;
    }
    return complete(player, itemId, tileX, tileY);
  }

  public static boolean complete(Player player, MapItem mapItem) {
    return complete(player, mapItem.getId(), mapItem.getX(), mapItem.getY());
  }

  public static boolean complete(Player player, int itemId, int tileX, int tileY) {
    var result = player.getWorld().pickupMapItem(player, itemId, tileX, tileY);
    if (result == null) {
      return false;
    }
    if (result.partialSuccess()
        && WildernessPlugin.isBloodyKey(itemId)
        && (player.getArea().inWilderness() || player.getArea().inPvpWorld())) {
      player.getCombat().setPKSkullDelay(PCombat.SKULL_DELAY);
      player.getMovement().setEnergy(0);
      player
          .getGameEncoder()
          .sendMessage("<col=ff0000>Carrying the key prevents you from teleporting.");
      if (WildernessPlugin.isActiveBloodierKey(itemId)) {
        player.getWorld().getWorldEvent(WildernessKeyEvent.class).updateTrackedLocation(player);
        player
            .getWorld()
            .sendMessage(
                "<col=ff0000>A "
                    + ItemDef.getName(itemId)
                    + " has been picked up by "
                    + player.getUsername()
                    + " at level "
                    + player.getArea().getWildernessLevel()
                    + " wilderness!");
      }
    }
    return true;
  }

  private static boolean canUseLootWidget(Player player, int itemId, int tileX, int tileY) {
    if (player.getArea().inWilderness()) {
      return false;
    }
    var plugin = player.getPlugin(LootPlugin.class);
    if (player.getGameEncoder().getVarbit(VarbitId.IS_MOBILE) == 1) {
      if (!plugin.isMobileWidgetEnabled()) {
        return false;
      }
    } else {
      if (!plugin.isWidgetEnabled()) {
        return false;
      }
    }
    if (player.getX() != tileX) {
      return false;
    }
    if (player.getY() != tileY) {
      return false;
    }
    var items = plugin.getItems();
    var itemSize = items.size();
    if (itemSize < 2) {
      return false;
    }
    return getUniqueItemCount(items) <= 15;
  }

  private static int getUniqueItemCount(List<MapItem> items) {
    var ids = new ArrayList<Integer>();
    for (var item : items) {
      if (ids.contains(item.getId())) {
        continue;
      }
      ids.add(item.getId());
    }
    return ids.size();
  }
}
