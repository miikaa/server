package com.palidinodh.worldevent.pvptournament.state;

public interface State {

  String getMessage();

  int getTime();

  void execute();
}
