package com.palidinodh.worldevent.bondrelicdiscount.handler.command;

import com.google.inject.Inject;
import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.worldevent.bondrelicdiscount.BondRelicDiscountEvent;

@ReferenceName("bondrelicdiscount")
class BondRelicDiscountCommand implements CommandHandler, CommandHandler.ModeratorRank {

  @Inject private BondRelicDiscountEvent event;

  @Override
  public String getExample(String name) {
    return "randomdaily";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.split(message);
    switch (messages[0]) {
      case "randomdaily":
        {
          event.randomizeDailyRelic();
          player.getGameEncoder().sendMessage("Randomized daily relic discount");
          break;
        }
    }
    DiscordBot.sendCodedMessage(
        DiscordChannel.MODERATION_LOG,
        player.getUsername() + " used ::bondrelicdiscount " + message);
  }
}
