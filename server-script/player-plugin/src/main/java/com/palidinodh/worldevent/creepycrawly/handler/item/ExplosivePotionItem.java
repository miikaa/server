package com.palidinodh.worldevent.creepycrawly.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.worldevent.creepycrawly.CreepyCrawlyController;

@ReferenceId(ItemId.EXPLOSIVE_POTION_32390)
class ExplosivePotionItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var controller = player.getController().as(CreepyCrawlyController.class);
    var minigame = controller.getMinigame();
    item.remove(1);
    minigame.explosivePotion(player);
  }
}
