package com.palidinodh.worldevent.clancup;

import com.palidinodh.osrscore.model.tile.Tile;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ClanCupDisplay {
  private ClanCupTeamType team;
  private Tile tile;
}
