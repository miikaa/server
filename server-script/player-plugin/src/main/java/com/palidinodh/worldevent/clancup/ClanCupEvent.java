package com.palidinodh.worldevent.clancup;

import com.google.inject.Inject;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.osrscore.world.WorldEvent;
import java.util.EnumMap;
import java.util.Map;

public class ClanCupEvent extends WorldEvent {

  @Inject private transient World world;

  private Map<ClanCupStyleType, ClanCupTeamType> displays = new EnumMap<>(ClanCupStyleType.class);

  public ClanCupEvent() {
    super(1);
  }

  @Override
  public void load() {
    displays.forEach(
        (k, v) -> {
          var mapObject = buildDisplayMapObject(v, k);
          world.getRegion(mapObject.getRegionId(), false).addMapObject(mapObject);
          sendDisplayMapObject(mapObject);
        });
  }

  private static MapObject buildDisplayMapObject(ClanCupTeamType team, ClanCupStyleType style) {
    return new MapObject(team == null ? -1 : team.getObjectId(style), 10, 0, style.getTile());
  }

  public void changeDisplay(ClanCupTeamType team, ClanCupStyleType style) {
    if (team == null) {
      displays.remove(style);
    } else {
      displays.put(style, team);
    }
    var mapObject = buildDisplayMapObject(team, style);
    world.getRegion(mapObject.getRegionId(), false).addMapObject(mapObject);
    sendDisplayMapObject(mapObject);
  }

  private void sendDisplayMapObject(MapObject mapObject) {
    for (var player : world.getPlayers(mapObject.getRegionIds())) {
      if (player.withinMapDistance(mapObject)) {
        player.getGameEncoder().sendMapObject(mapObject);
      }
    }
  }
}
