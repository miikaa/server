package com.palidinodh.worldevent.wildernesskey.handler.command;

import com.google.inject.Inject;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.worldevent.wildernesskey.WildernessKeyEvent;

@ReferenceName("wildkey")
class WildKeyCommand implements CommandHandler, CommandHandler.ModeratorRank {

  @Inject private WildernessKeyEvent event;

  @Override
  public String getExample(String name) {
    return "on/off";
  }

  @Override
  public void execute(Player player, String name, String message) {
    event.setEnabled(message.equals("on"));
    player.getGameEncoder().sendMessage("Wild key: " + event.isEnabled());
  }
}
