package com.palidinodh.worldevent.clancup;

import com.palidinodh.osrscore.model.tile.Tile;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ClanCupStyleType {
  ALL("All", new Tile(3107, 3511)),
  TWENTY_V_TWENTY("20v20", new Tile(3107, 3513)),
  TEN_V_TEN("10v10", new Tile(3107, 3515));

  private final String name;
  private final Tile tile;
}
