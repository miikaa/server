package com.palidinodh.worldevent.pvptournament.mode;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Loadout;
import com.palidinodh.osrscore.model.entity.player.magic.SpellbookType;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.clanwars.ClanWarsRule;
import com.palidinodh.playerplugin.clanwars.ClanWarsRuleOption;
import com.palidinodh.worldevent.pvptournament.ModeMap;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

@Getter
class MiscMode implements Mode {

  private List<SubMode> subModes = new ArrayList<>();

  MiscMode() {
    var mode =
        SubMode.builder()
            .name("Void Range")
            .map(ModeMap.FIVE_BY_FIVE_ROOMS)
            .spellbook(SpellbookType.LUNAR)
            .brewCap(3);
    mode.rules(Mode.buildRules(ClanWarsRule.PRAYER, ClanWarsRuleOption.NO_OVERHEADS));
    mode.attackLevel(99)
        .strengthLevel(99)
        .rangedLevel(99)
        .magicLevel(99)
        .defenceLevel(42)
        .hitpointsLevel(99)
        .prayerLevel(99);
    mode.rune(ItemId.DEATH_RUNE).rune(ItemId.ASTRAL_RUNE).rune(ItemId.EARTH_RUNE);
    mode.loadout(
        new Loadout.Entry(
            "Load-out",
            new Item[] {
              new Item(ItemId.VOID_RANGER_HELM),
              new Item(ItemId.AVAS_ASSEMBLER),
              new Item(ItemId.NECKLACE_OF_ANGUISH),
              new Item(ItemId.DRAGON_KNIFE, 8000),
              new Item(ItemId.VOID_KNIGHT_TOP),
              new Item(ItemId.BLACK_DHIDE_SHIELD),
              null,
              new Item(ItemId.VOID_KNIGHT_ROBE),
              null,
              new Item(ItemId.VOID_KNIGHT_GLOVES),
              new Item(ItemId.RANGER_BOOTS),
              null,
              new Item(ItemId.ARCHERS_RING_I),
              null
            },
            new Item[] {
              new Item(ItemId.RANGING_POTION_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              null,
              null,
              new Item(ItemId.RUNE_POUCH)
            },
            -1));
    mode.loadout(
        new Loadout.Entry(
            "+ Heavy Ballista",
            new Item(ItemId.HEAVY_BALLISTA),
            new Item(ItemId.DRAGON_JAVELIN_P_PLUS_PLUS, 8000)));
    mode.loadout(
        new Loadout.Entry(
            "+ Dark Bow",
            new Item(ItemId.DARK_BOW),
            new Item(ItemId.DRAGON_ARROW_P_PLUS_PLUS, 8000)));
    mode.loadout(
        new Loadout.Entry(
            "+ Zaryte Crossbow",
            new Item(ItemId.ZARYTE_CROSSBOW),
            new Item(ItemId.DRAGONSTONE_DRAGON_BOLTS_E, 8000)));
    mode.loadout(new Loadout.Entry("+ Dragon Thrownaxe", new Item(ItemId.DRAGON_THROWNAXE, 8000)));
    subModes.add(mode.build());
  }
}
