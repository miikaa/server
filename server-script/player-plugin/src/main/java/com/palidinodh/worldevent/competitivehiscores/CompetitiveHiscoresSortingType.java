package com.palidinodh.worldevent.competitivehiscores;

public enum CompetitiveHiscoresSortingType {
  INDIVIDUALS,
  FRIENDS,
  CLAN_INDIVIDUALS,
  CLANS,
  SELF
}
