package com.palidinodh.worldevent.wildernesshp;

import com.google.inject.Inject;
import com.palidinodh.cache.definition.osrs.NpcDefinition;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.osrscore.world.WorldEvent;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.util.PArrayList;
import com.palidinodh.util.PTime;
import java.util.Calendar;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

public class WildernessHpEvent extends WorldEvent {

  private static final int SOON_MINUTES = 15;
  private static final int MAX_WAIT_TIME = (int) PTime.minToTick(15);
  private static final int NPC_ID = NpcId.THE_MIMIC_186_8633;
  private static final String[] TIME = {"6:00", "14:00", "20:00"};
  private static final Map<Integer, Integer> ONE_DEFENCE_TIMES =
      Map.of(
          Calendar.MONDAY, 20,
          Calendar.TUESDAY, 14,
          Calendar.WEDNESDAY, 20,
          Calendar.THURSDAY, 14,
          Calendar.FRIDAY, 20,
          Calendar.SATURDAY, 14);
  private static final int[] HOURS;
  private static final int[] MINUTES;

  static {
    if (TIME == null || TIME.length == 0) {
      HOURS = null;
      MINUTES = null;
    } else {
      HOURS = new int[TIME.length];
      MINUTES = new int[TIME.length];
      for (var i = 0; i < TIME.length; i++) {
        var data = TIME[i].split(":");
        HOURS[i] = Integer.parseInt(data[0]);
        MINUTES[i] = Integer.parseInt(data[1]);
      }
    }
  }

  @Inject private transient World world;
  @Setter @Getter private transient boolean enabled = true;
  private transient boolean announcedStartingSoon;
  private transient Spawn spawn;
  private transient Npc npc;

  public WildernessHpEvent() {
    super(4);
    rotateSpawn();
  }

  @Override
  public Object script(String name, Object... args) {
    boolean isPrimary = isPrimary();
    if (name.equals("world_event_name")) {
      return isPrimary ? "Wilderness HP" : null;
    }
    if (name.equals("world_event_tile")) {
      return isPrimary ? new Tile(spawn.getTeleportTile()).setHeight(getHeight()) : null;
    }
    if (name.equals("wilderness_hp_message")) {
      return getNextTimeText();
    }
    return null;
  }

  @Override
  public void execute() {
    setTick(4);
    if (!canRun()) {
      return;
    }
    checkNpcState();
    if (npc != null && !npc.isVisible()) {
      world.removeNpc(npc);
      npc = null;
      rotateSpawn();
    }
    var currentHour = PTime.getHour24();
    var currentMinute = PTime.getMinute();
    var dayMinute = (int) PTime.hourToMin(currentHour) + currentMinute;
    var nextTime = getNextTime();
    if (nextTime[0] == Integer.MAX_VALUE) {
      return;
    }
    var remainingMinutes = PTime.getRemainingMinutes(dayMinute, nextTime[0] * 60 + nextTime[1]);
    if (remainingMinutes == SOON_MINUTES) {
      var message =
          NpcDefinition.getName(NPC_ID)
              + " will spawn near the "
              + getLocation()
              + " in "
              + SOON_MINUTES
              + " minutes!";
      world.sendNews(message);
      DiscordBot.sendCodedMessage(DiscordChannel.EVENTS, message);
      announcedStartingSoon = true;
      setTick(105);
    } else if (remainingMinutes == 0) {
      startEvent();
      setTick(105);
    }
  }

  public boolean canRun() {
    if (!enabled) {
      return false;
    }
    if (TIME == null) {
      return false;
    }
    if (TIME.length == 0) {
      return false;
    }
    return world == null || world.isPrimary();
  }

  private void checkNpcState() {
    if (npc == null) {
      return;
    }
    if (!npc.isVisible()) {
      return;
    }
    if (npc.getTotalTicks() < MAX_WAIT_TIME) {
      return;
    }
    if (npc.getCombat().getHitpoints() != npc.getCombat().getMaxHitpoints()) {
      return;
    }
    npc.setVisible(false);
  }

  private boolean isPrimary() {
    if (!canRun()) {
      return false;
    }
    if (spawn == null) {
      return false;
    }
    return announcedStartingSoon || npc != null;
  }

  private String getNextTimeText() {
    var location = getShortLocation();
    if (npc != null && spawn != null) {
      return location;
    }
    if (!canRun()) {
      return "N/A";
    }
    int currentHour = PTime.getHour24();
    int currentMinute = PTime.getMinute();
    int dayMinute = (int) PTime.hourToMin(currentHour) + currentMinute;
    int[] nextTime = getNextTime();
    int remainingMinutes = PTime.getRemainingMinutes(dayMinute, nextTime[0] * 60 + nextTime[1]);
    return location + ": " + PTime.ticksToLongDuration(PTime.minToTick(remainingMinutes));
  }

  private void startEvent() {
    announcedStartingSoon = false;
    if (!canRun()) {
      return;
    }
    world.removeNpc(npc);
    var npcTile = new Tile(spawn.getNpcTile()).setHeight(getHeight());
    npc = world.addNpc(new NpcSpawn(npcTile, NPC_ID));
    var message = NpcDefinition.getName(NPC_ID) + " has spawned near the " + getLocation() + "!";
    world.sendBroadcast(message);
    DiscordBot.sendCodedMessage(DiscordChannel.EVENTS, message);
    // DiscordBot.sendCodedMessage(DiscordChannel.GENERAL_CHAT, message);
  }

  private int[] getNextTime() {
    if (!canRun() || npc != null) {
      return new int[] {Integer.MAX_VALUE, Integer.MAX_VALUE};
    }
    var currentHour = PTime.getHour24();
    var currentMinute = PTime.getMinute();
    for (var i = 0; i < HOURS.length; i++) {
      var hour = HOURS[i];
      var minute = MINUTES[i];
      if (currentHour > hour || currentHour == hour && currentMinute > minute) {
        continue;
      }
      return new int[] {hour, minute};
    }
    return new int[] {HOURS[0], MINUTES[0]};
  }

  private void rotateSpawn() {
    var spawns = new PArrayList<>(Spawn.values());
    spawns.remove(spawn);
    spawn = spawns.getRandom();
  }

  private String getLocation() {
    var location = spawn.getLocation();
    var height = getHeight();
    switch (height) {
      case 4:
        location += " (1 Defence)";
        break;
      case 8:
        location += "(F2P 1 Defence)";
        break;
    }
    return location;
  }

  private String getShortLocation() {
    var location = spawn.getLocation();
    var height = getHeight();
    switch (height) {
      case 4:
        location = spawn.getShortLocation() + " (1 Def)";
        break;
      case 8:
        location = spawn.getShortLocation() + "(F2P 1 Def)";
        break;
    }
    return location;
  }

  private int getHeight() {
    /*var oneDefenceTime = ONE_DEFENCE_TIMES.getOrDefault(PTime.getDayOfWeek(), -1);
    if (oneDefenceTime == PTime.getHour24() || oneDefenceTime == getNextTime()[0]) {
      return 4;
    }*/
    return 0;
  }
}
