package com.palidinodh.playerplugin.combat.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.OSMUMTENS_FANG, ItemId.OSMUMTENS_FANG_OR})
class OsmumtensFangItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "dismantle":
        {
          if (item.getInfoDef().getExchangeIds() == null) {
            break;
          }
          player.openDialogue(
              new OptionsDialogue(
                  "Are you sure you want to dismantle " + item.getName() + "?",
                  new DialogueOption(
                      "Yes, dismantle it.",
                      (c, s) -> {
                        if (item.getSlot() == -1) {
                          return;
                        }
                        if (player.getInventory().getRemainingSlots()
                            < item.getInfoDef().getExchangeIds().length - 1) {
                          player.getInventory().notEnoughSpace();
                          return;
                        }
                        item.remove();
                        for (var exchangeId : item.getInfoDef().getExchangeIds()) {
                          player.getInventory().addItem(exchangeId);
                        }
                      }),
                  new DialogueOption("Cancel.")));
          break;
        }
    }
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    var onSlot = onItem.getSlot();
    if (ItemHandler.used(useItem, onItem, ItemId.OSMUMTENS_FANG, ItemId.CURSED_PHALANX)) {
      player.getInventory().deleteItem(ItemId.OSMUMTENS_FANG);
      player.getInventory().deleteItem(ItemId.CURSED_PHALANX);
      player.getInventory().addItem(ItemId.OSMUMTENS_FANG_OR, 1, onSlot);
      return true;
    }
    return false;
  }
}
