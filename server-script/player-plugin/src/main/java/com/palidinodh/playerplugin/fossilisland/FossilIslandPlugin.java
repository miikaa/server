package com.palidinodh.playerplugin.fossilisland;

import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import java.util.ArrayList;
import java.util.List;

public class FossilIslandPlugin implements PlayerPlugin {

  private List<MyceliumLocationType> myceliumLocations = new ArrayList<>();

  public void unlockMyceliumLocation(MyceliumLocationType type) {
    if (myceliumLocations.contains(type)) {
      return;
    }
    myceliumLocations.add(type);
  }

  public boolean isMyceliumLocationUnlocked(MyceliumLocationType type) {
    return myceliumLocations.contains(type);
  }
}
