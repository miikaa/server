package com.palidinodh.playerplugin.farming.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Farming;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.SEED_BOX, ItemId.OPEN_SEED_BOX})
class SeedBoxItem implements ItemHandler {

  private static boolean isBox(Item item) {
    return item.getId() == ItemId.SEED_BOX || item.getId() == ItemId.OPEN_SEED_BOX;
  }

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "fill":
        {
          for (var i = player.getInventory().size() - 1; i >= 0; i--) {
            var addingId = player.getInventory().getId(i);
            if (!Farming.isSeed(addingId)) {
              continue;
            }
            var addingAmount = player.getInventory().getAmount(i);
            addingAmount =
                player.getWidgetManager().getSeedBox().canAddAmount(addingId, addingAmount);
            addingAmount =
                player
                    .getWidgetManager()
                    .getSeedBox()
                    .addItem(addingId, addingAmount)
                    .getSuccessAmount();
            player.getInventory().deleteItem(addingId, addingAmount, i);
          }
          break;
        }
      case "empty":
        {
          for (var i = player.getWidgetManager().getSeedBox().size() - 1; i >= 0; i--) {
            var addingId = player.getWidgetManager().getSeedBox().getId(i);
            var addingAmount = player.getWidgetManager().getSeedBox().getAmount(i);
            addingAmount = player.getInventory().canAddAmount(addingId, addingAmount);
            addingAmount = player.getInventory().addItem(addingId, addingAmount).getSuccessAmount();
            player.getWidgetManager().getSeedBox().deleteItem(addingId, addingAmount);
          }
          break;
        }
      case "check":
        player.getWidgetManager().getSeedBox().displayItemList();
        break;
      case "open":
        item.replace(new Item(ItemId.OPEN_SEED_BOX));
        break;
      case "close":
        item.replace(new Item(ItemId.SEED_BOX));
        break;
    }
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    var seedItem = isBox(useItem) ? onItem : useItem;
    if (player.getController().isItemStorageDisabled()) {
      return false;
    }
    if (!Farming.isSeed(seedItem.getId())) {
      return false;
    }
    System.out.println(
        player.getWidgetManager().getSeedBox().canAddItem(seedItem)
            + ", "
            + player.getWidgetManager().getSeedBox().canAddAmount(seedItem));
    if (!player.getWidgetManager().getSeedBox().canAddItem(seedItem)) {
      return false;
    }
    player.getWidgetManager().getSeedBox().addItem(seedItem);
    seedItem.remove();
    return true;
  }
}
