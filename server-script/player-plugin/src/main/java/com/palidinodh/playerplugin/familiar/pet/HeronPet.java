package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.familiar.Pet;
import com.palidinodh.util.PNumber;

class HeronPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.HERON, NpcId.HERON, NpcId.HERON_6722));
    builder.entry(
        new Pet.Entry(
            ItemId.GREAT_BLUE_HERON, NpcId.GREAT_BLUE_HERON, NpcId.GREAT_BLUE_HERON_10636));
    builder.itemVariation(
        (p, n, i) -> {
          var npcId = -1;
          var itemQuantity = 1;
          switch (i.getId()) {
            case ItemId.SPIRIT_FLAKES:
              npcId = NpcId.GREAT_BLUE_HERON;
              itemQuantity = 3_000;
              break;
          }
          if (npcId == -1) {
            return;
          }
          if (p.getInventory().getCount(i.getId()) < itemQuantity) {
            p.getGameEncoder()
                .sendMessage(
                    "You need "
                        + PNumber.formatNumber(itemQuantity)
                        + " "
                        + i.getName()
                        + " to do this.");
            return;
          }
          p.getPlugin(FamiliarPlugin.class).transformPet(npcId);
          i.remove(itemQuantity);
        });
    return builder;
  }
}
