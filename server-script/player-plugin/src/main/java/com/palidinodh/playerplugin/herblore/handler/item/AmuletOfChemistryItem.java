package com.palidinodh.playerplugin.herblore.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.herblore.HerblorePlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.AMULET_OF_CHEMISTRY)
class AmuletOfChemistryItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var plugin = player.getPlugin(HerblorePlugin.class);
    switch (option.getText()) {
      case "check":
        player
            .getGameEncoder()
            .sendMessage(
                "Your amulet of chemistry has "
                    + plugin.getAmuletOfChemistryCharges()
                    + " charges left.");
        break;
      case "break":
        plugin.breakAmuletOfChemistry();
        break;
    }
  }
}
