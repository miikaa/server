package com.palidinodh.playerplugin.godwars.handler.item;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.GODSWORD_BLADE)
class GodswordBladeItem implements ItemHandler {

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    var hilt = ItemHandler.unmatchItem(useItem, onItem, ItemId.GODSWORD_BLADE);
    Item godsword;
    switch (hilt.getId()) {
      case ItemId.ARMADYL_HILT:
        godsword = new Item(ItemId.ARMADYL_GODSWORD);
        break;
      case ItemId.BANDOS_HILT:
        godsword = new Item(ItemId.BANDOS_GODSWORD);
        break;
      case ItemId.SARADOMIN_HILT:
        godsword = new Item(ItemId.SARADOMIN_GODSWORD);
        break;
      case ItemId.ZAMORAK_HILT:
        godsword = new Item(ItemId.ZAMORAK_GODSWORD);
        break;
      case ItemId.ANCIENT_HILT:
        godsword = new Item(ItemId.ANCIENT_GODSWORD);
        break;
      default:
        return false;
    }
    useItem.remove();
    onItem.replace(godsword);
    return true;
  }
}
