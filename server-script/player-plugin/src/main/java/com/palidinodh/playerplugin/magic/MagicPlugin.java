package com.palidinodh.playerplugin.magic;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.cache.widget.SpellbookChild;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.MessageDialogue;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.util.PNumber;
import java.util.EnumMap;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

public class MagicPlugin implements PlayerPlugin {

  @Inject private transient Player player;
  @Getter @Setter private transient int recastDelay;
  @Getter @Setter private transient Npc thrall;
  @Setter private transient int thrallTime;

  private Map<SpellbookChild, Integer> recastDelays = new EnumMap<>(SpellbookChild.class);
  @Getter private int alchWarningValue = 30_000;
  @Getter private boolean leatherStatus;
  @Getter @Setter private int deathChargeDelay;

  @Override
  public void logout() {
    player.getController().removeNpc(thrall);
    thrall = null;
  }

  @Override
  public void tick() {
    if (!recastDelays.isEmpty()) {
      for (var it = recastDelays.entrySet().iterator(); it.hasNext(); ) {
        var entry = it.next();
        entry.setValue(entry.getValue() - 1);
        if (entry.getValue() == 0) {
          it.remove();
        }
      }
    }
    if (recastDelay > 0) {
      recastDelay--;
    }
    if (deathChargeDelay > 0) {
      deathChargeDelay--;
    }
    checkThrall();
  }

  @Override
  public void npcKilled(Npc npc) {
    if (deathChargeDelay > 0) {
      deathChargeDelay = 0;
      player.getCombat().setSpecialAttackAmount(player.getCombat().getSpecialAttackAmount() + 150);
      player
          .getGameEncoder()
          .sendMessage("<col=ff0000>Some of your special attack energy has been restored.</col>");
    }
  }

  public boolean canRecast(SpellbookChild spell) {
    return recastDelay == 0 && !recastDelays.containsKey(spell);
  }

  public void setRecast(SpellbookChild spell, int delay) {
    recastDelay = 1;
    recastDelays.put(spell, delay);
  }

  public void checkThrall() {
    if (thrall == null) {
      return;
    }
    if (thrallTime == 0) {
      return;
    }
    if (!thrall.isVisible()) {
      player.getController().removeNpc(thrall);
      thrall = null;
      return;
    }
    if (--thrallTime == 0) {
      thrall.getCombat().startDeath();
      switch (thrall.getId()) {
        case NpcId.LESSER_GHOSTLY_THRALL:
        case NpcId.SUPERIOR_GHOSTLY_THRALL:
        case NpcId.GREATER_GHOSTLY_THRALL:
          player.getGameEncoder().sendMessage("Your ghostly thrall returns to the grave.");
          break;
        case NpcId.LESSER_SKELETAL_THRALL:
        case NpcId.SUPERIOR_SKELETAL_THRALL:
        case NpcId.GREATER_SKELETAL_THRALL:
          player.getGameEncoder().sendMessage("Your skeleton thrall returns to the grave.");
          break;
        case NpcId.LESSER_ZOMBIFIED_THRALL:
        case NpcId.SUPERIOR_ZOMBIFIED_THRALL:
        case NpcId.GREATER_ZOMBIFIED_THRALL:
          player.getGameEncoder().sendMessage("Your zombie thrall returns to the grave.");
          break;
      }
      return;
    }
    var attackingEntity = player.getCombat().getAttackingEntity();
    var lastAttackedBy = player.getCombat().getLastAttackedByEntity();
    if (attackingEntity != null && !attackingEntity.isPlayer()) {
      thrall.getCombat().setAttackingEntity(attackingEntity);
    } else if (lastAttackedBy != null && !lastAttackedBy.isPlayer()) {
      thrall.getCombat().setAttackingEntity(lastAttackedBy);
    } else {
      thrall.getCombat().setAttackingEntity(null);
    }
  }

  public void tanLeatherSetupDialogue() {
    if (leatherStatus) {
      player.openDialogue(
          new OptionsDialogue(
              "The spell currently gives SOFT leather.",
              new DialogueOption(
                  "Switch to HARD leather.",
                  (c, s) -> {
                    leatherStatus = false;
                  }),
              new DialogueOption("Soft leather is fine.")));
    } else if (!leatherStatus) {
      player.openDialogue(
          new OptionsDialogue(
              "The spell currently gives HARD leather.",
              new DialogueOption(
                  "Switch to SOFT leather.",
                  (c, s) -> {
                    leatherStatus = true;
                  }),
              new DialogueOption("Hard leather is fine.")));
    }
  }

  public void alchWarningsDialogue() {
    player.openDialogue(
        new MessageDialogue(
            "A warning will be shown if the item is worth at least:<br><col=800000>"
                + PNumber.formatNumber(alchWarningValue)
                + " coins</col>.<br>Untradeable items <col=800000>will always</col> trigger such a warning.",
            (c, s) -> {
              alchOptionsDialogue();
            }));
  }

  private void alchOptionsDialogue() {
    player.openOptionsDialogue(
        new DialogueOption(
            "Set value threshold",
            (c, s) -> {
              alchSetValueDialogue();
            }),
        new DialogueOption("Cancel"));
  }

  private void alchSetValueDialogue() {
    player
        .getGameEncoder()
        .sendEnterAmount(
            "Set value threshold for alchemy warnings:",
            v -> {
              alchWarningValue = v;
              alchWarningsDialogue();
            });
  }
}
