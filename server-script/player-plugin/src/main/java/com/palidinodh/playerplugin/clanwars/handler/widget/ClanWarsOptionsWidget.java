package com.palidinodh.playerplugin.clanwars.handler.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.WidgetSetting;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.clanwars.ClanWarsPlugin;
import com.palidinodh.playerplugin.clanwars.ClanWarsRule;
import com.palidinodh.playerplugin.clanwars.ClanWarsRuleOption;
import com.palidinodh.playerplugin.clanwars.ClanWarsStages;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.Arrays;
import java.util.List;

@ReferenceId(WidgetId.CLAN_WARS_OPTIONS)
class ClanWarsOptionsWidget implements WidgetHandler {

  private static final List<Integer> CHILDREN =
      Arrays.asList(7, 11, 15, 18, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44);

  @Override
  public void onOpen(Player player, int widgetId) {
    for (var child : CHILDREN) {
      player
          .getGameEncoder()
          .sendWidgetSettings(WidgetId.CLAN_WARS_OPTIONS, child, 0, 100, WidgetSetting.OPTION_0);
    }
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    var plugin = player.getPlugin(ClanWarsPlugin.class);
    switch (childId) {
      case 7:
        plugin.changeRule(ClanWarsRule.GAME_END, slot / 3);
        break;
      case 11:
        plugin.changeRule(ClanWarsRule.ARENA, slot / 3);
        break;
      case 15:
        plugin.changeRule(ClanWarsRule.STRAGGLERS, slot / 3);
        break;
      case 18:
        {
          switch (slot) {
            case 0:
              plugin.changeRule(
                  ClanWarsRule.BROADCAST_OUTCOME,
                  plugin.ruleSelected(ClanWarsRule.BROADCAST_OUTCOME, ClanWarsRuleOption.ON)
                      ? ClanWarsRuleOption.OFF
                      : ClanWarsRuleOption.ON);
              break;
            case 3:
              plugin.changeRule(
                  ClanWarsRule.DROP_ITEMS_ON_DEATH,
                  plugin.ruleSelected(ClanWarsRule.DROP_ITEMS_ON_DEATH, ClanWarsRuleOption.ON)
                      ? ClanWarsRuleOption.OFF
                      : ClanWarsRuleOption.ON);
              break;
            case 6:
              plugin.changeRule(
                  ClanWarsRule.ONE_DEFENCE_MODE,
                  plugin.ruleSelected(ClanWarsRule.ONE_DEFENCE_MODE, ClanWarsRuleOption.ON)
                      ? ClanWarsRuleOption.OFF
                      : ClanWarsRuleOption.ON);
              break;
            case 9:
              plugin.changeRule(
                  ClanWarsRule.F2P_CONTENT_ONLY,
                  plugin.ruleSelected(ClanWarsRule.F2P_CONTENT_ONLY, ClanWarsRuleOption.ON)
                      ? ClanWarsRuleOption.OFF
                      : ClanWarsRuleOption.ON);
              break;
            case 12:
              plugin.changeRule(
                  ClanWarsRule.IGNORE_FREEZING,
                  plugin.ruleSelected(ClanWarsRule.IGNORE_FREEZING, ClanWarsRuleOption.ON)
                      ? ClanWarsRuleOption.OFF
                      : ClanWarsRuleOption.ON);
              break;
            case 15:
              plugin.changeRule(
                  ClanWarsRule.PJ_TIMER,
                  plugin.ruleSelected(ClanWarsRule.PJ_TIMER, ClanWarsRuleOption.ON)
                      ? ClanWarsRuleOption.OFF
                      : ClanWarsRuleOption.ON);
              break;
            case 18:
              plugin.changeRule(
                  ClanWarsRule.SINGLE_SPELLS,
                  plugin.ruleSelected(ClanWarsRule.SINGLE_SPELLS, ClanWarsRuleOption.ON)
                      ? ClanWarsRuleOption.OFF
                      : ClanWarsRuleOption.ON);
              break;
            case 21:
              plugin.changeRule(
                  ClanWarsRule.ALLOW_POWERED_STAVES,
                  plugin.ruleSelected(ClanWarsRule.ALLOW_POWERED_STAVES, ClanWarsRuleOption.ON)
                      ? ClanWarsRuleOption.OFF
                      : ClanWarsRuleOption.ON);
              break;
            case 24:
              plugin.changeRule(
                  ClanWarsRule.ALLOW_BLIGHTED_VLS,
                  plugin.ruleSelected(ClanWarsRule.ALLOW_BLIGHTED_VLS, ClanWarsRuleOption.ON)
                      ? ClanWarsRuleOption.OFF
                      : ClanWarsRuleOption.ON);
              break;
            case 27:
              plugin.changeRule(
                  ClanWarsRule.NO_SNARE_ENTANGLE,
                  plugin.ruleSelected(ClanWarsRule.NO_SNARE_ENTANGLE, ClanWarsRuleOption.ON)
                      ? ClanWarsRuleOption.OFF
                      : ClanWarsRuleOption.ON);
              break;
          }
          break;
        }
      case 35:
        plugin.changeRule(ClanWarsRule.MELEE, slot - 1);
        break;
      case 36:
        plugin.changeRule(ClanWarsRule.RANGING, slot - 1);
        break;
      case 37:
        plugin.changeRule(ClanWarsRule.MAGIC, slot - 1);
        break;
      case 38:
        plugin.changeRule(ClanWarsRule.FOOD, slot - 1);
        break;
      case 39:
        plugin.changeRule(ClanWarsRule.DRINKS, slot - 1);
        break;
      case 40:
        plugin.changeRule(ClanWarsRule.PRAYER, slot - 1);
        break;
      case 41:
        plugin.changeRule(ClanWarsRule.SPECIAL_ATTACKS, slot - 1);
        break;
      case 42:
        plugin.changeRule(ClanWarsRule.LEAVING_CHANNEL, slot - 1);
        break;
      case 43:
        plugin.changeRule(ClanWarsRule.TEAM_CAP, slot - 1);
        break;
      case 44:
        // plugin.changeRule(ClanWarsRule.REJOINING, slot - 1);
        player.getGameEncoder().sendMessage("Coming soon.");
        break;
      case 31:
        ClanWarsStages.acceptRuleSelection(player);
        break;
    }
  }
}
