package com.palidinodh.playerplugin.grandexchange.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.RANDOM_DK_RING_32394)
class RandomDkRingItem implements ItemHandler {

  private static final int[] RINGS = {
    ItemId.SEERS_RING, ItemId.ARCHERS_RING, ItemId.WARRIOR_RING, ItemId.BERSERKER_RING
  };

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.replace(new Item(PRandom.arrayRandom(RINGS)));
  }
}
