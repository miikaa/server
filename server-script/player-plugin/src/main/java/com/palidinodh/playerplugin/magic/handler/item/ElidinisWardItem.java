package com.palidinodh.playerplugin.magic.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.ELIDINIS_WARD, ItemId.ELIDINIS_WARD_F, ItemId.ELIDINIS_WARD_OR})
class ElidinisWardItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "dismantle":
        {
          if (item.getInfoDef().getExchangeIds() == null) {
            break;
          }
          player.openDialogue(
              new OptionsDialogue(
                  "Are you sure you want to dismantle " + item.getName() + "?",
                  new DialogueOption(
                      "Yes, dismantle it.",
                      (c, s) -> {
                        if (item.getSlot() == -1) {
                          return;
                        }
                        if (player.getInventory().getRemainingSlots()
                            < item.getInfoDef().getExchangeIds().length - 1) {
                          player.getInventory().notEnoughSpace();
                          return;
                        }
                        item.remove();
                        for (var exchangeId : item.getInfoDef().getExchangeIds()) {
                          player.getInventory().addItem(exchangeId);
                        }
                      }),
                  new DialogueOption("Cancel.")));
          break;
        }
    }
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    var onSlot = onItem.getSlot();
    if (ItemHandler.used(useItem, onItem, ItemId.ELIDINIS_WARD, ItemId.ARCANE_SIGIL)) {
      if (player.getSkills().getLevel(Skills.SMITHING) < 90) {
        player.getGameEncoder().sendMessage("You need a Smithing level of 90 to do this.");
        return true;
      }
      if (player.getSkills().getLevel(Skills.PRAYER) < 90) {
        player.getGameEncoder().sendMessage("You need a Prayer level of 90 to do this.");
        return true;
      }
      if (player.getInventory().getCount(ItemId.SOUL_RUNE) < 10_000) {
        player.getGameEncoder().sendMessage("You need 10,000 soul runes to do this.");
        return true;
      }
      player.getInventory().deleteItem(ItemId.ELIDINIS_WARD);
      player.getInventory().deleteItem(ItemId.ARCANE_SIGIL);
      player.getInventory().deleteItem(ItemId.SOUL_RUNE, 10_000);
      player.getInventory().addItem(ItemId.ELIDINIS_WARD_F, 1, onSlot);
      player.getSkills().addXp(Skills.SMITHING, 260);
      player.getSkills().addXp(Skills.PRAYER, 260);
      return true;
    }
    if (ItemHandler.used(useItem, onItem, ItemId.ELIDINIS_WARD_F, ItemId.MENAPHITE_ORNAMENT_KIT)) {
      player.getInventory().deleteItem(ItemId.ELIDINIS_WARD_F);
      player.getInventory().deleteItem(ItemId.MENAPHITE_ORNAMENT_KIT);
      player.getInventory().addItem(ItemId.ELIDINIS_WARD_OR, 1, onSlot);
      return true;
    }
    return false;
  }
}
