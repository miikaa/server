package com.palidinodh.playerplugin.collectionlog;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum CompletionType {
  NONE(""),
  COMPLETE(":g:");

  private final String mask;
}
