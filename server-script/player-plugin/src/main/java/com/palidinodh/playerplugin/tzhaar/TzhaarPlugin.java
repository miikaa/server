package com.palidinodh.playerplugin.tzhaar;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Appearance;
import com.palidinodh.osrscore.model.entity.player.BondRelicType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.playerplugin.collectionlog.CollectionLogPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.setting.UserRank;
import com.palidinodh.util.PTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import lombok.Getter;

@Getter
public class TzhaarPlugin implements PlayerPlugin {

  @Inject private transient Player player;

  private WavesMinigame wavesMinigame = new WavesMinigame();
  private TheInferno inferno = new TheInferno();
  @Deprecated private int minigameTime;
  @Deprecated private int minigameNpcSpawn;
  @Deprecated private boolean minigameFromFirstWave;
  @Deprecated private boolean minigamePractice;
  @Deprecated private boolean minigameItemSpawn;
  @Deprecated private Tile minigameTile;
  @Deprecated private int[] minigameStats;

  @Override
  public void loadLegacy(Map<String, Object> map) {
    if (map.containsKey("tzhaar.infernoBestTime")) {
      inferno.setBestTime((int) map.get("tzhaar.infernoBestTime"));
      inferno.setComplete(true);
    }
    if (map.containsKey("tzhaar.infernoDefenceLevel")) {
      inferno.setDefenceLevel(((int[]) map.get("tzhaar.infernoDefenceLevel"))[1]);
    }
    if (map.containsKey("tzhaar.infernoSacrificedCape")) {
      inferno.setSacrificedCape((boolean) map.get("tzhaar.infernoSacrificedCape"));
    }
    if (map.containsKey("tzhaar.infernoWave")) {
      inferno.setWave((int) map.get("tzhaar.infernoWave"));
    }
    if (map.containsKey("tzhaar.spawnIndex")) {
      minigameNpcSpawn = (int) map.get("tzhaar.spawnIndex");
    }
    if (map.containsKey("tzhaar.time")) {
      minigameTime = (int) map.get("tzhaar.time");
    }
    if (map.containsKey("tzhaar.fromBeginning")) {
      minigameFromFirstWave = (boolean) map.get("tzhaar.fromBeginning");
    }
    if (map.containsKey("tzhaar.spawnTile")) {
      minigameTile = (Tile) map.get("tzhaar.spawnTile");
    }
    if (map.containsKey("tzhaar.inferno")) {
      inferno.setComplete((boolean) map.get("tzhaar.inferno"));
    }
    if (inferno.getWave() > 0 && wavesMinigame.getTime() == 0 && minigameTime != 0) {
      wavesMinigame.setTime(minigameTime);
      wavesMinigame.setNpcSpawn(minigameNpcSpawn);
      wavesMinigame.setFromFirstWave(minigameFromFirstWave);
      wavesMinigame.setPractice(minigamePractice);
      wavesMinigame.setItemSpawn(minigameItemSpawn);
      wavesMinigame.setTile(minigameTile);
      wavesMinigame.setStats(minigameStats);
      minigameTime = 0;
    }
  }

  @Override
  public void login() {
    if (player.getId() == TzhaarFightPitMinigame.getWinnerUserId()
        && player.getAppearance().getSkullIcon() == -1) {
      player.getAppearance().setSkullIcon(Appearance.PK_ICON_RED_SKULL);
    }
    if (inferno.getWave() > 0) {
      if (wavesMinigame.getTile() != null) {
        player.setTile(wavesMinigame.getTile());
      }
      startInferno(
          inferno.getWave(),
          wavesMinigame.isFromFirstWave(),
          wavesMinigame.isPractice(),
          wavesMinigame.isItemSpawn());
    }
    var collectionLogPlugin = player.getPlugin(CollectionLogPlugin.class);
    if (inferno.isComplete()) {
      collectionLogPlugin.addMissingItem(
          NpcCombatDefinition.getDefinition(NpcId.TZKAL_ZUK_1400).getKillCountName(),
          ItemId.INFERNAL_CAPE);
    }
    if (player.hasItem(ItemId.JAL_NIB_REK) || player.hasItem(ItemId.TZREK_ZUK)) {
      collectionLogPlugin.addMissingItem(
          NpcCombatDefinition.getDefinition(NpcId.TZKAL_ZUK_1400).getKillCountName(),
          ItemId.JAL_NIB_REK);
    }
  }

  @Override
  public void logout() {
    if (inferno.getWave() > 0) {
      wavesMinigame.setTile(new Tile(player));
      inferno.setSupportHitpoints(new int[wavesMinigame.getSupportNpcs().size()]);
      for (var i = 0; i < wavesMinigame.getSupportNpcs().size(); i++) {
        if (wavesMinigame.getSupportNpcs().get(i) == null) {
          continue;
        }
        inferno.getSupportHitpoints()[i] =
            wavesMinigame.getSupportNpcs().get(i).getCombat().getHitpoints();
      }
    }
  }

  @Override
  public boolean widgetHook(int option, int widgetId, int childId, int slot, int itemId) {
    switch (widgetId) {
      case WidgetId.LOGOUT:
        pauseMinigame();
        break;
    }
    return false;
  }

  public void joinFightPit() {
    if (!TzhaarFightPitMinigame.isOpen()) {
      player.openOptionsDialogue(
          new DialogueOption(
              "Open the lobby?",
              (c, s) -> {
                if (TzhaarFightPitMinigame.isOpen()) {
                  player.getGameEncoder().sendMessage("The lobby is already open.");
                  return;
                }
                TzhaarFightPitMinigame.start(false, false);
                joinFightPit();
              }),
          new DialogueOption("Nevermind."));
      return;
    }
    player.setController(new TzhaarFightPitController());
  }

  public void pauseMinigame() {
    if (inferno.getWave() == 0 || wavesMinigame.isPaused()) {
      return;
    }
    wavesMinigame.setPaused(true);
    player.getGameEncoder().sendMessage("<col=ff0000>The Inferno has been paused.");
  }

  public void startInferno(int wave, boolean fromFirstWave) {
    startInferno(wave, fromFirstWave, false, false);
  }

  public void startPracticeInferno(int wave, boolean itemSpawn) {
    if (!player.isUsergroup(UserRank.SAPPHIRE_MEMBER)) {
      player.getGameEncoder().sendMessage("You need to be a Sapphire Member to use this feature.");
      return;
    }
    if (itemSpawn) {
      if (!player.isUsergroup(UserRank.EMERALD_MEMBER)) {
        player
            .getGameEncoder()
            .sendMessage("You need to be an Emerald Member to use this feature.");
        return;
      }
      if (!player.getInventory().isEmpty() || !player.getEquipment().isEmpty()) {
        player.getGameEncoder().sendMessage("You can't bring items in this practice mode.");
        return;
      }
    }
    startInferno(wave, false, true, itemSpawn);
  }

  public int getInfernoBoost() {
    var wave = 1;
    switch (player.getPlugin(BondPlugin.class).getDonatorRank()) {
      case ZENYTE:
      case ONYX:
      case DRAGONSTONE:
        wave = 67;
        break;
      case DIAMOND:
        wave = 60;
        break;
      case RUBY:
        wave = 45;
        break;
      case EMERALD:
        wave = 30;
        break;
      case SAPPHIRE:
        wave = 15;
        break;
    }
    if (player.getPlugin(BondPlugin.class).isRelicUnlocked(BondRelicType.INFERNO_CHALLENGE)) {
      wave = Math.max(wave, 62);
    }
    if (inferno.isComplete()) {
      wave = 67;
    }
    return wave;
  }

  public void endInferno() {
    inferno.setWave(0);
    player.restore();
    player.getController().stop();
    wavesMinigame.reset();
  }

  public void completeInferno() {
    if (wavesMinigame.isPractice()) {
      endInferno();
      return;
    }
    if (!player.getInventory().addItem(ItemId.INFERNAL_CAPE).success()) {
      player.getBank().add(new Item(ItemId.INFERNAL_CAPE));
    }
    var collectionLogPlugin = player.getPlugin(CollectionLogPlugin.class);
    collectionLogPlugin.addCount(
        NpcCombatDefinition.getDefinition(NpcId.TZKAL_ZUK_1400).getKillCountName());
    collectionLogPlugin.addItem(
        NpcCombatDefinition.getDefinition(NpcId.TZKAL_ZUK_1400).getKillCountName(),
        new Item(ItemId.INFERNAL_CAPE));
    inferno.setComplete(true);
    if (wavesMinigame.isFromFirstWave()) {
      if (wavesMinigame.getTime() < inferno.getBestTime()) {
        inferno.setBestTime(wavesMinigame.getTime());
        player
            .getGameEncoder()
            .sendMessage(
                "Duration: <col=ff0000>"
                    + PTime.ticksToDuration(wavesMinigame.getTime())
                    + "</col> (new personal best)");
      } else {
        player
            .getGameEncoder()
            .sendMessage("Duration: <col=ff0000>" + PTime.ticksToDuration(wavesMinigame.getTime()));
      }
    }
    if (!player
        .getInventory()
        .addItem(ItemId.TOKKUL, wavesMinigame.isFromFirstWave() ? 16_440 : 8_220)
        .success()) {
      player
          .getBank()
          .add(new Item(ItemId.TOKKUL, wavesMinigame.isFromFirstWave() ? 16_440 : 8_220));
    }
    player
        .getWorld()
        .sendAchievement(
            player.getMessaging().getIconImage()
                + player.getUsername()
                + " has completed The Inferno!");
    player.getSkills().rollPet(ItemId.JAL_NIB_REK, 1);
    if (inferno.getDefenceLevel() == 0
        || player.getController().getLevelForXP(Skills.DEFENCE) < inferno.getDefenceLevel()) {
      inferno.setDefenceLevel(player.getController().getLevelForXP(Skills.DEFENCE));
    }
    endInferno();
  }

  private void startInferno(int wave, boolean fromFirstWave, boolean practice, boolean itemSpawn) {
    if (!practice) {
      itemSpawn = false;
    }
    wavesMinigame.setPaused(false);
    wavesMinigame.setFromFirstWave(fromFirstWave);
    wavesMinigame.setPractice(practice);
    wavesMinigame.setItemSpawn(itemSpawn);
    if (itemSpawn
        && (wavesMinigame.getStats() == null
            || wavesMinigame.getStats().length != Skills.SKILL_COUNT)) {
      wavesMinigame.setStats(new int[Skills.SKILL_COUNT]);
      Arrays.fill(wavesMinigame.getStats(), 99);
      player.getCombat().setMaxHitpoints(99);
    }
    if (inferno.getWave() == 0) {
      player
          .getMovement()
          .animatedTeleport(
              new Tile(2270, 5344, 0),
              6723,
              -1,
              4191,
              null,
              null,
              null,
              -1,
              6,
              "You hit the ground in the centre of The Inferno.");
      player
          .getGameEncoder()
          .sendMessage("You jump into the fiery cauldron of The Inferno; your heart is pulsating.");
      wavesMinigame.setNpcSpawn(PRandom.randomE(TzhaarInfernoController.SPAWNS.length));
      inferno.setSupportHitpoints(null);
    }
    inferno.setWave(wave);
    infernoWaveTeleport();
    player.setController(new TzhaarInfernoController());
    wavesMinigame.setNpcs(new ArrayList<>());
    wavesMinigame.setSupportNpcs(new ArrayList<>());
    if (inferno.getWave() < 67) {
      for (var i = 0; i < TzhaarInfernoController.SUPPORTS.length; i++) {
        if (inferno.getSupportHitpoints() != null && inferno.getSupportHitpoints()[i] <= 0) {
          wavesMinigame.getSupportNpcs().add(null);
          continue;
        }
        var npc =
            player
                .getController()
                .addNpc(new NpcSpawn(TzhaarInfernoController.SUPPORTS[i], NpcId.ROCKY_SUPPORT_1));
        wavesMinigame.getSupportNpcs().add(npc);
        if (inferno.getSupportHitpoints() != null) {
          npc.getCombat().setHitpoints(inferno.getSupportHitpoints()[i]);
        }
        player
            .getController()
            .addMapObject(
                new MapObject(
                    ObjectId.ROCKY_SUPPORT_30353, 10, 0, TzhaarInfernoController.SUPPORTS[i]));
      }
    }
    if (practice && itemSpawn) {
      wavesMinigame.setPaused(true);
      player
          .getGameEncoder()
          .sendMessage("<col=ff0000>Use the exit to get items and unpause the minigame.");
    }
    wavesMinigame.loadPractice();
  }

  public void infernoWaveTeleport() {
    switch (inferno.getWave()) {
      case 69:
        player.getMovement().teleport(2272, 5356);
        break;
      case 68:
        player.getMovement().teleport(2272, 5341);
        break;
    }
  }
}
