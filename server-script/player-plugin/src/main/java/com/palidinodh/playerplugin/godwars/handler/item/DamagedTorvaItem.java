package com.palidinodh.playerplugin.godwars.handler.item;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.TORVA_FULL_HELM_DAMAGED,
  ItemId.TORVA_PLATEBODY_DAMAGED,
  ItemId.TORVA_PLATELEGS_DAMAGED
})
class DamagedTorvaItem implements ItemHandler {

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    if (!ItemHandler.used(useItem, onItem, ItemId.BANDOSIAN_COMPONENTS)) {
      return false;
    }
    var damagedTorvaItem = ItemHandler.unmatchItem(useItem, onItem, ItemId.BANDOSIAN_COMPONENTS);
    Item repairedTorvaItem = null;
    var components = 0;
    switch (damagedTorvaItem.getId()) {
      case ItemId.TORVA_FULL_HELM_DAMAGED:
        {
          repairedTorvaItem = new Item(ItemId.TORVA_FULL_HELM);
          components = 1;
          break;
        }
      case ItemId.TORVA_PLATEBODY_DAMAGED:
        {
          repairedTorvaItem = new Item(ItemId.TORVA_PLATEBODY);
          components = 2;
          break;
        }
      case ItemId.TORVA_PLATELEGS_DAMAGED:
        {
          repairedTorvaItem = new Item(ItemId.TORVA_PLATELEGS);
          components = 2;
          break;
        }
    }
    if (repairedTorvaItem == null || components == 0) {
      return false;
    }
    if (player.getInventory().getCount(ItemId.BANDOSIAN_COMPONENTS) < components) {
      player.getGameEncoder().sendMessage("You don't have enough components to do this.");
      return true;
    }
    if (player.getSkills().getLevel(Skills.SMITHING) < 90) {
      player.getGameEncoder().sendMessage("You need a Smithing level of 90 to do this.");
      return true;
    }
    player.getSkills().addXp(Skills.SMITHING, 2250);
    player.getInventory().deleteItem(ItemId.BANDOSIAN_COMPONENTS, components);
    damagedTorvaItem.replace(repairedTorvaItem);
    return true;
  }
}
