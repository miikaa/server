package com.palidinodh.playerplugin.outfit;

import com.google.inject.Inject;
import com.palidinodh.osrscore.model.entity.player.Equipment;
import com.palidinodh.osrscore.model.entity.player.OutfitOverrideType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.item.ItemDef;
import com.palidinodh.util.PCollection;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

@Getter
public class OutfitPlugin implements PlayerPlugin {

  @Inject private transient Player player;

  private List<OutfitOverrideType> outfitsUnlocked = new ArrayList<>();
  private int[] itemIds = PCollection.getIntArray(Equipment.SIZE, -1);

  @Override
  public void login() {
    loadOutfit();
  }

  public OutfitOverrideType getOutfit(int index) {
    return index >= 0 && index < outfitsUnlocked.size() ? outfitsUnlocked.get(index) : null;
  }

  public int getOutfitCount() {
    return outfitsUnlocked.size();
  }

  public boolean isOutfitUnlocked(OutfitOverrideType outfit) {
    return outfitsUnlocked.contains(outfit);
  }

  public void unlockOutfit(OutfitOverrideType outfit) {
    if (isOutfitUnlocked(outfit)) {
      return;
    }
    outfitsUnlocked.add(outfit);
  }

  public void loadItem(int itemId) {
    var equipSlot = ItemDef.getItemDef(itemId).getEquipSlot();
    if (equipSlot == null) {
      return;
    }
    itemIds[equipSlot.ordinal()] = itemId;
    loadOutfit();
  }

  public void loadOutfit() {
    if (player.getArea().inWilderness()) {
      return;
    }
    player.getAppearance().setOverrideItemIds(itemIds);
  }

  public void resetOutfit() {
    itemIds = PCollection.getIntArray(Equipment.SIZE, -1);
    loadOutfit();
  }
}
