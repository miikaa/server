package com.palidinodh.playerplugin.wilderness.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.URSINE_CHAINMACE_U, ItemId.URSINE_CHAINMACE})
class UrsineChainmaceItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "uncharge":
        {
          if (player.getInventory().getRemainingSlots() < 1) {
            player.getInventory().notEnoughSpace();
            return;
          }
          player.getInventory().addItem(ItemId.REVENANT_ETHER, item.getCharges());
          item.replace(new Item(ItemId.URSINE_CHAINMACE_U));
          break;
        }
    }
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    if (ItemHandler.used(useItem, onItem, ItemId.URSINE_CHAINMACE_U, ItemId.REVENANT_ETHER)
        || ItemHandler.used(useItem, onItem, ItemId.URSINE_CHAINMACE, ItemId.REVENANT_ETHER)) {
      player
          .getCharges()
          .chargeFromInventory(
              ItemId.URSINE_CHAINMACE,
              useItem.getId() == ItemId.REVENANT_ETHER ? onItem.getSlot() : useItem.getSlot(),
              player.getInventory().getCount(ItemId.REVENANT_ETHER),
              new Item(ItemId.REVENANT_ETHER),
              1);
      return true;
    }
    return false;
  }
}
