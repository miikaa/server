package com.palidinodh.playerplugin.tzhaar;

import com.palidinodh.osrscore.model.item.Item;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TheInferno {

  private boolean sacrificedCape;
  private boolean complete;
  private int bestTime;
  private int defenceLevel;
  private int wave;
  private int[] supportHitpoints;
  private int[] statsLoadout;
  private Item[] inventoryLoadout;
  private Item[] equipmentLoadout;
}
