package com.palidinodh.playerplugin.slayer.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.EXPEDITIOUS_BRACELET)
class ExpeditiousBraceletItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var plugin = player.getPlugin(SlayerPlugin.class);
    switch (option.getText()) {
      case "check":
        player
            .getGameEncoder()
            .sendMessage(
                "Your expeditious bracelet has "
                    + plugin.getExpeditiousBraceletCharges()
                    + " charges left.");
        break;
      case "break":
        plugin.breakExpeditiousBracelet();
        break;
    }
  }
}
