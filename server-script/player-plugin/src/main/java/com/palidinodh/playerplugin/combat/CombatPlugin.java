package com.palidinodh.playerplugin.combat;

import com.google.inject.Inject;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.playerplugin.skill.SkillPlugin;
import com.palidinodh.util.PTime;
import java.util.HashMap;
import java.util.Map;

public class CombatPlugin implements PlayerPlugin {

  @Inject private transient Player player;

  private Map<String, Integer> killTimes = new HashMap<>();

  public boolean updateKillTime(String name, int time) {
    var existingTime = killTimes.getOrDefault(name, Integer.MAX_VALUE);
    if (time > existingTime) {
      player
          .getGameEncoder()
          .sendMessage(
              "Fight duration: <col=ff0000>"
                  + PTime.ticksToDuration(time)
                  + "</col>. Personal best: "
                  + PTime.ticksToDuration(existingTime));
      return false;
    }
    killTimes.put(name, time);
    player
        .getGameEncoder()
        .sendMessage(
            "Fight duration: <col=ff0000>"
                + PTime.ticksToDuration(time)
                + "</col> (new personal best)");
    return true;
  }

  public void f2pRejuvinate() {
    for (var i = 0; i < Skills.SKILL_COUNT; i++) {
      var level = player.getController().getLevelForXP(i);
      var boostedLevel = player.getSkills().getLevel(i);
      var maxBoostedLevel = level;
      if (i == Skills.STRENGTH) {
        maxBoostedLevel = (int) (level + (level * 0.1) + 3);
      }
      if (boostedLevel <= maxBoostedLevel) {
        continue;
      }
      player.getSkills().setLevel(i, maxBoostedLevel);
      player.getGameEncoder().sendSkillLevel(i);
    }
    player.getSkills().setStaminaTime(0);
    player.getSkills().setOverload(0, 0);
    player.getSkills().setPrayerEnhance(null);
    player.getPlugin(SkillPlugin.class).stopRestores();
    player.getMagic().setVengeanceCast(false);
    player.getMagic().setStaffOfTheDeadSpecial(0);
    player.getMagic().setZurielsStaffSpecial(0);
    player.getPrayer().deactivateAll();
  }
}
