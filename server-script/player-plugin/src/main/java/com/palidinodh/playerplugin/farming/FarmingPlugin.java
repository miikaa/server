package com.palidinodh.playerplugin.farming;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Farming;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.item.ItemDef;
import com.palidinodh.osrscore.model.map.MapItem;

public class FarmingPlugin implements PlayerPlugin {

  @Inject private transient Player player;

  @Override
  public boolean approvedPickupMapItemHook(MapItem mapItem) {
    return addSeedBoxMapItem(mapItem);
  }

  private boolean addSeedBoxMapItem(MapItem mapItem) {
    var item = mapItem.getItem();
    if (player.getController().isItemStorageDisabled()) {
      return false;
    }
    if (!player.getInventory().hasItem(ItemId.OPEN_SEED_BOX)) {
      return false;
    }
    if (ItemDef.getStackable(item.getId()) && player.getInventory().hasItem(item.getId())) {
      return false;
    }
    if (!Farming.isSeed(mapItem.getId())) {
      return false;
    }
    if (!player.getWidgetManager().getSeedBox().canAddItem(item)) {
      return false;
    }
    player.getWidgetManager().getSeedBox().addItem(item);
    return true;
  }
}
