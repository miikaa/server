package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.familiar.Pet;
import com.palidinodh.util.PCollection;
import java.util.Map;

class RockyPet implements Pet.BuildType {

  public static final Map<Integer, Integer> RECOLORS =
      PCollection.toMap(ItemId.REDBERRIES, NpcId.RED);

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.ROCKY, NpcId.ROCKY, NpcId.ROCKY_7353));
    builder.entry(new Pet.Entry(ItemId.RED, NpcId.RED, NpcId.RED_9852));
    builder.itemVariation(
        (p, n, i) -> {
          var recolor = RECOLORS.get(i.getId());
          if (recolor == null) {
            return;
          }
          p.getPlugin(FamiliarPlugin.class).transformPet(recolor);
          i.remove();
        });
    return builder;
  }
}
