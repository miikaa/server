package com.palidinodh.playerplugin.bond.handler.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.worldevent.bondoutfits.BondOutfitsEvent;

@ReferenceId(WidgetId.BOND_OUTFITS_1029)
class BondOutfitsWidget implements WidgetHandler {

  @Override
  public void onClose(Player player, int widgetId) {
    player.getAppearance().setTemporaryItemIds(null);
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    player.getWorld().getWorldEvent(BondOutfitsEvent.class).widgetOption(player, childId);
  }
}
