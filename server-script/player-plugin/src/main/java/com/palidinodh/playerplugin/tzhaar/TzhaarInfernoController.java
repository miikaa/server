package com.palidinodh.playerplugin.tzhaar;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.NullObjectId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.controller.PController;
import com.palidinodh.osrscore.model.map.MapItem;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PEventTasks;

class TzhaarInfernoController extends PController {

  public static final int[][] WAVE_MONSTERS = {
    {NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_MEJRAH_85},
    {
      NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_MEJRAH_85, NpcId.JAL_MEJRAH_85
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32
    },
    {NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_AK_165},
    {NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_MEJRAH_85, NpcId.JAL_AK_165},
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_AK_165
    },
    {NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_AK_165, NpcId.JAL_AK_165},
    {NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32},
    {NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_IMKOT_240},
    {
      NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_MEJRAH_85, NpcId.JAL_IMKOT_240
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_IMKOT_240
    },
    {NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_AK_165, NpcId.JAL_IMKOT_240},
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_AK_165,
      NpcId.JAL_IMKOT_240
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_AK_165,
      NpcId.JAL_IMKOT_240
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_AK_165,
      NpcId.JAL_AK_165,
      NpcId.JAL_IMKOT_240
    },
    {
      NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_IMKOT_240, NpcId.JAL_IMKOT_240
    },
    {NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32},
    {NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_XIL_370},
    {NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_MEJRAH_85, NpcId.JAL_XIL_370},
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_XIL_370
    },
    {NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_AK_165, NpcId.JAL_XIL_370},
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_AK_165,
      NpcId.JAL_XIL_370
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_AK_165,
      NpcId.JAL_XIL_370
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_AK_165,
      NpcId.JAL_AK_165,
      NpcId.JAL_XIL_370
    },
    {NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_IMKOT_240, NpcId.JAL_XIL_370},
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_XIL_370
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_XIL_370
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_AK_165,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_XIL_370
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_AK_165,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_XIL_370
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_AK_165,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_XIL_370
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_AK_165,
      NpcId.JAL_AK_165,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_XIL_370
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_XIL_370
    },
    {NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_XIL_370, NpcId.JAL_XIL_370},
    {NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32},
    {NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_ZEK_490},
    {NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_MEJRAH_85, NpcId.JAL_ZEK_490},
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_ZEK_490
    },
    {NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_AK_165, NpcId.JAL_ZEK_490},
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_AK_165,
      NpcId.JAL_ZEK_490
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_AK_165,
      NpcId.JAL_ZEK_490
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_AK_165,
      NpcId.JAL_AK_165,
      NpcId.JAL_ZEK_490
    },
    {NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_IMKOT_240, NpcId.JAL_ZEK_490},
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_ZEK_490
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_ZEK_490
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_AK_165,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_ZEK_490
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_AK_165,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_ZEK_490
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_AK_165,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_ZEK_490
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_AK_165,
      NpcId.JAL_AK_165,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_ZEK_490
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_ZEK_490
    },
    {NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_XIL_370, NpcId.JAL_ZEK_490},
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_XIL_370,
      NpcId.JAL_ZEK_490
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_XIL_370,
      NpcId.JAL_ZEK_490
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_AK_165,
      NpcId.JAL_XIL_370,
      NpcId.JAL_ZEK_490
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_AK_165,
      NpcId.JAL_XIL_370,
      NpcId.JAL_ZEK_490
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_AK_165,
      NpcId.JAL_XIL_370,
      NpcId.JAL_ZEK_490
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_AK_165,
      NpcId.JAL_AK_165,
      NpcId.JAL_XIL_370,
      NpcId.JAL_ZEK_490
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_XIL_370,
      NpcId.JAL_ZEK_490
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_XIL_370,
      NpcId.JAL_ZEK_490
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_XIL_370,
      NpcId.JAL_ZEK_490
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_AK_165,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_XIL_370,
      NpcId.JAL_ZEK_490
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_AK_165,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_XIL_370,
      NpcId.JAL_ZEK_490
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_MEJRAH_85,
      NpcId.JAL_AK_165,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_XIL_370,
      NpcId.JAL_ZEK_490
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_AK_165,
      NpcId.JAL_AK_165,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_XIL_370,
      NpcId.JAL_ZEK_490
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_IMKOT_240,
      NpcId.JAL_XIL_370,
      NpcId.JAL_ZEK_490
    },
    {
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_NIB_32,
      NpcId.JAL_XIL_370,
      NpcId.JAL_XIL_370,
      NpcId.JAL_ZEK_490
    },
    {NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_NIB_32, NpcId.JAL_ZEK_490, NpcId.JAL_ZEK_490},
    {NpcId.JALTOK_JAD_900},
    {NpcId.JALTOK_JAD_900_7704, NpcId.JALTOK_JAD_900_7704, NpcId.JALTOK_JAD_900_7704},
    {NpcId.TZKAL_ZUK_1400}
  };
  public static final Tile[] SPAWNS = {
    new Tile(2258, 5353),
    new Tile(2279, 5353),
    new Tile(2260, 5347),
    new Tile(2280, 5346),
    new Tile(2273, 5341),
    new Tile(2262, 5335),
    new Tile(2258, 5330),
    new Tile(2272, 5330),
    new Tile(2280, 5333)
  };
  public static final Tile[] SUPPORTS = {
    new Tile(2257, 5349), new Tile(2275, 5351), new Tile(2267, 5335)
  };

  @Inject private Player player;
  private TzhaarPlugin plugin;
  private WavesMinigame minigame;
  private TheInferno inferno;
  private int spawnDelay;

  @Override
  public void startHook() {
    player.setLargeVisibility();
    plugin = player.getPlugin(TzhaarPlugin.class);
    minigame = plugin.getWavesMinigame();
    inferno = plugin.getInferno();
    spawnDelay = 15;
    startInstance();
    setTeleportsDisabled(true);
    setKeepItemsOnDeath(true);
    setAreaLocked("TzHaarInferno");
    setExitTile(new Tile(2496, 5114));
    if (minigame.isItemSpawn()) {
      setItemStorageDisabled(true);
      player.getPrayer().setAllowAllPrayers(true);
    }
  }

  @Override
  public void stoppedHook() {
    if (minigame.isItemSpawn()) {
      player.getInventory().clear();
      player.getEquipment().clear();
      player.getEquipment().weaponUpdate(true);
      player.getPrayer().setAllowAllPrayers(false);
    }
    player.getSkills().setCombatLevel();
    player.getController().removeNpcs(minigame.getNpcs());
    player.getController().removeNpcs(minigame.getSupportNpcs());
    player.getWidgetManager().removeInteractiveWidgets();
    player.getWidgetManager().removeOverlay();
    player.restore();
  }

  @Override
  public void tickHook() {
    if (player.isLocked()) {
      return;
    }
    if (!player.isVisible()) {
      return;
    }
    if (!player.getSession().isOpen()) {
      return;
    }
    checkNpcs();
    checkSpawns();
  }

  @Override
  public void applyDeadCompleteEndHook() {
    plugin.endInferno();
  }

  @Override
  public MapItem addMapItemHook(MapItem mapItem, boolean npcDrop) {
    mapItem.setNeverAppear();
    return mapItem;
  }

  @Override
  public int getLevelForXP(int index) {
    return minigame.isItemSpawn() ? minigame.getStats()[index] : super.getLevelForXP(index);
  }

  @Override
  public int getXP(int index) {
    return minigame.isItemSpawn()
        ? Skills.XP_TABLE[minigame.getStats()[index]]
        : super.getXP(index);
  }

  @Override
  public boolean canGainXP() {
    return !minigame.isPractice();
  }

  @Override
  public int getExpMultiplier(int id) {
    return minigame.isPractice() ? 1 : -1;
  }

  private void checkNpcs() {
    if (spawnDelay > 0) {
      return;
    }
    minigame.getNpcs().removeIf(n -> !n.isVisible() && n.getCombat().isDead());
    if (!minigame.getNpcs().isEmpty()) {
      return;
    }
    inferno.setWave(inferno.getWave() + 1);
    spawnDelay = 5;
    if (inferno.getWave() == 68) {
      spawnDelay = 10;
    } else if (inferno.getWave() == 69) {
      spawnDelay = 10;
    }
    plugin.infernoWaveTeleport();
    if (inferno.getWave() - 1 == WAVE_MONSTERS.length) {
      plugin.completeInferno();
    }
    if (inferno.getWave() == 67) {
      killSupports();
    }
  }

  private void checkSpawns() {
    if (spawnDelay == 0) {
      minigame.setTime(minigame.getTime() + 1);
      return;
    }
    if (!minigame.isPaused()) {
      spawnDelay--;
    }
    if (spawnDelay == 0) {
      player.getGameEncoder().sendMessage("<col=ff0000>Wave: " + inferno.getWave());
      spawn();
    }
  }

  private void spawn() {
    var npcIds = WAVE_MONSTERS[inferno.getWave() - 1];
    for (var i = 0; i < npcIds.length; i++) {
      var tile = getTile(npcIds[i]);
      switch (npcIds[i]) {
        case NpcId.JAL_NIB_32:
          tile = new Tile(2265 + PRandom.randomI(2), 5345 + PRandom.randomI(2));
          break;
        case NpcId.JALTOK_JAD_900:
          tile = new Tile(2265, 5347);
          break;
        case NpcId.JALTOK_JAD_900_7704:
          switch (i) {
            case 0:
              tile = new Tile(2265, 5347);
              break;
            case 1:
              tile = new Tile(2274, 5347);
              break;
            case 2:
              tile = new Tile(2268, 5335);
              break;
          }
          break;
        case NpcId.TZKAL_ZUK_1400:
          tile = new Tile(2268, 5364);
          break;
      }
      var npc = player.getController().addNpc(new NpcSpawn(tile, npcIds[i]));
      npc.getMovement().setClipNpcs(true);
      if (npc.getId() == NpcId.JALTOK_JAD_900_7704) {
        npc.getCombat().setHitDelay(npc.getCombat().getHitDelay() + i * 3);
      }
      if (npc.getId() != NpcId.JAL_NIB_32) {
        npc.getCombat().setTarget(player);
      }
      minigame.addNpc(npc);
    }
    Npc targetedSupport = null;
    for (var npc : minigame.getSupportNpcs()) {
      if (npc.isLocked()) {
        continue;
      }
      targetedSupport = npc;
      break;
    }
    for (int i = 0; i < minigame.getNpcs().size(); i++) {
      var npc = minigame.getNpcs().get(i);
      if (npc.getId() == NpcId.JAL_NIB_32) {
        npc.setLargeVisibility();
        npc.getCombat().startAttacking(targetedSupport != null ? targetedSupport : player);
      } else if (npc.getId() == NpcId.JAL_ZEK_490) {
        npc.getCombat().script("monsters", minigame.getNpcs());
      }
    }
    if (inferno.getWave() == 69) {
      player.getGameEncoder().setVarp(1574, 1);
      player
          .getController()
          .addMapObject(new MapObject(NullObjectId.NULL_30340, 10, 1, new Tile(2267, 5364)));
      player
          .getController()
          .addMapObject(new MapObject(NullObjectId.NULL_30342, 10, 1, new Tile(2267, 5366)));
      player
          .getController()
          .addMapObject(new MapObject(NullObjectId.NULL_30339, 10, 3, new Tile(2275, 5364)));
      player
          .getController()
          .addMapObject(new MapObject(NullObjectId.NULL_30341, 10, 3, new Tile(2275, 5366)));
      var leftRock = new MapObject(ObjectId.FALLING_ROCK_30344, 10, 3, new Tile(2268, 5364));
      var rightRock = new MapObject(ObjectId.FALLING_ROCK, 10, 3, new Tile(2273, 5364));
      player.getController().addMapObject(leftRock);
      player.getController().addMapObject(rightRock);
      var tasks = new PEventTasks();
      tasks.execute(
          t -> {
            player.getController().addMapObject(new MapObject(-1, 10, 0, new Tile(2270, 5363)));
            player.getGameEncoder().sendMapObjectAnimation(leftRock, 7560);
            player.getGameEncoder().sendMapObjectAnimation(rightRock, 7559);
          });
      tasks.execute(
          8,
          t -> {
            player
                .getController()
                .addMapObject(new MapObject(NullObjectId.NULL_30346, 10, 3, new Tile(2268, 5364)));
            player
                .getController()
                .addMapObject(new MapObject(NullObjectId.NULL_30345, 10, 3, new Tile(2273, 5364)));
          });
      player.getController().addEvent(tasks);
    }
  }

  private Tile getTile(int npcId) {
    var tile = SPAWNS[minigame.getNpcSpawn()];
    minigame.setNpcSpawn((minigame.getNpcSpawn() + 1) % SPAWNS.length);
    return tile;
  }

  private void killSupports() {
    if (minigame.getSupportNpcs() == null) {
      return;
    }
    for (var npc : minigame.getSupportNpcs()) {
      if (npc.isLocked()) {
        continue;
      }
      npc.getCombat().startDeath();
    }
  }
}
