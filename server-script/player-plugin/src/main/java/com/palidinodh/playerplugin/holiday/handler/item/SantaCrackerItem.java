package com.palidinodh.playerplugin.holiday.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.SANTA_CRACKER_32398)
class SantaCrackerItem implements ItemHandler {

  private static final int[] SANTA_HATS = {
    ItemId.SANTA_HAT, ItemId.BLACK_SANTA_HAT, ItemId.INVERTED_SANTA_HAT
  };

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.replace(new Item(PRandom.arrayRandom(SANTA_HATS)));
  }
}
