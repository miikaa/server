package com.palidinodh.playerplugin.playstyle.handler.widget;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NotedItemId;
import com.palidinodh.cache.id.ScriptId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Equipment;
import com.palidinodh.osrscore.model.entity.player.Messaging;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.playerplugin.playstyle.PlayStylePlugin;
import com.palidinodh.rs.adaptive.RsDifficultyMode;
import com.palidinodh.rs.adaptive.RsGameMode;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.EnumMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;

@ReferenceId(WidgetId.PLAY_STYLE_1011)
class PlayStyleWidget implements WidgetHandler {

  private static final Map<RsGameMode, String> GAME_MODE_DESCRIPTIONS =
      new EnumMap<>(RsGameMode.class);
  private static final Map<RsDifficultyMode, String> DIFFICULTY_MODE_DESCRIPTIONS =
      new EnumMap<>(RsDifficultyMode.class);

  static {
    GAME_MODE_DESCRIPTIONS.put(RsGameMode.UNSET, "");
    GAME_MODE_DESCRIPTIONS.put(
        RsGameMode.REGULAR, "  * Play without any restrictions.<br>  * Drop rate boost: x2-x6.");
    GAME_MODE_DESCRIPTIONS.put(
        RsGameMode.IRONMAN,
        "  * No trading, grand exchange, or staking.<br>  * Less items can be purchased from shops.<br>  * Drop rate boost: x2.");
    GAME_MODE_DESCRIPTIONS.put(
        RsGameMode.HARDCORE_IRONMAN,
        "  * No trading, grand exchange, or staking.<br>  * Less items can be purchased from shops.<br>  * On 'unsafe' death, your mode is reverted to ironman.<br>  * Drop rate boost: x2.");

    DIFFICULTY_MODE_DESCRIPTIONS.put(RsDifficultyMode.UNSET, "");
    DIFFICULTY_MODE_DESCRIPTIONS.put(
        RsDifficultyMode.NORMAL,
        "  * Combat experience boost: x200.<br>  * Skill experience boost: x20.");
    DIFFICULTY_MODE_DESCRIPTIONS.put(
        RsDifficultyMode.HARD,
        "  * Combat experience boost: x20.<br>  * Skill experience boost: x20.<br>  * OSRS Drop rate boost: x1.1.");
    DIFFICULTY_MODE_DESCRIPTIONS.put(
        RsDifficultyMode.ELITE,
        "  * Combat experience boost: x5.<br>  * Skill experience boost: x5.<br>  * OSRS Drop rate boost: x1.2.");
  }

  private static boolean canPair(RsGameMode gameMode, RsDifficultyMode difficultyMode) {
    if (gameMode.isUnset() || difficultyMode.isUnset()) {
      return true;
    }
    if (gameMode.isIronType()) {
      return difficultyMode.isHard() || difficultyMode.isElite();
    }
    return true;
  }

  private static boolean canChangeGameMode(RsGameMode currentGameMode, RsGameMode newGameMode) {
    if (newGameMode.isUnset()) {
      return false;
    }
    if (currentGameMode.isUnset()) {
      return true;
    }
    if (currentGameMode == newGameMode) {
      return true;
    }
    switch (currentGameMode) {
      case IRONMAN:
        return newGameMode.isRegular();
      case ULTIMATE_IRONMAN:
      case HARDCORE_IRONMAN:
        return newGameMode.isIronman();
      default:
        return false;
    }
  }

  private static boolean setGameMode(Player player, RsGameMode gameMode) {
    if (gameMode.isUnset()) {
      player.getGameEncoder().sendMessage("Please select a game mode.");
      return false;
    }
    if (!canChangeGameMode(player.getGameMode(), gameMode)) {
      player.getGameEncoder().sendMessage("You can't select this game mode.");
      return false;
    }
    player.putAttribute("game_mode_selected", gameMode);
    if (gameMode.isIronType()) {
      player.putAttribute("difficulty_mode_selected", RsDifficultyMode.HARD);
    }
    player.putAttribute(
        "group_mode_selected", player.getPlugin(PlayStylePlugin.class).isGroupMode());
    return true;
  }

  private static boolean canChangeDifficultyMode(
      RsDifficultyMode currentDifficultyMode, RsDifficultyMode newDifficultyMode) {
    if (currentDifficultyMode.isUnset()) {
      return true;
    }
    if (currentDifficultyMode == newDifficultyMode) {
      return true;
    }
    return currentDifficultyMode.ordinal() > newDifficultyMode.ordinal();
  }

  private static boolean setDifficultyMode(
      Player player, RsGameMode gameMode, RsDifficultyMode difficultyMode) {
    if (difficultyMode.isUnset()) {
      player.getGameEncoder().sendMessage("Please select a difficulty mode.");
      return false;
    }
    if (!canChangeDifficultyMode(player.getDifficultyMode(), difficultyMode)) {
      player
          .getGameEncoder()
          .sendMessage("You can't select a difficulty mode harder than your current mode.");
      return false;
    }
    if (!canPair(gameMode, difficultyMode)) {
      player
          .getGameEncoder()
          .sendMessage("This game mode and difficulty mode can't be paired together.");
      return false;
    }
    if (!player.getGameMode().isUnset()
        && player.getGameMode() != gameMode
        && player.getDifficultyMode() != difficultyMode) {
      player
          .getGameEncoder()
          .sendMessage("You can't change your game mode and difficulty mode at the same time.");
      return false;
    }
    player.putAttribute("difficulty_mode_selected", difficultyMode);
    return true;
  }

  private static boolean setGroupMode(Player player, RsGameMode gameMode, boolean group) {
    var plugin = player.getPlugin(PlayStylePlugin.class);
    if (plugin.getGameMode() != RsGameMode.UNSET) {
      if (group && !plugin.isGroupMode()) {
        player.getGameEncoder().sendMessage("Changing your group status is not possible.");
        return false;
      }
    }
    if (group && !gameMode.isIronman() && !gameMode.isHardcoreIronman()) {
      player.getGameEncoder().sendMessage("This game mode can't use groups.");
      return false;
    }
    player.putAttribute("group_mode_selected", group);
    return true;
  }

  private static void sendModes(Player player) {
    var gameMode = (RsGameMode) player.getAttribute("game_mode_selected");
    if (gameMode != RsGameMode.UNSET) {
      player
          .getGameEncoder()
          .sendWidgetText(WidgetId.PLAY_STYLE_1011, 22, gameMode.getFormattedName());
      player
          .getGameEncoder()
          .sendWidgetText(WidgetId.PLAY_STYLE_1011, 23, GAME_MODE_DESCRIPTIONS.get(gameMode));
    }
    var difficultyMode = (RsDifficultyMode) player.getAttribute("difficulty_mode_selected");
    if (difficultyMode != RsDifficultyMode.UNSET) {
      player
          .getGameEncoder()
          .sendWidgetText(WidgetId.PLAY_STYLE_1011, 24, difficultyMode.getFormattedName());
      player
          .getGameEncoder()
          .sendWidgetText(
              WidgetId.PLAY_STYLE_1011, 25, DIFFICULTY_MODE_DESCRIPTIONS.get(difficultyMode));
      player
          .getGameEncoder()
          .sendClientScript(
              ScriptId.GRAPHIC_SWAPPER,
              WidgetId.PLAY_STYLE_1011 << 16 | 28,
              player.getAttributeBool("group_mode_selected") ? 1213 : 1211);
    }
    for (var w : GameModeWidget.values()) {
      var color = "";
      if (gameMode == w.getGameMode()) {
        color = "<col=33CC00>";
      }
      player
          .getGameEncoder()
          .sendWidgetText(
              WidgetId.PLAY_STYLE_1011,
              w.getChildId() + 2,
              color + w.getGameMode().getFormattedName());
    }
    for (var w : DifficultyModeWidget.values()) {
      var color = "";
      if (difficultyMode == w.getDifficultyMode()) {
        color = "<col=33CC00>";
      }
      player
          .getGameEncoder()
          .sendWidgetText(
              WidgetId.PLAY_STYLE_1011,
              w.getChildId() + 2,
              color + w.getDifficultyMode().getFormattedName());
    }
  }

  private static void accept(Player player) {
    var gameMode = (RsGameMode) player.getAttribute("game_mode_selected");
    var difficultyMode = (RsDifficultyMode) player.getAttribute("difficulty_mode_selected");
    var groupMode = player.getAttributeBool("group_mode_selected");
    var plugin = player.getPlugin(PlayStylePlugin.class);
    if (plugin.getGameMode() == gameMode
        && plugin.getDifficultyMode() == difficultyMode
        && plugin.isGroupMode() == groupMode) {
      player.getGameEncoder().sendMessage("No changes have been selected.");
      return;
    }
    if (!setGameMode(player, gameMode)) {
      return;
    }
    if (!setDifficultyMode(player, gameMode, difficultyMode)) {
      return;
    }
    if (!setGroupMode(player, gameMode, groupMode)) {
      return;
    }
    var isFirstTime = player.getGameMode().isUnset() || player.getDifficultyMode().isUnset();
    if (!isFirstTime && player.getBank().pinRequired(false)) {
      player.getGameEncoder().sendMessage("You need to enter your bank pin to do this.");
      return;
    }
    player.getWidgetManager().removeInteractiveWidgets();
    player.unlock();
    plugin.setDifficultyMode(difficultyMode);
    plugin.setGameMode(gameMode);
    if (groupMode) {
      plugin.enableGroup();
    } else {
      plugin.disableGroup();
    }
    if (!isFirstTime) {
      return;
    }
    player.getInventory().addItem(ItemId.GUIDE_BOOK);
    player.getInventory().addItem(ItemId.STARTER_PACK_32288);
    player.getInventory().addItem(ItemId.COINS, 100_000);
    player.getInventory().addItem(NotedItemId.MONKFISH, 100);
    player.getEquipment().setItem(Equipment.Slot.WEAPON, ItemId.IRON_SCIMITAR, 1);
    player.getEquipment().setItem(Equipment.Slot.NECK, ItemId.AMULET_OF_POWER, 1);
    player.getEquipment().setItem(Equipment.Slot.CAPE, ItemId.AVAS_ATTRACTOR, 1);
    if (player.getGameMode().isIronman()) {
      player.getEquipment().setItem(Equipment.Slot.HEAD, ItemId.IRONMAN_HELM, 1);
      player.getEquipment().setItem(Equipment.Slot.CHEST, ItemId.IRONMAN_PLATEBODY, 1);
      player.getEquipment().setItem(Equipment.Slot.LEG, ItemId.IRONMAN_PLATELEGS, 1);
    } else if (player.getGameMode().isHardcoreIronman()) {
      player.getEquipment().setItem(Equipment.Slot.HEAD, ItemId.HARDCORE_IRONMAN_HELM, 1);
      player.getEquipment().setItem(Equipment.Slot.CHEST, ItemId.HARDCORE_IRONMAN_PLATEBODY, 1);
      player.getEquipment().setItem(Equipment.Slot.LEG, ItemId.HARDCORE_IRONMAN_PLATELEGS, 1);
    } else {
      player.getEquipment().setItem(Equipment.Slot.CHEST, ItemId.MONKS_ROBE_TOP, 1);
      player.getEquipment().setItem(Equipment.Slot.LEG, ItemId.MONKS_ROBE, 1);
      player.getEquipment().setItem(Equipment.Slot.NECK, ItemId.AMULET_OF_GLORY_4, 1);
    }
    player.getEquipment().setItem(Equipment.Slot.HAND, ItemId.MITHRIL_GLOVES, 1);
    player.getEquipment().setItem(Equipment.Slot.FOOT, ItemId.CLIMBING_BOOTS, 1);
    player.getEquipment().setItem(Equipment.Slot.SHIELD, ItemId.UNHOLY_BOOK, 1);
    player.getEquipment().weaponUpdate(true);
    player.getEquipment().setUpdate(true);
    player.getAppearance().setUpdate(true);
    player
        .getGameEncoder()
        .sendMessage(
            "If you're not sure where to start, begin with the Training Day achievement diary!",
            Messaging.CHAT_TYPE_BROADCAST);
    player.sendDiscordNewAccountLog(
        "Play Style: " + difficultyMode.getFormattedName() + " " + gameMode.getFormattedName());
  }

  @Override
  public boolean isLockedUsable() {
    return true;
  }

  @Override
  public void onOpen(Player player, int widgetId) {
    var plugin = player.getPlugin(PlayStylePlugin.class);
    player.putAttribute("game_mode_selected", plugin.getGameMode());
    player.putAttribute("difficulty_mode_selected", plugin.getDifficultyMode());
    player.putAttribute("group_mode_selected", plugin.isGroupMode());

    sendModes(player);
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    var gameMode = (RsGameMode) player.getAttribute("game_mode_selected");
    var selectedGameMode = GameModeWidget.getModeByChildId(childId);
    if (selectedGameMode != null) {
      setGameMode(player, selectedGameMode);
    }
    var selectedDifficultyMode = DifficultyModeWidget.getModeByChildId(childId);
    if (selectedDifficultyMode != null) {
      setDifficultyMode(player, gameMode, selectedDifficultyMode);
    }
    switch (childId) {
      case 28:
        setGroupMode(player, gameMode, !player.getAttributeBool("group_mode_selected"));
        break;
      case 18:
        {
          var options =
              new OptionsDialogue(
                  "Confirm Your Selection",
                  new DialogueOption(
                      "Yes, this is the mode and difficulty I want.",
                      (c, s) -> {
                        accept(player);
                      }),
                  new DialogueOption("Nevermind, I'm still deciding.")) {
                @Override
                public boolean isLockedUsable() {
                  return true;
                }
              };
          player.openDialogue(options);
          return;
        }
    }
    sendModes(player);
  }

  @AllArgsConstructor
  @Getter
  private enum GameModeWidget {
    REGULAR(34, RsGameMode.REGULAR),
    IRONMAN(37, RsGameMode.IRONMAN),
    HARDCORE_IRONMAN(40, RsGameMode.HARDCORE_IRONMAN);

    private final int childId;
    private final RsGameMode gameMode;

    public static RsGameMode getModeByChildId(int id) {
      for (var w : values()) {
        if (id != w.getChildId()) {
          continue;
        }
        return w.getGameMode();
      }
      return null;
    }

    public static int getChildIdByMode(RsGameMode m) {
      for (var w : values()) {
        if (m != w.getGameMode()) {
          continue;
        }
        return w.getChildId();
      }
      return -1;
    }
  }

  @AllArgsConstructor
  @Getter
  private enum DifficultyModeWidget {
    NORMAL(46, RsDifficultyMode.NORMAL),
    HARD(49, RsDifficultyMode.HARD),
    ELITE(52, RsDifficultyMode.ELITE);

    private final int childId;
    private final RsDifficultyMode difficultyMode;

    public static RsDifficultyMode getModeByChildId(int id) {
      for (var w : values()) {
        if (id != w.getChildId()) {
          continue;
        }
        return w.getDifficultyMode();
      }
      return null;
    }

    public static int getChildIdByMode(RsDifficultyMode m) {
      for (var w : values()) {
        if (m != w.getDifficultyMode()) {
          continue;
        }
        return w.getChildId();
      }
      return -1;
    }
  }
}
