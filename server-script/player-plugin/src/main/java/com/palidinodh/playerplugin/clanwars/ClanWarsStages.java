package com.palidinodh.playerplugin.clanwars;

import com.palidinodh.cache.id.ScriptId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.random.PRandom;

public class ClanWarsStages {

  public static void openRuleSelection(Player player) {
    var plugin = player.getPlugin(ClanWarsPlugin.class);
    if (plugin.getOpponent() == null) {
      plugin.cancel();
      return;
    }
    if (plugin.getOpponent().isLocked()) {
      plugin.cancel();
      return;
    }
    if (player.getWidgetManager().hasInteractiveWidgets()) {
      plugin.cancel();
      return;
    }
    plugin.setTeammate(null);
    plugin.setRules(ClanWarsRule.getDefault());
    plugin.setState(ClanWarsPlayerState.RULE_SELECTION);
    player
        .getWidgetManager()
        .sendInteractiveOverlay(
            WidgetId.CLAN_WARS_OPTIONS,
            () -> {
              var opponentPlayer = plugin.getOpponent();
              plugin.cancel();
              if (opponentPlayer == null) {
                return;
              }
              if (opponentPlayer.isLocked()) {
                return;
              }
              if (!opponentPlayer.getWidgetManager().hasWidget(WidgetId.CLAN_WARS_OPTIONS)) {
                return;
              }
              player.getGameEncoder().sendMessage("You decline the war.");
              opponentPlayer
                  .getGameEncoder()
                  .sendMessage(player.getUsername() + " declined the war.");
              opponentPlayer.getWidgetManager().removeInteractiveWidgets();
            });
    player
        .getGameEncoder()
        .sendWidgetText(
            WidgetId.CLAN_WARS_OPTIONS,
            3,
            "Clan Wars Setup: Challenging " + plugin.getOpponent().getUsername());
    sendVarbits(player);
  }

  public static void acceptRuleSelection(Player player) {
    var plugin = player.getPlugin(ClanWarsPlugin.class);
    if (plugin.getOpponent() == null) {
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    if (plugin.getOpponent().isLocked()) {
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    if (!player.getWidgetManager().hasWidget(WidgetId.CLAN_WARS_OPTIONS)) {
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    if (!plugin.getOpponent().getWidgetManager().hasWidget(WidgetId.CLAN_WARS_OPTIONS)) {
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    if (plugin.getArena().getArenaTop().getX() == 0) {
      player.getGameEncoder().sendMessage("This arena is not currently supported.");
      return;
    }
    if (plugin.ruleSelected(ClanWarsRule.ONE_DEFENCE_MODE, ClanWarsRuleOption.ON)) {
      if (player.getController().getLevelForXP(Skills.DEFENCE) > 1) {
        player.getGameEncoder().sendMessage("Your defence level is too high for these rules.");
        return;
      }
    }
    var opponentPlugin = plugin.getOpponent().getPlugin(ClanWarsPlugin.class);
    if (opponentPlugin.getState() == ClanWarsPlayerState.RULE_SELECTION) {
      player.getGameEncoder().sendMessage("<col=ff0000>Waiting for other player...");
      plugin.getOpponent().getGameEncoder().sendMessage("<col=ff0000>Other player has accepted.");
      plugin.setState(ClanWarsPlayerState.ACCEPT_RULE_SELECTION);
      return;
    }
    if (opponentPlugin.getState() != ClanWarsPlayerState.ACCEPT_RULE_SELECTION) {
      return;
    }
    opponentPlugin.setState(ClanWarsPlayerState.RULE_CONFIRMATION);
    plugin.setState(ClanWarsPlayerState.RULE_CONFIRMATION);
    openRuleConfirmation(player);
    openRuleConfirmation(plugin.getOpponent());
  }

  public static void openRuleConfirmation(Player player) {
    var plugin = player.getPlugin(ClanWarsPlugin.class);
    player.getWidgetManager().removeWidgetCloseEvents();
    player
        .getWidgetManager()
        .sendInteractiveOverlay(
            WidgetId.CLAN_WARS_CONFIRM,
            () -> {
              var opponentPlayer = plugin.getOpponent();
              plugin.cancel();
              if (opponentPlayer == null) {
                return;
              }
              if (opponentPlayer.isLocked()) {
                return;
              }
              if (!opponentPlayer.getWidgetManager().hasWidget(WidgetId.CLAN_WARS_OPTIONS)) {
                return;
              }
              player.getGameEncoder().sendMessage("You decline the war.");
              opponentPlayer
                  .getGameEncoder()
                  .sendMessage(player.getUsername() + " declined the war.");
              opponentPlayer.getWidgetManager().removeInteractiveWidgets();
            });
    player
        .getGameEncoder()
        .sendWidgetText(
            WidgetId.CLAN_WARS_CONFIRM,
            2,
            "Clan Wars Setup: Challenging " + plugin.getOpponent().getUsername());
    player
        .getGameEncoder()
        .sendScript(
            ScriptId.CLANWARS_CONFIRM_SETUP, ClanWarsRule.getDescriptions(plugin.getRules()));
    player
        .getGameEncoder()
        .sendHideWidget(
            WidgetId.CLAN_WARS_CONFIRM,
            7,
            plugin.ruleSelected(ClanWarsRule.DROP_ITEMS_ON_DEATH, ClanWarsRuleOption.OFF));
    sendVarbits(player);
  }

  public static void acceptRuleConfirmation(Player player) {
    var plugin = player.getPlugin(ClanWarsPlugin.class);
    if (plugin.getState() == ClanWarsPlayerState.JOIN) {
      acceptJoin(player);
      return;
    }
    if (plugin.getState() == ClanWarsPlayerState.VIEW_START) {
      acceptViewing(player);
      return;
    }
    if (plugin.getOpponent() == null) {
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    if (plugin.getOpponent().isLocked()) {
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    if (!player.getWidgetManager().hasWidget(WidgetId.CLAN_WARS_CONFIRM)) {
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    if (!plugin.getOpponent().getWidgetManager().hasWidget(WidgetId.CLAN_WARS_CONFIRM)) {
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    var opponentPlugin = plugin.getOpponent().getPlugin(ClanWarsPlugin.class);
    if (opponentPlugin.getState() == ClanWarsPlayerState.RULE_CONFIRMATION) {
      player.getGameEncoder().sendMessage("<col=ff0000>Waiting for other player...");
      plugin.getOpponent().getGameEncoder().sendMessage("<col=ff0000>Other player has accepted.");
      plugin.setState(ClanWarsPlayerState.ACCEPT_RULE_CONFIRMATION);
      return;
    }
    if (opponentPlugin.getState() != ClanWarsPlayerState.ACCEPT_RULE_CONFIRMATION) {
      return;
    }
    if ((player.carryingMembersItems() || plugin.getOpponent().carryingMembersItems())
        && plugin.ruleSelected(ClanWarsRule.F2P_CONTENT_ONLY, ClanWarsRuleOption.ON)) {
      player
          .getGameEncoder()
          .sendMessage("Members items can't be taken with the F2P Content rule set.");
      plugin
          .getOpponent()
          .getGameEncoder()
          .sendMessage("Members items can't be taken with the F2P Content rule set.");
      return;
    }
    if (player.getWorld().getPlayerInstance(player, "clan_wars_battle") != null) {
      player
          .getGameEncoder()
          .sendMessage("There is already a clan wars instance for this Clan Chat.");
      plugin
          .getOpponent()
          .getGameEncoder()
          .sendMessage("There is already a clan wars instance for their Clan Chat.");
      return;
    }
    if (player.getWorld().getPlayerInstance(plugin.getOpponent(), "clan_wars_battle") != null) {
      player
          .getGameEncoder()
          .sendMessage("There is already a clan wars instance for their Clan Chat.");
      plugin
          .getOpponent()
          .getGameEncoder()
          .sendMessage("There is already a clan wars instance for this Clan Chat.");
      return;
    }
    for (var nearbyPlayer : player.getController().getPlayers()) {
      if (nearbyPlayer == player || nearbyPlayer == plugin.getOpponent()) {
        continue;
      }
      if (!nearbyPlayer.getMessaging().matchesClanChat(player.getMessaging().getClanChatUsername())
          && !nearbyPlayer
              .getMessaging()
              .matchesClanChat(plugin.getOpponent().getMessaging().getClanChatUsername())) {
        continue;
      }
      nearbyPlayer
          .getGameEncoder()
          .sendMessage(
              "<col=7F007F>Your clan is in battle! Step through the portals to join them.");
    }
    if (PRandom.randomI(1) == 0) {
      plugin.startWar(true);
      opponentPlugin.startWar(false);
    } else {
      opponentPlugin.startWar(true);
      plugin.startWar(false);
    }
  }

  public static void openJoin(Player player) {
    var plugin = player.getPlugin(ClanWarsPlugin.class);
    plugin.cancel();
    var clanUsername = player.getMessaging().getClanChatUsername();
    if (clanUsername == null) {
      player.getGameEncoder().sendMessage("You need to be in a Clan Chat to join.");
      return;
    }
    plugin.setTeammate(player.getWorld().getPlayerInstance(player, "clan_wars_battle"));
    if (plugin.getTeammate() == null) {
      player.getGameEncoder().sendMessage("Unable to find active war.");
      return;
    }
    plugin.setRules(plugin.getTeammate().getPlugin(ClanWarsPlugin.class).getRules());
    plugin.setState(ClanWarsPlayerState.JOIN);
    player
        .getWidgetManager()
        .sendInteractiveOverlay(
            WidgetId.CLAN_WARS_CONFIRM,
            () -> {
              if (plugin.getState() == ClanWarsPlayerState.BATTLE) {
                return;
              }
              plugin.cancel();
            });
    player.getGameEncoder().sendWidgetText(WidgetId.CLAN_WARS_CONFIRM, 2, "Joining Clan Wars");
    player
        .getGameEncoder()
        .sendScript(
            ScriptId.CLANWARS_CONFIRM_SETUP, ClanWarsRule.getDescriptions(plugin.getRules()));
    sendVarbits(player);
  }

  public static void acceptJoin(Player player) {
    var plugin = player.getPlugin(ClanWarsPlugin.class);
    if (!player.getWidgetManager().hasWidget(WidgetId.CLAN_WARS_CONFIRM)) {
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    if (plugin.getTeammate() == null || plugin.getTeammate().isLocked()) {
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    var teammatePlugin = plugin.getTeammate().getPlugin(ClanWarsPlugin.class);
    if (!player
            .getMessaging()
            .matchesClanChat(plugin.getTeammate().getMessaging().getClanChatUsername())
        || teammatePlugin.getState() != ClanWarsPlayerState.BATTLE) {
      player.getGameEncoder().sendMessage("Error joining war.");
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    if (plugin.ruleSelected(ClanWarsRule.GAME_END, ClanWarsRuleOption.LAST_TEAM_STANDING)
        && teammatePlugin.getWarState().getCountdown() == 0) {
      player.getGameEncoder().sendMessage("The war has already started.");
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    if (plugin.ruleSelected(ClanWarsRule.F2P_CONTENT_ONLY, ClanWarsRuleOption.ON)
        && player.carryingMembersItems()) {
      player.getGameEncoder().sendMessage("Members items can't be taken into this war.");
      return;
    }
    if (plugin.ruleSelected(ClanWarsRule.ONE_DEFENCE_MODE, ClanWarsRuleOption.ON)
        && player.getController().getLevelForXP(Skills.DEFENCE) > 1) {
      player.getGameEncoder().sendMessage("This war is limited to 1 Defence players.");
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    if (plugin.ruleSelected(ClanWarsRule.TEAM_CAP, ClanWarsRuleOption.ONE_V_ONE)
        && teammatePlugin.getWarState().getTeamCount() >= 1) {
      player.getGameEncoder().sendMessage("Your team cap is currently reached.");
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    if (plugin.ruleSelected(ClanWarsRule.TEAM_CAP, ClanWarsRuleOption.FIVE_V_FIVE)
        && teammatePlugin.getWarState().getTeamCount() >= 5) {
      player.getGameEncoder().sendMessage("Your team cap is currently reached.");
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    if (plugin.ruleSelected(ClanWarsRule.TEAM_CAP, ClanWarsRuleOption.TEN_V_TEN)
        && teammatePlugin.getWarState().getTeamCount() >= 10) {
      player.getGameEncoder().sendMessage("Your team cap is currently reached.");
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    if (plugin.ruleSelected(ClanWarsRule.TEAM_CAP, ClanWarsRuleOption.FIFTEEN_V_FIFTEEN)
        && teammatePlugin.getWarState().getTeamCount() >= 15) {
      player.getGameEncoder().sendMessage("Your team cap is currently reached.");
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    if (plugin.ruleSelected(ClanWarsRule.TEAM_CAP, ClanWarsRuleOption.TWENTY_V_TWENTY)
        && teammatePlugin.getWarState().getTeamCount() >= 20) {
      player.getGameEncoder().sendMessage("Your team cap is currently reached.");
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    if (plugin.ruleSelected(ClanWarsRule.TEAM_CAP, ClanWarsRuleOption.FIFTY_V_FIFTY)
        && teammatePlugin.getWarState().getTeamCount() >= 50) {
      player.getGameEncoder().sendMessage("Your team cap is currently reached.");
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    if (plugin.ruleSelected(ClanWarsRule.TEAM_CAP, ClanWarsRuleOption.HUNDRED_V_HUNDRED)
        && teammatePlugin.getWarState().getTeamCount() >= 100) {
      player.getGameEncoder().sendMessage("Your team cap is currently reached.");
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    plugin.joinWar(ClanWarsPlayerState.BATTLE);
  }

  public static void openView(Player player, String clanChatUsername) {
    var plugin = player.getPlugin(ClanWarsPlugin.class);
    plugin.cancel();
    plugin.setTeammate(
        player.getWorld().getPlayerInstance(player, clanChatUsername, "clan_wars_battle"));
    if (plugin.getTeammate() == null) {
      player.getGameEncoder().sendMessage("Unable to find active war.");
      return;
    }
    plugin.setRules(plugin.getTeammate().getPlugin(ClanWarsPlugin.class).getRules());
    plugin.setState(ClanWarsPlayerState.VIEW_START);
    player
        .getWidgetManager()
        .sendInteractiveOverlay(
            WidgetId.CLAN_WARS_CONFIRM,
            () -> {
              if (plugin.getState() == ClanWarsPlayerState.VIEW) {
                return;
              }
              plugin.cancel();
            });
    player.getGameEncoder().sendWidgetText(WidgetId.CLAN_WARS_CONFIRM, 2, "Viewing Clan Wars");
    player
        .getGameEncoder()
        .sendScript(
            ScriptId.CLANWARS_CONFIRM_SETUP, ClanWarsRule.getDescriptions(plugin.getRules()));
    sendVarbits(player);
  }

  public static void acceptViewing(Player player) {
    var plugin = player.getPlugin(ClanWarsPlugin.class);
    if (plugin.getTeammate() == null) {
      player.getGameEncoder().sendMessage("Error joining war.");
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    if (plugin.getTeammate().isLocked()) {
      player.getGameEncoder().sendMessage("Error joining war.");
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    if (!player.getWidgetManager().hasWidget(WidgetId.CLAN_WARS_CONFIRM)) {
      player.getGameEncoder().sendMessage("Error joining war.");
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    if (plugin.getTeammate().getPlugin(ClanWarsPlugin.class).getState()
        != ClanWarsPlayerState.BATTLE) {
      player.getGameEncoder().sendMessage("Error joining war.");
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    player.getWidgetManager().removeWidgetCloseEvents();
    plugin.joinWar(ClanWarsPlayerState.VIEW);
  }

  public static void openCompletedState(Player player) {
    var plugin = player.getPlugin(ClanWarsPlugin.class);
    switch (plugin.getCompleted()) {
      case WIN:
        {
          player.getWidgetManager().sendInteractiveOverlay(WidgetId.CLAN_WARS_COMPLETE);
          player
              .getGameEncoder()
              .sendScript(
                  ScriptId.CLANWARS_GAMEOVER,
                  1,
                  CompletedState.getDescription(plugin.getRule(ClanWarsRule.GAME_END), true));
          player.getGameEncoder().sendMessage("Congratulations, you've won the war!");
          break;
        }
      case LOSE:
        {
          player.getWidgetManager().sendInteractiveOverlay(WidgetId.CLAN_WARS_COMPLETE);
          player
              .getGameEncoder()
              .sendScript(
                  ScriptId.CLANWARS_GAMEOVER,
                  CompletedState.getDescription(plugin.getRule(ClanWarsRule.GAME_END), false));
          player.getGameEncoder().sendMessage("Oh dear, you've lost the war!");
          break;
        }
    }
    plugin.cancel();
  }

  public static void sendVarbits(Player player) {
    var plugin = player.getPlugin(ClanWarsPlugin.class);
    for (var rule : ClanWarsRule.values()) {
      player.getGameEncoder().setVarbit(rule.getVarbit(), plugin.getRules()[rule.ordinal()]);
    }
  }
}
