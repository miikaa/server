package com.palidinodh.playerplugin.tirannwn.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.CRYSTAL_AXE, ItemId.CRYSTAL_PICKAXE, ItemId.CRYSTAL_HARPOON})
class CrystalToolItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "revert":
        player.openDialogue(
            new OptionsDialogue(
                "Are you sure you want to uncharge the "
                    + item.getName()
                    + "?<br>You will not get back any of the shards.",
                new DialogueOption(
                    "Yes, turn it back!",
                    (c, s) -> {
                      if (item.getSlot() == -1) {
                        return;
                      }
                      item.replace(new Item(ItemId.CRYSTAL_TOOL_SEED));
                      switch (item.getId()) {
                        case ItemId.CRYSTAL_AXE:
                          player.getInventory().addOrDropItem(ItemId.DRAGON_AXE);
                          break;
                        case ItemId.CRYSTAL_PICKAXE:
                          player.getInventory().addOrDropItem(ItemId.DRAGON_PICKAXE);
                          break;
                        case ItemId.CRYSTAL_HARPOON:
                          player.getInventory().addOrDropItem(ItemId.DRAGON_HARPOON);
                          break;
                      }
                    }),
                new DialogueOption("No!")));
        break;
    }
  }
}
