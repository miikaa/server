package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.familiar.Pet;

class InfernoPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.JAL_NIB_REK, NpcId.JAL_NIB_REK, NpcId.JAL_NIB_REK_7675));
    builder.entry(new Pet.Entry(ItemId.TZREK_ZUK, NpcId.TZREK_ZUK, NpcId.TZREK_ZUK_8011));
    builder.optionVariation(
        (p, n) -> {
          switch (n.getId()) {
            case NpcId.TZREK_ZUK:
              p.getPlugin(FamiliarPlugin.class).transformPet(NpcId.JAL_NIB_REK);
              break;
            case NpcId.JAL_NIB_REK:
              p.getPlugin(FamiliarPlugin.class).transformPet(NpcId.TZREK_ZUK);
              break;
          }
        });
    return builder;
  }
}
