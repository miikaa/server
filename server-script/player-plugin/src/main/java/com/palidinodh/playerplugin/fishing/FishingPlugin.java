package com.palidinodh.playerplugin.fishing;

import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;

public class FishingPlugin implements PlayerPlugin {

  public static final int UNUSUAL_FISH_VALUE = 100_000;
}
