package com.palidinodh.playerplugin.thieving.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.thieving.ThievingPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.DODGY_NECKLACE)
class DodgyNecklaceItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var plugin = player.getPlugin(ThievingPlugin.class);
    switch (option.getText()) {
      case "check":
        player
            .getGameEncoder()
            .sendMessage(
                "Your dodgy necklace has " + plugin.getDodgyNecklaceCharges() + " charges left.");
        break;
      case "break":
        plugin.breakDodgyNecklace();
        break;
    }
  }
}
