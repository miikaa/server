package com.palidinodh.playerplugin.combat.hook;

import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.player.BondRelicType;
import com.palidinodh.osrscore.model.entity.player.Equipment;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.combat.PlayerCombatHitChanceHooks;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.entity.player.magic.SpellbookType;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.playerplugin.tirannwn.CrystalArmourType;
import com.palidinodh.random.PRandom;

class PlayerCombatHitChance implements PlayerCombatHitChanceHooks {

  private static final double ACCURACY_BONUS_MULTIPLIER = 1.2;

  public static double getMeleeWeaponMultiplier(Player player, BonusType bonus) {
    var weaponId = player.getEquipment().getWeaponId();
    var attackingEntity = player.getCombat().getAttackingEntity();
    var multiplier = 1.0;

    if (bonus == BonusType.ATTACK_CRUSH) {
      var inquisitorsEffect = 0;
      if (player.getEquipment().getHeadId() == ItemId.INQUISITORS_GREAT_HELM) {
        inquisitorsEffect += 5;
      }
      if (player.getEquipment().getChestId() == ItemId.INQUISITORS_HAUBERK) {
        inquisitorsEffect += 5;
      }
      if (player.getEquipment().getLegId() == ItemId.INQUISITORS_PLATESKIRT) {
        inquisitorsEffect += 5;
      }
      if (inquisitorsEffect == 15) {
        inquisitorsEffect += 10;
      }
      multiplier += inquisitorsEffect / 1000.0;
    }

    if (player.getEquipment().wearingObsidianArmourSetup()) {
      multiplier += 0.1;
    }

    if (attackingEntity != null && attackingEntity.isNpc()) {
      var npc = attackingEntity.asNpc();

      if (npc.getCombatDef().isTypeDemon() && weaponId == ItemId.ARCLIGHT) {
        multiplier += 0.7;
      }

      if (npc.getCombatDef().isTypeDragon() && weaponId == ItemId.DRAGON_HUNTER_LANCE) {
        multiplier += 0.2;
      }

      if (weaponId == ItemId.VIGGORAS_CHAINMACE || weaponId == ItemId.URSINE_CHAINMACE) {
        if (player.getEquipment().getItem(Equipment.Slot.WEAPON).getCharges() > 1000
            && player.getArea().inWilderness()) {
          multiplier += 0.5;
        }
      }
    }
    return multiplier;
  }

  public static double getMagicWeaponMultiplier(Player player) {
    var weaponId = player.getEquipment().getWeaponId();
    var attackingEntity = player.getCombat().getAttackingEntity();
    var spell = player.getMagic().getActiveSpell();

    switch (weaponId) {
      case ItemId.SMOKE_BATTLESTAFF:
      case ItemId.MYSTIC_SMOKE_STAFF:
        {
          if (player.getMagic().getSpellbook() == SpellbookType.STANDARD) {
            return 1.1;
          }
          break;
        }
      case ItemId.ZURIELS_STAFF_CHARGED_32257:
        {
          if (spell != null) {
            switch (spell.getSpellbook()) {
              case ICE_RUSH:
              case ICE_BURST:
              case ICE_BLITZ:
              case ICE_BARRAGE:
                return 1.1;
            }
          }
          break;
        }
      case ItemId.THAMMARONS_SCEPTRE:
      case ItemId.THAMMARONS_SCEPTRE_A:
      case ItemId.ACCURSED_SCEPTRE:
      case ItemId.ACCURSED_SCEPTRE_A:
        {
          if (attackingEntity != null && attackingEntity.isNpc()) {
            if (player.getEquipment().getItem(Equipment.Slot.WEAPON).getCharges() > 1000
                && player.getArea().inWilderness()) {
              return 1.5;
            }
          }
          break;
        }
    }

    if (player.getEquipment().getShieldId() == ItemId.TOME_OF_WATER && spell != null) {
      switch (spell.getSpellbook()) {
        case WATER_STRIKE:
        case WATER_BOLT:
        case WATER_BLAST:
        case WATER_WAVE:
        case WATER_SURGE:
        case CONFUSE:
        case WEAKEN:
        case CURSE:
        case BIND:
        case SNARE:
        case VULNERABILITY:
        case ENFEEBLE:
        case ENTANGLE:
        case STUN:
        case TELE_BLOCK:
          return 1.2;
      }
    }
    return 1;
  }

  @Override
  public boolean success(
      Player player, Entity entity, HitStyleType hitStyleType, double accuracy, double defence) {
    accuracy = Math.max(1, accuracy);
    defence = Math.max(1, defence);

    var weaponId = player.getEquipment().getWeaponId();

    if (weaponId == ItemId.VESTAS_BLIGHTED_LONGSWORD
        || weaponId == ItemId.VESTAS_LONGSWORD_CHARGED_32254) {
      if (player.getCombat().isUsingSpecialAttack() && hitStyleType == HitStyleType.MELEE) {
        defence *= 0.25;
      }
    }

    if (hitStyleType == HitStyleType.MAGIC
        && player.getEquipment().getRingId() == ItemId.BRIMSTONE_RING
        && PRandom.inRange(1, 4)) {
      defence *= 0.9;
      /*player
      .getGameEncoder()
      .sendMessage(
          "<col=ff0000>Your attack ignored 10% of your opponent's magic defense.</col>");*/
    }

    var hitChance = 0.0;
    if (accuracy < defence) {
      hitChance = accuracy / (2 * (defence + 1));
    } else {
      hitChance = 1 - (defence + 2) / (2 * (accuracy + 1));
    }

    return PRandom.nextDouble() <= hitChance;
  }

  @Override
  public int getAccuracy(Player player, HitStyleType hitStyleType, BonusType meleeBonus) {
    var skillId =
        hitStyleType == HitStyleType.RANGED
            ? Skills.RANGED
            : hitStyleType == HitStyleType.MAGIC ? Skills.MAGIC : Skills.ATTACK;
    var level = player.getSkills().getLevel(skillId);
    var xpStyle = player.getCombat().getXPStyle();
    var prayerBoost = player.getPrayer().getAttackBoost(hitStyleType);
    var equipmentBonus = getEquipmentAccuracy(player, hitStyleType, meleeBonus);
    var attackingEntity = player.getCombat().getAttackingEntity();
    var usingSpecialAttack =
        player.getCombat().isUsingSpecialAttack()
            && SpecialAttack.isHitStyleTypeMatch(player, hitStyleType);

    var effectiveLevel = (level * prayerBoost) + 8;
    if (xpStyle == Skills.ATTACK) {
      effectiveLevel += 3;
    } else if (xpStyle == PCombat.SHARED) {
      effectiveLevel += 1;
    }
    if (player.getEquipment().wearingFullVoid(hitStyleType)) {
      effectiveLevel *= hitStyleType == HitStyleType.MAGIC ? 1.45 : 1.1;
    }
    var max = (effectiveLevel * (equipmentBonus + 64.0));

    if (attackingEntity != null && attackingEntity.isNpc()) {
      var attackingNpc = attackingEntity.asNpc();
      var isUndead = attackingNpc.getCombatDef().isTypeUndead();
      var isSlayerTask = player.getPlugin(SlayerPlugin.class).isAnyTask(attackingNpc);
      var neckId = player.getEquipment().getNeckId();
      var headName = ItemDefinition.getLowerCaseName(player.getEquipment().getHeadId());

      if (attackingNpc.getCombatDef().isTypeVampyre()
          && player.getEquipment().getWeaponId() == ItemId.BLISTERWOOD_FLAIL
          && hitStyleType == HitStyleType.MELEE) {
        max *= 1.05;
      }

      var typeBonus = 1.0;
      if (isUndead) {
        switch (neckId) {
          case ItemId.SALVE_AMULET:
            {
              if (hitStyleType == HitStyleType.MELEE) {
                typeBonus = Math.max(1.15, typeBonus);
              }
              break;
            }
          case ItemId.SALVE_AMULET_I:
            typeBonus = Math.max(1.15, typeBonus);
            break;
          case ItemId.SALVE_AMULET_E:
            {
              if (hitStyleType == HitStyleType.MELEE) {
                typeBonus = Math.max(1.2, typeBonus);
              }
              break;
            }
          case ItemId.SALVE_AMULET_EI:
            typeBonus = Math.max(1.2, typeBonus);
            break;
        }
      }
      if (isSlayerTask) {
        var styleMatch = hitStyleType == HitStyleType.MELEE || headName.contains("(i)");
        var hasHelm = headName.contains("black mask") || headName.contains("slayer helmet");
        if (styleMatch && hasHelm) {
          typeBonus = Math.max(1.15, typeBonus);
        }
        if (attackingNpc.getDef().getLowerCaseName().contains("hunllef")
            && player.isRelicUnlocked(BondRelicType.SLAYER_GAUNTLET_TASKS)) {
          typeBonus = Math.max(1.15, typeBonus);
        }
      }
      if (player.getEquipment().getNeckId() == ItemId.AMULET_OF_AVARICE
          && (player.getArea().is("RevenantCaves")
              || player.getArea().is("WildernessSlayerCave"))) {
        typeBonus = Math.max(1.2, typeBonus);
      }
      max *= typeBonus;
    }

    if (hitStyleType == HitStyleType.MELEE) {
      max *= getMeleeWeaponMultiplier(player, meleeBonus);
    } else if (hitStyleType == HitStyleType.RANGED) {
      max *= getRangedWeaponMultiplier(player);
    } else if (hitStyleType == HitStyleType.MAGIC) {
      max *= getMagicWeaponMultiplier(player);
    }

    if (usingSpecialAttack) {
      var specialAttack = SpecialAttack.getSpecialAttack(player.getEquipment().getWeaponId());
      if (specialAttack != null) {
        max *= specialAttack.getAccuracyModifier();
      }
    }

    if (ACCURACY_BONUS_MULTIPLIER != 0) {
      max *= ACCURACY_BONUS_MULTIPLIER;
    }

    return (int) max;
  }

  @Override
  public int getDefence(Player player, HitStyleType hitStyleType, BonusType meleeBonus) {
    if (hitStyleType == HitStyleType.TYPELESS) {
      return 1;
    }
    var level = player.getSkills().getLevel(Skills.DEFENCE);
    var xpStyle = player.getCombat().getXPStyle();
    var prayerBoost = player.getPrayer().getDefenceBoost();
    var equipmentBonus = getEquipmentDefence(player, hitStyleType, meleeBonus);

    var effectiveLevel = (level * prayerBoost) + 8;
    if (xpStyle == Skills.DEFENCE) {
      effectiveLevel += 3;
    } else if (xpStyle == PCombat.SHARED) {
      effectiveLevel += 1;
    }

    if (hitStyleType == HitStyleType.MAGIC) {
      var magicPrayerBoost = player.getPrayer().getAttackBoost(HitStyleType.MAGIC);
      var effectiveMagicLevel = (player.getSkills().getLevel(Skills.MAGIC) * magicPrayerBoost);
      effectiveLevel = (effectiveLevel * 0.3 + effectiveMagicLevel * 0.7);
    }

    var max = effectiveLevel * (equipmentBonus + 64.0);

    return (int) max;
  }

  @Override
  public int getEquipmentAccuracy(Player player, HitStyleType hitStyleType, BonusType meleeBonus) {
    if (hitStyleType == HitStyleType.MELEE) {
      if (meleeBonus == BonusType.ATTACK_STAB) {
        return player.getEquipment().getBonus(BonusType.ATTACK_STAB);
      }
      if (meleeBonus == BonusType.ATTACK_SLASH) {
        return player.getEquipment().getBonus(BonusType.ATTACK_SLASH);
      }
      if (meleeBonus == BonusType.ATTACK_CRUSH) {
        return player.getEquipment().getBonus(BonusType.ATTACK_CRUSH);
      }
      var stabAttack = player.getEquipment().getBonus(BonusType.ATTACK_STAB);
      var slashAttack = player.getEquipment().getBonus(BonusType.ATTACK_SLASH);
      var crushAttack = player.getEquipment().getBonus(BonusType.ATTACK_CRUSH);
      return Math.max(stabAttack, Math.max(slashAttack, crushAttack));
    }
    if (hitStyleType == HitStyleType.RANGED) {
      return player.getEquipment().getBonus(BonusType.ATTACK_RANGED);
    }
    if (hitStyleType == HitStyleType.MAGIC) {
      return player.getEquipment().getBonus(BonusType.ATTACK_MAGIC);
    }
    return 0;
  }

  @Override
  public int getEquipmentDefence(Player player, HitStyleType hitStyleType, BonusType meleeBonus) {
    if (hitStyleType == HitStyleType.MELEE) {
      if (meleeBonus == BonusType.ATTACK_STAB || meleeBonus == BonusType.DEFENCE_STAB) {
        return player.getEquipment().getBonus(BonusType.DEFENCE_STAB);
      }
      if (meleeBonus == BonusType.ATTACK_SLASH || meleeBonus == BonusType.DEFENCE_SLASH) {
        return player.getEquipment().getBonus(BonusType.DEFENCE_SLASH);
      }
      if (meleeBonus == BonusType.ATTACK_CRUSH || meleeBonus == BonusType.DEFENCE_CRUSH) {
        return player.getEquipment().getBonus(BonusType.DEFENCE_CRUSH);
      }
      var stabDefence = player.getEquipment().getBonus(BonusType.DEFENCE_STAB);
      var slashDefence = player.getEquipment().getBonus(BonusType.DEFENCE_SLASH);
      var crushDefence = player.getEquipment().getBonus(BonusType.DEFENCE_CRUSH);
      return Math.max(stabDefence, Math.max(slashDefence, crushDefence));
    }
    if (hitStyleType == HitStyleType.RANGED) {
      return player.getEquipment().getBonus(BonusType.DEFENCE_RANGED);
    }
    if (hitStyleType == HitStyleType.MAGIC) {
      return player.getEquipment().getBonus(BonusType.DEFENCE_MAGIC);
    }
    return 0;
  }

  public double getRangedWeaponMultiplier(Player player) {
    var weaponId = player.getEquipment().getWeaponId();
    var attackingEntity = player.getCombat().getAttackingEntity();

    switch (weaponId) {
      case ItemId.CRYSTAL_BOW:
      case ItemId.CRYSTAL_BOW_24123:
      case ItemId.BOW_OF_FAERDHINEN:
      case ItemId.BOW_OF_FAERDHINEN_C:
      case ItemId.BOW_OF_FAERDHINEN_C_25884:
      case ItemId.BOW_OF_FAERDHINEN_C_25886:
      case ItemId.BOW_OF_FAERDHINEN_C_25888:
      case ItemId.BOW_OF_FAERDHINEN_C_25890:
      case ItemId.BOW_OF_FAERDHINEN_C_25892:
      case ItemId.BOW_OF_FAERDHINEN_C_25894:
      case ItemId.BOW_OF_FAERDHINEN_C_25896:
        {
          var crystalArmourEffect = 0;
          crystalArmourEffect +=
              CrystalArmourType.getAccuracyBonus(player.getEquipment().getHeadId());
          crystalArmourEffect +=
              CrystalArmourType.getAccuracyBonus(player.getEquipment().getChestId());
          crystalArmourEffect +=
              CrystalArmourType.getAccuracyBonus(player.getEquipment().getLegId());
          return 1 + (crystalArmourEffect / 100.0);
        }
    }

    if (attackingEntity != null) {
      if (weaponId == ItemId.TWISTED_BOW) {
        var oppMagicLevel = 1;
        var oppMagicBonus = 1;
        if (attackingEntity.isNpc()) {
          oppMagicLevel = attackingEntity.asNpc().getCombatDef().getStats().getMagicLevel();
          oppMagicBonus =
              attackingEntity.asNpc().getCombatDef().getStats().getBonus(BonusType.ATTACK_MAGIC);
        } else if (attackingEntity.isPlayer()) {
          oppMagicLevel = attackingEntity.asPlayer().getSkills().getLevel(Skills.MAGIC);
          oppMagicBonus =
              getEquipmentAccuracy(attackingEntity.asPlayer(), HitStyleType.MAGIC, null);
        }
        var oppLevel = Math.max(oppMagicLevel, oppMagicBonus);
        oppLevel = Math.min(oppLevel, 140);
        return (140.0
                + (3.0 * oppLevel - 10.0) / 100.0
                - StrictMath.pow(0.3 * oppLevel - 100.0, 2.0) / 100.0)
            / 100.0;
      }

      if (attackingEntity.isNpc()) {
        var npc = attackingEntity.asNpc();

        if (npc.getCombatDef().isTypeDragon()
            && (weaponId == ItemId.DRAGON_HUNTER_CROSSBOW
                || weaponId == ItemId.DRAGON_HUNTER_CROSSBOW_T
                || weaponId == ItemId.DRAGON_HUNTER_CROSSBOW_B)) {
          return 1.3;
        }

        if (weaponId == ItemId.CRAWS_BOW || weaponId == ItemId.WEBWEAVER_BOW) {
          if (player.getEquipment().getItem(Equipment.Slot.WEAPON).getCharges() > 1000
              && player.getArea().inWilderness()) {
            return 1.5;
          }
        }
      }
    }
    return 1;
  }
}
