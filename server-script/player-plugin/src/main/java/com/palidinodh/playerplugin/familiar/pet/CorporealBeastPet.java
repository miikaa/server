package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.familiar.Pet;

class CorporealBeastPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.PET_DARK_CORE, NpcId.DARK_CORE_388, NpcId.DARK_CORE));
    builder.entry(
        new Pet.Entry(
            ItemId.PET_CORPOREAL_CRITTER, NpcId.CORPOREAL_CRITTER, NpcId.CORPOREAL_CRITTER_8010));
    builder.optionVariation(
        (p, n) -> {
          switch (n.getId()) {
            case NpcId.CORPOREAL_CRITTER:
              p.getPlugin(FamiliarPlugin.class).transformPet(NpcId.DARK_CORE_388);
              break;
            case NpcId.DARK_CORE_388:
              p.getPlugin(FamiliarPlugin.class).transformPet(NpcId.CORPOREAL_CRITTER);
              break;
          }
        });
    return builder;
  }
}
