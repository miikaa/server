package com.palidinodh.playerplugin.boss;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.util.PArrayList;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

public class BossPlugin implements PlayerPlugin {

  public static final Tile TOB_CENTER_LOBBY = new Tile(3680, 3232);
  public static final Tile VERZIK_ENTER_TILE = new Tile(3168, 4298);
  public static final NpcSpawn VERZIK_SPAWN =
      new NpcSpawn(new Tile(3166, 4323), NpcId.VERZIK_VITUR_1040);

  @Inject private transient Player player;
  @Getter private transient PArrayList<MapObject> mapObjects = new PArrayList<>();
  @Getter private transient List<Item> items = new ArrayList<>();
  @Getter @Setter private transient int points;

  public static Npc getVerzikVitur(Player player) {
    return player
        .getController()
        .getNpc(
            NpcId.VERZIK_VITUR_1040,
            NpcId.VERZIK_VITUR_1040_8370,
            NpcId.VERZIK_VITUR_1040_8371,
            NpcId.VERZIK_VITUR_1265,
            NpcId.VERZIK_VITUR_1265_8373,
            NpcId.VERZIK_VITUR_1520,
            NpcId.VERZIK_VITUR_1520);
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("boss_instance_plugin_cancel")) {
      cancel();
      return Boolean.TRUE;
    }
    return null;
  }

  public void start(int npcId, boolean clanChat) {
    BossInstance.start(player, npcId, clanChat);
  }

  public void openBossInstanceDialogue(int npcId) {
    openBossInstanceDialogue(npcId, true);
  }

  public void openBossInstanceDialogue(int npcId, boolean clanChat) {
    if (clanChat) {
      player.openOptionsDialogue(
          new DialogueOption("Enter Public", (c, s) -> BossInstance.join(player, npcId, false)),
          new DialogueOption("Join Instance", (c, s) -> BossInstance.join(player, npcId, true)),
          new DialogueOption(
              "Start Instance", (c, s) -> BossInstance.start(player, npcId, clanChat)));
    } else {
      player.openOptionsDialogue(
          new DialogueOption("Enter Public", (c, s) -> BossInstance.join(player, npcId, false)),
          new DialogueOption(
              "Start Instance", (c, s) -> BossInstance.start(player, npcId, clanChat)));
    }
  }

  private void cancel() {
    var clanChatUsername = player.getMessaging().getClanChatUsername();
    var playerInstance =
        player.getWorld().getPlayerBossInstance(clanChatUsername, player.getController());
    if (playerInstance == null || !playerInstance.is(BossInstanceController.class)) {
      player.getGameEncoder().sendMessage("There is no boss instance for this Clan Chat.");
      return;
    }
    if (!player.getMessaging().canClanChatEvent()) {
      player
          .getGameEncoder()
          .sendMessage("Your Clan Chat privledges aren't high enough to do that.");
      return;
    }
    playerInstance.as(BossInstanceController.class).expire();
  }
}
