package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.familiar.Pet;
import com.palidinodh.util.PCollection;
import java.util.Map;

class PhoenixPet implements Pet.BuildType {

  private static final Map<Integer, Integer> RECOLORS =
      PCollection.toMap(
          ItemId.RED_FIRELIGHTER,
          NpcId.PHOENIX_7368,
          ItemId.GREEN_FIRELIGHTER,
          NpcId.PHOENIX,
          ItemId.BLUE_FIRELIGHTER,
          NpcId.PHOENIX_3078,
          ItemId.WHITE_FIRELIGHTER,
          NpcId.PHOENIX_3079,
          ItemId.PURPLE_FIRELIGHTER,
          NpcId.PHOENIX_3080);

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.PHOENIX, NpcId.PHOENIX_7368, NpcId.PHOENIX_7370));
    builder.entry(new Pet.Entry(ItemId.PHOENIX_24483, NpcId.PHOENIX, NpcId.PHOENIX_3081));
    builder.entry(new Pet.Entry(ItemId.PHOENIX_24484, NpcId.PHOENIX_3078, NpcId.PHOENIX_3082));
    builder.entry(new Pet.Entry(ItemId.PHOENIX_24485, NpcId.PHOENIX_3079, NpcId.PHOENIX_3083));
    builder.entry(new Pet.Entry(ItemId.PHOENIX_24486, NpcId.PHOENIX_3080, NpcId.PHOENIX_3084));
    builder.itemVariation(
        (p, n, i) -> {
          var recolor = RECOLORS.get(i.getId());
          if (recolor == null) {
            return;
          }
          if (i.getAmount() < 250) {
            p.getGameEncoder().sendMessage("You need 250 firelighters to do this.");
            return;
          }
          p.getPlugin(FamiliarPlugin.class).transformPet(recolor);
          i.remove(250);
        });
    return builder;
  }
}
