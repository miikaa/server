package com.palidinodh.playerplugin.combat.handler.item;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.KRAKEN_TENTACLE)
class KrakenTentacleItem implements ItemHandler {

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    if (ItemHandler.used(
        useItem, onItem, ItemId.KRAKEN_TENTACLE, ItemId.KRAKEN_ORNAMENT_KIT_60059)) {
      onItem.replace(new Item(ItemId.KRAKEN_TENTACLE_OR_60048));
      useItem.remove();
      return true;
    }
    return false;
  }
}
