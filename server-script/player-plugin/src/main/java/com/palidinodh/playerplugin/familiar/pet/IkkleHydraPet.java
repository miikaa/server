package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.familiar.Pet;
import com.palidinodh.random.PRandom;

class IkkleHydraPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.IKKLE_HYDRA, NpcId.IKKLE_HYDRA_8517, NpcId.IKKLE_HYDRA));
    builder.entry(
        new Pet.Entry(ItemId.IKKLE_HYDRA_22748, NpcId.IKKLE_HYDRA_8518, NpcId.IKKLE_HYDRA_8493));
    builder.entry(
        new Pet.Entry(ItemId.IKKLE_HYDRA_22750, NpcId.IKKLE_HYDRA_8519, NpcId.IKKLE_HYDRA_8494));
    builder.entry(
        new Pet.Entry(ItemId.IKKLE_HYDRA_22752, NpcId.IKKLE_HYDRA_8520, NpcId.IKKLE_HYDRA_8495));
    builder.optionVariation(
        (p, n) -> {
          switch (n.getId()) {
            case NpcId.IKKLE_HYDRA_8517:
              p.getPlugin(FamiliarPlugin.class)
                  .transformPet(
                      PRandom.arrayRandom(
                          NpcId.IKKLE_HYDRA_8518, NpcId.IKKLE_HYDRA_8519, NpcId.IKKLE_HYDRA_8520));
              break;
            case NpcId.IKKLE_HYDRA_8518:
              p.getPlugin(FamiliarPlugin.class)
                  .transformPet(
                      PRandom.arrayRandom(
                          NpcId.IKKLE_HYDRA_8517, NpcId.IKKLE_HYDRA_8519, NpcId.IKKLE_HYDRA_8520));
              break;
            case NpcId.IKKLE_HYDRA_8519:
              p.getPlugin(FamiliarPlugin.class)
                  .transformPet(
                      PRandom.arrayRandom(
                          NpcId.IKKLE_HYDRA_8517, NpcId.IKKLE_HYDRA_8518, NpcId.IKKLE_HYDRA_8520));
              break;
            case NpcId.IKKLE_HYDRA_8520:
              p.getPlugin(FamiliarPlugin.class)
                  .transformPet(
                      PRandom.arrayRandom(
                          NpcId.IKKLE_HYDRA_8517, NpcId.IKKLE_HYDRA_8518, NpcId.IKKLE_HYDRA_8519));
              break;
          }
        });
    return builder;
  }
}
