package com.palidinodh.playerplugin.combat.handler.item;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.CLEANING_CLOTH)
class CleaningClothItem implements ItemHandler {

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    var cleanedItem = useItem.getId() == ItemId.CLEANING_CLOTH ? onItem : useItem;
    switch (cleanedItem.getId()) {
      case ItemId.VOLCANIC_ABYSSAL_WHIP:
      case ItemId.FROZEN_ABYSSAL_WHIP:
        cleanedItem.replace(new Item(ItemId.ABYSSAL_WHIP));
        return true;
      case ItemId.DARK_BOW_12765:
      case ItemId.DARK_BOW_12766:
      case ItemId.DARK_BOW_12767:
      case ItemId.DARK_BOW_12768:
        cleanedItem.replace(new Item(ItemId.DARK_BOW));
        return true;
      case ItemId.PARTYHAT_SPECS:
        {
          player.getInventory().deleteItem(ItemId.PARTYHAT_SPECS);
          player.getInventory().addOrDropItem(ItemId.BLUE_PARTYHAT);
          player.getInventory().addOrDropItem(ItemId.SAGACIOUS_SPECTACLES);
          return true;
        }
      case ItemId.PIRATE_HAT_PATCH:
        {
          player.getInventory().deleteItem(ItemId.PIRATE_HAT_PATCH);
          player.getInventory().addOrDropItem(ItemId.BIG_PIRATE_HAT);
          player.getInventory().addOrDropItem(ItemId.RIGHT_EYE_PATCH);
          return true;
        }
      case ItemId.CAVALIER_MASK:
        {
          player.getInventory().deleteItem(ItemId.CAVALIER_MASK);
          player.getInventory().addOrDropItem(ItemId.BLACK_CAVALIER);
          player.getInventory().addOrDropItem(ItemId.HIGHWAYMAN_MASK);
          return true;
        }
      case ItemId.BERET_MASK:
        {
          player.getInventory().deleteItem(ItemId.BERET_MASK);
          player.getInventory().addOrDropItem(ItemId.BLACK_BERET);
          player.getInventory().addOrDropItem(ItemId.MIME_MASK);
          return true;
        }
      case ItemId.DOUBLE_EYE_PATCH:
        {
          player.getInventory().deleteItem(ItemId.DOUBLE_EYE_PATCH);
          player.getInventory().addOrDropItem(ItemId.LEFT_EYE_PATCH);
          player.getInventory().addOrDropItem(ItemId.RIGHT_EYE_PATCH);
          return true;
        }
      case ItemId.TOP_HAT_MONOCLE:
        {
          player.getInventory().deleteItem(ItemId.TOP_HAT_MONOCLE);
          player.getInventory().addOrDropItem(ItemId.TOP_HAT);
          player.getInventory().addOrDropItem(ItemId.MONOCLE);
          return true;
        }
    }
    return false;
  }
}
