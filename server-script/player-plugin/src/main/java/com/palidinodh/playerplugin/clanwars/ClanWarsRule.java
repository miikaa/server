package com.palidinodh.playerplugin.clanwars;

import com.palidinodh.cache.id.VarbitId;
import com.palidinodh.util.PCollection;
import java.util.List;
import java.util.Map;
import lombok.Getter;

@Getter
public enum ClanWarsRule {
  GAME_END(
      VarbitId.CLAN_WARS_GAME_END,
      ClanWarsRuleOption.LAST_TEAM_STANDING,
      ClanWarsRuleOption.KILLS_25,
      ClanWarsRuleOption.KILLS_50,
      ClanWarsRuleOption.KILLS_100,
      ClanWarsRuleOption.KILLS_200,
      ClanWarsRuleOption.KILLS_500,
      ClanWarsRuleOption.KILLS_1000,
      ClanWarsRuleOption.KILLS_5000,
      ClanWarsRuleOption.KILLS_10000,
      ClanWarsRuleOption.KING_80_POINTS,
      ClanWarsRuleOption.KING_250_POINTS,
      ClanWarsRuleOption.KING_750_POINTS,
      ClanWarsRuleOption.KING_1500_POINTS,
      ClanWarsRuleOption.KING_2500_POINTS,
      ClanWarsRuleOption.KING_4000_POINTS,
      ClanWarsRuleOption.KING_6000_POINTS,
      ClanWarsRuleOption.MOST_KILLS_5_MINS,
      ClanWarsRuleOption.MOST_KILLS_10_MINS,
      ClanWarsRuleOption.MOST_KILLS_20_MINS,
      ClanWarsRuleOption.MOST_KILLS_60_MINS,
      ClanWarsRuleOption.MOST_KILLS_120_MINS,
      ClanWarsRuleOption.ODDSKULL_100_POINTS,
      ClanWarsRuleOption.ODDSKULL_300_POINTS,
      ClanWarsRuleOption.ODDSKULL_500_POINTS),
  TEAM_CAP(
      VarbitId.CLAN_WARS_ALLOW_BLIGHTED_VLS,
      ClanWarsRuleOption.UNCAPPED,
      ClanWarsRuleOption.ONE_V_ONE,
      ClanWarsRuleOption.FIVE_V_FIVE,
      ClanWarsRuleOption.TEN_V_TEN,
      ClanWarsRuleOption.FIFTEEN_V_FIFTEEN,
      ClanWarsRuleOption.TWENTY_V_TWENTY,
      ClanWarsRuleOption.FIFTY_V_FIFTY,
      ClanWarsRuleOption.HUNDRED_V_HUNDRED),
  LEAVING_CHANNEL(
      VarbitId.CLAN_WARS_LEAVING_CHANNEL, ClanWarsRuleOption.DEATH, ClanWarsRuleOption.NO_PENALTY),
  REJOINING(
      VarbitId.CLAN_WARS_REJOINING,
      ClanWarsRuleOption.UNRESTRICTED,
      ClanWarsRuleOption._60_SECS_DELAY),
  MELEE(VarbitId.CLAN_WARS_MELEE, ClanWarsRuleOption.ALLOWED, ClanWarsRuleOption.DISABLED),
  RANGING(VarbitId.CLAN_WARS_RANGING, ClanWarsRuleOption.ALLOWED, ClanWarsRuleOption.DISABLED),
  MAGIC(
      VarbitId.CLAN_WARS_MAGIC,
      ClanWarsRuleOption.ALLOWED,
      ClanWarsRuleOption.STANDARD_SPELLS,
      ClanWarsRuleOption.BINDING_ONLY,
      ClanWarsRuleOption.DISABLED),
  PRAYER(
      VarbitId.CLAN_WARS_PRAYER,
      ClanWarsRuleOption.ALLOWED,
      ClanWarsRuleOption.NO_OVERHEADS,
      ClanWarsRuleOption.DISABLED),
  FOOD(VarbitId.CLAN_WARS_FOOD, ClanWarsRuleOption.ALLOWED, ClanWarsRuleOption.DISABLED),
  DRINKS(VarbitId.CLAN_WARS_DRINKS, ClanWarsRuleOption.ALLOWED, ClanWarsRuleOption.DISABLED),
  SPECIAL_ATTACKS(
      VarbitId.CLAN_WARS_SPECIAL_ATTACKS,
      ClanWarsRuleOption.ALLOWED,
      ClanWarsRuleOption.NO_STAFF_OF_THE_DEAD,
      ClanWarsRuleOption.DISABLED),
  STRAGGLERS(
      VarbitId.CLAN_WARS_STRAGGLERS, ClanWarsRuleOption.KILL_EM_ALL, ClanWarsRuleOption.IGNORE_5),
  DROP_ITEMS_ON_DEATH(
      VarbitId.CLAN_WARS_DROP_ITEMS_ON_DEATH_31746, ClanWarsRuleOption.OFF, ClanWarsRuleOption.ON),
  ONE_DEFENCE_MODE(
      VarbitId.CLAN_WARS_1_DEFENCE_MODE_31744, ClanWarsRuleOption.OFF, ClanWarsRuleOption.ON),
  F2P_CONTENT_ONLY(
      VarbitId.CLAN_WARS_F2P_CONTENT_ONLY_31745, ClanWarsRuleOption.OFF, ClanWarsRuleOption.ON),
  IGNORE_FREEZING(
      VarbitId.CLAN_WARS_IGNORE_FREEZING, ClanWarsRuleOption.OFF, ClanWarsRuleOption.ON),
  PJ_TIMER(VarbitId.CLAN_WARS_PJ_TIMER, ClanWarsRuleOption.OFF, ClanWarsRuleOption.ON),
  ALLOW_POWERED_STAVES(
      VarbitId.CLAN_WARS_ALLOW_POWERED_STAVES, ClanWarsRuleOption.OFF, ClanWarsRuleOption.ON),
  SINGLE_SPELLS(VarbitId.CLAN_WARS_SINGLE_SPELLS, ClanWarsRuleOption.OFF, ClanWarsRuleOption.ON),
  ALLOW_BLIGHTED_VLS(
      VarbitId.CLAN_WARS_ALLOW_BLIGHTED_VLS, ClanWarsRuleOption.OFF, ClanWarsRuleOption.ON),
  NO_SNARE_ENTANGLE(
      VarbitId.CLAN_WARS_NO_SNARE_ENTANGLE_31747, ClanWarsRuleOption.OFF, ClanWarsRuleOption.ON),
  BROADCAST_OUTCOME(
      VarbitId.CLAN_WARS_BROADCAST_OUTCOME_31748, ClanWarsRuleOption.OFF, ClanWarsRuleOption.ON),
  ARENA(
      VarbitId.CLAN_WARS_ARENA,
      ClanWarsRuleOption.WASTELAND,
      ClanWarsRuleOption.PLATEAU,
      ClanWarsRuleOption.SYLVAN_GLADE,
      ClanWarsRuleOption.FORSAKEN_QUARRY,
      ClanWarsRuleOption.TURRETS,
      ClanWarsRuleOption.CLAN_CUP,
      ClanWarsRuleOption.SOGGY_SWAMP,
      ClanWarsRuleOption.GHASTLY_SWAMP,
      ClanWarsRuleOption.NORTHLEACH_QUELL,
      ClanWarsRuleOption.GRIDLOCK,
      ClanWarsRuleOption.ETHREAL,
      ClanWarsRuleOption.CLASSIC,
      ClanWarsRuleOption.LUMBRIDGE,
      ClanWarsRuleOption.FALADOR);

  public static final int TOTAL = values().length;
  private static final int[] DEFAULT = new int[TOTAL];
  private static final Map<Integer, String> DESCRIPTIONS;

  static {
    DESCRIPTIONS =
        Map.ofEntries(
            Map.entry(
                GAME_END.getKey(ClanWarsRuleOption.LAST_TEAM_STANDING),
                "The battle ends when all members of a team have been defeated.<br>"
                    + "Fighters may not join or re-join the battle after it has begun.<br><br>"),
            Map.entry(
                GAME_END.getKey(ClanWarsRuleOption.KILLS_25),
                "The first team to score 25 kills will win.<br>"
                    + "Fighters may enter the battle at any time.<br><br>"),
            Map.entry(
                GAME_END.getKey(ClanWarsRuleOption.KILLS_50),
                "The first team to score 50 kills will win.<br>"
                    + "Fighters may enter the battle at any time.<br><br>"),
            Map.entry(
                GAME_END.getKey(ClanWarsRuleOption.KILLS_100),
                "The first team to score 100 kills will win.<br>"
                    + "Fighters may enter the battle at any time.<br><br>"),
            Map.entry(
                GAME_END.getKey(ClanWarsRuleOption.KILLS_200),
                "The first team to score 200 kills will win.<br>"
                    + "Fighters may enter the battle at any time.<br><br>"),
            Map.entry(
                GAME_END.getKey(ClanWarsRuleOption.KILLS_500),
                "The first team to score 500 kills will win.<br>"
                    + "Fighters may enter the battle at any time.<br><br>"),
            Map.entry(
                GAME_END.getKey(ClanWarsRuleOption.KILLS_1000),
                "The first team to score 1,000 kills will win.<br>"
                    + "Fighters may enter the battle at any time.<br><br>"),
            Map.entry(
                GAME_END.getKey(ClanWarsRuleOption.KILLS_5000),
                "The first team to score 5,000 kills will win.<br>"
                    + "Fighters may enter the battle at any time.<br><br>"),
            Map.entry(
                GAME_END.getKey(ClanWarsRuleOption.KILLS_10000),
                "The first team to score 10,000 kills will win.<br>"
                    + "Fighters may enter the battle at any time.<br><br>"),
            Map.entry(
                MELEE.getKey(ClanWarsRuleOption.ALLOWED),
                "<col=EEEEEE>Melee:</col> Melee combat is allowed.<br>"),
            Map.entry(
                MELEE.getKey(ClanWarsRuleOption.DISABLED),
                "<col=EEEEEE>Melee:</col> Melee combat is disabled.<br>"),
            Map.entry(
                RANGING.getKey(ClanWarsRuleOption.ALLOWED),
                "<col=EEEEEE>Ranging:</col> Ranging is allowed.<br>"),
            Map.entry(
                RANGING.getKey(ClanWarsRuleOption.DISABLED),
                "<col=EEEEEE>Ranging:</col> Ranging is disabled.<br>"),
            Map.entry(
                MAGIC.getKey(ClanWarsRuleOption.ALLOWED),
                "<col=EEEEEE>Magic:</col> All spellbooks are allowed.<br><br>"),
            Map.entry(
                MAGIC.getKey(ClanWarsRuleOption.STANDARD_SPELLS),
                "<col=EEEEEE>Magic:</col> Only the standard spellbook is allowed.<br><br>"),
            Map.entry(
                MAGIC.getKey(ClanWarsRuleOption.BINDING_ONLY),
                "<col=EEEEEE>Magic:</col> Only the Bind, Snare and Entangle spells are allowed.<br><br>"),
            Map.entry(
                MAGIC.getKey(ClanWarsRuleOption.DISABLED),
                "<col=EEEEEE>Magic:</col> No magical combat is allowed.<br><br>"),
            Map.entry(
                PRAYER.getKey(ClanWarsRuleOption.ALLOWED),
                "<col=EEEEEE>Prayer:</col> All prayers are allowed.<br><br>"),
            Map.entry(
                PRAYER.getKey(ClanWarsRuleOption.NO_OVERHEADS),
                "<col=EEEEEE>Prayer:</col> Prayers that use an overhead icon are forbidden. "
                    + "Other prayers are allowed.<br><br>"),
            Map.entry(
                PRAYER.getKey(ClanWarsRuleOption.DISABLED),
                "<col=EEEEEE>Prayer:</col> No prayers are allowed.<br><br>"),
            Map.entry(
                FOOD.getKey(ClanWarsRuleOption.ALLOWED),
                "<col=EEEEEE>Food:</col> Food may be eaten during the battle.<br>"),
            Map.entry(
                FOOD.getKey(ClanWarsRuleOption.DISABLED),
                "<col=EEEEEE>Food:</col> No food may be eaten during the battle.<br>"),
            Map.entry(
                DRINKS.getKey(ClanWarsRuleOption.ALLOWED),
                "<col=EEEEEE>Drinks:</col> Drinks, such as potions, may be used during the battle.<br><br>"),
            Map.entry(
                DRINKS.getKey(ClanWarsRuleOption.DISABLED),
                "<col=EEEEEE>Drinks:</col> No drinks may be consumed during the battle.<br><br>"),
            Map.entry(
                SPECIAL_ATTACKS.getKey(ClanWarsRuleOption.ALLOWED),
                "Special attacks are allowed.<br><br>"),
            Map.entry(
                SPECIAL_ATTACKS.getKey(ClanWarsRuleOption.NO_STAFF_OF_THE_DEAD),
                "The Staff of the Dead cannot use its special attack, but all other special attacks are allowed.<br><br>"),
            Map.entry(
                SPECIAL_ATTACKS.getKey(ClanWarsRuleOption.DISABLED),
                "Special attacks are forbidden.<br><br>"),
            Map.entry(
                BROADCAST_OUTCOME.getKey(ClanWarsRuleOption.ON),
                "The outcome of the war will be broadcasted.<br>"),
            Map.entry(BROADCAST_OUTCOME.getKey(ClanWarsRuleOption.OFF), ""),
            Map.entry(
                DROP_ITEMS_ON_DEATH.getKey(ClanWarsRuleOption.ON),
                "Items will be lost and dropped on death.<br>"),
            Map.entry(DROP_ITEMS_ON_DEATH.getKey(ClanWarsRuleOption.OFF), ""),
            Map.entry(
                ONE_DEFENCE_MODE.getKey(ClanWarsRuleOption.ON),
                "Only players with a Defence level of 1 can participate in this battle.<br>"),
            Map.entry(ONE_DEFENCE_MODE.getKey(ClanWarsRuleOption.OFF), ""),
            Map.entry(
                F2P_CONTENT_ONLY.getKey(ClanWarsRuleOption.ON),
                "Only F2P content is allowed in this battle.<br>"),
            Map.entry(F2P_CONTENT_ONLY.getKey(ClanWarsRuleOption.OFF), ""),
            Map.entry(
                IGNORE_FREEZING.getKey(ClanWarsRuleOption.ON),
                "Spells such as Entangle and Ice Barrage will not prevent their targets from moving.<br>"),
            Map.entry(IGNORE_FREEZING.getKey(ClanWarsRuleOption.OFF), ""),
            Map.entry(
                PJ_TIMER.getKey(ClanWarsRuleOption.ON),
                "In single-way combat areas, players are prevented from being attacked for 10 secs after they have been attacking someone else.<br>"),
            Map.entry(PJ_TIMER.getKey(ClanWarsRuleOption.OFF), ""),
            Map.entry(
                SINGLE_SPELLS.getKey(ClanWarsRuleOption.ON),
                "Multi-target attacks (such as chinchompas) will hit only one target, even in multi-way combat areas.<br>"),
            Map.entry(SINGLE_SPELLS.getKey(ClanWarsRuleOption.OFF), ""),
            Map.entry(
                ALLOW_POWERED_STAVES.getKey(ClanWarsRuleOption.ON),
                "The Trident of the Seas is able to cast its spell on players.<br>"),
            Map.entry(ALLOW_POWERED_STAVES.getKey(ClanWarsRuleOption.OFF), ""),
            Map.entry(
                ALLOW_BLIGHTED_VLS.getKey(ClanWarsRuleOption.ON),
                "Allow use of the blighted Vesta's longsword.<br>"),
            Map.entry(ALLOW_BLIGHTED_VLS.getKey(ClanWarsRuleOption.OFF), ""),
            Map.entry(
                NO_SNARE_ENTANGLE.getKey(ClanWarsRuleOption.ON),
                "Snare and entangle are unable to be cast.<br>"),
            Map.entry(NO_SNARE_ENTANGLE.getKey(ClanWarsRuleOption.OFF), ""));
  }

  private final int varbit;
  private final List<ClanWarsRuleOption> options;

  ClanWarsRule(int varbit, ClanWarsRuleOption... options) {
    this.varbit = varbit;
    this.options = PCollection.toList(options);
  }

  public static ClanWarsRule getByIndex(int index) {
    var values = values();
    if (index < 0 || index >= values.length) {
      return null;
    }
    return values[index];
  }

  public static int[] getDefault() {
    var rules = new int[TOTAL];
    System.arraycopy(DEFAULT, 0, rules, 0, TOTAL);
    return rules;
  }

  public static String getDescriptions(int[] rules) {
    return "<u=FFFFFF><col=FFFFFF>The Game:</col></u><br><br>"
        + DESCRIPTIONS.get(GAME_END.getKey(rules[GAME_END.ordinal()]))
        + "<u=FFFFFF><col=FFFFFF>The Arena:</col></u> "
        + Arena.get(rules[ARENA.ordinal()]).getName()
        + "<br><br>"
        + "<u=FFFFFF><col=FFFFFF>The Combat:</col></u><br><br>"
        + DESCRIPTIONS.get(MELEE.getKey(rules[MELEE.ordinal()]))
        + DESCRIPTIONS.get(RANGING.getKey(rules[RANGING.ordinal()]))
        + DESCRIPTIONS.get(MAGIC.getKey(rules[MAGIC.ordinal()]))
        + DESCRIPTIONS.get(PRAYER.getKey(rules[PRAYER.ordinal()]))
        + DESCRIPTIONS.get(FOOD.getKey(rules[FOOD.ordinal()]))
        + DESCRIPTIONS.get(DRINKS.getKey(rules[DRINKS.ordinal()]))
        + "<col=EEEEEE>Special attacks:</col><br>"
        + DESCRIPTIONS.get(SPECIAL_ATTACKS.getKey(rules[SPECIAL_ATTACKS.ordinal()]))
        + "<u=FFFFFF><col=FFFFFF>Other settings:</col></u><br><br>"
        + DESCRIPTIONS.get(DROP_ITEMS_ON_DEATH.getKey(rules[DROP_ITEMS_ON_DEATH.ordinal()]))
        + DESCRIPTIONS.get(ONE_DEFENCE_MODE.getKey(rules[ONE_DEFENCE_MODE.ordinal()]))
        + DESCRIPTIONS.get(F2P_CONTENT_ONLY.getKey(rules[F2P_CONTENT_ONLY.ordinal()]))
        + DESCRIPTIONS.get(IGNORE_FREEZING.getKey(rules[IGNORE_FREEZING.ordinal()]))
        + DESCRIPTIONS.get(PJ_TIMER.getKey(rules[PJ_TIMER.ordinal()]))
        + DESCRIPTIONS.get(SINGLE_SPELLS.getKey(rules[SINGLE_SPELLS.ordinal()]))
        + DESCRIPTIONS.get(ALLOW_POWERED_STAVES.getKey(rules[ALLOW_POWERED_STAVES.ordinal()]))
        + DESCRIPTIONS.get(ALLOW_BLIGHTED_VLS.getKey(rules[ALLOW_BLIGHTED_VLS.ordinal()]))
        + DESCRIPTIONS.get(NO_SNARE_ENTANGLE.getKey(rules[NO_SNARE_ENTANGLE.ordinal()]));
  }

  public ClanWarsRuleOption getOption(int index) {
    if (index < 0 || index >= options.size()) {
      return null;
    }
    return options.get(index);
  }

  public boolean hasOption(ClanWarsRuleOption option) {
    return options.contains(option);
  }

  public int getIndex(ClanWarsRuleOption option) {
    for (var i = 0; i < options.size(); i++) {
      if (option != options.get(i)) {
        continue;
      }
      return i;
    }
    return -1;
  }

  public int getKey(ClanWarsRuleOption option) {
    return ordinal() << 16 | getIndex(option);
  }

  public int getKey(int index) {
    return getKey(getOption(index));
  }
}
