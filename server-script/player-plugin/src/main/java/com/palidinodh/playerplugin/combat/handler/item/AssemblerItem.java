package com.palidinodh.playerplugin.combat.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.AVAS_ASSEMBLER,
  ItemId.MASORI_ASSEMBLER,
  ItemId.ASSEMBLER_MAX_CAPE,
  ItemId.MASORI_ASSEMBLER_MAX_CAPE
})
class AssemblerItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (item.getId()) {
      case ItemId.MASORI_ASSEMBLER:
        {
          switch (option.getText()) {
            case "dismantle":
              {
                if (!player.getInventory().hasItem(ItemId.NEEDLE)) {
                  player.getGameEncoder().sendMessage("You need a needle to do this.");
                  break;
                }
                if (player.getInventory().getRemainingSlots() < 1) {
                  player.getInventory().notEnoughSpace();
                  break;
                }
                item.replace(new Item(ItemId.AVAS_ASSEMBLER));
                player.getInventory().addOrDropItem(ItemId.MASORI_CRAFTING_KIT);
                break;
              }
          }
          break;
        }
    }
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    var onSlot = onItem.getSlot();
    if (ItemHandler.used(useItem, onItem, ItemId.AVAS_ASSEMBLER, ItemId.MASORI_CRAFTING_KIT)) {
      if (!player.getInventory().hasItem(ItemId.NEEDLE)) {
        player.getGameEncoder().sendMessage("You need a needle to do this.");
        return true;
      }
      useItem.remove();
      onItem.replace(new Item(ItemId.MASORI_ASSEMBLER));
      return true;
    }
    if (ItemHandler.used(useItem, onItem, ItemId.ASSEMBLER_MAX_CAPE, ItemId.MASORI_CRAFTING_KIT)) {
      if (!player.getInventory().hasItem(ItemId.NEEDLE)) {
        player.getGameEncoder().sendMessage("You need a needle to do this.");
        return true;
      }
      useItem.remove();
      onItem.replace(new Item(ItemId.MASORI_ASSEMBLER_MAX_CAPE));
      if (player.getInventory().hasItem(ItemId.ASSEMBLER_MAX_HOOD)) {
        player
            .getInventory()
            .getItemById(ItemId.ASSEMBLER_MAX_HOOD)
            .replace(new Item(ItemId.MASORI_ASSEMBLER_MAX_HOOD));
      }
      return true;
    }
    if (ItemHandler.used(useItem, onItem, ItemId.AVAS_ASSEMBLER, ItemId.MAX_CAPE)) {
      if (!player.getInventory().hasItem(ItemId.MAX_HOOD)) {
        player.getGameEncoder().sendMessage("You need a Max hood to do this.");
        return true;
      }
      player
          .getInventory()
          .getItemById(ItemId.MAX_HOOD)
          .replace(new Item(ItemId.ASSEMBLER_MAX_HOOD));
      onItem.replace(new Item(ItemId.ASSEMBLER_MAX_CAPE));
      useItem.remove();
      return true;
    }
    if (ItemHandler.used(useItem, onItem, ItemId.MASORI_ASSEMBLER, ItemId.MAX_CAPE)) {
      if (!player.getInventory().hasItem(ItemId.MAX_HOOD)) {
        player.getGameEncoder().sendMessage("You need a Max hood to do this.");
        return true;
      }
      player
          .getInventory()
          .getItemById(ItemId.MAX_HOOD)
          .replace(new Item(ItemId.MASORI_ASSEMBLER_MAX_HOOD));
      onItem.replace(new Item(ItemId.MASORI_ASSEMBLER_MAX_CAPE));
      useItem.remove();
      return true;
    }
    return false;
  }
}
