package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.familiar.Pet;

class GiantSquirrelPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(
        new Pet.Entry(ItemId.GIANT_SQUIRREL, NpcId.GIANT_SQUIRREL, NpcId.GIANT_SQUIRREL_7351));
    builder.entry(
        new Pet.Entry(ItemId.DARK_SQUIRREL, NpcId.DARK_SQUIRREL_9638, NpcId.DARK_SQUIRREL));
    builder.itemVariation(
        (p, n, i) -> {
          var plugin = p.getPlugin(FamiliarPlugin.class);
          if (plugin.getUnlockedVariations().contains(ItemId.DARK_SQUIRREL)) {
            p.getGameEncoder().sendMessage("You have already unlocked this.");
            return;
          }
          plugin.getUnlockedVariations().add(ItemId.DARK_SQUIRREL);
          p.getPlugin(FamiliarPlugin.class).transformPet(NpcId.DARK_SQUIRREL_9638);
          i.remove();
        });
    builder.optionVariation(
        (p, n) -> {
          switch (n.getId()) {
            case NpcId.GIANT_SQUIRREL:
              {
                var plugin = p.getPlugin(FamiliarPlugin.class);
                if (!plugin.getUnlockedVariations().contains(ItemId.DARK_SQUIRREL)) {
                  return;
                }
                p.getPlugin(FamiliarPlugin.class).transformPet(NpcId.DARK_SQUIRREL_9638);
                break;
              }
            case NpcId.DARK_SQUIRREL_9638:
              p.getPlugin(FamiliarPlugin.class).transformPet(NpcId.GIANT_SQUIRREL);
              break;
          }
        });
    return builder;
  }
}
