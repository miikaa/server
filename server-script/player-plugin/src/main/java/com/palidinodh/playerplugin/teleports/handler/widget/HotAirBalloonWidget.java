package com.palidinodh.playerplugin.teleports.handler.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.teleports.HotAirBalloonType;
import com.palidinodh.playerplugin.teleports.TeleportsPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.HOT_AIR_BALLOON_TRANSPORT)
class HotAirBalloonWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    player.getWidgetManager().removeInteractiveWidgets();
    var type = HotAirBalloonType.getByWidgetChildId(childId);
    if (type == null) {
      return;
    }
    var plugin = player.getPlugin(TeleportsPlugin.class);
    if (!plugin.getHotAirBalloons().contains(type)) {
      plugin.getHotAirBalloons().add(type);
      player.getGameEncoder().setVarbit(type.getVarbit(), 1);
    }
    player.getMovement().teleport(type.getTile());
  }
}
