package com.palidinodh.playerplugin.herblore;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Equipment;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.random.PRandom;
import lombok.Getter;

public class HerblorePlugin implements PlayerPlugin {

  private static final int AMULET_OF_CHEMISTRY_CHARGES = 5;

  @Inject private transient Player player;

  @Getter private int amuletOfChemistryCharges = AMULET_OF_CHEMISTRY_CHARGES;

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("deincriment_amulet_of_chemistry_charges")) {
      return deincrimentAmuletOfChemistryCharges() ? Boolean.TRUE : Boolean.FALSE;
    }
    return null;
  }

  public void breakAmuletOfChemistry() {
    if (amuletOfChemistryCharges == AMULET_OF_CHEMISTRY_CHARGES) {
      player.getGameEncoder().sendMessage("Your amulet of chemistry is fully charged.");
      return;
    }
    amuletOfChemistryCharges = AMULET_OF_CHEMISTRY_CHARGES;
    player.getEquipment().setItem(Equipment.Slot.NECK, null);
  }

  public boolean deincrimentAmuletOfChemistryCharges() {
    if (player.getEquipment().getNeckId() != ItemId.AMULET_OF_CHEMISTRY) {
      return false;
    }
    if (!PRandom.inRange(1, 20)) {
      return false;
    }
    if (--amuletOfChemistryCharges > 0) {
      player
          .getGameEncoder()
          .sendMessage(
              "Your amulet of chemistry helps you create a 4-dose potion. <col=ff0000>It has "
                  + amuletOfChemistryCharges
                  + " charges left.</col>");
      return true;
    }
    amuletOfChemistryCharges = AMULET_OF_CHEMISTRY_CHARGES;
    player.getEquipment().setItem(Equipment.Slot.NECK, null);
    player
        .getGameEncoder()
        .sendMessage(
            "Your amulet of chemistry helps you create a 4-dose potion. <col=ff0000>It then crumbles to dust.</col>");
    return true;
  }
}
