package com.palidinodh.playerplugin.clanwars;

import com.google.inject.Inject;
import com.palidinodh.cache.definition.osrs.ObjectDefinition;
import com.palidinodh.cache.id.NullObjectId;
import com.palidinodh.cache.id.ScriptId;
import com.palidinodh.cache.id.VarbitId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.player.Messaging;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PTime;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
public class ClanWarsPlugin implements PlayerPlugin {

  public static final int COUNT_DOWN = 200;

  @Getter @Setter private static boolean enabled = true;

  @Inject private transient Player player;
  @Setter private transient Player opponent;
  @Setter private transient Player teammate;
  @Setter private transient ClanWarsPlayerState state = ClanWarsPlayerState.NONE;
  @Setter private transient CompletedState completed = CompletedState.NONE;
  private transient ClanWarsWarState warState = new ClanWarsWarState(this);
  private transient int[] rules;
  private transient boolean inClanWarsChallengeArea;
  private transient boolean inTournamentPlayerStateArea;
  @Setter private transient int tournamentFightDelay;

  private int tournamentWins;
  @Setter private int points;

  @Override
  public int getCurrency(String identifier) {
    if (identifier.equals("tournament points")) {
      return points;
    }
    return 0;
  }

  @Override
  public void changeCurrency(String identifier, int amount) {
    if (identifier.equals("tournament points")) {
      points += amount;
    }
  }

  @Override
  public void tick() {
    if (inClanWarsChallengeArea != player.inClanWarsChallengeArea()) {
      inClanWarsChallengeArea = player.inClanWarsChallengeArea();
      player
          .getGameEncoder()
          .sendPlayerOption(inClanWarsChallengeArea ? "Challenge" : "null", 1, false);
    }
    var inClanWarsTournamentStatusArea = player.inClanWarsTournamentStatusArea();
    if (inTournamentPlayerStateArea
        || inTournamentPlayerStateArea != inClanWarsTournamentStatusArea) {
      inTournamentPlayerStateArea = inClanWarsTournamentStatusArea;
      if (inTournamentPlayerStateArea) {
        if (player.getWidgetManager().getOverlay() != WidgetId.LMS_LOBBY_OVERLAY) {
          player.getWidgetManager().sendOverlay(WidgetId.LMS_LOBBY_OVERLAY);
          player.getGameEncoder().sendWidgetText(WidgetId.LMS_LOBBY_OVERLAY, 7, "");
          player.getGameEncoder().sendWidgetText(WidgetId.LMS_LOBBY_OVERLAY, 9, "");
          player.getGameEncoder().sendWidgetText(WidgetId.LMS_LOBBY_OVERLAY, 11, "");
        }
      } else if (!inTournamentPlayerStateArea
          && player.getWidgetManager().getOverlay() == WidgetId.LMS_LOBBY_OVERLAY) {
        player.getWidgetManager().removeOverlay();
      }
    }
    if (warState.getCountdown() > 0) {
      warState.setCountdown(warState.getCountdown() - 1);
      if (state == ClanWarsPlayerState.BATTLE) {
        if (warState.getCountdown() == 4) {
          loadBarrier(BarrierState.DROP);
        } else if (warState.getCountdown() == 0) {
          loadBarrier(BarrierState.DELETE);
          player.getGameEncoder().sendMessage("<col=ff0000>The war has begun!");
        }
      }
    }
    if (state == ClanWarsPlayerState.BATTLE) {
      warState.setTime(warState.getTime() + 1);
    }
    if (tournamentFightDelay > 0) {
      tournamentFightDelay--;
      if (tournamentFightDelay == 0) {
        player.setForceMessage("FIGHT!");
      } else if (tournamentFightDelay % 2 == 0) {
        player.setForceMessage(String.valueOf(PTime.tickToSec(tournamentFightDelay)));
      }
    }
  }

  @Override
  public boolean playerOptionHook(int option, Player player2) {
    if (option == 0 && player.inClanWarsChallengeArea() && player2.inClanWarsChallengeArea()) {
      if (state != ClanWarsPlayerState.NONE) {
        return true;
      }
      if (player2.getPlugin(ClanWarsPlugin.class).getState() != ClanWarsPlayerState.NONE) {
        return true;
      }
      if (!player.getMessaging().canClanChatEvent()) {
        player
            .getGameEncoder()
            .sendMessage("Your Clan Chat privledges aren't high enough to do that.");
        return true;
      }
      if (!player2.getMessaging().canClanChatEvent()) {
        player
            .getGameEncoder()
            .sendMessage("Their Clan Chat privledges aren't high enough to do that.");
        return true;
      }
      if (!enabled) {
        player.getGameEncoder().sendMessage("Clan Wars is currently disabled.");
        return true;
      }
      opponent = player2;
      if (player == player2.getPlugin(ClanWarsPlugin.class).getOpponent()) {
        ClanWarsStages.openRuleSelection(player);
        ClanWarsStages.openRuleSelection(player2);
      } else {
        player.getGameEncoder().sendMessage("Sending Clan Wars challenge...");
        player2
            .getGameEncoder()
            .sendMessage(
                player.getUsername() + " wishes to challenge your clan to a Clan War.",
                Messaging.CHAT_TYPE_DUEL,
                player.getUsername());
      }
      return true;
    }
    return false;
  }

  public void startWar(boolean top) {
    if (opponent == null) {
      player.getWidgetManager().removeInteractiveWidgets();
      player
          .getGameEncoder()
          .sendMessage("There was a problem finding your opponent, please try again.");
      return;
    }
    player.getWidgetManager().removeWidgetCloseEvents();
    player.getWidgetManager().removeInteractiveWidgets();
    state = ClanWarsPlayerState.BATTLE;
    warState.reset();
    warState.setCountdown(Settings.getInstance().isLocal() ? 10 : COUNT_DOWN);
    warState.setTop(top);
    warState.setClanNames(
        player.getMessaging().getClanChatName(),
        opponent.getMessaging().getClanChatName(),
        player.getMessaging().getClanChatUsername(),
        opponent.getMessaging().getClanChatUsername());
    player.rejuvenate();
    player.setController(new ClanWarsPC());
    if (top) {
      player.getController().startInstance();
    } else {
      player.getController().joinInstance(opponent.getController());
    }
    loadBarrier(BarrierState.LOAD);
    teleport();
  }

  public void joinWar(ClanWarsPlayerState playerState) {
    if (teammate == null) {
      player.getWidgetManager().removeInteractiveWidgets();
      player.getGameEncoder().sendMessage("There was a problem finding the war, please try again.");
      return;
    }
    if (teammate.getPlugin(ClanWarsPlugin.class).getState() != ClanWarsPlayerState.BATTLE) {
      player.getWidgetManager().removeInteractiveWidgets();
      player.getGameEncoder().sendMessage("There was a problem finding the war, please try again.");
      return;
    }
    player.getWidgetManager().removeWidgetCloseEvents();
    player.getWidgetManager().removeInteractiveWidgets();
    state = playerState;
    warState.copy(teammate.getPlugin(ClanWarsPlugin.class).getWarState());
    player.rejuvenate();
    player.setController(new ClanWarsPC());
    player.getController().joinInstance(teammate.getController());
    switch (state) {
      case BATTLE:
        warState.teammateJoined();
        teleport();
        break;
      case VIEW:
        teleportViewing();
        break;
    }
  }

  public void cancel() {
    opponent = null;
    teammate = null;
    state = ClanWarsPlayerState.NONE;
    rules = null;
    warState.reset();
    completed = CompletedState.NONE;
  }

  public void teleport() {
    if (rules == null) {
      return;
    }
    player.getMovement().teleport(getArena().getArenaTile(warState.isTop()));
  }

  public void teleportViewing() {
    if (rules == null) {
      return;
    }
    Arena arena = getArena();
    if (arena == null) {
      return;
    }
    player.getMovement().teleport(arena.getViewTile(warState.isTop()));
  }

  public void loadBarrier(BarrierState stage) {
    List<MapObject> barriers = null;
    var alreadyLoaded = false;
    if (getArena().getBarrierObjectId() == -1) {
      return;
    }
    for (var i = getArena().getBarrierStartX(); i <= getArena().getBarrierEndX(); i++) {
      if (ruleSelected(ClanWarsRule.ARENA, ClanWarsRuleOption.SYLVAN_GLADE)
          && i > 3419
          && i < 3428) {
        continue;
      } else if (ruleSelected(ClanWarsRule.ARENA, ClanWarsRuleOption.FORSAKEN_QUARRY)
          && i > 3415
          && i < 3432) {
        continue;
      }
      var barrier =
          new MapObject(
              getArena().getBarrierObjectId(),
              10,
              MapObject.getRandomDirection(),
              new Tile(i, getArena().getBarrierY()));
      if (stage == BarrierState.DROP) {
        barrier.setId(getArena().getBarrierObjectId() + 1);
      } else if (stage == BarrierState.DELETE) {
        barrier.setId(-1);
      }
      var region = player.getController().getRegion(barrier.getRegionId(), true);
      var current = region.getSolidMapObject(i, getArena().getBarrierY(), 0);
      if (current != null && current.getId() == barrier.getId()) {
        alreadyLoaded = true;
        break;
      }
      if (stage != BarrierState.DELETE
          && current != null
          && current.getId() != getArena().getBarrierObjectId()
          && current.getId() + 1 != getArena().getBarrierObjectId()) {
        continue;
      }
      if (current != null) {
        barrier.setDirection(current.getDirection());
      }
      if (current != null) {
        region.addMapObject(new MapObject(-1, current));
      }
      if (stage != BarrierState.DELETE) {
        region.addMapObject(barrier);
      }
      if (barriers == null) {
        barriers = new ArrayList<>();
      }
      barriers.add(barrier);
    }
    if (alreadyLoaded) {
      return;
    }
    var players = player.getController().getPlayers();
    if (players.isEmpty() || barriers == null) {
      return;
    }
    for (var nearbyPlayer : players) {
      for (var barrier : barriers) {
        if (!nearbyPlayer.withinMapDistance(barrier)) {
          continue;
        }
        nearbyPlayer.getGameEncoder().sendMapObject(barrier);
      }
    }
  }

  public boolean ruleSelected(ClanWarsRule rule, ClanWarsRuleOption option) {
    if (state == ClanWarsPlayerState.NONE || rules == null || rule == null || option == null) {
      return false;
    }
    return rule.getOption(rules[rule.ordinal()]) == option;
  }

  public void changeRule(ClanWarsRule rule, ClanWarsRuleOption option) {
    if (rule == null || option == null || !rule.hasOption(option)) {
      return;
    }
    changeRule(rule, rule.getIndex(option));
  }

  public void changeRule(ClanWarsRule rule, int slot) {
    if (state != ClanWarsPlayerState.RULE_SELECTION
        && state != ClanWarsPlayerState.ACCEPT_RULE_SELECTION) {
      return;
    }
    if (opponent == null || opponent.isLocked()) {
      return;
    }
    if (!player.getWidgetManager().hasWidget(WidgetId.CLAN_WARS_OPTIONS)
        || !opponent.getWidgetManager().hasWidget(WidgetId.CLAN_WARS_OPTIONS)) {
      return;
    }
    if (opponent.getPlugin(ClanWarsPlugin.class).getState() != ClanWarsPlayerState.RULE_SELECTION
        && opponent.getPlugin(ClanWarsPlugin.class).getState()
            != ClanWarsPlayerState.ACCEPT_RULE_SELECTION) {
      return;
    }
    if (rule == null || slot < 0 || slot >= rule.getOptions().size()) {
      return;
    }
    rules[rule.ordinal()] = slot;
    opponent.getPlugin(ClanWarsPlugin.class).setRule(rule.ordinal(), slot);
    player.getGameEncoder().setVarbit(rule.getVarbit(), slot);
    opponent.getGameEncoder().setVarbit(rule.getVarbit(), slot);
    state = ClanWarsPlayerState.RULE_SELECTION;
    opponent.getPlugin(ClanWarsPlugin.class).setState(ClanWarsPlayerState.RULE_SELECTION);
  }

  public void sendBattleVarbits() {
    player.getGameEncoder().setVarbit(VarbitId.CLAN_WARS_COUNTDOWN, warState.getCountdown());
    player.getGameEncoder().setVarbit(VarbitId.CLAN_WARS_TEAMMATES, warState.getTeamCount());
    player
        .getGameEncoder()
        .setVarbit(VarbitId.CLAN_WARS_OPPONENTS, warState.getOpposingTeamCount());
    player.getGameEncoder().setVarbit(VarbitId.CLAN_WARS_TEAM_KILLS, warState.getTeamKills());
    player
        .getGameEncoder()
        .setVarbit(VarbitId.CLAN_WARS_OPPONENT_KILLS, warState.getOpposingTeamKills());
  }

  public void teleportViewing(int option) {
    if (player.isLocked() || !player.getMovement().isTeleportStateNone()) {
      return;
    }
    Arena arena = getArena();
    if (arena == null) {
      return;
    }
    Tile tile;
    if (state == ClanWarsPlayerState.NONE
        || state != ClanWarsPlayerState.VIEW
        || option < 0
        || option >= arena.getOrbs().length) {
      return;
    }
    if (!player.getWidgetManager().hasWidget(WidgetId.CLAN_WARS_ORBS)) {
      player.getWidgetManager().sendInventoryOverlay(WidgetId.CLAN_WARS_ORBS);
    }
    player
        .getGameEncoder()
        .sendScript(
            ScriptId.CLANWARS_VIEW_SETUP,
            ObjectDefinition.getDefinition(NullObjectId.NULL_26742).getFirstModelId(),
            option);
    tile = arena.getOrbs()[option];
    if (tile == null) {
      return;
    }
    player.getMovement().setViewing(tile.getX(), tile.getY(), tile.getHeight());
  }

  public Arena getArena() {
    if (rules == null) {
      return null;
    }
    return Arena.get(rules[ClanWarsRule.ARENA.ordinal()]);
  }

  public ClanWarsRuleOption getRule(ClanWarsRule rule) {
    if (state == ClanWarsPlayerState.NONE || rules == null || rule == null) {
      return null;
    }
    return rule.getOption(rules[rule.ordinal()]);
  }

  public void setRule(int option, int slot) {
    rules[option] = slot;
  }

  public void setRules(int[] rules) {
    if (this.rules == null) {
      this.rules = new int[ClanWarsRule.TOTAL];
    }
    System.arraycopy(rules, 0, this.rules, 0, ClanWarsRule.TOTAL);
  }

  public void incrimentTournamentWins() {
    tournamentWins++;
  }
}
