package com.palidinodh.playerplugin.agility;

import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.util.PTime;

public class AgilityPlugin implements PlayerPlugin {

  private String capeStaminaBoostDate;

  public boolean canUseCapeStaminaBoost() {
    return !PTime.getDate().equals(capeStaminaBoostDate);
  }

  public void updateCapeStaminaBoostDate() {
    capeStaminaBoostDate = PTime.getDate();
  }
}
