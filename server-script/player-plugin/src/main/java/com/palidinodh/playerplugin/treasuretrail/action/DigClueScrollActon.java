package com.palidinodh.playerplugin.treasuretrail.action;

import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueChain;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.treasuretrail.TreasureTrailPlugin;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class DigClueScrollActon extends ClueScrollAction {

  private Tile tile;

  public DigClueScrollActon(Tile tile, OptionsDialogue dialogue) {
    super(new DialogueChain(dialogue));
    this.tile = tile;
  }

  @Override
  public boolean digHook(Player player, ClueScrollType type) {
    if (!player.withinDistance(tile, 2)) {
      return false;
    }
    if (getDialogueChain() != null) {
      player.putAttribute("clue_scroll_type", type);
      player.openDialogue(getDialogueChain());
      return true;
    }
    player.getPlugin(TreasureTrailPlugin.class).stageComplete(type);
    return true;
  }
}
