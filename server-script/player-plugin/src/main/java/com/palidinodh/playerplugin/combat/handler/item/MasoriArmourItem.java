package com.palidinodh.playerplugin.combat.handler.item;

import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.MASORI_MASK, ItemId.MASORI_BODY, ItemId.MASORI_CHAPS})
class MasoriArmourItem implements ItemHandler {

  private static void fortify(Player player, int fromItemId, int toItemId, int count) {
    if (!player.getInventory().hasItem(fromItemId)) {
      player
          .getGameEncoder()
          .sendMessage("You need an " + ItemDefinition.getName(fromItemId) + " to do this.");
      return;
    }
    if (!player.getInventory().hasItem(ItemId.HAMMER)) {
      player.getGameEncoder().sendMessage("You need a hammer to do this.");
      return;
    }
    if (player.getSkills().getLevel(Skills.CRAFTING) < 90) {
      player.getGameEncoder().sendMessage("You need a Crafting level of 90 to do this.");
      return;
    }
    player.getSkills().addXp(Skills.CRAFTING, 830 * count);
    player.getInventory().deleteItem(ItemId.ARMADYLEAN_PLATE, count);
    player.getInventory().deleteItem(fromItemId);
    player.getInventory().addOrDropItem(toItemId);
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    var onSlot = onItem.getSlot();
    if (ItemHandler.used(useItem, onItem, ItemId.MASORI_MASK, ItemId.ARMADYLEAN_PLATE)) {
      fortify(player, ItemId.MASORI_MASK, ItemId.MASORI_MASK_F, 1);
      return true;
    }
    if (ItemHandler.used(useItem, onItem, ItemId.MASORI_BODY, ItemId.ARMADYLEAN_PLATE)) {
      fortify(player, ItemId.MASORI_BODY, ItemId.MASORI_BODY_F, 4);
      return true;
    }
    if (ItemHandler.used(useItem, onItem, ItemId.MASORI_CHAPS, ItemId.ARMADYLEAN_PLATE)) {
      fortify(player, ItemId.MASORI_CHAPS, ItemId.MASORI_CHAPS_F, 3);
      return true;
    }
    return false;
  }
}
