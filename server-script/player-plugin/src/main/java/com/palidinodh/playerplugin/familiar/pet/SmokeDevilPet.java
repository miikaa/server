package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.familiar.Pet;

class SmokeDevilPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.PET_SMOKE_DEVIL, NpcId.SMOKE_DEVIL_6655, NpcId.SMOKE_DEVIL));
    builder.entry(
        new Pet.Entry(
            ItemId.PET_SMOKE_DEVIL_22663, NpcId.SMOKE_DEVIL_8482, NpcId.SMOKE_DEVIL_8483));
    builder.optionVariation(
        (p, n) -> {
          switch (n.getId()) {
            case NpcId.SMOKE_DEVIL_6655:
              p.getPlugin(FamiliarPlugin.class).transformPet(NpcId.SMOKE_DEVIL_8482);
              break;
            case NpcId.SMOKE_DEVIL_8482:
              p.getPlugin(FamiliarPlugin.class).transformPet(NpcId.SMOKE_DEVIL_6655);
              break;
          }
        });
    return builder;
  }
}
