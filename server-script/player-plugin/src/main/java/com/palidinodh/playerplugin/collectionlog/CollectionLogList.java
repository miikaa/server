package com.palidinodh.playerplugin.collectionlog;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.util.PString;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class CollectionLogList {

  public static final CollectionLogEntry OTHER_MISCELLANEOUS =
      new CollectionLogEntry(
          "Miscellaneous",
          Arrays.asList(
              ItemId.EVIL_CHICKEN_HEAD,
              ItemId.EVIL_CHICKEN_WINGS,
              ItemId.EVIL_CHICKEN_LEGS,
              ItemId.EVIL_CHICKEN_FEET));

  public static final List<NpcCombatDefinition> BOSSES;
  public static final java.util.List<NpcCombatDefinition> RAIDS;
  public static final List<NpcCombatDefinition> MONSTERS;
  public static final List<CollectionLogEntry> OTHER = Arrays.asList(OTHER_MISCELLANEOUS);

  static {
    var bosses = new ArrayList<NpcCombatDefinition>();
    var raids = new ArrayList<NpcCombatDefinition>();
    var monsters = new ArrayList<NpcCombatDefinition>();
    for (var definition : NpcCombatDefinition.DROP_TABLES.keySet()) {
      if (definition.getKillCountName().equalsIgnoreCase("chambers of xeric")
          || definition.getKillCountName().equalsIgnoreCase("theatre of blood")) {
        raids.add(definition);
        continue;
      }
      if (definition.getKillCount().isSendMessage() || isBoss(definition.getKillCountName())) {
        bosses.add(definition);
        continue;
      }
      monsters.add(definition);
    }
    bosses.sort(Comparator.comparing(NpcCombatDefinition::getKillCountName));
    raids.sort(Comparator.comparing(NpcCombatDefinition::getKillCountName));
    monsters.sort(Comparator.comparing(NpcCombatDefinition::getKillCountName));
    BOSSES = java.util.List.copyOf(bosses);
    RAIDS = java.util.List.copyOf(raids);
    MONSTERS = java.util.List.copyOf(monsters);
  }

  public static int getTotalLogs() {
    return BOSSES.size()
        + RAIDS.size()
        + MONSTERS.size()
        + OTHER.size()
        + (ClueScrollType.values().length - 1);
  }

  public static String getDisplayName(String name) {
    switch (name) {
      case "Gauntlet":
        return "The Gauntlet";
      case "Corrupted Gauntlet":
        return "The Corrupted Gauntlet";
      case "Barrows chest":
        return "Barrows Chests";
      case "Inferno":
        return "The Inferno";
    }
    return PString.formatName(name);
  }

  public static String getCountDescription(String name) {
    switch (name) {
      case "Inferno":
        return "TzKal-Zuk kills";
      case "Gauntlet":
        return "Gauntlet completion count";
      case "Corrupted Gauntlet":
        return "Corrupted Gauntlet completion count";
      case "Barrows chest":
        return "Barrows Chests opened";
      case "Chambers of Xeric":
        return "Chambers of Xeric completions";
      case "Theatre of Blood":
        return "Theatre of Blood completions";
      case "Beginner Treasure Trails":
        return "Beginner clues completed";
      case "Easy Treasure Trails":
        return "Easy clues completed";
      case "Medium Treasure Trails":
        return "Medium clues completed";
      case "Hard Treasure Trails":
        return "Hard clues completed";
      case "Elite Treasure Trails":
        return "Elite clues completed";
      case "Master Treasure Trails":
        return "Master clues completed";
      case "All Pets":
      case "Miscellaneous":
        return "";
    }
    return PString.formatName(name) + " kills";
  }

  public static boolean isBoss(String name) {
    switch (name) {
      case "Barrows chest":
      case "The Nightmare":
      case "Inferno":
        return true;
    }
    return false;
  }

  public static boolean isSpecialBroadItem(int itemId) {
    switch (itemId) {
      case ItemId.DRACONIC_VISAGE:
      case ItemId.DRAGON_AXE:
      case ItemId.DRAGON_2H_SWORD:
      case ItemId.DRAGON_PICKAXE:
      case ItemId.GODSWORD_SHARD_1:
      case ItemId.GODSWORD_SHARD_2:
      case ItemId.GODSWORD_SHARD_3:
      case ItemId.MALEDICTION_WARD:
      case ItemId.ODIUM_WARD:
      case ItemId.DRAGON_THROWNAXE:
      case ItemId.DRAGON_KNIFE:
      case ItemId.TZTOK_SLAYER_HELMET:
      case ItemId.TZKAL_SLAYER_HELMET:
      case ItemId.INFERNAL_CAPE:
        return true;
    }
    return FamiliarPlugin.isPetItem(itemId);
  }

  public static void updateEntryMatches(Player player) {
    var plugin = player.getPlugin(CollectionLogPlugin.class);
    for (var entry : OTHER) {
      for (var itemId : entry.getItems()) {
        if (plugin.hasItem(entry.getName(), itemId)) {
          continue;
        }
        if (!player.hasItem(itemId)) {
          continue;
        }
        plugin.addItem(entry, new Item(itemId));
      }
    }
  }
}
