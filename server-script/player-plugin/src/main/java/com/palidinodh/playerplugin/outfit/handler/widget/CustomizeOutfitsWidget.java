package com.palidinodh.playerplugin.outfit.handler.widget;

import com.palidinodh.cache.clientscript2.ScrollbarSizeCs2;
import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.OutfitOverrideType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.outfit.OutfitPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.CUSTOMIZE_OUTFITS_1030)
class CustomizeOutfitsWidget implements WidgetHandler {

  private static final int OUTFIT_LENGTH = 40;
  private static final int OUTFIT_FIRST_CHILD = 30;
  private static final int OUTFIT_ENTRY_CHILDREN = 2;

  private static final int PIECE_LENGTH = 40;
  private static final int PIECE_FIRST_CHILD = 157;
  private static final int PIECE_ENTRY_CHILDREN = 2;

  public static void loadOutfits(Player player) {
    var plugin = player.getPlugin(OutfitPlugin.class);
    var selectedOutfit = (OutfitOverrideType) player.getAttribute("outfit_override");

    player
        .getGameEncoder()
        .sendClientScriptData(
            ScrollbarSizeCs2.builder()
                .root(WidgetId.CUSTOMIZE_OUTFITS_1030)
                .container(29)
                .scrollbar(28)
                .height(20 * plugin.getOutfitCount())
                .build());

    for (var i = 0; i < 12; i++) {
      var childOffset = i * (OUTFIT_ENTRY_CHILDREN + 1);
      player
          .getGameEncoder()
          .sendHideWidget(
              WidgetId.CUSTOMIZE_OUTFITS_1030,
              OUTFIT_FIRST_CHILD + childOffset,
              i >= plugin.getOutfitCount());
    }

    for (var i = 0; i < plugin.getOutfitCount(); i++) {
      var outfit = plugin.getOutfit(i);
      var childOffset = i * (OUTFIT_ENTRY_CHILDREN + 1) + OUTFIT_ENTRY_CHILDREN;
      var color = "";
      if (outfit == selectedOutfit) {
        color = "<col=FFFFFF>";
      }
      player
          .getGameEncoder()
          .sendWidgetText(
              WidgetId.CUSTOMIZE_OUTFITS_1030,
              OUTFIT_FIRST_CHILD + childOffset,
              color + outfit.getName());
    }
  }

  public static void loadPieces(Player player) {
    var plugin = player.getPlugin(OutfitPlugin.class);
    var selectedOutfit = (OutfitOverrideType) player.getAttribute("outfit_override");
    var selectedPiece = player.getAttributeInt("outfit_override_piece");
    var pieceCount = selectedOutfit == null ? 0 : selectedOutfit.getItemIds().size();

    player
        .getGameEncoder()
        .sendClientScriptData(
            ScrollbarSizeCs2.builder()
                .root(WidgetId.CUSTOMIZE_OUTFITS_1030)
                .container(156)
                .scrollbar(155)
                .height(20 * pieceCount)
                .build());

    for (var i = 0; i < 12; i++) {
      var childOffset = i * (PIECE_ENTRY_CHILDREN + 1);
      player
          .getGameEncoder()
          .sendHideWidget(
              WidgetId.CUSTOMIZE_OUTFITS_1030, PIECE_FIRST_CHILD + childOffset, i >= pieceCount);
    }

    for (var i = 0; i < pieceCount; i++) {
      var itemId = selectedOutfit.getItemIds().get(i);
      var childOffset = i * (PIECE_ENTRY_CHILDREN + 1) + PIECE_ENTRY_CHILDREN;
      player
          .getGameEncoder()
          .sendWidgetText(
              WidgetId.CUSTOMIZE_OUTFITS_1030,
              PIECE_FIRST_CHILD + childOffset,
              ItemDefinition.getName(itemId));
    }
  }

  public static void loadName(Player player) {
    var selectedOutfit = (OutfitOverrideType) player.getAttribute("outfit_override");
    player
        .getGameEncoder()
        .sendWidgetText(
            WidgetId.CUSTOMIZE_OUTFITS_1030,
            22,
            selectedOutfit == null ? "" : selectedOutfit.getName());
  }

  @Override
  public void onOpen(Player player, int widgetId) {
    var plugin = player.getPlugin(OutfitPlugin.class);
    var outfit = plugin.getOutfit(0);
    player.putAttribute("outfit_override", outfit);
    player.putAttribute("outfit_override_piece", 0);
    loadOutfits(player);
    loadPieces(player);
    loadName(player);
  }

  @Override
  public void onClose(Player player, int widgetId) {
    player.getAppearance().setTemporaryItemIds(null);
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    var plugin = player.getPlugin(OutfitPlugin.class);
    if (childId == 17) {
      player.getWidgetManager().sendInteractiveOverlay(WidgetId.BOND_POUCH_1017);
      return;
    }
    if (childId == 278) {
      plugin.resetOutfit();
      return;
    }
    if (childId >= OUTFIT_FIRST_CHILD
        && childId <= OUTFIT_FIRST_CHILD + OUTFIT_LENGTH * (OUTFIT_ENTRY_CHILDREN + 1)) {
      var index = (childId - OUTFIT_FIRST_CHILD) / (OUTFIT_ENTRY_CHILDREN + 1);
      if (index >= plugin.getOutfitCount()) {
        return;
      }
      var category = plugin.getOutfit(index);
      player.putAttribute("outfit_override", category);
    }
    if (childId >= PIECE_FIRST_CHILD
        && childId <= PIECE_FIRST_CHILD + PIECE_LENGTH * (PIECE_ENTRY_CHILDREN + 1)) {
      var selectedOutfit = (OutfitOverrideType) player.getAttribute("outfit_override");
      if (selectedOutfit == null) {
        return;
      }
      var index = (childId - PIECE_FIRST_CHILD) / (PIECE_ENTRY_CHILDREN + 1);
      if (index >= selectedOutfit.getItemIds().size()) {
        return;
      }
      var item = selectedOutfit.getItemIds().get(index);
      plugin.loadItem(item);
    }
    loadOutfits(player);
    loadPieces(player);
    loadName(player);
  }
}
