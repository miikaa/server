package com.palidinodh.playerplugin.wilderness.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.VESTAS_LONGSWORD_INACTIVE, ItemId.VESTAS_BLIGHTED_LONGSWORD})
class VestasBlightedLongswordItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "activate":
        {
          if (player.getInventory().getCount(ItemId.COINS) < 50_000_000) {
            player.getGameEncoder().sendMessage("You need 50,000,000 coins to activate this.");
            break;
          }
          player.getInventory().deleteItem(ItemId.COINS, 50_000_000);
          item.replace(new Item(ItemId.VESTAS_BLIGHTED_LONGSWORD));
          break;
        }
      case "uncharge":
        {
          player.openDialogue(
              new OptionsDialogue(
                  "Uncharging the longsword will only return its inactive form. Continue?",
                  new DialogueOption(
                      "Yes.",
                      (c, s) -> {
                        if (!player.getInventory().hasItem(ItemId.VESTAS_BLIGHTED_LONGSWORD)) {
                          return;
                        }
                        player.getInventory().deleteItem(ItemId.VESTAS_BLIGHTED_LONGSWORD);
                        player.getInventory().addItem(ItemId.VESTAS_LONGSWORD_INACTIVE);
                      }),
                  new DialogueOption("No.")));
          break;
        }
    }
  }
}
