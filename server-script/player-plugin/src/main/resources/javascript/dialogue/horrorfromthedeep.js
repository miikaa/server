var entries = new ArrayList();
var title = "";
var lines = new ArrayList();
var actions = new ArrayList();

title = "Select an Option";
lines.add("View Books");
actions.add("close|script");
lines.add("Miniquest");
actions.add("close|script");
var obj0 = new DialogueEntry();
entries.add(obj0);
obj0.setSelection(title, PString.toStringArray(lines, true), PString.toStringArray(actions, true));

instance = new DialogueScript() {
    execute: function(player, index, childId, slot) {
        if (index == 0) {
            if (slot == 0) {
                if (!player.getCombat().getHorrorFromTheDeep()) {
                    player.getGameEncoder().sendMessage("You can't view this shop.");
                    return;
                }
                player.openShop("horror_from_the_deep");
            } else if (slot == 1) {
                if (player.getCombat().getHorrorFromTheDeep()) {
                    player.getGameEncoder().sendMessage("You've already completed this.");
                    return;
                }
                player.getMovement().teleport(2515, 4632, 4);
                var mother = player.getController().addNpc(new NpcSpawn(new Tile(2520, 4648, 4), NpcId.DAGANNOTH_MOTHER_100_983));
                mother.getCombat().setTarget(player);
            }
        }
    },

    getDialogueEntries: function() {
        return entries;
    }
}
