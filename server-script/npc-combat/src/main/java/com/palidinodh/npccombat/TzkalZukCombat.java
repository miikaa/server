package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.VarbitId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitDamage;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.combat.CombatPlugin;
import com.palidinodh.util.PEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class TzkalZukCombat extends NpcCombat {

  private static final int FIRST_SPAWN_DELAY = 81;
  private static final int SPAWN_DELAY = 350;

  private static final Tile MAGE_TILE = new Tile(2266, 5351);
  private static final Tile RANGER_TILE = new Tile(2275, 5351);
  private static final Tile JAD_TILE = new Tile(2270, 5347);
  private static final Tile[] JAK_TILES = {
    new Tile(2262, 5363), new Tile(2266, 5363), new Tile(2276, 5363), new Tile(2280, 5363)
  };

  @Inject private Npc npc;
  private int totalTime;
  private Npc glyph;
  private int spawnDelay = FIRST_SPAWN_DELAY;
  private boolean spawnPaused;
  private boolean spawnedJad;
  private boolean spawnedHealers;
  private List<Npc> spawns = new ArrayList<>();

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.TZKAL_ZUK_1400);
    combat.spawn(NpcCombatSpawn.builder().lock(8).animation(7563).build());
    combat.hitpoints(
        NpcCombatHitpoints.builder().total(1200).barType(HitpointsBarType.GREEN_RED_160).build());
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(350)
            .magicLevel(150)
            .rangedLevel(400)
            .defenceLevel(260)
            .bonus(BonusType.ATTACK_CRUSH, 300)
            .bonus(BonusType.ATTACK_MAGIC, 550)
            .bonus(BonusType.ATTACK_RANGED, 550)
            .bonus(BonusType.DEFENCE_MAGIC, 350)
            .bonus(BonusType.DEFENCE_RANGED, 100)
            .build());
    combat.aggression(NpcCombatAggression.builder().range(8).always(true).forceable(false).build());
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.focus(
        NpcCombatFocus.builder()
            .disableFacingOpponent(true)
            .disableFollowingOpponent(true)
            .build());
    combat.killCount(NpcCombatKillCount.builder().asName("Inferno").save(false).build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(
        NpcCombatDamage.builder()
            .maximum(251)
            .applyType(NpcCombatDamage.ApplyType.UNCAPPED)
            .ignorePrayer(true)
            .build());
    style.animation(7566).attackSpeed(10).attackRange(40);
    style.projectile(NpcCombatProjectile.builder().id(1375).speedMinimumDistance(4).build());
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public List<NpcCombatDropTable> getAdditionalDropTables(int npcId) {
    var dropTable = NpcCombatDropTable.builder().probabilityNumerator(0).probabilityDenominator(0);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.JAL_NIB_REK)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.INFERNAL_CAPE)));

    return Arrays.asList(dropTable.build());
  }

  @Override
  public void npcApplyHitStartHook(Hit hit) {
    if (hit.getDamage().getRoll() > 250) {
      hit.setDamage(new HitDamage(250));
    }
  }

  @Override
  public void npcApplyHitEndHook(Hit hit) {
    updateHealth();
  }

  @Override
  public void restoreHook() {
    totalTime = 0;
  }

  @Override
  public void spawnHook() {
    spawnedJad = false;
    spawnedHealers = false;
    spawnDelay = FIRST_SPAWN_DELAY;
    spawnPaused = false;
    glyph =
        npc.getController()
            .addNpc(new NpcSpawn(new Tile(2270, 5363, npc.getHeight()), NpcId.ANCESTRAL_GLYPH));
    glyph.getCombat().script("zuk", npc);
    setHitDelay(8 + 13);
    var target = getTarget();
    if (target != null && target.isPlayer()) {
      updateHealth();
      addEvent(
          PEvent.singleEvent(
              2, e -> target.asPlayer().getWidgetManager().sendOverlay(WidgetId.TZKAL_ZUK)));
    }
  }

  @Override
  public void despawnHook() {
    npc.getWorld().removeNpc(glyph);
    npc.getWorld().removeNpcs(spawns);
  }

  @Override
  public void tickStartHook() {
    totalTime++;
    if (!npc.isVisible() || isDead()) {
      return;
    }
    if (!spawns.isEmpty()) {
      spawns.removeIf(npc -> npc.getCombat().isDead());
    }
    if (!spawnPaused && spawns.isEmpty() && spawnDelay-- <= 0) {
      spawnDelay = SPAWN_DELAY;
      var ranger =
          npc.getController()
              .addNpc(
                  new NpcSpawn(
                      new Tile(RANGER_TILE.getX(), RANGER_TILE.getY(), npc.getHeight()),
                      NpcId.JAL_XIL_370_7702));
      ranger.getMovement().setClipNpcs(true);
      ranger.setLargeVisibility();
      ranger.getCombat().startAttacking(glyph);
      spawns.add(ranger);
      var mage =
          npc.getController()
              .addNpc(
                  new NpcSpawn(
                      new Tile(MAGE_TILE.getX(), MAGE_TILE.getY(), npc.getHeight()),
                      NpcId.JAL_ZEK_490_7703));
      mage.getMovement().setClipNpcs(true);
      mage.setLargeVisibility();
      mage.getCombat().startAttacking(glyph);
      spawns.add(mage);
    }
    spawnPaused = getHitpoints() > 480 && getHitpoints() <= 600;
    if (getHitpoints() <= 480 && !spawnedJad) {
      spawnDelay += 175;
      spawnedJad = true;
      var jad =
          npc.getController()
              .addNpc(
                  new NpcSpawn(
                      new Tile(JAD_TILE.getX(), JAD_TILE.getY(), npc.getHeight()),
                      NpcId.JALTOK_JAD_900_7704));
      jad.setLargeVisibility();
      jad.getMovement().setClipNpcs(true);
      jad.getCombat().startAttacking(glyph);
      jad.getCombat().as(TztokJadCombat.class).setSpawnHealersNorth(true);
      spawns.add(jad);
    }
    if (getHitpoints() <= 240 && !spawnedHealers) {
      spawnedHealers = true;
      for (var tile : JAK_TILES) {
        var jak =
            npc.getController()
                .addNpc(
                    new NpcSpawn(
                        new Tile(tile.getX(), tile.getY(), npc.getHeight()), NpcId.JAL_MEJJAK_250));
        jak.getMovement().setClipNpcs(true);
        jak.setLargeVisibility();
        jak.getCombat().startAttacking(npc);
        spawns.add(jak);
      }
    }
  }

  @Override
  public void applyAttackEndHook(
      NpcCombatStyle combatStyle, Entity opponent, int applyAttackLoopCount, HitEvent hitEvent) {
    if (opponent == glyph) {
      hitEvent.stop();
    }
  }

  @Override
  public Entity applyAttackEntityHook(NpcCombatStyle combatStyle, Entity opponent) {
    return entityIsSafe(opponent) ? glyph : opponent;
  }

  @Override
  public double damageInflictedHook(NpcCombatStyle combatStyle, Entity opponent, double damage) {
    if (entityIsSafe(opponent)) {
      return 0;
    }
    return damage;
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    player
        .getPlugin(CombatPlugin.class)
        .updateKillTime(npc.getCombatDef().getKillCountName(), totalTime);
  }

  public boolean entityIsSafe(Entity entity) {
    return !glyph.isLocked()
        && (entity.getX() + 1 == glyph.getX()
            || entity.getX() == glyph.getX()
            || entity.getX() - 1 == glyph.getX()
            || entity.getX() - 2 == glyph.getX()
            || entity.getX() - 3 == glyph.getX());
  }

  public void updateHealth() {
    var target = getTarget();
    if (target == null || !target.isPlayer()) {
      return;
    }
    var player = target.asPlayer();
    player
        .getGameEncoder()
        .setVarbit(VarbitId.TZKAL_ZULK_HEALTH_OVERLAY_TOTAL, Math.max(1, getMaxHitpoints()));
    player
        .getGameEncoder()
        .setVarbit(VarbitId.TZKAL_ZULK_HEALTH_OVERLAY_CURRENT, Math.max(1, getHitpoints()));
  }
}
