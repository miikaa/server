package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.map.MapObject;
import java.util.Arrays;
import java.util.List;

class SkotizoAltarCombat extends NpcCombat {

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.AWAKENED_ALTAR);
    combat.spawn(NpcCombatSpawn.builder().respawnDelay(6000).build());
    combat.hitpoints(NpcCombatHitpoints.total(100));
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void spawnHook() {
    npc.getController().addMapObject(new MapObject(ObjectId.AWAKENED_ALTAR, 10, 0, npc));
  }

  @Override
  public void despawnHook() {
    removeMapObject();
  }

  @Override
  public void applyDeadStartHook(int deathDelay) {
    removeMapObject();
  }

  @Override
  public double damageReceivedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceType) {
    if (!opponent.isPlayer()) {
      return damage;
    }
    var player = opponent.asPlayer();
    if (hitStyleType == HitStyleType.MELEE
        && player.getEquipment().getWeaponId() == ItemId.ARCLIGHT) {
      damage = 100;
    }
    return damage;
  }

  private void removeMapObject() {
    npc.getController().removeMapObject(new MapObject(ObjectId.AWAKENED_ALTAR, 10, 0, npc));
  }
}
