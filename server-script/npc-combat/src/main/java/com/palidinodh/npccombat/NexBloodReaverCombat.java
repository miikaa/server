package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.graphic.Graphic;
import java.util.Arrays;
import java.util.List;

class NexBloodReaverCombat extends NpcCombat {

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.BLOOD_REAVER_161);
    combat.hitpoints(NpcCombatHitpoints.total(150));
    combat.stats(
        NpcCombatStats.builder()
            .magicLevel(190)
            .defenceLevel(100)
            .bonus(BonusType.DEFENCE_STAB, 20)
            .bonus(BonusType.DEFENCE_SLASH, 80)
            .bonus(BonusType.DEFENCE_CRUSH, 120)
            .bonus(BonusType.DEFENCE_MAGIC, 300)
            .bonus(BonusType.DEFENCE_RANGED, 55)
            .build());
    combat.aggression(NpcCombatAggression.builder().range(32).build());
    combat.deathAnimation(9192).blockAnimation(9196);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(20).splashOnMiss(true).build());
    style.animation(9194).attackSpeed(5);
    style.targetGraphic(new Graphic(373));
    style.projectile(NpcCombatProjectile.id(374));
    style.effect(NpcCombatEffect.builder().healByDamage(0.25).build());
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }
}
