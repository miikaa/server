package com.palidinodh.npccombat;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.item.RandomItem;
import java.util.Arrays;
import java.util.List;

class ProbitaCombat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.PROBITA);
    combat.killCount(NpcCombatKillCount.builder().asName("All Pets").build());

    return Arrays.asList(combat.build());
  }

  @Override
  public List<NpcCombatDropTable> getAdditionalDropTables(int npcId) {
    var dropTable = NpcCombatDropTable.builder().probabilityNumerator(0).probabilityDenominator(0);

    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ABYSSAL_PROTECTOR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BABY_CHINCHOMPA)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BEAVER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GIANT_SQUIRREL)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.HERBI)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.HERON)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PHOENIX)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RIFT_GUARDIAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ROCK_GOLEM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ROCKY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SMOLCANO)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TANGLEROOT)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TINY_TEMPOR)));

    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ABYSSAL_ORPHAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BABY_MOLE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CALLISTO_CUB)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.HELLPUPPY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.OLMLET)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PET_CHAOS_ELEMENTAL)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PET_DARK_CORE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PET_DAGANNOTH_PRIME)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PET_DAGANNOTH_REX)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PET_DAGANNOTH_SUPREME)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PET_GENERAL_GRAARDOR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NOON)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.IKKLE_HYDRA)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.KALPHITE_PRINCESS)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PET_KRAKEN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PET_KREEARRA)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PET_KRIL_TSUTSAROTH)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LITTLE_NIGHTMARE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NEXLING)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PRINCE_BLACK_DRAGON)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SCORPIAS_OFFSPRING)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SKOTOS)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PET_SMOKE_DEVIL)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PET_SNAKELING)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LIL_ZIK)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TZREK_JAD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.JAL_NIB_REK)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.VENENATIS_SPIDERLING)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.VETION_JR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.VORKI)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.YOUNGLLEF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PET_ZILYANA)));

    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOODHOUND)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TINY_MELEE_JAD_60044)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TINY_RANGED_JAD_60045)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TINY_MAGIC_JAD_60046)));

    return Arrays.asList(dropTable.build());
  }
}
