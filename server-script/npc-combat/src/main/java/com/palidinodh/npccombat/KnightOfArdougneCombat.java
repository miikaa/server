package com.palidinodh.npccombat;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import java.util.Arrays;
import java.util.List;

class KnightOfArdougneCombat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder().clue(ClueScrollType.EASY, 128);
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BONES)));
    drop.table(dropTable.build());

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.KNIGHT_OF_ARDOUGNE_46);
    combat.hitpoints(NpcCombatHitpoints.total(52));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(38)
            .defenceLevel(41)
            .bonus(BonusType.MELEE_ATTACK, 8)
            .bonus(BonusType.DEFENCE_STAB, 39)
            .bonus(BonusType.DEFENCE_SLASH, 40)
            .bonus(BonusType.DEFENCE_CRUSH, 36)
            .bonus(BonusType.DEFENCE_MAGIC, -11)
            .bonus(BonusType.DEFENCE_RANGED, 36)
            .build());
    combat.deathAnimation(836).blockAnimation(388);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.damage(NpcCombatDamage.maximum(6));
    style.animation(451).attackSpeed(5);
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }
}
