package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.item.RandomItem;
import java.util.Arrays;
import java.util.List;

class ChickenCombat extends NpcCombat {

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {

    var chickenDrop = NpcCombatDrop.builder();
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.FEATHER)));
    chickenDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BONES)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RAW_CHICKEN)));
    chickenDrop.table(dropTable.build());

    var chickenCombat = NpcCombatDefinition.builder();
    chickenCombat.id(NpcId.CHICKEN_1).id(NpcId.CHICKEN_1_2693);
    chickenCombat.hitpoints(NpcCombatHitpoints.total(3));
    chickenCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(1)
            .defenceLevel(1)
            .bonus(BonusType.DEFENCE_MAGIC, -42)
            .bonus(BonusType.DEFENCE_RANGED, -42)
            .bonus(BonusType.DEFENCE_SLASH, -42)
            .bonus(BonusType.DEFENCE_STAB, -42)
            .bonus(BonusType.DEFENCE_CRUSH, -42)
            .build());
    chickenCombat.deathAnimation(5389);
    chickenCombat.drop(chickenDrop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(1));
    style.animation(5387).attackSpeed(4);
    chickenCombat.style(style.build());

    return Arrays.asList(chickenCombat.build());
  }
}
