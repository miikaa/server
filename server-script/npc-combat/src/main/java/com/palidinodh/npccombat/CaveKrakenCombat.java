package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NotedItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropLocationType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSlayer;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.shared.Movement;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import java.util.Arrays;
import java.util.List;

class CaveKrakenCombat extends NpcCombat {

  private static final List<Integer> CURSED_IDS =
      Arrays.asList(
          NpcId.CURSED_WHIRLPOOL_127_16014,
          NpcId.CURSED_KRAKEN_127_16013,
          NpcId.CURSED_KRAKEN_LEECH_209_16060);

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop =
        NpcCombatDrop.builder()
            .locationType(NpcCombatDropLocationType.UNDER_OPPONENT)
            .gemDropTableDenominator(33)
            .clue(ClueScrollType.ELITE, 1200)
            .clue(ClueScrollType.HARD, 100);
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(1200).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.KRAKEN_TENTACLE)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(600).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TOME_OF_WATER_EMPTY)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(200).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCHARGED_TRIDENT)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SNAPDRAGON_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TORSTOL_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_RANARR_WEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_LANTADYME)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_SPEAR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_WARHAMMER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BATTLESTAFF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WATER_BATTLESTAFF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MYSTIC_WATER_STAFF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BELLADONNA_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TOADFLAX_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.IRIT_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.AVANTOE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.KWUARM_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LANTADYME_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CADANTINE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.POISON_IVY_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CACTUS_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_IRIT_LEAF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_AVANTOE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_KWUARM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_CADANTINE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_DWARF_WEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 123, 19770)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BUCKET)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.RAW_LOBSTER, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.WATER_ORB, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WATER_TALISMAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_TALISMAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.OYSTER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SWORDFISH, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SHARK)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.ANTIDOTE_PLUS_PLUS_4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.VIAL_OF_WATER, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SOAKED_PAGE, 7, 43)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STAFF_OF_WATER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_MED_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEAM_RUNE, 7)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.FIRE_RUNE, 30)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 30)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_GUAM_LEAF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_MARRENTILL)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TARROMIN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_HARRALANDER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.OLD_BOOT)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(NotedItemId.SEAWEED, 30)));
    drop.table(dropTable.build());

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.CAVE_KRAKEN_127).id(NpcId.WHIRLPOOL_127);
    combat.hitpoints(NpcCombatHitpoints.total(125));
    combat.stats(
        NpcCombatStats.builder()
            .magicLevel(120)
            .defenceLevel(150)
            .bonus(BonusType.DEFENCE_MAGIC, -63)
            .bonus(BonusType.DEFENCE_RANGED, 100)
            .build());
    combat.slayer(NpcCombatSlayer.builder().level(87).taskOnly(true).build());
    combat.immunity(NpcCombatImmunity.builder().melee(true).ranged(true).build());
    combat.focus(
        NpcCombatFocus.builder().disableFollowingOpponent(true).bypassMapObjects(true).build());
    combat.deathAnimation(3993).blockAnimation(3990);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(13).splashOnMiss(true).build());
    style.animation(3991).attackSpeed(6);
    style.targetGraphic(new Graphic(163, 124));
    style.projectile(NpcCombatProjectile.id(162));
    combat.style(style.build());

    var cursedCombat = NpcCombatDefinition.builder();
    cursedCombat.id(NpcId.CURSED_KRAKEN_127_16013).id(NpcId.CURSED_WHIRLPOOL_127_16014);
    cursedCombat.hitpoints(NpcCombatHitpoints.total(188));
    cursedCombat.stats(
        NpcCombatStats.builder()
            .magicLevel(180)
            .defenceLevel(225)
            .bonus(BonusType.DEFENCE_MAGIC, -63)
            .bonus(BonusType.DEFENCE_RANGED, 150)
            .build());
    cursedCombat.slayer(NpcCombatSlayer.builder().level(87).build());
    cursedCombat.immunity(NpcCombatImmunity.builder().melee(true).ranged(true).build());
    cursedCombat.focus(
        NpcCombatFocus.builder().disableFollowingOpponent(true).bypassMapObjects(true).build());
    cursedCombat.deathAnimation(3993).blockAnimation(3990);
    // cursedCombat.drop(drop.rolls(2).build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(18).splashOnMiss(true).build());
    style.animation(3991).attackSpeed(6);
    style.targetGraphic(new Graphic(163, 124));
    style.projectile(NpcCombatProjectile.id(162));
    cursedCombat.style(style.build());

    var cursedLeechCombat = NpcCombatDefinition.builder();
    cursedLeechCombat.id(NpcId.CURSED_KRAKEN_LEECH_209_16060);
    cursedLeechCombat.hitpoints(NpcCombatHitpoints.total(188));
    cursedLeechCombat.stats(
        NpcCombatStats.builder()
            .magicLevel(180)
            .defenceLevel(225)
            .bonus(BonusType.DEFENCE_MAGIC, -63)
            .bonus(BonusType.DEFENCE_RANGED, 150)
            .build());
    cursedLeechCombat.slayer(NpcCombatSlayer.builder().level(87).build());
    cursedLeechCombat.immunity(NpcCombatImmunity.builder().melee(true).ranged(true).build());
    cursedLeechCombat.focus(
        NpcCombatFocus.builder().disableFollowingOpponent(true).bypassMapObjects(true).build());
    cursedLeechCombat.deathAnimation(1272).blockAnimation(1271);
    cursedLeechCombat.drop(drop.rolls(2).build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(18).splashOnMiss(true).build());
    style.animation(1273).attackSpeed(6);
    style.targetGraphic(new Graphic(163, 124));
    style.projectile(NpcCombatProjectile.id(162));
    cursedLeechCombat.style(style.build());

    return Arrays.asList(combat.build(), cursedCombat.build(), cursedLeechCombat.build());
  }

  @Override
  public void spawnHook() {
    npc.getMovement().setType(Movement.Type.SWIM);
  }

  @Override
  public void tickStartHook() {
    if (npc.isLocked()) {
      return;
    }
    if (npc.getId() == NpcId.WHIRLPOOL_127 || npc.getId() == NpcId.CAVE_KRAKEN_127) {
      if (!inRecentCombat() && !isAttacking() && npc.getId() != NpcId.WHIRLPOOL_127) {
        npc.setId(NpcId.WHIRLPOOL_127);
      } else if (inRecentCombat() || isAttacking()) {
        npc.getMovement().clear();
        if (npc.getId() != NpcId.CAVE_KRAKEN_127) {
          npc.setId(NpcId.CAVE_KRAKEN_127);
          npc.setAnimation(7135);
          setHitDelay(4);
        }
      }
    } else if (npc.getId() == NpcId.CURSED_WHIRLPOOL_127_16014
        || npc.getId() == NpcId.CURSED_KRAKEN_127_16013) {
      if (!inRecentCombat() && !isAttacking() && npc.getId() != NpcId.CURSED_WHIRLPOOL_127_16014) {
        npc.setId(NpcId.CURSED_WHIRLPOOL_127_16014);
      } else if (inRecentCombat() || isAttacking()) {
        npc.getMovement().clear();
        if (npc.getId() != NpcId.CURSED_KRAKEN_127_16013) {
          npc.setId(NpcId.CURSED_KRAKEN_127_16013);
          npc.setAnimation(7135);
          setHitDelay(4);
        }
      }
    }
  }

  @Override
  public void attackedActionsOpponentHook(Entity opponent) {
    if (!opponent.isPlayer()) {
      return;
    }
    if (!CURSED_IDS.contains(npc.getId())) {
      return;
    }
    opponent.asPlayer().getCombat().setPKSkullDelay(PCombat.SKULL_DELAY);
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    if (npc.getId() != NpcId.CURSED_KRAKEN_127_16013
        && npc.getId() != NpcId.CURSED_KRAKEN_LEECH_209_16060) {
      return;
    }
    if (!player.getPlugin(SlayerPlugin.class).isWildernessTask(npc)) {
      return;
    }
    var table =
        NpcCombatDefinition.getDefinition(NpcId.KRAKEN_291)
            .getDrop()
            .getTable(npc, player, 2, 0, true);
    if (table == null || table.getProbabilityDenominator() < 32) {
      return;
    }
    table.dropItems(npc, player, dropTile);
  }

  @Override
  public NpcCombatDropTable deathDropItemsTableHook(
      Player player, int dropRateDivider, int roll, NpcCombatDropTable table) {
    if (npc.getId() == NpcId.CURSED_KRAKEN_127_16013
        || npc.getId() == NpcId.CURSED_KRAKEN_LEECH_209_16060) {
      if (!player.getPlugin(SlayerPlugin.class).isWildernessTask(npc)) {
        player.getGameEncoder().sendMessage("Without an assigned task, the loot turns to dust...");
        return null;
      }
      if (roll != 0) {
        return table;
      }
      if (table.getProbabilityDenominator() != NpcCombatDropTable.COMMON) {
        return table;
      }
      return NpcCombatDefinition.getDefinition(NpcId.KRAKEN_291)
          .getDrop()
          .getTable(NpcCombatDropTable.COMMON);
    }
    return table;
  }
}
