package com.palidinodh.npccombat;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import java.util.Arrays;
import java.util.List;

class PoisonSpiderCombat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.POISON_SPIDER_48);
    combat.hitpoints(NpcCombatHitpoints.total(73));
    combat.stats(
        NpcCombatStats.builder()
            .rangedLevel(45)
            .defenceLevel(35)
            .bonus(BonusType.ATTACK_RANGED, 63)
            .bonus(BonusType.DEFENCE_STAB, 20)
            .bonus(BonusType.DEFENCE_SLASH, 17)
            .bonus(BonusType.DEFENCE_CRUSH, 10)
            .bonus(BonusType.DEFENCE_RANGED, 14)
            .bonus(BonusType.DEFENCE_MAGIC, 14)
            .build());
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.deathAnimation(5329).blockAnimation(5328);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(8));
    style.animation(5327).attackSpeed(5);
    style.effect(NpcCombatEffect.builder().includeMiss(true).chance(25).poison(6).build());
    style.projectile(NpcCombatProjectile.id(124));
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }
}
