package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.DRAGON_DAGGER,
  ItemId.DRAGON_DAGGER_P,
  ItemId.DRAGON_DAGGER_P_PLUS,
  ItemId.DRAGON_DAGGER_P_PLUS_PLUS,
  ItemId.DRAGON_DAGGER_20407,
  ItemId.BLIGHTED_DRAGON_DAGGER_P_PLUS_PLUS_32368
})
class DragonDaggerSpecialAttack extends SpecialAttack {

  DragonDaggerSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(25);
    entry.animation(1062);
    entry.castGraphic(new Graphic(252, 100));
    entry.castSound(new Sound(2537));
    entry.accuracyModifier(1.25);
    entry.damageModifier(1.15);
    entry.doubleHit(true);
    addEntry(entry);
  }
}
