package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.VESTAS_BLIGHTED_LONGSWORD, ItemId.VESTAS_LONGSWORD_CHARGED_32254})
class VestasLongswordSpecialAttack extends SpecialAttack {

  VestasLongswordSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(25);
    entry.animation(7515);
    addEntry(entry);
  }

  @Override
  public void damagePreRollHook(DamagePreRollHooks hooks) {
    hooks.setMinDamage((int) (hooks.getMaxDamage() * 0.2));
    hooks.setMaxDamage((int) (hooks.getMaxDamage() * 1.2));
  }
}
