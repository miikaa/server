package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitMarkType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.WEBWEAVER_BOW)
class WebweaverBowSpecialAttack extends SpecialAttack {

  WebweaverBowSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(50);
    entry.animation(9964);
    entry.projectileId(-2);
    entry.castGraphic(new Graphic(2354));
    entry.impactGraphic(new Graphic(2355));
    entry.accuracyModifier(2);
    entry.damageModifier(0.4);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    var player = hooks.getPlayer();
    var opponent = hooks.getOpponent();
    for (var i = 0; i < 3; i++) {
      var damage =
          player.getCombat().getCombatFormulas().getHitDamage(opponent, HitStyleType.RANGED);
      var hit = new Hit(player, damage, HitMarkType.HIT, HitStyleType.RANGED);
      opponent
          .getCombat()
          .addHitEvent(
              new HitEvent(hooks.getProjectile().getEventDelay() + 1, opponent, player, hit));
      hooks.setTotalAdditionalDamage(hooks.getTotalAdditionalDamage() + damage.getRoll());
    }
    for (var i = 0; i < 4; i++) {
      if (!opponent.isPoisonImmune() && PRandom.randomE(4) == 0) {
        opponent.setPoison(4);
      }
    }
  }
}
