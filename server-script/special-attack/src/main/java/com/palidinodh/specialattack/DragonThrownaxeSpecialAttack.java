package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.DRAGON_THROWNAXE, ItemId.DRAGON_THROWNAXE_21207})
class DragonThrownaxeSpecialAttack extends SpecialAttack {

  DragonThrownaxeSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(25);
    entry.animation(7521);
    entry.castGraphic(new Graphic(1317, 96));
    entry.projectileId(1318);
    entry.castSound(new Sound(2528));
    entry.accuracyModifier(1.25);
    entry.instant(true);
    addEntry(entry);
  }
}
