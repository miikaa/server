package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.ACCURSED_SCEPTRE, ItemId.ACCURSED_SCEPTRE_A})
class AccursedSceptreSpecialAttack extends SpecialAttack {

  AccursedSceptreSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(50);
    entry.animation(9961);
    entry.castGraphic(new Graphic(2338));
    entry.impactGraphic(new Graphic(157, 124));
    entry.projectileId(2339);
    entry.accuracyModifier(1.5);
    entry.damageModifier(1.5);
    entry.magic(true);
    entry.magicLevelScaleDivider(3);
    entry.magicDamageScale(6);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    if (hooks.getDamage().getRoll() < 1) {
      return;
    }
    if (!hooks.getOpponent().isPlayer()) {
      return;
    }
    var opponent = hooks.getOpponent().asPlayer();
    opponent.getSkills().percentStat(Skills.DEFENCE, -15);
    opponent.getSkills().percentStat(Skills.MAGIC, -15);
  }
}
