package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitMarkType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.URSINE_CHAINMACE)
class UrsineChainmaceSpecialAttack extends SpecialAttack {

  UrsineChainmaceSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(50);
    entry.animation(9963);
    entry.castGraphic(new Graphic(2342, 124));
    entry.accuracyModifier(2);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    if (hooks.getDamage().getRoll() < 1) {
      return;
    }
    var opponent = hooks.getOpponent();
    opponent
        .getCombat()
        .addContinuousEvent(
            2,
            e -> {
              if (opponent.isLocked()) {
                e.stop();
                return;
              }
              if (e.getExecutions() >= 5) {
                e.stop();
                return;
              }
              opponent
                  .getCombat()
                  .addHit(new Hit(hooks.getPlayer(), 4, HitMarkType.HIT, HitStyleType.TYPELESS));
            });
    if (!opponent.isPlayer()) {
      return;
    }
    opponent.asPlayer().getMovement().setRunningDisabled(6);
    opponent.asPlayer().getSkills().changeStat(Skills.AGILITY, -20);
  }
}
