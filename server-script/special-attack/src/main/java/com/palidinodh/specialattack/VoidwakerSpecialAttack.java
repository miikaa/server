package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.VOIDWAKER)
class VoidwakerSpecialAttack extends SpecialAttack {

  VoidwakerSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(50);
    entry.animation(1378);
    entry.impactGraphic(new Graphic(2363));
    addEntry(entry);
  }

  @Override
  public void damagePreRollHook(DamagePreRollHooks hooks) {
    var maxDamage = hooks.getMaxDamage();
    hooks.setMinDamage((int) (maxDamage * 0.5));
    hooks.setMaxDamage((int) (maxDamage * 1.5));
    hooks.setAttackType(HitStyleType.MAGIC);
    hooks.setDefenceType(HitStyleType.TYPELESS);
  }
}
