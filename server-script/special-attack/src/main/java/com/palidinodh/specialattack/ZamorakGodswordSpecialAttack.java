package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.ZAMORAK_GODSWORD, ItemId.ZAMORAK_GODSWORD_OR})
class ZamorakGodswordSpecialAttack extends SpecialAttack {

  ZamorakGodswordSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(50);
    entry.animation(7638);
    entry.castGraphic(new Graphic(1210));
    entry.impactGraphic(new Graphic(369));
    entry.castSound(new Sound(2850));
    entry.accuracyModifier(2);
    entry.damageModifier(1.1);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    if (hooks.getDamage().getRoll() < 1) {
      hooks.setImpactGraphic(new Graphic(85, 124));
      return;
    }
    var opponent = hooks.getOpponent();
    if (!opponent.getController().canMagicBind()) {
      return;
    }
    opponent.getController().setMagicBind(34, hooks.getPlayer());
    if (opponent.isPlayer()) {
      opponent.asPlayer().getGameEncoder().sendMessage("<col=ef1020>You have been frozen!</col>");
    }
  }
}
