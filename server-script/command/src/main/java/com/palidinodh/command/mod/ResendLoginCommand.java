package com.palidinodh.command.mod;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.communication.WorldCommunicationServer;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("resendlogin")
class ResendLoginCommand implements CommandHandler, CommandHandler.ModeratorRank {

  @Override
  public void execute(Player player, String name, String message) {
    WorldCommunicationServer.getInstance().resendEvents();
  }
}
