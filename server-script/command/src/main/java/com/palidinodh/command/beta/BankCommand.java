package com.palidinodh.command.beta;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler.Beta;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName({"bnk", "b", "clearbank"})
class BankCommand implements CommandHandler, Beta {

  @Override
  public void execute(Player player, String name, String message) {
    switch (name) {
      case "bnk":
      case "b":
        player.getBank().open();
        break;
      case "clearbank":
        for (var tab : player.getBank().getBankTabs()) {
          if (tab == null) {
            continue;
          }
          tab.clear();
        }
        player.getBank().clearCollapsedTabs();
        break;
    }
  }
}
