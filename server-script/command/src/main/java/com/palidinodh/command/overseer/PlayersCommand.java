package com.palidinodh.command.overseer;

import com.palidinodh.cache.clientscript2.ScreenSelectionCs2;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.InventoryOptionsDialogue;
import com.palidinodh.osrscore.model.entity.player.dialogue.LargeOptions2Dialogue;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.util.PTime;
import java.util.ArrayList;
import java.util.Comparator;

@ReferenceName({"players", "view"})
class PlayersCommand
    implements CommandHandler, CommandHandler.OverseerRank, CommandHandler.Teleport {

  private static String getAreaName(Player player) {
    return player.getArea().getClass().getSimpleName().replace("Area", "");
  }

  private static void openPlayers(Player player) {
    var players = new ArrayList<>(player.getWorld().getPlayers());
    players.sort(Comparator.comparingInt(Player::getIdleTime));
    var options = new ArrayList<DialogueOption>();
    for (var player2 : players) {
      var icon = player2.getMessaging().getIconImage();
      var username = player2.getUsername();
      var location = getAreaName(player2);
      options.add(
          new DialogueOption(
              icon
                  + username
                  + ": on: "
                  + PTime.ticksToLongDuration(player2.getTotalTicks())
                  + ", map: "
                  + PTime.ticksToLongDuration(player2.getLastMapUpdate())
                  + ", idle: "
                  + PTime.ticksToLongDuration(player2.getIdleTime())
                  + ", loc: "
                  + location,
              (c, s) -> {
                viewPlayer(player, player2);
                player
                    .getWorld()
                    .sendHigherStaffMessage(
                        player.getUsername()
                            + " viewed "
                            + player2.getUsername()
                            + " in "
                            + getAreaName(player2)
                            + " from the ::players interface");
              }));
    }
    var cs2 = new ScreenSelectionCs2();
    cs2.title("Players");
    var dialogue = new LargeOptions2Dialogue(cs2, options);
    player.openDialogue(dialogue);
  }

  private static void viewPlayer(Player player, Player player2) {
    if (player.getCombat().isDead() || player.getCombat().inRecentCombat()) {
      return;
    }
    if (player2 == null || !player2.isVisible()) {
      player.getGameEncoder().sendMessage("Unable to find user.");
      openPlayers(player);
      return;
    }
    if (player == player2) {
      player.getGameEncoder().sendMessage("You can't view yourself.");
      openPlayers(player);
      return;
    }
    if (player.getController().isInstanced()) {
      player.getGameEncoder().sendMessage("You can't view while in an instance.");
      openPlayers(player);
      return;
    }
    if (player2.getController().isInstanced()) {
      player
          .getGameEncoder()
          .sendMessage(
              player2.getUsername()
                  + " is in an instance located at: "
                  + player2.getX()
                  + ", "
                  + player2.getY()
                  + ", "
                  + player2.getHeight()
                  + ".");
      player.getGameEncoder().sendMessage(player2 + " is in " + getAreaName(player2) + ".");
      openPlayers(player);
      return;
    }
    if (!player2.getController().canTeleport(false) && !player.isHigherStaff()) {
      if (player2.getArea().inWilderness()) {
        player
            .getGameEncoder()
            .sendMessage("The player you are trying to view is in the Wilderness.");
      } else {
        player
            .getGameEncoder()
            .sendMessage("The player you are trying to view is in " + getAreaName(player2) + ".");
      }
      player
          .getWorld()
          .sendHigherStaffMessage(
              player.getUsername()
                  + " failed to view "
                  + player2.getUsername()
                  + " in "
                  + getAreaName(player2));
      openPlayers(player);
      return;
    }
    var viewTile = new Tile(player2);
    viewTile.randomize(1);
    player.getMovement().setViewing(viewTile);
    player.openDialogue(
        new InventoryOptionsDialogue(
            new DialogueOption(
                "Refresh",
                (c, s) -> {
                  viewPlayer(player, player2);
                }),
            new DialogueOption(
                "Random Player",
                (c, s) -> {
                  viewPlayer(player, player.getWorld().getPlayers().getRandom());
                }),
            new DialogueOption(
                "All Players",
                (c, s) -> {
                  openPlayers(player);
                })));
  }

  @Override
  public void execute(Player player, String name, String message) {
    switch (name) {
      case "players":
        {
          openPlayers(player);
          player
              .getGameEncoder()
              .sendMessage(
                  "There are currently "
                      + player.getWorld().getPlayerCount()
                      + " players online, with "
                      + player.getWorld().getWildernessPlayerCount()
                      + " in the Wilderness.");
          break;
        }
      case "view":
        viewPlayer(player, player.getWorld().getPlayerByUsername(message));
        break;
    }
  }
}
