package com.palidinodh.command.beta;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler.Beta;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.util.PNumber;

@ReferenceName("god")
class GodCommand implements CommandHandler, Beta {

  @Override
  public void execute(Player player, String name, String message) {
    var power = 1024;
    if (!message.isEmpty()) {
      power = PNumber.getNumber(message);
    }
    for (var i = 0; i <= 6; i++) {
      player.getSkills().setLevel(i, power);
      player.getGameEncoder().sendSkillLevel(i);
    }
    player.getMovement().setEnergy(Integer.MAX_VALUE);
    player.getCombat().setHitpoints(Integer.MAX_VALUE);
    player.getPrayer().setPoints(Integer.MAX_VALUE);
    player.getGameEncoder().sendMessage("You feel like a god..");
  }
}
