package com.palidinodh.command.all;

import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.world.BadWords;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.rs.communication.event.PlayerLogEvent;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.rs.setting.UserRank;
import com.palidinodh.util.PString;
import com.palidinodh.util.PTime;
import lombok.Getter;
import lombok.Setter;

@ReferenceName("yell")
public class YellCommand implements CommandHandler {

  @Getter @Setter private static boolean enabled = true;

  @Override
  public String getExample(String name) {
    return "- Yells out your message";
  }

  @Override
  public boolean canUse(Player player) {
    return player.isUsergroup(UserRank.SAPPHIRE_MEMBER)
        || player.isUsergroup(UserRank.OVERSEER)
        || player.isStaff();
  }

  @Override
  public void execute(Player player, String name, String message) {
    if (!enabled) {
      player.getGameEncoder().sendMessage("Yell is currently disabled.");
      return;
    }
    if (player.getMessaging().getYellDelay() > 0) {
      player
          .getGameEncoder()
          .sendMessage(
              "You need to wait "
                  + PTime.tickToSec(player.getMessaging().getYellDelay())
                  + " seconds before you can yell again.");
      return;
    }
    if (player.getMessaging().isYellMuted()) {
      player
          .getGameEncoder()
          .sendMessage(
              "You were yellmuted by "
                  + player.getMessaging().getYellMutedByUsername()
                  + " for "
                  + player.getMessaging().getMutedYellMinutes()
                  + " minutes");
      player
          .getGameEncoder()
          .sendMessage(
              "You need to wait "
                  + player.getMessaging().yellMuteLeft()
                  + " minutes before you can yell again.");
      return;
    }
    if (player.inJail()) {
      player.getGameEncoder().sendMessage("You can not yell while in jail.");
      return;
    }
    if (player.getMessaging().isMuted()) {
      player.getGameEncoder().sendMessage("You can not yell while muted.");
      return;
    }
    if (BadWords.containsBadWord(message)) {
      player.getGameEncoder().sendMessage("Please don't use that word.");
      return;
    }
    if (!player.isUsergroup(UserRank.ADMINISTRATOR)) {
      message = PString.cleanString(PString.removeHtml(message));
    }
    if (message.contains("a funny feeling")) {
      player
          .getGameEncoder()
          .sendMessage("<col=ff0000>You have a funny feeling like you're being followed.</col>");
      return;
    }
    var yellDelay = player.getPlugin(BondPlugin.class).getDonatorRank().getYellCooldown();
    if (player.isStaff()) {
      yellDelay = (int) PTime.secToTick(5);
    }
    if (yellDelay == -1) {
      return;
    }
    player
        .getWorld()
        .sendMessage(
            player,
            "[<col=ff0000>Yell</col>] "
                + player.getMessaging().getIconImage()
                + player.getUsername()
                + ": "
                + message);
    player.getMessaging().setYellDelay(yellDelay);
    player.log(PlayerLogEvent.LogType.YELL, message);
    DiscordBot.sendCodedMessage(
        DiscordChannel.YELL_CHAT_LOG, player.getUsername() + ": " + message);
  }
}
