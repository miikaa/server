package com.palidinodh.command.mod;

import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.lootkey.LootKeyPlugin;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;

@ReferenceName("lootkey")
class LootKeyCommand implements CommandHandler, CommandHandler.ModeratorRank {

  @Override
  public String getExample(String name) {
    return "on/off";
  }

  @Override
  public void execute(Player player, String name, String message) {
    LootKeyPlugin.setEnabled(message.equals("on"));
    player.getGameEncoder().sendMessage("Loot Keys: " + LootKeyPlugin.isEnabled());
    DiscordBot.sendCodedMessage(
        DiscordChannel.MODERATION_LOG, player.getUsername() + " used ::lootkey " + message);
  }
}
