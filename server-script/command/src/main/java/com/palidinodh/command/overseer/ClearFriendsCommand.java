package com.palidinodh.command.overseer;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("clearfriends")
class ClearFriendsCommand implements CommandHandler, CommandHandler.OverseerRank {

  @Override
  public void execute(Player player, String name, String message) {
    var friendsList = player.getMessaging().cloneFriends();
    for (var friend : friendsList) {
      player.getMessaging().removeFriend(friend.getUsername());
    }
  }
}
