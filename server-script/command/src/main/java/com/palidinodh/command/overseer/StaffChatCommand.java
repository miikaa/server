package com.palidinodh.command.overseer;

import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;

@ReferenceName("sc")
class StaffChatCommand implements CommandHandler, CommandHandler.OverseerRank {

  @Override
  public String getExample(String name) {
    return "- Staff only chat";
  }

  @Override
  public void execute(Player player, String name, String message) {
    player
        .getWorld()
        .sendStaffMessage(
            player.getMessaging().getIconImage()
                + player.getUsername()
                + ": "
                + message
                + "</col>");
    DiscordBot.sendCodedMessage(
        DiscordChannel.MODERATION_LOG, player.getUsername() + " sent staff chat: " + message);
  }
}
