package com.palidinodh.command.beta;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler.Beta;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapItem;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.util.PEvent;
import java.util.Arrays;
import java.util.List;

@ReferenceName({"bronzekeys", "silverkeys", "goldkeys", "diamondkeys"})
class SpawnKeysCommand implements CommandHandler, Beta {

  @Override
  public void execute(Player player, String name, String message) {
    int id;
    List<Tile> tiles;
    switch (name) {
      case "bronzekeys":
        {
          id = ItemId.BRONZE_KEY_32306;
          tiles =
              Arrays.asList(
                  new Tile(player).randomize(4),
                  new Tile(player).randomize(4),
                  new Tile(player).randomize(4),
                  new Tile(player).randomize(4),
                  new Tile(player).randomize(4));
          break;
        }
      case "silverkeys":
        {
          id = ItemId.SILVER_KEY_32307;
          tiles =
              Arrays.asList(
                  new Tile(player).randomize(4),
                  new Tile(player).randomize(4),
                  new Tile(player).randomize(4));
          break;
        }
      case "goldkeys":
        {
          id = ItemId.GOLD_KEY_32308;
          tiles =
              Arrays.asList(
                  new Tile(player).randomize(4),
                  new Tile(player).randomize(4),
                  new Tile(player).randomize(4));
          break;
        }
      case "diamondkeys":
        {
          id = ItemId.DIAMOND_KEY_32309;
          tiles = Arrays.asList(new Tile(player).randomize(4), new Tile(player).randomize(4));
          break;
        }
      default:
        return;
    }
    player.getWorld().sendMessage("<col=ff0000>" + player.getUsername() + " has summoned keys!");
    for (var tile : tiles) {
      player.getController().sendMapGraphic(tile, new Graphic(725, 124));
    }
    player
        .getController()
        .addEvent(
            PEvent.singleEvent(
                4,
                e -> {
                  for (var tile : tiles) {
                    player
                        .getController()
                        .addMapItem(new Item(id), tile, MapItem.NORMAL_TIME, MapItem.ALWAYS_APPEAR);
                  }
                }));
  }
}
