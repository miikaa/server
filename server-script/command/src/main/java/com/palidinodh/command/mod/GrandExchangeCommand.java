package com.palidinodh.command.mod;

import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.grandexchange.GrandExchangePlugin;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;

@ReferenceName("grandexchange")
class GrandExchangeCommand implements CommandHandler, CommandHandler.ModeratorRank {

  @Override
  public String getExample(String name) {
    return "on/off";
  }

  @Override
  public void execute(Player player, String name, String message) {
    GrandExchangePlugin.setEnabled(message.equals("on"));
    player.getGameEncoder().sendMessage("Grand Exchange: " + GrandExchangePlugin.isEnabled());
    DiscordBot.sendCodedMessage(
        DiscordChannel.MODERATION_LOG, player.getUsername() + " used ::grandexchange " + message);
  }
}
