package com.palidinodh.command.overseer;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.communication.event.PlayerLogEvent;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.UserRank;

@ReferenceName({"mute", "unmute"})
class MuteCommand implements CommandHandler, CommandHandler.OverseerRank {

  private static int getMaxMuteLength(Player player) {
    if (player.isUsergroup(UserRank.ADMINISTRATOR)) {
      return 168;
    }
    if (player.isUsergroup(UserRank.SENIOR_MODERATOR)
        || player.isUsergroup(UserRank.ADVERTISEMENT_MANAGER)
        || player.isUsergroup(UserRank.COMMUNITY_MANAGER)) {
      return 96;
    }
    if (player.isUsergroup(UserRank.MODERATOR)) {
      return 48;
    }
    return 12;
  }

  @Override
  public String getExample(String name) {
    switch (name) {
      case "mute":
        return "\"username or userid\" hours";
      case "unmute":
        return "username or userid";
    }
    return "";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.split(message);
    var username = messages[0].replace("_", " ");
    var hours = 0;
    if (messages.length == 2) {
      hours = Integer.parseInt(messages[1]);
    }
    var maxHours = getMaxMuteLength(player);
    if (hours > maxHours) {
      player.getGameEncoder().sendMessage("Max mute time is " + maxHours + " hours.");
      return;
    }
    var targetPlayer = player.getWorld().getPlayerByUsername(username);
    if (targetPlayer == null) {
      var userId = -1;
      try {
        userId = Integer.parseInt(username);
      } catch (Exception e) {
      }
      if (userId != -1) {
        targetPlayer = player.getWorld().getPlayerById(userId);
      }
    }
    if (targetPlayer == null) {
      player.getGameEncoder().sendMessage("Unable to find user " + username + ".");
      return;
    }
    if (player == targetPlayer) {
      player.getGameEncoder().sendMessage("You can't (un)mute yourself.");
      return;
    }
    if (name.equals("mute")) {
      targetPlayer
          .getGameEncoder()
          .sendMessage(player.getUsername() + " has muted you for " + hours + " hours.");
      targetPlayer.getMessaging().setMuteTime(hours * 60, player.getUsername());
      player.getGameEncoder().sendMessage(username + " has been muted for " + hours + " hours.");
      player
          .getWorld()
          .sendStaffMessage(
              player.getUsername()
                  + " has muted "
                  + targetPlayer.getUsername()
                  + " for "
                  + hours
                  + " hours.");
      player.log(
          PlayerLogEvent.LogType.STAFF,
          "muted " + targetPlayer.getLogName() + " for " + hours + " hours");
    } else if (name.equals("unmute")) {
      targetPlayer.getGameEncoder().sendMessage(player.getUsername() + " has unmuted you.");
      targetPlayer.getMessaging().setMuteTime(0, null);
      player.getGameEncoder().sendMessage(username + " has been unmuted.");
      player
          .getWorld()
          .sendStaffMessage(
              player.getUsername() + " has unmuted " + targetPlayer.getUsername() + ".");
      player.log(PlayerLogEvent.LogType.STAFF, "unmuted " + targetPlayer.getLogName());
    }
  }
}
