package com.palidinodh.command.admin;

import com.palidinodh.cache.definition.osrs.ObjectDefinition;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.io.Writers;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.util.PString;
import java.io.File;
import java.util.ArrayList;

@ReferenceName({"obj", "ooption"})
class ObjectCommand implements CommandHandler, CommandHandler.AdministratorRank {

  private static final String BASE_DIR = "./server-script/map-area/src/main/java";

  private static void createOption(File file, String idName) {
    var filePath = file.getPath();
    var packageName =
        filePath.substring(BASE_DIR.length() + 1).replace("/", ".").replace("\\", ".");
    packageName = packageName.substring(0, packageName.lastIndexOf('.'));
    var className = file.getName().substring(0, file.getName().indexOf('.'));
    System.out.println(packageName);
    packageName = packageName.substring(0, packageName.lastIndexOf('.'));
    var lines = new ArrayList<String>();

    lines.add("package " + packageName + ";");
    lines.add("");
    lines.add("import com.palidinodh.cache.definition.util.DefinitionOption;");
    lines.add("import com.palidinodh.cache.id.ObjectId;");
    lines.add("import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;");
    lines.add("import com.palidinodh.osrscore.model.entity.player.Player;");
    lines.add("import com.palidinodh.osrscore.model.map.MapObject;");
    lines.add("import com.palidinodh.rs.reference.ReferenceId;");
    lines.add("");
    lines.add("@ReferenceId(ObjectId." + idName + ")");
    lines.add("public class " + className + " implements MapObjectHandler {");
    lines.add("");
    lines.add("  @Override");
    lines.add(
        "  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {}");
    lines.add("}");
    Writers.writeTextFile(file, lines);
  }

  private static void spawn(Player player, String name, String message) {
    var messages = CommandHandler.splitInt(message);
    var id = messages[0];
    var type = messages[1];
    var direction = messages[2];
    var mapObject = new MapObject(id, type, direction, player);
    player.getController().addMapObject(mapObject);
  }

  private static void option(Player player, String name, String message) {
    var messages = CommandHandler.split(message);
    var objectId = -1;
    var objectIdName = "";
    if (messages[0].matches("[0-9]+")) {
      objectId = Integer.parseInt(messages[0]);
      objectIdName = ObjectId.valueOf(objectId);
    } else {
      objectIdName = messages[0].replace(" ", "_").toUpperCase();
      objectId = ObjectId.valueOf(objectIdName);
    }
    if (objectId == -1 || objectIdName == null || objectIdName.isEmpty()) {
      player.getGameEncoder().sendMessage("Couldn't find object.");
      return;
    }
    var formattedObjectName =
        PString.formatName(ObjectDefinition.getName(objectId)).replace(" ", "");
    var areaName = player.getArea().getClass().getName();
    if (areaName.endsWith(".Area")) {
      player.getGameEncoder().sendMessage(areaName + ": bad class name.");
      return;
    }
    areaName = areaName.replace(".", "/");
    areaName = BASE_DIR + "/" + areaName + ".java";
    var areaPackage = areaName.substring(0, areaName.lastIndexOf('/'));
    if (!(new File(areaName)).exists()) {
      player.getGameEncoder().sendMessage(areaName + ": area doesn't exist.");
      return;
    }
    var optionName = areaPackage + "/handler/mapobject/" + formattedObjectName + "MapObject.java";
    var optionFile = new File(optionName);
    if (optionFile.exists()) {
      player.getGameEncoder().sendMessage("Object option already exists.");
      return;
    }
    player.getGameEncoder().sendMessage("Saved to " + optionFile.getName());
    createOption(optionFile, objectIdName);
  }

  @Override
  public String getExample(String name) {
    return "id type direction";
  }

  @Override
  public void execute(Player player, String name, String message) {
    switch (name) {
      case "obj":
        spawn(player, name, message);
        break;
      case "ooption":
        option(player, name, message);
        break;
    }
  }
}
