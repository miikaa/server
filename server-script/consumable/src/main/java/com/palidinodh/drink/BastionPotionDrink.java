package com.palidinodh.drink;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableStat;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.playerplugin.skill.SkillPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.BASTION_POTION_1,
  ItemId.BASTION_POTION_2,
  ItemId.BASTION_POTION_3,
  ItemId.BASTION_POTION_4
})
class BastionPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.RANGED, 4, 10, true));
    builder.stat(new ConsumableStat(Skills.DEFENCE, 5, 15, true));
    return builder;
  }
}

@ReferenceId({
  ItemId.BLIGHTED_BASTION_1_60027,
  ItemId.BLIGHTED_BASTION_2_60025,
  ItemId.BLIGHTED_BASTION_3_60023,
  ItemId.BLIGHTED_BASTION_4_60021
})
class BlightedBastionPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.RANGED, 4, 10, true));
    builder.stat(new ConsumableStat(Skills.DEFENCE, 5, 15, true));
    return builder;
  }
}

@ReferenceId({
  ItemId.DIVINE_BASTION_POTION_1,
  ItemId.DIVINE_BASTION_POTION_2,
  ItemId.DIVINE_BASTION_POTION_3,
  ItemId.DIVINE_BASTION_POTION_4
})
class DivineBastionPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.RANGED, 4, 10, true));
    builder.stat(new ConsumableStat(Skills.DEFENCE, 5, 15, true));
    builder.complete(
        (p, c) -> {
          if (p.getCombat().getHitpoints() <= 10) {
            p.getGameEncoder().sendMessage("You don't have enough hitpoints to do this.");
            return false;
          }
          return true;
        });
    builder.action(
        (p, c) -> {
          p.getPlugin(SkillPlugin.class)
              .getRestore(Skills.RANGED)
              .startToLimits(500, p.getSkills().getLevel(Skills.RANGED));
          p.getPlugin(SkillPlugin.class)
              .getRestore(Skills.DEFENCE)
              .startToLimits(500, p.getSkills().getLevel(Skills.DEFENCE));
          p.getCombat().applyHit(new Hit(10));
        });
    return builder;
  }
}
