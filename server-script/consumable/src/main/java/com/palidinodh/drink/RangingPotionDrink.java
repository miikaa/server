package com.palidinodh.drink;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableStat;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.playerplugin.skill.SkillPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.RANGING_POTION_1,
  ItemId.RANGING_POTION_2,
  ItemId.RANGING_POTION_3,
  ItemId.RANGING_POTION_4
})
class RangingPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.RANGED, 4, 10, true));
    return builder;
  }
}

@ReferenceId({
  ItemId.DIVINE_RANGING_POTION_1,
  ItemId.DIVINE_RANGING_POTION_2,
  ItemId.DIVINE_RANGING_POTION_3,
  ItemId.DIVINE_RANGING_POTION_4
})
class DivineRangingPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.RANGED, 4, 10, true));
    builder.complete(
        (p, c) -> {
          if (p.getCombat().getHitpoints() <= 10) {
            p.getGameEncoder().sendMessage("You don't have enough hitpoints to do this.");
            return false;
          }
          return true;
        });
    builder.action(
        (p, c) -> {
          p.getPlugin(SkillPlugin.class)
              .getRestore(Skills.RANGED)
              .startToLimits(500, p.getSkills().getLevel(Skills.RANGED));
          p.getCombat().applyHit(new Hit(10));
        });
    return builder;
  }
}
