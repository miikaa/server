package com.palidinodh.drink;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableStat;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.playerplugin.skill.SkillPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.STRENGTH_POTION_1,
  ItemId.STRENGTH_POTION_2,
  ItemId.STRENGTH_POTION_3,
  ItemId.STRENGTH_POTION_4
})
class StrengthPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.STRENGTH, 3, 10, true));
    return builder;
  }
}

@ReferenceId({
  ItemId.SUPER_STRENGTH_1,
  ItemId.SUPER_STRENGTH_2,
  ItemId.SUPER_STRENGTH_3,
  ItemId.SUPER_STRENGTH_4
})
class SuperStrengthPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.STRENGTH, 5, 15, true));
    return builder;
  }
}

@ReferenceId({
  ItemId.DIVINE_SUPER_STRENGTH_POTION_1,
  ItemId.DIVINE_SUPER_STRENGTH_POTION_2,
  ItemId.DIVINE_SUPER_STRENGTH_POTION_3,
  ItemId.DIVINE_SUPER_STRENGTH_POTION_4
})
class DivineSuperStrengthPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.STRENGTH, 5, 15, true));
    builder.complete(
        (p, c) -> {
          if (p.getCombat().getHitpoints() <= 10) {
            p.getGameEncoder().sendMessage("You don't have enough hitpoints to do this.");
            return false;
          }
          return true;
        });
    builder.action(
        (p, c) -> {
          p.getPlugin(SkillPlugin.class)
              .getRestore(Skills.STRENGTH)
              .startToLimits(500, p.getSkills().getLevel(Skills.STRENGTH));
          p.getCombat().applyHit(new Hit(10));
        });
    return builder;
  }
}
