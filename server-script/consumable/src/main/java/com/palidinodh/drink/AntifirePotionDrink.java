package com.palidinodh.drink;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.ANTIFIRE_POTION_1,
  ItemId.ANTIFIRE_POTION_2,
  ItemId.ANTIFIRE_POTION_3,
  ItemId.ANTIFIRE_POTION_4
})
class AntifirePotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.action(
        (p, c) -> {
          p.getSkills().setAntifireTime(600);
        });
    return builder;
  }
}

@ReferenceId({
  ItemId.EXTENDED_ANTIFIRE_1,
  ItemId.EXTENDED_ANTIFIRE_2,
  ItemId.EXTENDED_ANTIFIRE_3,
  ItemId.EXTENDED_ANTIFIRE_4
})
class ExtendedAntifirePotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.action(
        (p, c) -> {
          p.getSkills().setAntifireTime(1200);
        });
    return builder;
  }
}

@ReferenceId({
  ItemId.SUPER_ANTIFIRE_POTION_1,
  ItemId.SUPER_ANTIFIRE_POTION_2,
  ItemId.SUPER_ANTIFIRE_POTION_3,
  ItemId.SUPER_ANTIFIRE_POTION_4
})
class SuperAntifirePotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.action(
        (p, c) -> {
          p.getSkills().setSuperAntifireTime(300);
        });
    return builder;
  }
}

@ReferenceId({
  ItemId.EXTENDED_SUPER_ANTIFIRE_1,
  ItemId.EXTENDED_SUPER_ANTIFIRE_2,
  ItemId.EXTENDED_SUPER_ANTIFIRE_3,
  ItemId.EXTENDED_SUPER_ANTIFIRE_4
})
class ExtendedSuperAntifirePotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.action(
        (p, c) -> {
          p.getSkills().setSuperAntifireTime(600);
        });
    return builder;
  }
}
