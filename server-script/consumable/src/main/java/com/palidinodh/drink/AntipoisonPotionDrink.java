package com.palidinodh.drink;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.ANTIPOISON_1, ItemId.ANTIPOISON_2, ItemId.ANTIPOISON_3, ItemId.ANTIPOISON_4})
class AntipoisonPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.action(
        (p, c) -> {
          p.getSkills().setAntipoisonTime(150);
          p.setPoison(0);
        });
    return builder;
  }
}

@ReferenceId({
  ItemId.SUPERANTIPOISON_1,
  ItemId.SUPERANTIPOISON_2,
  ItemId.SUPERANTIPOISON_3,
  ItemId.SUPERANTIPOISON_4
})
class SuperAntipoisonPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.action(
        (p, c) -> {
          p.getSkills().setSuperAntipoisonTime(575);
          p.setPoison(0);
        });
    return builder;
  }
}
