package com.palidinodh.drink;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableStat;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.SANFEW_SERUM_1,
  ItemId.SANFEW_SERUM_2,
  ItemId.SANFEW_SERUM_3,
  ItemId.SANFEW_SERUM_4
})
class SanfewSerumDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    for (var i = 0; i < Skills.SKILL_COUNT; i++) {
      if (i == Skills.HITPOINTS) {
        continue;
      }
      builder.stat(new ConsumableStat(i, 9, 25, false));
    }
    builder.action(
        (p, c) -> {
          p.getSkills().setSuperAntipoisonTime(575);
          p.setPoison(0);
        });
    return builder;
  }
}
