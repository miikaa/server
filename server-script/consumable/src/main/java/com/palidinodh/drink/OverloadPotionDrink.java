package com.palidinodh.drink;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.OVERLOAD_1, ItemId.OVERLOAD_2, ItemId.OVERLOAD_3, ItemId.OVERLOAD_4})
class NightmareZoneOverloadPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    return Consumable.builder();
  }
}
