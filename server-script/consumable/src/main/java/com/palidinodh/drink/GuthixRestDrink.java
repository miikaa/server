package com.palidinodh.drink;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.PMovement;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableStat;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.GUTHIX_REST_1,
  ItemId.GUTHIX_REST_2,
  ItemId.GUTHIX_REST_3,
  ItemId.GUTHIX_REST_4
})
class GuthixRestDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.HITPOINTS, 2, 5, true));
    builder.action(
        (p, c) -> {
          var energyRestore = (int) (PMovement.MAX_ENERGY * 0.05);
          p.getMovement().setEnergy(p.getMovement().getEnergy() + energyRestore);
          if (p.getVenom() > 0) {
            p.setPoison(0);
          } else if (p.getPoison() > 1) {
            p.setPoison(p.getPoison() - 1);
          }
        });
    return builder;
  }
}
