package com.palidinodh.drink;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.PMovement;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.STAMINA_POTION_1,
  ItemId.STAMINA_POTION_2,
  ItemId.STAMINA_POTION_3,
  ItemId.STAMINA_POTION_4
})
class StaminaPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.action(
        (p, c) -> {
          var restore = (int) (PMovement.MAX_ENERGY * 0.2);
          p.getMovement().setEnergy(p.getMovement().getEnergy() + restore);
          p.getSkills().setStaminaTime(200);
        });
    return builder;
  }
}
