package com.palidinodh.weapontype;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponAttackSet;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponAttackStyle;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponConfigType;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.KERIS_PARTISAN,
  ItemId.KERIS_PARTISAN_OF_BREACHING,
  ItemId.KERIS_PARTISAN_OF_CORRUPTION,
  ItemId.KERIS_PARTISAN_OF_THE_SUN
})
class PartisanWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_17);
    type.renderAnimations(new int[] {813, 1209, 1205, 1206, 1207, 1208, 1210});
    type.equipSound(new Sound(2238));
    type.attackSpeed(4);
    type.defendAnimation(397);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(429).attackSound(new Sound(2549)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_SLASH)
            .attackAnimation(440)
            .attackSound(new Sound(2548))
            .build());
    return type;
  }
}
