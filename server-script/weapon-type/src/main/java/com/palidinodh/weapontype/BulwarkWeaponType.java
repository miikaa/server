package com.palidinodh.weapontype;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponAttackSet;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponConfigType;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.DINHS_BULWARK)
class DinhsBulwarkWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_27);
    type.renderAnimations(new int[] {7508, 7510, 7510, 7510, 7510, 7510, 7509});
    type.twoHanded(true);
    type.attackSpeed(5);
    type.defendAnimation(7512);
    type.attackSet(WeaponAttackSet.builder().attackAnimation(7511).build());
    return type;
  }
}
