package com.palidinodh.mysterybox;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.MysteryBox;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.List;

@ReferenceId(ItemId.SUPER_MYSTERY_BOX_32286)
class SuperBox extends MysteryBox {

  private static List<RandomItem> weightless =
      RandomItem.minWeight(
          RandomItem.combine(
              ItemTables.VERY_RARE, ItemTables.RARE, ItemTables.UNCOMMON, ItemTables.BARROWS_SETS));

  @Override
  public int getRolls() {
    return 2;
  }

  @Override
  public boolean shouldSendItemDropNews(Item item) {
    if (ItemTables.VERY_RARE.containsIf(i -> i.getId() == item.getId())) {
      return true;
    }
    return ItemTables.RARE.containsIf(i -> i.getId() == item.getId());
  }

  @Override
  public Item getRandomItem(Player player) {
    if (PRandom.randomE(200) == 0) {
      return RandomItem.getItem(ItemTables.VERY_RARE);
    }
    if (PRandom.randomE(10) == 0) {
      return RandomItem.getItem(ItemTables.RARE);
    }
    return RandomItem.getItem(ItemTables.UNCOMMON_WITH_BARROWS_SETS);
  }

  @Override
  public List<RandomItem> getAllItems(Player player) {
    return weightless;
  }
}
