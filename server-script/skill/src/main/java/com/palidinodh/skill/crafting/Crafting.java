package com.palidinodh.skill.crafting;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.WidgetSetting;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.BondRelicType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerUnlock;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PEvent;
import java.util.Arrays;
import java.util.List;

class Crafting extends SkillContainer {

  private static final List<Integer> JEWELRY_RELATED =
      Arrays.asList(
          ItemId.RING_MOULD,
          ItemId.NECKLACE_MOULD,
          ItemId.BRACELET_MOULD,
          ItemId.AMULET_MOULD,
          ItemId.GOLD_BAR,
          ItemId.SAPPHIRE,
          ItemId.EMERALD,
          ItemId.RUBY,
          ItemId.DIAMOND,
          ItemId.DRAGONSTONE,
          ItemId.ONYX,
          ItemId.ZENYTE,
          ItemId.ENCHANTED_GEM);
  private static final List<Integer> SILVER_CASTING_RELATED =
      Arrays.asList(
          ItemId.RING_MOULD,
          ItemId.NECKLACE_MOULD,
          ItemId.BRACELET_MOULD,
          ItemId.AMULET_MOULD,
          ItemId.SILVER_BAR,
          ItemId.OPAL,
          ItemId.JADE,
          ItemId.RED_TOPAZ);

  @Override
  public int getSkillId() {
    return Skills.CRAFTING;
  }

  public List<SkillEntry> getEntries() {
    return CraftingEntries.getEntries();
  }

  @Override
  public int getEventTick(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    var speed = entry.getTick();
    if (player.isRelicUnlocked(BondRelicType.PRODUCTION_SPEEDSTER_CRAFTING)) {
      speed /= 2;
    }
    return speed;
  }

  @Override
  public Item deleteConsumedItemHook(Player player, PEvent event, Item item, SkillEntry entry) {
    if (entry.getExperience() > 0
        && player
            .getPlugin(BondPlugin.class)
            .isRelicUnlocked(BondRelicType.EFFICIENT_PRODUCTION_CRAFTING)) {
      if (item.getInfoDef().getEquipSlot() != null) {
        return item;
      }
      switch (item.getId()) {
        case ItemId.SERPENTINE_VISAGE:
        case ItemId.MAGIC_FANG:
          return item;
        case ItemId.UNCUT_DRAGONSTONE:
        case ItemId.DRAGONSTONE:
          if (PRandom.randomE(8) == 0) {
            if (event != null) {
              int remainingAmount = (int) event.getAttachment(SkillContainer.AMOUNT_ATTACHMENT);
              event.setAttachment(SkillContainer.AMOUNT_ATTACHMENT, remainingAmount + 1);
            }
            return null;
          }
          break;
        case ItemId.UNCUT_ONYX:
        case ItemId.ONYX:
          if (PRandom.randomE(20) == 0) {
            if (event != null) {
              int remainingAmount = (int) event.getAttachment(SkillContainer.AMOUNT_ATTACHMENT);
              event.setAttachment(SkillContainer.AMOUNT_ATTACHMENT, remainingAmount + 1);
            }
            return null;
          }
          break;
        case ItemId.UNCUT_ZENYTE:
        case ItemId.ZENYTE:
          if (PRandom.randomE(40) == 0) {
            if (event != null) {
              int remainingAmount = (int) event.getAttachment(SkillContainer.AMOUNT_ATTACHMENT);
              event.setAttachment(SkillContainer.AMOUNT_ATTACHMENT, remainingAmount + 1);
            }
            return null;
          }
          break;
        default:
          if (PRandom.randomE(4) == 0) {
            if (event != null) {
              int remainingAmount = (int) event.getAttachment(SkillContainer.AMOUNT_ATTACHMENT);
              event.setAttachment(SkillContainer.AMOUNT_ATTACHMENT, remainingAmount + 1);
            }
            return null;
          }
          break;
      }
    }
    return item;
  }

  @Override
  public boolean widgetHook(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    if (widgetId == WidgetId.MAKE_JEWELRY || widgetId == WidgetId.SILVER_CASTING) {
      switch (itemId) {
        case ItemId.GOLD_BRACELET_11068:
          itemId = ItemId.GOLD_BRACELET;
          break;
        case ItemId.SAPPHIRE_BRACELET_11071:
          itemId = ItemId.SAPPHIRE_BRACELET;
          break;
        case ItemId.EMERALD_BRACELET_11078:
          itemId = ItemId.EMERALD_BRACELET;
          break;
        case ItemId.RUBY_BRACELET_11087:
          itemId = ItemId.RUBY_BRACELET;
          break;
        case ItemId.DIAMOND_BRACELET_11094:
          itemId = ItemId.DIAMOND_BRACELET;
          break;
        case ItemId.DRAGON_BRACELET:
          itemId = ItemId.DRAGONSTONE_BRACELET;
          break;
        case ItemId.ONYX_BRACELET_11132:
          itemId = ItemId.ONYX_BRACELET;
          break;
        case ItemId.ZENYTE_BRACELET_19492:
          itemId = ItemId.ZENYTE_BRACELET;
          break;
        case ItemId.SLAYER_RING_8:
          if (!Boolean.TRUE.equals(
              player.pluginScript("slayer_is_unlocked", SlayerUnlock.RING_BLING))) {
            player.getGameEncoder().sendMessage("You need to unlock this feature through Slayer.");
            return false;
          }
          break;
      }
      var entry = findEntryFromCreate(itemId);
      if (entry == null) {
        return true;
      }
      switch (option) {
        case 0:
          startEvent(player, entry, 1);
          break;
        case 1:
          startEvent(player, entry, 5);
          break;
        case 2:
          startEvent(player, entry, 10);
          break;
        case 3:
          player
              .getGameEncoder()
              .sendEnterAmount(
                  value -> {
                    startEvent(player, entry, value);
                  });
          break;
        case 4:
          startEvent(player, entry, 28);
          break;
      }
      return true;
    }
    return false;
  }

  @Override
  public boolean widgetOnMapObjectHook(
      Player player, int widgetId, int childId, int slot, int itemId, MapObject mapObject) {
    if (widgetId == WidgetId.INVENTORY && mapObject.getDef().hasOption("smelt")) {
      if (JEWELRY_RELATED.contains(itemId)) {
        player.getWidgetManager().sendInteractiveOverlay(WidgetId.MAKE_JEWELRY);
        return true;
      }
      if (SILVER_CASTING_RELATED.contains(itemId)) {
        player.getWidgetManager().sendInteractiveOverlay(WidgetId.SILVER_CASTING);
        player
            .getGameEncoder()
            .sendWidgetSettings(
                WidgetId.SILVER_CASTING,
                4,
                0,
                50,
                WidgetSetting.OPTION_0,
                WidgetSetting.OPTION_1,
                WidgetSetting.OPTION_2,
                WidgetSetting.OPTION_3,
                WidgetSetting.OPTION_4);
        return true;
      }
    }
    return false;
  }
}
