package com.palidinodh.skill.fishing;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.entity.player.skill.SkillModel;
import com.palidinodh.osrscore.model.entity.player.skill.SkillPet;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.util.PTime;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

class FishingEntries {

  @Getter private static List<SkillEntry> entries = SkillEntry.processList(load());

  private static List<SkillEntry> load() {
    var list = new ArrayList<SkillEntry>();

    var entry = SkillEntry.builder();
    entry
        .level(1)
        .experience(5)
        .animation(618)
        .npc(new SkillModel(NpcId.AFK_TEMPOROSS_16075, 0))
        .tool(new Item(ItemId.HARPOON))
        .create(new RandomItem(ItemId.AFK_TOKEN_60052))
        .startMessage(
            "<col=ff0000>If you logout, you will continue AFK skilling for "
                + PTime.tickToMin(SkillContainer.DISCONNECTED_IDLE_TIMEOUT)
                + " minutes!");
    ;
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(1)
        .experience(10)
        .animation(621)
        .npcMovementReset(true)
        .npc(new SkillModel(NpcId.FISHING_SPOT_1514, 0))
        .tool(new Item(ItemId.SMALL_FISHING_NET))
        .create(new RandomItem(ItemId.RAW_SHRIMPS))
        .pet(new SkillPet(ItemId.HERON, 435165))
        .clueChance(870330);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(5)
        .experience(20)
        .animation(623)
        .npcMovementReset(true)
        .npc(new SkillModel(NpcId.FISHING_SPOT_1514, 2))
        .tool(new Item(ItemId.FISHING_ROD))
        .consume(new RandomItem(ItemId.FISHING_BAIT))
        .create(new RandomItem(ItemId.RAW_SARDINE))
        .pet(new SkillPet(ItemId.HERON, 528000))
        .clueChance(1056000);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(20)
        .experience(50)
        .animation(622)
        .npcMovementReset(true)
        .npc(new SkillModel(NpcId.ROD_FISHING_SPOT, 0))
        .tool(new Item(ItemId.FLY_FISHING_ROD))
        .consume(new RandomItem(ItemId.FEATHER))
        .create(new RandomItem(ItemId.RAW_TROUT))
        .pet(new SkillPet(ItemId.HERON, 461808))
        .clueChance(923616);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(25)
        .experience(60)
        .animation(623)
        .npcMovementReset(true)
        .npc(new SkillModel(NpcId.ROD_FISHING_SPOT, 2))
        .tool(new Item(ItemId.FISHING_ROD))
        .consume(new RandomItem(ItemId.FISHING_BAIT))
        .create(new RandomItem(ItemId.RAW_PIKE))
        .pet(new SkillPet(ItemId.HERON, 305792))
        .clueChance(305792);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(40)
        .experience(90)
        .animation(619)
        .npcMovementReset(true)
        .npc(new SkillModel(NpcId.FISHING_SPOT_1510, 0))
        .tool(new Item(ItemId.LOBSTER_POT))
        .create(new RandomItem(ItemId.RAW_LOBSTER))
        .pet(new SkillPet(ItemId.HERON, 116129))
        .clueChance(116129);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(50)
        .experience(100)
        .animation(618)
        .npcMovementReset(true)
        .npc(new SkillModel(NpcId.FISHING_SPOT_1510, 2))
        .tool(new Item(ItemId.HARPOON))
        .create(new RandomItem(ItemId.RAW_SWORDFISH))
        .pet(new SkillPet(ItemId.HERON, 128885))
        .clueChance(257770);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(46)
        .experience(100)
        .animation(620)
        .npcMovementReset(true)
        .npc(new SkillModel(NpcId.FISHING_SPOT_1511, 0))
        .tool(new Item(ItemId.BIG_FISHING_NET))
        .create(new RandomItem(ItemId.RAW_BASS))
        .pet(new SkillPet(ItemId.HERON, 128885))
        .clueChance(1147827);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(76)
        .experience(110)
        .animation(618)
        .npcMovementReset(true)
        .npc(new SkillModel(NpcId.FISHING_SPOT_1511, 2))
        .npc(new SkillModel(NpcId.FISHING_SPOT_1520, 2))
        .tool(new Item(ItemId.HARPOON))
        .create(new RandomItem(ItemId.RAW_SHARK))
        .pet(new SkillPet(ItemId.HERON, 82243))
        .clueChance(82243);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(62)
        .experience(120)
        .animation(621)
        .npcMovementReset(true)
        .npc(new SkillModel(NpcId.FISHING_SPOT_4316, 0))
        .tool(new Item(ItemId.SMALL_FISHING_NET))
        .create(new RandomItem(ItemId.RAW_MONKFISH))
        .pet(new SkillPet(ItemId.HERON, 138583))
        .clueChance(138583);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(65)
        .experience(105)
        .animation(620)
        .npcMovementReset(true)
        .npc(new SkillModel(NpcId.FISHING_SPOT_4712, 0))
        .tool(new Item(ItemId.BIG_FISHING_NET))
        .create(new RandomItem(ItemId.RAW_KARAMBWAN))
        .pet(new SkillPet(ItemId.HERON, 170874))
        .clueChance(170874);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(85)
        .experience(130)
        .animation(619)
        .npcMovementReset(true)
        .npc(new SkillModel(NpcId.FISHING_SPOT_1536, 0))
        .tool(new Item(ItemId.LOBSTER_POT))
        .consume(new RandomItem(ItemId.DARK_FISHING_BAIT))
        .create(new RandomItem(ItemId.RAW_DARK_CRAB))
        .pet(new SkillPet(ItemId.HERON, 149434))
        .clueChance(149434);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(82)
        .experience(120)
        .animation(623)
        .npcMovementReset(true)
        .npc(new SkillModel(NpcId.ROD_FISHING_SPOT_6825, 0))
        .tool(new Item(ItemId.FISHING_ROD))
        .consume(new RandomItem(ItemId.SANDWORMS))
        .create(new RandomItem(ItemId.RAW_ANGLERFISH))
        .pet(new SkillPet(ItemId.HERON, 78649))
        .clueChance(78649);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(82)
        .experience(26)
        .animation(621)
        .npcMovementReset(true)
        .npc(new SkillModel(NpcId.FISHING_SPOT_7730, 0))
        .npc(new SkillModel(NpcId.FISHING_SPOT_7731, 0))
        .tool(new Item(ItemId.SMALL_FISHING_NET))
        .create(new RandomItem(ItemId.MINNOW, 15, 21))
        .pet(new SkillPet(ItemId.HERON, 977778));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(70)
        .extraLevel(Skills.STRENGTH, 45)
        .extraLevel(Skills.AGILITY, 45)
        .experience(80)
        .animation(622)
        .npcMovementReset(true)
        .npc(new SkillModel(NpcId.FISHING_SPOT_1542, 0))
        .tool(new Item(ItemId.BARBARIAN_ROD))
        .consume(new RandomItem(ItemId.FEATHER))
        .create(new RandomItem(ItemId.LEAPING_STURGEON))
        .pet(new SkillPet(ItemId.HERON, 426954))
        .clueChance(1147827);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(80)
        .experience(95)
        .animation(622)
        .npcMovementReset(true)
        .npc(new SkillModel(NpcId.ROD_FISHING_SPOT_7676, 0))
        .tool(new Item(ItemId.OILY_FISHING_ROD))
        .consume(new RandomItem(ItemId.FISHING_BAIT))
        .create(new RandomItem(ItemId.INFERNAL_EEL))
        .pet(new SkillPet(ItemId.HERON, 99000))
        .clueChance(99000);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(87)
        .experience(105)
        .animation(622)
        .npcMovementReset(true)
        .npc(new SkillModel(NpcId.FISHING_SPOT_6488, 0))
        .tool(new Item(ItemId.FISHING_ROD))
        .consume(new RandomItem(ItemId.FISHING_BAIT))
        .create(new RandomItem(ItemId.SACRED_EEL))
        .pet(new SkillPet(ItemId.HERON, 99000))
        .clueChance(99000);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(81)
        .experience(115)
        .animation(620)
        .npcMovementReset(true)
        .npc(new SkillModel(NpcId.FISHING_SPOT_16079, 0))
        .tool(new Item(ItemId.DRIFT_NET))
        .create(new RandomItem(ItemId.RAW_MANTA_RAY))
        .pet(new SkillPet(ItemId.HERON, 78649))
        .clueChance(78649);
    list.add(entry.build());

    return list;
  }
}
